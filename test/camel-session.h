/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#define ENABLE_THREADS 1
#include <camel/camel-session.h>

#define E_CAMEL_SCALIX_SESSION_TYPE     (e_camel_scalix_session_get_type ())
#define E_CAMEL_SCALIX_SESSION(obj)     (CAMEL_CHECK_CAST((obj), E_CAMEL_SCALIX_SESSION_TYPE, ECamelScalixSession))
#define E_CAMEL_SCALIX_SESSION_CLASS(k) (CAMEL_CHECK_CLASS_CAST ((k), E_CAMEL_SCALIX_SESSION_TYPE, ECamelScalixSessionClass))
#define E_CAMEL_SCALIX_IS_SESSION(o)    (CAMEL_CHECK_TYPE((o), E_CAMEL_SCALIX_SESSION_TYPE))

typedef struct _ECamelScalixSession ECamelScalixSession;
typedef struct _ECamelScalixSessionClass ECamelScalixSessionClass;

struct _ECamelScalixSession {
    CamelSession parent_object;
};
struct _ECamelScalixSessionClass {
    CamelSessionClass parent_class;
};

CamelType camel_scalix_session_get_type (void);
CamelSession *camel_scalix_session_new (const char *path);
