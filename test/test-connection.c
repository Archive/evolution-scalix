/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include <stdio.h>
#include <scalix.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <scalix-camel-session.h>

#define URI "scalix://ckellner@localhost:31337/"

int
main (int argc, char **argv)
{
    ScalixConnection *connection;
    ScalixContainer *container;
    ScalixItem *item;
    GPtrArray *iis;
    GError *error;
    int i;
    char *cf, *cof, *dummy, *drafts, *sent;

    g_type_init ();

    if (!g_thread_supported ())
        g_thread_init (NULL);

    if (camel_init ("/tmp", FALSE)) {
        return FALSE;
    }

    camel_type_init ();
    scalix_debug_init (SCALIX_MAIN | SCALIX_DATA_SERVER);
    LIBXML_TEST_VERSION scalix_camel_session_init ("/tmp");

    connection = scalix_connection_acquire (URI, NULL);

    if (connection == NULL) {

        g_print ("Could not acquire connection\n");
        exit (1);
    }

    scalix_connection_set_auth (connection, "ckellner", "ckellner");

    scalix_connection_connect (connection, NULL);

    scalix_connection_get_special_folder (connection, "calendar", &cf,
                                          "contacts", &cof, "bla", &dummy,
                                          "drafts", &drafts, "sent-items",
                                          &sent, NULL);

    g_print ("Calendar Folder %s\n", cf);
    g_print ("Contacts Folder %s\n", cof);
    g_print ("Drafts Folder %s\n", drafts);
    g_print ("Sent Folder %s\n", sent);

    return 0;
}
