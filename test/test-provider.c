/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include <camel/camel.h>
#include <stdio.h>

#include <camel-scalix-store.h>
#include <camel/camel-imap-store.h>

#include "camel-session.h"

const char *path = "/tmp/scalix-camel-test";

static void
dump_folder_info (CamelFolderInfo * fi)
{
    int depth;
    CamelFolderInfo *n = fi;

    if (fi == NULL)
        return;

    depth = 0;
    while (n->parent) {
        depth++;
        n = n->parent;
    }

    while (fi) {
        g_print ("%s\n", fi->name);
        if (fi->child)
            dump_folder_info (fi->child);
        fi = fi->next;
    }
}

int
main (int argc, char **argv)
{
    CamelURL *url;
    CamelStore *store;
    CamelSession *session;
    CamelFolderInfo *folder_info;
    CamelFolder *folder;
    CamelException *ex;
    GList *calendar_folder, *iter;
    char *tstr;
    GPtrArray *ids;
    int i;

    printf ("Camel Scalix Provider Test\n");
    printf ("(c) 2004 Christian Kellner\n");

    if (argc != 3) {

        printf ("Usage %s <url> <show_special_folder>\n", argv[0]);
        return -1;
    }

    url = camel_url_new (argv[1], NULL);

    if (url == NULL) {
        fprintf (stderr, "Error parsing Url\n");
        return -1;
    }

    g_type_init ();

    if (!g_thread_supported ())
        g_thread_init (NULL);

    if (camel_init (path, FALSE)) {
        return FALSE;
    }

    camel_type_init ();

    ex = camel_exception_new ();

    camel_exception_init (ex);

    session = camel_scalix_session_new (path);

    printf ("URL: %s\n", argv[1]);

    camel_exception_clear (ex);
    store = (CamelStore *) camel_session_get_store (session, argv[1], ex);

    if (store == NULL || camel_exception_is_set (ex)) {
        fprintf (stderr, "couldn't get service %s: %s\n", argv[1],
                 camel_exception_get_description (ex));

        camel_exception_clear (ex);
        return -1;
    }

    printf ("Hide Special Folders: %d\n", atoi (argv[2]));

    camel_object_set (store, NULL,
                      CAMEL_SCALIX_STORE_HIDE_SF, atoi (argv[2]), NULL);

    printf ("Connecting ...\n");
    camel_service_connect (CAMEL_SERVICE (store), ex);

    camel_service_disconnect (CAMEL_SERVICE (store), FALSE, ex);
    camel_service_connect (CAMEL_SERVICE (store), ex);

    if (camel_exception_is_set (ex)) {
        fprintf (stderr, "couldn't not connect to %s: %s\n", argv[1],
                 camel_exception_get_description (ex));

        camel_exception_clear (ex);
        return -1;
    }

    camel_object_get (store, NULL, CAMEL_SCALIX_STORE_SERVER_NAME, &tstr, NULL);
    printf ("-> Server Name: %s\n", tstr);

    folder_info = camel_store_get_folder_info (store, NULL, 0, ex);
    printf ("----------\n");
    dump_folder_info (folder_info);
    printf ("----------\n");

    camel_object_get (store, NULL,
                      CAMEL_SCALIX_STORE_CALENDAR_FOLDER, &calendar_folder,
                      NULL);

    printf ("Calendar folder:\n");
    for (iter = calendar_folder; iter; iter = iter->next) {
        printf ("%s\n", (char *) iter->data);
    }

    folder = camel_store_get_folder (store, "Contacts", 0, ex);

    ids = camel_folder_get_uids (folder);

    for (i = 0; i < ids->len; i++) {
        gchar *uid = g_ptr_array_index (ids, i);
        CamelMimeMessage *message;

        message = camel_folder_get_message (folder, uid, ex);

        g_print ("Message Id: %s\n",
                 camel_mime_message_get_message_id (message));

    }

    return 0;
}
