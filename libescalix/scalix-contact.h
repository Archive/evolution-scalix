/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifndef SCALIX_CONTACT_H
#define SCALIX_CONTACT_H

#include <glib.h>

#include <libebook/e-vcard.h>
#include <libebook/e-contact.h>

G_BEGIN_DECLS
#define SCALIX_TYPE_CONTACT             (scalix_contact_get_type ())
#define SCALIX_CONTACT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_CONTACT, ScalixContact))
#define SCALIX_CONTACT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_CONTACT, ScalixContactClass))
#define SCALIX_IS_CONTACT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_CONTACT))
#define SCALIX_IS_CONTACT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_CONTACT))
#define SCALIX_CONTACT_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_CONTACT, ScalixContactPrivate))
typedef struct _ScalixContact ScalixContact;
typedef struct _ScalixContactClass ScalixContactClass;

struct _ScalixContact {
    EContact parent;
};

struct _ScalixContactClass {
    EContactClass parent_class;
};

GType scalix_contact_get_type (void);
ScalixContact *scalix_contact_new (const char *vcard);
GList *scalix_contact_get_fields (void);

G_END_DECLS
#endif /* SCALIX_CONTACT_H */
