/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include <glib.h>
#include <glib-object.h>

#ifndef SCALIX_DEBUG_H
#define SCALIX_DEBUG_H

G_BEGIN_DECLS
#define SCALIX_DEBUG 0
#ifdef SCALIX_DEBUG
# ifdef G_HAVE_ISO_VARARGS
#  define	scalix_debug(_MODULE, _FMT, ...) _scalix_debug (_MODULE, __FUNCTION__, _FMT, ##__VA_ARGS__)
# elif defined(G_HAVE_GNUC_VARARGS)
#  warning defined(G_HAVE_GNUC_VARARGS)
#  define g_debug(format...) scalix_debug (_MODULE, _FMT, ##__VA_ARGS__)
# else
#  error "No debugging available"
# endif /* G_HAVE_ISO_VARGS */
    typedef enum {

    SCALIX_NONE = 0,
    SCALIX_MAIN = (1 << 0),
    SCALIX_OBJECT = (1 << 1),
    SCALIX_ALL = ~SCALIX_NONE
} ScalixDebugModules;

#define scalix_debug_register(__name) \
	static char *__debug = scalix_debug_register_module (__name, __FILE__)

void scalix_debug_init (int default_modules);
void
_scalix_debug (int module, const char *func, const char *fmt, ...)
G_GNUC_PRINTF (3, 4);

const char *
scalix_debug_register_module (const char *name, const char *file);

#else /* SCALIX_DEBUG */
# define scalix_debug(_MODULE, _FMT, ...)	G_STMT_START{ (void)0; }G_STMT_END
# define scalix_debug_register(__name)      G_STMT_START{ (void)0; }G_STMT_END
#endif /* SCALIX_DEBUG */
    G_END_DECLS
#endif /* SCALIX_DEBUG_H */
