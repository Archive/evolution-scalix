/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include "scalix-object.h"

#include "scalix-oc-entry.h"

struct _ScalixOCEntryPrivate {
    char *ouid;
    int iuid;
    int type;
    int flags;
    gboolean have_object;
    char *object_data;
    ScalixObject *object;
};

enum {
    PROP_0,
    PROP_OUID,
    PROP_IUID,
    PROP_IPM_TYPE,
    PROP_FLAGS,
    PROP_OBJECT_DATA,
    PROP_OBJECT
};

static GObjectClass *parent_class = NULL;

static void scalix_oc_entry_set_property (GObject * object,
                                          guint prop_id,
                                          const GValue * value,
                                          GParamSpec * pspec);

static void scalix_oc_entry_get_property (GObject * object,
                                          guint prop_id,
                                          GValue * value, GParamSpec * pspec);

G_DEFINE_TYPE (ScalixOCEntry, scalix_oc_entry, G_TYPE_OBJECT);

static void
scalix_oc_entry_finalize (GObject * object)
{
    ScalixOCEntry *entry;
    ScalixOCEntryPrivate *priv;

    entry = SCALIX_OC_ENTRY (object);
    priv = SCALIX_OC_ENTRY_GET_PRIVATE (entry);

    g_free (priv->ouid);
    g_free (priv->object_data);

    if (priv->have_object) {
        g_object_unref (priv->object);
    }

    G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
scalix_oc_entry_class_init (ScalixOCEntryClass * klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);

    g_type_class_add_private (klass, sizeof (ScalixOCEntryPrivate));

    gobject_class->finalize = scalix_oc_entry_finalize;
    gobject_class->set_property = scalix_oc_entry_set_property;
    gobject_class->get_property = scalix_oc_entry_get_property;

    g_object_class_install_property (gobject_class,
                                     PROP_OUID,
                                     g_param_spec_string ("object-uid",
                                                          "Uid of the object",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE |
                                                          G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (gobject_class,
                                     PROP_IUID,
                                     g_param_spec_int ("imap-uid",
                                                       "UID of the item on the server",
                                                       "",
                                                       G_MININT,
                                                       G_MAXINT,
                                                       0,
                                                       G_PARAM_READABLE |
                                                       G_PARAM_WRITABLE |
                                                       G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (gobject_class,
                                     PROP_IPM_TYPE,
                                     g_param_spec_int ("ipm-type",
                                                       "the type of the object",
                                                       "",
                                                       IPM_NO_TYPE,
                                                       IPM_LAST,
                                                       IPM_NO_TYPE,
                                                       G_PARAM_READABLE |
                                                       G_PARAM_WRITABLE |
                                                       G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (gobject_class,
                                     PROP_FLAGS,
                                     g_param_spec_int ("flags",
                                                       "",
                                                       "",
                                                       G_MININT,
                                                       G_MAXINT,
                                                       0,
                                                       G_PARAM_READABLE |
                                                       G_PARAM_WRITABLE |
                                                       G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (gobject_class,
                                     PROP_OBJECT_DATA,
                                     g_param_spec_string ("object-data",
                                                          "unparsed object",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE |
                                                          G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (gobject_class,
                                     PROP_OBJECT,
                                     g_param_spec_object ("object",
                                                          "bla",
                                                          NULL,
                                                          SCALIX_TYPE_OBJECT,
                                                          G_PARAM_READABLE));

}

static void
scalix_oc_entry_init (ScalixOCEntry * cache)
{
    ScalixOCEntryPrivate *priv;

    priv = SCALIX_OC_ENTRY_GET_PRIVATE (cache);
    priv->have_object = FALSE;
    priv->ouid = NULL;
}

static void
scalix_oc_entry_set_property (GObject * object,
                              guint prop_id,
                              const GValue * value, GParamSpec * pspec)
{
    ScalixOCEntry *entry;
    ScalixOCEntryPrivate *priv;

    entry = SCALIX_OC_ENTRY (object);
    priv = SCALIX_OC_ENTRY_GET_PRIVATE (entry);

    switch (prop_id) {

    case PROP_OUID:
        priv->ouid = g_value_dup_string (value);
        break;

    case PROP_IUID:
        priv->iuid = g_value_get_int (value);
        break;

    case PROP_IPM_TYPE:
        priv->type = g_value_get_int (value);
        break;

    case PROP_FLAGS:
        priv->flags = g_value_get_int (value);
        break;

    case PROP_OBJECT_DATA:
        priv->object_data = g_value_dup_string (value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }

}

static void
scalix_oc_entry_get_property (GObject * object,
                              guint prop_id, GValue * value, GParamSpec * pspec)
{
    ScalixOCEntry *entry;
    ScalixOCEntryPrivate *priv;

    entry = SCALIX_OC_ENTRY (object);
    priv = SCALIX_OC_ENTRY_GET_PRIVATE (entry);

    switch (prop_id) {

    case PROP_OUID:
        g_value_set_string (value, priv->ouid);
        break;

    case PROP_IUID:
        g_value_set_int (value, priv->iuid);
        break;

    case PROP_IPM_TYPE:
        g_value_set_int (value, priv->type);
        break;

    case PROP_FLAGS:
        g_value_set_int (value, priv->flags);
        break;

    case PROP_OBJECT:
        if (priv->have_object) {
            g_value_set_object (value, priv->object);
            return;
        }

        /* create the object */
        priv->object = scalix_object_new_by_type (priv->type);

        if (!scalix_object_deserialize (priv->object, priv->object_data)) {
            g_object_unref (priv->object);
            g_value_set_object (value, NULL);
            g_warning ("Error while deserializing\n");
            return;
        }

        g_value_set_object (value, priv->object);
        priv->have_object = TRUE;
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}
