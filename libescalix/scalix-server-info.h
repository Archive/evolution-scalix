/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors:	Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifndef SCALIX_SERVER_INFO_H
#define SCALIX_SERVER_INFO_H

#include <glib-object.h>

G_BEGIN_DECLS
#define SCALIX_TYPE_SERVER_INFO             (scalix_server_info_get_type ())
#define SCALIX_SERVER_INFO(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_SERVER_INFO, ScalixServerInfo))
#define SCALIX_SERVER_INFO_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_SERVER_INFO, ScalixServerInfoClass))
#define SCALIX_IS_SERVER_INFO(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_SERVER_INFO))
#define SCALIX_IS_SERVER_INFO_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_SERVER_INFO))
#define SCALIX_SERVER_INFO_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_SERVER_INFO, ScalixServerInfoPrivate))
typedef struct _ScalixServerInfo ScalixServerInfo;
typedef struct _ScalixServerInfoClass ScalixServerInfoClass;
typedef struct _ScalixServerInfoPrivate ScalixServerInfoPrivate;

struct _ScalixServerInfo {
    GObject parent;
};

struct _ScalixServerInfoClass {
    GObjectClass parent_class;
};

GType scalix_server_info_get_type (void);

G_END_DECLS
#endif /* SCALIX_SERVER_INFO  */
