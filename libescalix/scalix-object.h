/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-
 * ex: set ts=4:
 *
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors:     Christian Kellner <Christian.Kellner@scalix.com>
 */
 
#ifndef SCALIX_OBJECT_H
#define SCALIX_OBJECT_H

#include <glib-object.h>
#include <camel/camel.h>


G_BEGIN_DECLS

#define SCALIX_TYPE_OBJECT            (scalix_object_get_type ())
#define SCALIX_OBJECT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_OBJECT, ScalixObject))
#define SCALIX_IS_OBJECT(obj)		  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_OBJECT))
#define SCALIX_OBJECT_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), SCALIX_TYPE_OBJECT, ScalixObjectIface))

typedef struct _ScalixObject        ScalixObject; /* Dummy typedef */
typedef struct _ScalixObjectIface   ScalixObjectIface;


struct _ScalixObjectIface
{
	GTypeInterface g_iface;

	/* Signals */


	/* Virtual Table */

	gboolean (* init_from_mime_message) (ScalixObject *object, CamelMimeMessage *message); 
	CamelMimeMessage * (* to_mime_message) (ScalixObject *object);

	char *(* to_string) (ScalixObject *object);
	
	char * (* serialize) (ScalixObject *object);
	gboolean (*deserialize) (ScalixObject *object, const char *string);
	
	ScalixObject *(*clone) (ScalixObject *object);
	
};

typedef enum {
	
	IPM_NO_TYPE = 0,
	IPM_APPOINTMENT,
	IPM_CONTACT,
    IPM_CONTACT_LIST,
	IPM_LAST
	
} IPMTypes;

GType			   scalix_object_get_type               (void);

ScalixObject * 	   scalix_object_new_by_type 			(int ipm_type);




char *		       scalix_object_to_string		        (ScalixObject *object);

ScalixObject *     scalix_object_new_from_message       (CamelMimeMessage *message,
                                                         const char *attachment_store);

gboolean		   scalix_object_init_from_mime_message (ScalixObject *object, 
													     CamelMimeMessage *message);

CamelMimeMessage * scalix_object_to_mime_message        (ScalixObject *object);

char *			   scalix_object_serialize				(ScalixObject *object);

gboolean 		   scalix_object_deserialize			(ScalixObject *object,
														 const char *string);
														 
ScalixObject *     scalix_object_clone                  (ScalixObject *object);

G_END_DECLS
#endif /* SCALIX_OBJECT_H */
