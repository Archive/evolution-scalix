/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifndef SCALIX_CONTAINER_H
#define SCALIX_CONTAINER_H

#include <glib-object.h>

#include <libescalix/scalix-object.h>

G_BEGIN_DECLS
#define SCALIX_TYPE_CONTAINER             (scalix_container_get_type ())
#define SCALIX_CONTAINER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_CONTAINER, ScalixContainer))
#define SCALIX_CONTAINER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_CONTAINER, ScalixContainerClass))
#define SCALIX_IS_CONTAINER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_CONTAINER))
#define SCALIX_IS_CONTAINER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_CONTAINER))
#define SCALIX_CONTAINER_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_CONTAINER, ScalixContainerPrivate))
#define SCALIX_CONTAINER_ERROR            (scalix_container_error_quark ())
typedef struct _ScalixContainer ScalixContainer;
typedef struct _ScalixContainerClass ScalixContainerClass;
typedef struct _ScalixContainerPrivate ScalixContainerPrivate;

struct _ScalixContainer {
    GObject parent;
};

struct _ScalixContainerClass {
    GObjectClass parent_class;
    void (*object_added) (ScalixContainer * container, ScalixObject *,
                          gpointer);
    void (*object_removed) (ScalixContainer * container, ScalixObject *,
                            gpointer);
    void (*object_changed) (ScalixContainer * container, ScalixObject *,
                            ScalixObject *, gpointer);
};

typedef enum {

    CONTAINER_STATE_OFFLINE,
    CONTAINER_STATE_ONLINE,
    CONTAINER_STATE_ERROR
} ContainerState;

typedef enum {

    CONTAINER_OK,
    CONTAINER_ERROR_GENERIC
} ContainerErrors;

GType scalix_container_get_type (void);
GQuark scalix_container_error_quark (void);

ScalixContainer *scalix_container_open (const char *uri);

gboolean scalix_container_set_online (ScalixContainer * container,
                                      gboolean online);

gboolean scalix_container_sync (ScalixContainer * container);

gboolean scalix_container_remove_object (ScalixContainer * container,
                                         ScalixObject * object);

gboolean scalix_container_remove_id (ScalixContainer * container,
                                     const char *uid);

gboolean scalix_container_remove (ScalixContainer * container);

ScalixObject *scalix_container_get_object (ScalixContainer * container,
                                           const char *object_uid);

GList *scalix_container_remove_objects (ScalixContainer * container,
                                        GList * to_remove);

gboolean scalix_container_update_object (ScalixContainer * container,
                                         ScalixObject * object,
                                         gboolean use_bstore);

gboolean scalix_container_add_object (ScalixContainer * container,
                                      ScalixObject * object);

gboolean scalix_container_foreach (ScalixContainer * container,
                                   GFunc func, gpointer user_data);

ScalixObject *scalix_container_refresh_object (ScalixContainer * container,
                                               const char *id);

char *scalix_container_get_freebusy (ScalixContainer * container,
                                     const char *request);

void scalix_container_set_freebusy (ScalixContainer * container,
                                    const char *request);

char *scalix_container_get_local_attachment_store (ScalixContainer * container);

G_END_DECLS
#endif /* SCALIX-CONTAINER_H */
