/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#include "scalix-object.h"
#include "scalix-appointment.h"
#include "scalix-contact.h"
#include "scalix-contact-list.h"
#include "scalix-debug.h"
#include "scalix-server-info.h"

#include <ctype.h>

static void
scalix_object_class_init (gpointer g_iface, gpointer class_data)
{

    g_object_interface_install_property (g_iface,
                                         g_param_spec_string ("uid",
                                                              "Uid of the object",
                                                              "",
                                                              NULL,
                                                              G_PARAM_READABLE));

    g_object_interface_install_property (g_iface,
                                         g_param_spec_int ("ipm-type",
                                                           "the type of the object",
                                                           "",
                                                           IPM_NO_TYPE,
                                                           IPM_LAST,
                                                           IPM_NO_TYPE,
                                                           G_PARAM_READABLE));
}

GType
scalix_object_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo info = {
            sizeof (ScalixObjectIface), /* class_size */
            NULL,               /* base_init */
            NULL,               /* base_finalize */
            scalix_object_class_init,
            NULL,               /* class_finalize */
            NULL,               /* class_data */
            0,
            0,                  /* n_preallocs */
            NULL
        };

        type = g_type_register_static (G_TYPE_INTERFACE, "ScalixObject",
                                       &info, 0);

        g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
    }

    return type;
}

char *
scalix_object_to_string (ScalixObject * object)
{
    g_return_val_if_fail (SCALIX_IS_OBJECT (object), NULL);
    g_return_val_if_fail (SCALIX_OBJECT_GET_IFACE (object)->to_string != NULL,
                          NULL);

    return (*SCALIX_OBJECT_GET_IFACE (object)->to_string) (object);
}

gboolean
scalix_object_init_from_mime_message (ScalixObject * object,
                                      CamelMimeMessage * message)
{
    g_return_val_if_fail (SCALIX_IS_OBJECT (object), FALSE);
    g_return_val_if_fail (SCALIX_OBJECT_GET_IFACE (object)->
                          init_from_mime_message != NULL, FALSE);

    return (*SCALIX_OBJECT_GET_IFACE (object)->init_from_mime_message) (object,
                                                                        message);
}

CamelMimeMessage *
scalix_object_to_mime_message (ScalixObject * object)
{
    g_return_val_if_fail (SCALIX_IS_OBJECT (object), NULL);
    g_return_val_if_fail (SCALIX_OBJECT_GET_IFACE (object)->to_mime_message !=
                          NULL, NULL);

    return (*SCALIX_OBJECT_GET_IFACE (object)->to_mime_message) (object);
}

char *
scalix_object_serialize (ScalixObject * object)
{
    g_return_val_if_fail (SCALIX_IS_OBJECT (object), NULL);
    g_return_val_if_fail (SCALIX_OBJECT_GET_IFACE (object)->serialize != NULL,
                          NULL);

    return (*SCALIX_OBJECT_GET_IFACE (object)->serialize) (object);
}

gboolean
scalix_object_deserialize (ScalixObject * object, const char *string)
{
    g_return_val_if_fail (SCALIX_IS_OBJECT (object), FALSE);
    g_return_val_if_fail (SCALIX_OBJECT_GET_IFACE (object)->deserialize != NULL,
                          FALSE);

    return (*SCALIX_OBJECT_GET_IFACE (object)->deserialize) (object, string);
}

ScalixObject *
scalix_object_clone (ScalixObject * object)
{
    g_return_val_if_fail (SCALIX_IS_OBJECT (object), NULL);
    g_return_val_if_fail (SCALIX_OBJECT_GET_IFACE (object)->clone != NULL,
                          NULL);

    return (*SCALIX_OBJECT_GET_IFACE (object)->clone) (object);
}

/* Object factory kinda stuff */

static ScalixObject *
scalix_object_new_by_class (const char *class)
{
    ScalixObject *object = NULL;

    if (class == NULL) {
        return NULL;
    }

    if (g_str_equal (class, "IPM.Appointment")) {
        object = SCALIX_OBJECT (g_object_new (SCALIX_TYPE_APPOINTMENT, NULL));
    } else if (g_str_equal (class, "[Serializied]ScalixServerInfo")) {
        object = SCALIX_OBJECT (g_object_new (SCALIX_TYPE_SERVER_INFO, NULL));
    } else if (g_str_equal (class, "IPM.Contact")) {
        object = SCALIX_OBJECT (g_object_new (SCALIX_TYPE_CONTACT, NULL));
    } else if (g_str_equal (class, "IPM.DistList")) {
        object = SCALIX_OBJECT (g_object_new (SCALIX_TYPE_CONTACT_LIST, NULL));
    }

    return object;
}

/* TODO: new by type with data? */

ScalixObject *
scalix_object_new_by_type (int ipm_type)
{
    GType type;

    switch (ipm_type) {

    case IPM_APPOINTMENT:
        type = SCALIX_TYPE_APPOINTMENT;
        break;

    case IPM_CONTACT:
        type = SCALIX_TYPE_CONTACT;
        break;

    case IPM_CONTACT_LIST:
        type = SCALIX_TYPE_CONTACT_LIST;
        break;

    default:
        return NULL;

    }

    return SCALIX_OBJECT (g_object_new (type, NULL));
}

ScalixObject *
scalix_object_new_from_message (CamelMimeMessage * message,
                                const char *attachment_store)
{
    CamelMedium *medium;
    const char *scalix_class;
    ScalixObject *object;

    medium = CAMEL_MEDIUM (message);

    scalix_class = camel_medium_get_header (medium, "X-Scalix-Class");

    if (scalix_class == NULL) {
        return NULL;
    }

    while (isspace (*scalix_class)) {
        scalix_class++;
    }

    object = scalix_object_new_by_class (scalix_class);

    if (object == NULL) {
        return NULL;
    }

    g_object_set_data (G_OBJECT (object),
                       "attachment-store", (gpointer) attachment_store);

    if (SCALIX_OBJECT_GET_IFACE (object)->init_from_mime_message) {
        SCALIX_OBJECT_GET_IFACE (object)->init_from_mime_message (object,
                                                                  message);
    }

    scalix_debug (SCALIX_OBJECT, "Created Object From Mime Message (class: %s)",
                  scalix_class);

    return object;
}
