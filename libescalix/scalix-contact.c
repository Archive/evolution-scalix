/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#include "scalix-object.h"
#include "scalix-contact.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libebook/e-vcard.h>
#include <libebook/e-contact.h>

#include <string.h>

typedef struct _ScalixContactPrivate ScalixContactPrivate;

struct _ScalixContactPrivate {
    char dummy;
};

/* ************************************************************************** */
static void scalix_contact_get_property (GObject * object,
                                         guint prop_id,
                                         GValue * value, GParamSpec * pspec);

static void scalix_contact_set_property (GObject * object,
                                         guint prop_id,
                                         const GValue * value,
                                         GParamSpec * pspec);

static char *scalix_contact_serialize (ScalixObject * object);

static gboolean scalix_contact_deserialize (ScalixObject * object,
                                            const char *string);

CamelMimeMessage *scalix_contact_to_mime_message (ScalixObject * object);

gboolean scalix_contact_init_from_mime_message (ScalixObject * object,
                                                CamelMimeMessage * msg);

static gboolean scalix_contact_set_xml_data (ScalixObject * object,
                                             GByteArray * data);

static void initialize_field_mapping_table (void);

gboolean scalix_contact_get (ScalixContact * contact,
                             const char *key, char **val);

void scalix_contact_set (ScalixContact * contact,
                         const char *key, const char *val);

/* ************************************************************************** */

/* GObject related stuff */

enum {
    PROP_0,
    /* overriden */
    PROP_UID,
    PROP_IPM_TYPE
};

/* Interface implementation */
static void
scalix_contact_object_iface_init (ScalixObjectIface * iface)
{
    iface->init_from_mime_message = scalix_contact_init_from_mime_message;
    iface->to_mime_message = scalix_contact_to_mime_message;
    iface->serialize = scalix_contact_serialize;
    iface->deserialize = scalix_contact_deserialize;
}

G_DEFINE_TYPE_EXTENDED (ScalixContact,
                        scalix_contact,
                        E_TYPE_CONTACT,
                        0,
                        G_IMPLEMENT_INTERFACE (SCALIX_TYPE_OBJECT,
                                               scalix_contact_object_iface_init));

static void
scalix_contact_init (ScalixContact * self)
{
}

static void
scalix_contact_class_init (ScalixContactClass * klass)
{
    GObjectClass *gobject_class;

    gobject_class = (GObjectClass *) klass;

    initialize_field_mapping_table ();

    gobject_class->set_property = scalix_contact_set_property;
    gobject_class->get_property = scalix_contact_get_property;

    g_object_class_override_property (gobject_class, PROP_UID, "uid");
    g_object_class_override_property (gobject_class, PROP_IPM_TYPE, "ipm-type");
}

/* ************************************************************************** */
static void
scalix_contact_get_property (GObject * object,
                             guint prop_id, GValue * value, GParamSpec * pspec)
{
    char *str;

    switch (prop_id) {

    case PROP_UID:
        str = e_contact_get (E_CONTACT (object), E_CONTACT_UID);
        g_value_take_string (value, str);
        break;

    case PROP_IPM_TYPE:
        g_value_set_int (value, IPM_CONTACT);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        return;
    }
}

static void
scalix_contact_set_property (GObject * object,
                             guint prop_id,
                             const GValue * value, GParamSpec * pspec)
{
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
}

/* ************************************************************************** */

/* XML <-> vCard field mapping */

typedef struct _EBookMapField EBookMapField;

typedef void (*xml_to_contact_handler) (EContact * contact,
                                        const EBookMapField * field,
                                        const char *value);

typedef void (*contact_to_xml_handler) (EContact * contact,
                                        const EBookMapField * field,
                                        xmlNodePtr parent);

enum {
    NONE = -1,
    STREET = 0,
    CITY = 1,
    STATE = 2,
    ZIP = 3,
    COUNTRY = 4,
};

enum {
    GIVEN = 0,
    MIDDLE,
    FAMILY,
    PREFIX,
    SUFFIX
};

struct _EBookMapField {
    EContactField e_field_id;
    char *xml_node_name;
    xml_to_contact_handler xml_to_contact;
    contact_to_xml_handler contact_to_xml;
    int sub_id;
};

static void
xtc_address (EContact * contact, const EBookMapField * field, const char *value)
{
    EContactAddress *address;

    if ((address = e_contact_get (contact, field->e_field_id)) == NULL)
        address = g_new0 (EContactAddress, 1);

    switch (field->sub_id) {

    case STREET:
        address->street = (char *) value;
        break;

    case CITY:
        address->locality = (char *) value;
        break;

    case STATE:
        address->region = (char *) value;
        break;

    case ZIP:
        address->code = (char *) value;
        break;

    case COUNTRY:
        address->country = (char *) value;
        break;

    default:
        return;
    }

    e_contact_set (contact, field->e_field_id, address);
    g_free (address);

}

static void
ctx_address (EContact * contact, const EBookMapField * field, xmlNodePtr parent)
{
    EContactAddress *address;

    if ((address = e_contact_get (contact, field->e_field_id)) == NULL)
        return;

    if (address->street != NULL && address->street[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + STREET)->xml_node_name,
                         (const xmlChar *) address->street);
    }

    if (address->locality != NULL && address->locality[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + CITY)->xml_node_name,
                         (const xmlChar *) address->locality);
    }

    if (address->region != NULL && address->region[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + STATE)->xml_node_name,
                         (const xmlChar *) address->region);
    }

    if (address->code != NULL && address->code[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + ZIP)->xml_node_name,
                         (const xmlChar *) address->code);
    }

    if (address->country != NULL && address->country[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + COUNTRY)->xml_node_name,
                         (const xmlChar *) address->country);
    }

    g_free (address);

}

static void
xtc_name (EContact * contact, const EBookMapField * field, const char *value)
{
    EContactName *name;

    if ((name = e_contact_get (contact, field->e_field_id)) == NULL)
        name = g_new0 (EContactName, 1);

    switch (field->sub_id) {

    case GIVEN:
        name->given = (char *) value;
        break;

    case MIDDLE:
        name->additional = (char *) value;
        break;

    case FAMILY:
        name->family = (char *) value;
        break;

    case PREFIX:
        name->prefixes = (char *) value;
        break;

    case SUFFIX:
        name->suffixes = (char *) value;
        break;

    default:
        return;
    }

    e_contact_set (contact, field->e_field_id, name);
    g_free (name);
}

static void
ctx_name (EContact * contact, const EBookMapField * field, xmlNodePtr parent)
{
    EContactName *name;

    if ((name = e_contact_get (contact, field->e_field_id)) == NULL)
        return;

    if (name->given != NULL && name->given[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + GIVEN)->xml_node_name,
                         (const xmlChar *) name->given);
    }

    if (name->additional != NULL && name->additional[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + MIDDLE)->xml_node_name,
                         (const xmlChar *) name->additional);
    }

    if (name->family != NULL && name->family[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + FAMILY)->xml_node_name,
                         (const xmlChar *) name->family);
    }

    if (name->prefixes != NULL && name->prefixes[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + PREFIX)->xml_node_name,
                         (const xmlChar *) name->prefixes);
    }

    if (name->suffixes != NULL && name->suffixes[0] != '\0') {
        xmlNewTextChild (parent, NULL,
                         (const xmlChar *) (field + SUFFIX)->xml_node_name,
                         (const xmlChar *) name->suffixes);
    }

    g_free (name);

}

static void
xtc_uid (EContact * contact, const EBookMapField * field, const char *value)
{
    e_contact_set (contact, E_CONTACT_UID, (char *) value);
}

static void
ctx_uid (EContact * contact, const EBookMapField * field, xmlNodePtr parent)
{
    char *uid;

    if ((uid = e_contact_get (contact, E_CONTACT_UID)) == NULL)
        return;

    xmlNewTextChild (parent, NULL,
                     (const xmlChar *) field->xml_node_name,
                     (const xmlChar *) uid);

    g_free (uid);
}

#if 0
static void
xtc_categories (EContact * contact, const EBookMapField * field,
                const char *value)
{
    e_contact_set (contact, E_CONTACT_CATEGORIES, (char *) value);
}

static void
ctx_categories (EContact * contact, const EBookMapField * field,
                xmlNodePtr parent)
{
    char *uid;

    if ((uid = e_contact_get (contact, E_CONTACT_UID)) == NULL)
        return;

    xmlNewTextChild (parent, NULL,
                     (const xmlChar *) field->xml_node_name,
                     (const xmlChar *) uid);

    g_free (uid);
}
#endif

/*
 * This table is magic! The sub_id _MUST_ match the field index of the field_group + 1
 * FIXME: missing: middle_name, profession, 
 */

static EBookMapField field_mapping[] = {

    {E_CONTACT_FULL_NAME, "display_name", NULL, NULL, NONE},
    {E_CONTACT_NAME, "first_name", xtc_name, ctx_name, GIVEN},
    {E_CONTACT_NAME, "middle_name", xtc_name, ctx_name, MIDDLE},
    {E_CONTACT_NAME, "last_name", xtc_name, ctx_name, FAMILY},
    {E_CONTACT_NAME, "display_name_prefix", xtc_name, ctx_name, PREFIX},
    {E_CONTACT_NAME, "suffix", xtc_name, ctx_name, SUFFIX},
    {E_CONTACT_FILE_AS, "file_as", NULL, NULL, NONE},
    {E_CONTACT_EMAIL_1, "email1_address", NULL, NULL, NONE},
    {E_CONTACT_EMAIL_2, "email2_address", NULL, NULL, NONE},
    {E_CONTACT_EMAIL_3, "email3_address", NULL, NULL, NONE},
    {E_CONTACT_PHONE_BUSINESS, "work_phone_number", NULL, NULL, NONE},
    {E_CONTACT_PHONE_BUSINESS_FAX, "work_fax_number", NULL, NULL, NONE},
    {E_CONTACT_PHONE_HOME, "home_phone_number", NULL, NULL, NONE},
    {E_CONTACT_PHONE_MOBILE, "mobile_phone_number", NULL, NULL, NONE},
    {E_CONTACT_ORG, "company_name", NULL, NULL, NONE},
    {E_CONTACT_OFFICE, "department", NULL, NULL, NONE},
    {E_CONTACT_TITLE, "job_title", NULL, NULL, NONE},
    {E_CONTACT_HOMEPAGE_URL, "web_page_address", NULL, NULL, NONE},
    {E_CONTACT_ADDRESS_WORK, "work_address_street", xtc_address, ctx_address,
     STREET},
    {E_CONTACT_ADDRESS_WORK, "work_address_city", xtc_address, ctx_address,
     CITY},
    {E_CONTACT_ADDRESS_WORK, "work_address_state", xtc_address, ctx_address,
     STATE},
    {E_CONTACT_ADDRESS_WORK, "work_address_zip", xtc_address, ctx_address, ZIP},
    {E_CONTACT_ADDRESS_WORK, "work_address_country", xtc_address, ctx_address,
     COUNTRY},
    {E_CONTACT_ADDRESS_HOME, "home_address_street", xtc_address, ctx_address,
     STREET},
    {E_CONTACT_ADDRESS_HOME, "home_address_city", xtc_address, ctx_address,
     CITY},
    {E_CONTACT_ADDRESS_HOME, "home_address_state", xtc_address, ctx_address,
     STATE},
    {E_CONTACT_ADDRESS_HOME, "home_address_zip", xtc_address, ctx_address, ZIP},
    {E_CONTACT_ADDRESS_HOME, "home_address_country", xtc_address, ctx_address,
     COUNTRY},
    {E_CONTACT_ADDRESS_OTHER, "other_address_street", xtc_address, ctx_address,
     STREET},
    {E_CONTACT_ADDRESS_OTHER, "other_address_city", xtc_address, ctx_address,
     CITY},
    {E_CONTACT_ADDRESS_OTHER, "other_address_state", xtc_address, ctx_address,
     STATE},
    {E_CONTACT_ADDRESS_OTHER, "other_address_zip", xtc_address, ctx_address,
     ZIP},
    {E_CONTACT_ADDRESS_OTHER, "other_address_country", xtc_address, ctx_address,
     COUNTRY},
    {E_CONTACT_UID, "direct_ref", xtc_uid, ctx_uid, NONE},
    {0, NULL, NULL, NULL, NONE}

};

static GHashTable *field_mapping_table = NULL;

static void
initialize_field_mapping_table ()
{
    EBookMapField *iter = NULL;

    if (field_mapping_table != NULL)
        return;

    field_mapping_table = g_hash_table_new (g_str_hash, g_str_equal);

    for (iter = field_mapping; iter->e_field_id != 0; iter++) {
        g_hash_table_insert (field_mapping_table, iter->xml_node_name, iter);
    }
}

#define e_contact_set_uid(__contact, __uid) e_contact_set (__contact, \
                                            E_CONTACT_UID, (gpointer) __uid)

/* ************************************************************************** */

/* ScalixObject interface impl.                                               */

static char *
scalix_contact_serialize (ScalixObject * object)
{
    char *out;

    out = e_vcard_to_string (E_VCARD (object), EVC_FORMAT_VCARD_30);
    return out;
}

static gboolean
scalix_contact_deserialize (ScalixObject * object, const char *string)
{
    /* No error handler here? */
    e_vcard_construct (E_VCARD (object), string);
    return TRUE;
}

static gboolean
scalix_contact_set_xml_data (ScalixObject * object, GByteArray * data)
{
    xmlDocPtr doc, mapi;
    xmlNodePtr root, iter, mapi_root, node;
    xmlNodePtr categories = NULL;
    xmlChar *mem;
    EContact *contact;
    int size;
    char *content;
    EBookMapField *field = NULL;

    contact = E_CONTACT (object);

    mapi = NULL;
    mapi_root = NULL;

    doc = xmlReadMemory (data->data, data->len, "scalix.xml", NULL, 0);

    if (doc == NULL) {
        return FALSE;
    }

    root = xmlDocGetRootElement (doc);

    if (root == NULL) {
        return FALSE;
    }

    if (strcmp (root->name, "contact") != 0) {
        return FALSE;
    }

    for (iter = root->children; iter != NULL; iter = iter->next) {
        /* remember the categories for later */
        if (strcmp (iter->name, "categories") == 0) {
            categories = iter;
            continue;
        }

        if (iter->type != XML_ELEMENT_NODE) {
            continue;
        }

        if (!(field = g_hash_table_lookup (field_mapping_table, iter->name))) {

            if (mapi == NULL) {
                mapi = xmlNewDoc ("1.0");
                mapi_root = xmlNewNode (NULL, (const char *) "mapi");
                xmlDocSetRootElement (mapi, mapi_root);
            }

            node = xmlCopyNode (iter, 1);
            xmlUnlinkNode (node);
            xmlAddChild (mapi_root, node);

            continue;
        }

        content = xmlNodeGetContent (iter);

        if (content == NULL || content[0] == '\0') {
            continue;
        }

        if (field->xml_to_contact == NULL) {
            e_contact_set (contact, field->e_field_id, content);
        } else {
            field->xml_to_contact (contact, field, content);
        }

        xmlFree (content);
    }

    /* set the categories */
    if (categories != NULL) {
        GString *categories_string = NULL;
        gboolean first = TRUE;

        categories_string = g_string_new ("");

        for (iter = categories->children; iter != NULL; iter = iter->next) {
            if (iter->type != XML_ELEMENT_NODE)
                continue;
            if (first == FALSE) {
                categories_string = g_string_append (categories_string, ",");
            } else {
                first = FALSE;
            }

            content = xmlNodeGetContent (iter);
            categories_string = g_string_append (categories_string, content);
            xmlFree (content);
        }

        e_contact_set (contact, E_CONTACT_CATEGORIES, categories_string->str);
        g_string_free (categories_string, FALSE);
    }

    if (mapi) {
        xmlDocDumpFormatMemory (mapi, &mem, &size, 0);

        e_vcard_add_attribute_with_value (E_VCARD (contact),
                                          e_vcard_attribute_new (NULL,
                                                                 "X-SCALIX-MAPI"),
                                          mem);

        xmlFree (mem);
    }

    return TRUE;
}

static EVCardAttribute *
e_contact_get_first_attr (EContact * contact, const char *attr_name)
{
    GList *attrs, *l;

    attrs = e_vcard_get_attributes (E_VCARD (contact));

    for (l = attrs; l; l = l->next) {
        EVCardAttribute *attr = l->data;
        const char *name;

        name = e_vcard_attribute_get_name (attr);

        if (!strcasecmp (name, attr_name))
            return attr;
    }

    return NULL;
}

CamelMimeMessage *
scalix_contact_to_mime_message (ScalixObject * object)
{
    CamelMimeMessage *message;
    CamelMedium *medium;
    xmlDocPtr doc;
    xmlNodePtr node, root, categories;
    xmlOutputBufferPtr buf;
    xmlChar *str, *data;
    EBookMapField *iter;
    EContact *contact;
    int len;
    EVCardAttribute *attr;
    const char *uid;
    const char *name;

    contact = E_CONTACT (object);
    message = camel_mime_message_new ();
    medium = CAMEL_MEDIUM (message);

    camel_medium_add_header (medium, "X-Scalix-Class", "IPM.Contact");

    doc = xmlNewDoc ((const char *) "1.0");
    root = xmlNewNode (NULL, (const char *) "contact");

    uid = e_contact_get_const (contact, E_CONTACT_UID);
    if (uid == NULL) {
        uid = camel_header_msgid_generate ();
        e_contact_set_uid (contact, uid);
    }

    camel_mime_message_set_message_id (message, uid);

    /* Set Subject */
    if ((name = e_contact_get_const (contact, E_CONTACT_FULL_NAME)) != NULL) {
        camel_mime_message_set_subject (message, name);
    }

    /* Merge MAPI xml */
    attr = e_contact_get_first_attr (contact, "X-SCALIX-MAPI");

    if (attr) {
        char *tstr = e_vcard_attribute_get_value (attr);
        xmlDocPtr mapi;

        mapi = xmlReadMemory (tstr, strlen (tstr), "contact.xml", NULL, 0);

        if (mapi != NULL) {
            xmlNodePtr mroot = xmlDocGetRootElement (mapi);
            xmlNodePtr niter;

            for (niter = mroot->children; niter != NULL; niter = niter->next) {
                node = xmlCopyNode (niter, 1);
                xmlUnlinkNode (node);
                xmlAddChild (root, node);
            }
        }
    }

    for (iter = field_mapping; iter->xml_node_name; iter++) {

        if (iter->xml_to_contact != NULL && iter->sub_id > 0)
            continue;

        if (iter->contact_to_xml != NULL) {
            iter->contact_to_xml (contact, iter, root);
        } else {
            str = (xmlChar *) e_contact_get_const (contact, iter->e_field_id);
            xmlNewTextChild (root, NULL, iter->xml_node_name, str);
        }
    }

    /* render the categories */
    char *categories_string =
        g_strdup (e_contact_get (contact, E_CONTACT_CATEGORIES));

    if (categories_string != NULL && *categories_string != '\0') {
        char *ptr1, *ptr2;

        ptr1 = ptr2 = categories_string;

        categories = xmlNewNode (NULL, (const char *) "categories");

        while ((ptr1 = strchr (ptr2, ','))) {
            *ptr1 = '\0';
            xmlNewTextChild (categories, NULL, "category", ptr2);
            ptr1++;
            ptr2 = ptr1;
        }

        if (ptr2 != NULL && *ptr2 != '\0') {
            xmlNewTextChild (categories, NULL, "category", ptr2);
        }

        xmlAddChild (root, categories);
    }

    /* render XML */
    buf = xmlAllocOutputBuffer (NULL);
    xmlNodeDumpOutput (buf, doc, root, 0, 1, NULL);
    xmlOutputBufferFlush (buf);

    len = buf->buffer->use;

    data = xmlStrndup (buf->buffer->content, buf->buffer->use);
    xmlOutputBufferClose (buf);

    camel_mime_part_set_content (CAMEL_MIME_PART (message), data, len,
                                 "application/scalix-properties");

    return message;
}

gboolean
scalix_contact_init_from_mime_message (ScalixObject * object,
                                       CamelMimeMessage * msg)
{
    char *mime_type;
    ScalixObjectIface *iface;
    GByteArray *data;
    CamelStream *stream;
    CamelDataWrapper *content;
    CamelMimePart *part;
    CamelMultipart *multipart;
    int i, num_parts;
    gboolean result = FALSE;

    iface = SCALIX_OBJECT_GET_IFACE (object);

    part = CAMEL_MIME_PART (msg);
    content = camel_medium_get_content_object (CAMEL_MEDIUM (part));

    if (content == NULL)
        return FALSE;

    mime_type = camel_content_type_simple (content->mime_type);

    if (CAMEL_IS_MULTIPART (content)) {
        multipart = (CamelMultipart *) content;
        num_parts = (int) camel_multipart_get_number (multipart);

        for (i = 0; i < num_parts; i++) {
            part = camel_multipart_get_part (multipart, i);
            content = camel_medium_get_content_object (CAMEL_MEDIUM (part));
            mime_type = camel_content_type_simple (content->mime_type);

            if (g_str_equal (mime_type, "application/scalix-properties")) {
                data = g_byte_array_new ();
                stream = camel_stream_mem_new_with_byte_array (data);
                camel_data_wrapper_decode_to_stream (content, stream);
                result = scalix_contact_set_xml_data (object, data);
            } else if (g_str_equal (mime_type, "multipart/alternative")) {
                // TBD
            } else {
                g_print ("XXXXX Unhandled mime part: %s\n", mime_type);
            }
        }
    } else {
        if (g_str_equal (mime_type, "application/scalix-properties")) {
            data = g_byte_array_new ();
            stream = camel_stream_mem_new_with_byte_array (data);
            camel_data_wrapper_decode_to_stream (content, stream);
            result = scalix_contact_set_xml_data (object, data);
        }
    }

    return result;
}

ScalixContact *
scalix_contact_new (const char *vcard)
{
    ScalixContact *contact;

    contact = g_object_new (SCALIX_TYPE_CONTACT, NULL);
    e_vcard_construct (E_VCARD (contact), vcard);

    return contact;
}

GList *
scalix_contact_get_fields ()
{
    EBookMapField *iter;
    const gchar *field_name;
    GList *fields = NULL;

    for (iter = field_mapping; iter->e_field_id != 0; iter++) {
        if (iter->xml_to_contact != NULL && iter->sub_id != 1)
            continue;

        field_name = e_contact_field_name (iter->e_field_id);
        fields = g_list_append (fields, g_strdup (field_name));
    }

    fields =
        g_list_append (fields,
                       g_strdup (e_contact_field_name (E_CONTACT_CATEGORIES)));

    return fields;
}

void
scalix_contact_set (ScalixContact * contact, const char *key, const char *val)
{
    EVCardAttribute *attr;

    attr = e_contact_get_first_attr (E_CONTACT (contact), key);

    if (attr) {
        e_vcard_attribute_remove_values (attr);
        if (val) {
            e_vcard_attribute_add_value (attr, val);
        } else {
            e_vcard_remove_attribute (E_VCARD (contact), attr);
        }
    } else if (val) {
        e_vcard_add_attribute_with_value (E_VCARD (contact),
                                          e_vcard_attribute_new (NULL, key),
                                          val);
    }
}

gboolean
scalix_contact_get (ScalixContact * contact, const char *key, char **val)
{
    EVCardAttribute *attr;

    attr = e_contact_get_first_attr (E_CONTACT (contact), key);

    if (attr) {
        *val = e_vcard_attribute_get_value (attr);
        return *val != NULL;
    } else {
        return FALSE;
    }
}
