/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#include "scalix-object.h"
#include "scalix-contact-list.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libebook/e-vcard.h>
#include <libebook/e-contact.h>

#include <string.h>

typedef struct _ScalixContactListPrivate ScalixContactListPrivate;

struct _ScalixContactListPrivate {

    char dummy;
};

/* ************************************************************************** */
static void scalix_contact_list_get_property (GObject * object,
                                              guint prop_id,
                                              GValue * value,
                                              GParamSpec * pspec);

static void scalix_contact_list_set_property (GObject * object,
                                              guint prop_id,
                                              const GValue * value,
                                              GParamSpec * pspec);

static char *scalix_contact_list_serialize (ScalixObject * object);

static gboolean scalix_contact_list_deserialize (ScalixObject * object,
                                                 const char *string);

CamelMimeMessage *scalix_contact_list_to_mime_message (ScalixObject * object);

gboolean scalix_contact_list_init_from_mime_message (ScalixObject * object,
                                                     CamelMimeMessage * msg);

static gboolean scalix_contact_list_set_xml_data (ScalixObject * object,
                                                  GByteArray * data);

static void initialize_field_mapping_table (void);

gboolean scalix_contact_list_get (ScalixContactList * list,
                                  const char *key, char **val);

void scalix_contact_list_set (ScalixContactList * list,
                              const char *key, const char *val);

/* ************************************************************************** */

/* GObject related stuff */

enum {
    PROP_0,
    /* overriden */
    PROP_UID,
    PROP_IPM_TYPE
};

enum {

    NONE = -1,

};

enum {

    TYPE_EMAIL,
    TYPE_CONTACT,
    TYPE_SYSTEM
};

/* Interface implementation */
static void
scalix_contact_list_object_iface_init (ScalixObjectIface * iface)
{
    iface->init_from_mime_message = scalix_contact_list_init_from_mime_message;
    iface->to_mime_message = scalix_contact_list_to_mime_message;
    iface->serialize = scalix_contact_list_serialize;
    iface->deserialize = scalix_contact_list_deserialize;
}

G_DEFINE_TYPE_EXTENDED (ScalixContactList,
                        scalix_contact_list,
                        E_TYPE_CONTACT,
                        0,
                        G_IMPLEMENT_INTERFACE (SCALIX_TYPE_OBJECT,
                                               scalix_contact_list_object_iface_init));

static void
scalix_contact_list_init (ScalixContactList * self)
{
}

static void
scalix_contact_list_class_init (ScalixContactListClass * klass)
{
    GObjectClass *gobject_class;

    gobject_class = (GObjectClass *) klass;

    initialize_field_mapping_table ();

    gobject_class->set_property = scalix_contact_list_set_property;
    gobject_class->get_property = scalix_contact_list_get_property;

    g_object_class_override_property (gobject_class, PROP_UID, "uid");
    g_object_class_override_property (gobject_class, PROP_IPM_TYPE, "ipm-type");
}

/* ************************************************************************** */
static void
scalix_contact_list_get_property (GObject * object,
                                  guint prop_id,
                                  GValue * value, GParamSpec * pspec)
{
    char *str;

    switch (prop_id) {

    case PROP_UID:
        str = e_contact_get (E_CONTACT (object), E_CONTACT_UID);
        g_value_take_string (value, str);
        break;

    case PROP_IPM_TYPE:
        g_value_set_int (value, IPM_CONTACT);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        return;
    }
}

static void
scalix_contact_list_set_property (GObject * object,
                                  guint prop_id,
                                  const GValue * value, GParamSpec * pspec)
{
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
}

/* ************************************************************************** */

/* XML <-> vCard field mapping */

typedef struct _EBookMapField EBookMapField;

typedef void (*xml_to_contact_handler) (EContact * contact,
                                        const EBookMapField * field,
                                        const char *value);

typedef void (*contact_to_xml_handler) (EContact * contact,
                                        const EBookMapField * field,
                                        xmlNodePtr parent);

struct _EBookMapField {
    EContactField e_field_id;
    char *xml_node_name;
    xml_to_contact_handler xml_to_contact;
    contact_to_xml_handler contact_to_xml;
    int sub_id;
};

static void
xtc_uid (EContact * contact, const EBookMapField * field, const char *value)
{
    e_contact_set (contact, E_CONTACT_UID, (char *) value);
}

static void
ctx_uid (EContact * contact, const EBookMapField * field, xmlNodePtr parent)
{
    char *uid;

    if ((uid = e_contact_get (contact, E_CONTACT_UID)) == NULL)
        return;

    xmlNewTextChild (parent, NULL,
                     (const xmlChar *) field->xml_node_name,
                     (const xmlChar *) uid);

    g_free (uid);
}

/*
 * This table is magic! The sub_id _MUST_ match the field index of the field_group + 1
 * FIXME: missing: middle_name, profession, 
 */

static EBookMapField field_mapping[] = {
    {E_CONTACT_FULL_NAME, "display_name", NULL, NULL, NONE},
    {E_CONTACT_FILE_AS, "file_as", NULL, NULL, NONE},
    {E_CONTACT_UID, "direct_ref", xtc_uid, ctx_uid, NONE},
    {0, NULL, NULL, NULL, NONE}
};

static GHashTable *field_mapping_table = NULL;

static void
initialize_field_mapping_table ()
{
    EBookMapField *iter = NULL;

    if (field_mapping_table != NULL)
        return;

    field_mapping_table = g_hash_table_new (g_str_hash, g_str_equal);

    for (iter = field_mapping; iter->e_field_id != 0; iter++)
        g_hash_table_insert (field_mapping_table, iter->xml_node_name, iter);
}

#define e_contact_list_set_uid(__contact, __uid) e_contact_set (__contact, \
                                            E_CONTACT_UID, (gpointer) __uid)

/* ************************************************************************** */

/* ScalixObject interface impl. */

static char *
scalix_contact_list_serialize (ScalixObject * object)
{
    char *out;

    out = e_vcard_to_string (E_VCARD (object), EVC_FORMAT_VCARD_30);
    return out;
}

static gboolean
scalix_contact_list_deserialize (ScalixObject * object, const char *string)
{
    /* No error handle here? */
    e_vcard_construct (E_VCARD (object), string);
    return TRUE;
}

static void
add_vcard_param_and_value (EVCardAttribute * attr, const char *name,
                           const char *value)
{
    e_vcard_attribute_add_param_with_value (attr,
                                            e_vcard_attribute_param_new (name),
                                            value);
}

static gboolean
scalix_contact_list_set_xml_data (ScalixObject * object, GByteArray * data)
{
    xmlDocPtr doc, mapi;
    xmlNodePtr root, iter, mapi_root, node;
    xmlChar *mem;
    EContact *contact;
    int size, i;
    xmlXPathContextPtr context;
    xmlXPathObjectPtr xpath_result;
    xmlNodeSetPtr nodeset1, nodeset2;
    EVCardAttribute *attr;
    gboolean valid_entry;

    contact = E_CONTACT (object);

    e_vcard_add_attribute_with_value (E_VCARD (contact),
                                      e_vcard_attribute_new (NULL, EVC_X_LIST),
                                      "TRUE");

    mapi = NULL;
    mapi_root = NULL;

    doc = xmlReadMemory (data->data, data->len, "scalix.xml", NULL, 0);

    if (doc == NULL) {
        return FALSE;
    }

    context = xmlXPathNewContext (doc);

    root = xmlDocGetRootElement (doc);

    if (root == NULL) {
        return FALSE;
    }

    if (strcmp (root->name, "distlist") != 0) {
        return FALSE;
    }

    for (iter = root->children; iter != NULL; iter = iter->next) {
        EBookMapField *field = NULL;
        char *content;

        if (iter->type != XML_ELEMENT_NODE)
            continue;

        if (!(field = g_hash_table_lookup (field_mapping_table, iter->name))) {

            if (mapi == NULL) {
                mapi = xmlNewDoc ("1.0");
                mapi_root = xmlNewNode (NULL, (const char *) "mapi");
                xmlDocSetRootElement (mapi, mapi_root);
            }

            node = xmlCopyNode (iter, 1);
            xmlUnlinkNode (node);
            xmlAddChild (mapi_root, node);

            continue;
        }

        content = xmlNodeGetContent (iter);

        if (content == NULL || content[0] == '\0')
            continue;

        if (field->xml_to_contact == NULL)
            e_contact_set (contact, field->e_field_id, content);
        else
            field->xml_to_contact (contact, field, content);

        xmlFree (content);
    }

    /* get a list of all PDL members */
    xpath_result = xmlXPathEvalExpression ("/distlist/members/member", context);

    if (xmlXPathNodeSetIsEmpty (xpath_result->nodesetval) == FALSE) {
        nodeset1 = xpath_result->nodesetval;
        xpath_result =
            xmlXPathEvalExpression ("/distlist/dl_members/dl_member", context);
        nodeset2 = xpath_result->nodesetval;

        for (i = 0; i < nodeset1->nodeNr; i++) {
            attr = e_vcard_attribute_new (NULL, EVC_EMAIL);
            xmlNodePtr member = nodeset1->nodeTab[i];
            xmlNodePtr dl_member = nodeset2->nodeTab[i];
            char *name, *address;

            valid_entry = TRUE;
            name = NULL;
            address = NULL;

            if (strcmp (xmlGetProp (member, "class"), "contact") == 0) {
                /* we found a contact */
                for (iter = member->children; iter; iter = iter->next) {
                    if (strcmp (iter->name, "contact_type") == 0) {
                        if (strcmp (xmlNodeGetContent (iter), "pdl") == 0) {
                            /* nested PDLs are not supported */
                            valid_entry = FALSE;
                        }
                    } else if (strcmp (iter->name, "contact_ref") == 0) {
                        add_vcard_param_and_value (attr, EVC_X_DEST_CONTACT_UID,
                                                   xmlNodeGetContent (iter));
                    } else if (strcmp (iter->name, "store_record_key") == 0) {
                        add_vcard_param_and_value (attr,
                                                   "X-SCALIX-PDL-STORE-RECORD-KEY",
                                                   xmlNodeGetContent (iter));
                    } else if (strcmp (iter->name, "sx_ms_ver") == 0) {
                        add_vcard_param_and_value (attr,
                                                   "X-SCALIX-PDL-SX-MS-VER",
                                                   xmlNodeGetContent (iter));
                    }
                }
                for (iter = dl_member->children; iter; iter = iter->next) {
                    if (strcmp (iter->name, "name") == 0) {
                        name = xmlNodeGetContent (iter);
                    } else if (strcmp (iter->name, "msg_format") == 0) {
                        char *value = xmlNodeGetContent (iter);

                        if (strcmp (value, "plain_text") == 0) {
                            add_vcard_param_and_value (attr,
                                                       EVC_X_DEST_HTML_MAIL,
                                                       "FALSE");
                        } else if (strcmp (value, "rtf") == 0) {
                            add_vcard_param_and_value (attr,
                                                       EVC_X_DEST_HTML_MAIL,
                                                       "TRUE");
                        }
                    } else if (strcmp (iter->name, "type") == 0) {
                        add_vcard_param_and_value (attr,
                                                   "X-SCALIX-PDL-ADDRESSTYPE",
                                                   xmlNodeGetContent (iter));
                    } else if (strcmp (iter->name, "address") == 0) {
                        address = xmlNodeGetContent (iter);
                    }
                }
            } else if (strcmp (xmlGetProp (member, "class"), "by_value") == 0) {
                /* we found an email entry */
                for (iter = member->children; iter; iter = iter->next) {
                    if (strcmp (iter->name, "display") == 0) {
                        add_vcard_param_and_value (attr, EVC_X_DEST_NAME,
                                                   xmlNodeGetContent (iter));
                    } else if (strcmp (iter->name, "address") == 0) {
                        address = xmlNodeGetContent (iter);
                    } else if (strcmp (iter->name, "address_type") == 0) {
                        add_vcard_param_and_value (attr,
                                                   "X-SCALIX-PDL-ADDRESSTYPE",
                                                   xmlNodeGetContent (iter));
                    } else if (strcmp (iter->name, "msg_format") == 0) {
                        char *value = xmlNodeGetContent (iter);

                        if (strcmp (value, "plain_text") == 0) {
                            add_vcard_param_and_value (attr,
                                                       EVC_X_DEST_HTML_MAIL,
                                                       "FALSE");
                        } else if (strcmp (value, "rtf") == 0) {
                            add_vcard_param_and_value (attr,
                                                       EVC_X_DEST_HTML_MAIL,
                                                       "TRUE");
                        }
                    }
                }
            } else if (strcmp (xmlGetProp (member, "class"), "directory") == 0
                       || strcmp (xmlGetProp (member, "class"),
                                  "unknown") == 0) {
                /* we found a system entry */
                for (iter = dl_member->children; iter; iter = iter->next) {
                    if (strcmp (iter->name, "name") == 0) {
                        name = xmlNodeGetContent (iter);
                    } else if (strcmp (iter->name, "msg_format") == 0) {
                        char *value = xmlNodeGetContent (iter);

                        if (strcmp (value, "plain_text") == 0) {
                            add_vcard_param_and_value (attr,
                                                       EVC_X_DEST_HTML_MAIL,
                                                       "FALSE");
                        } else if (strcmp (value, "rtf") == 0) {
                            add_vcard_param_and_value (attr,
                                                       EVC_X_DEST_HTML_MAIL,
                                                       "TRUE");
                        }
                    } else if (strcmp (iter->name, "type") == 0) {
                        add_vcard_param_and_value (attr,
                                                   "X-SCALIX-PDL-ADDRESSTYPE",
                                                   xmlNodeGetContent (iter));
                    } else if (strcmp (iter->name, "address") == 0) {
                        address = xmlGetProp (iter, "rfc822");
                    }
                }
            }

            if (valid_entry) {
                if (name) {
                    add_vcard_param_and_value (attr, EVC_X_DEST_NAME, name);
                }
                if (address) {
                    add_vcard_param_and_value (attr, EVC_X_DEST_EMAIL, address);
                    if (name) {
                        GString *name_address = g_string_new ("");

                        g_string_append_printf (name_address, "%s <%s>", name,
                                                address);
                        e_vcard_attribute_add_value (attr, name_address->str);
                        g_string_free (name_address, TRUE);
                    } else {
                        e_vcard_attribute_add_value (attr, address);
                    }
                }
                e_vcard_add_attribute (E_VCARD (contact), attr);
            } else {
                e_vcard_attribute_free (attr);
            }

            dl_member = dl_member->next;
        }
        xmlXPathFreeObject (xpath_result);
    }

    if (mapi) {
        xmlDocDumpFormatMemory (mapi, &mem, &size, 0);
        e_vcard_add_attribute_with_value (E_VCARD (contact),
                                          e_vcard_attribute_new (NULL,
                                                                 "X-SCALIX-MAPI"),
                                          mem);

        xmlFree (mem);
    }

    return TRUE;
}

/* c&p from e-contact.c */
static EVCardAttribute *
e_contact_list_get_first_attr (EContact * contact, const char *attr_name)
{
    GList *attrs, *l;

    attrs = e_vcard_get_attributes (E_VCARD (contact));

    for (l = attrs; l; l = l->next) {
        EVCardAttribute *attr = l->data;
        const char *name;

        name = e_vcard_attribute_get_name (attr);

        if (!strcasecmp (name, attr_name))
            return attr;
    }

    return NULL;
}

static char *
e_contact_list_get_first_param_value (EVCardAttributeParam * param)
{
    GList *v = NULL;

    v = e_vcard_attribute_param_get_values (param);
    return v->data;
}

CamelMimeMessage *
scalix_contact_list_to_mime_message (ScalixObject * object)
{
    CamelMimeMessage *message;
    CamelMedium *medium;
    xmlDocPtr doc;
    xmlNodePtr node, root, dl_members_root, members_root;
    xmlOutputBufferPtr buf;
    xmlChar *str, *data;
    EBookMapField *iter;
    EContact *contact;
    int len;
    EVCardAttribute *attr;
    EVCardAttributeParam *param;
    GList *params;
    const char *uid;
    const char *name;
    GList *attrs, *l;
    gboolean is_new = FALSE;
    int num_entries = 0;

    contact = E_CONTACT (object);
    message = camel_mime_message_new ();
    medium = CAMEL_MEDIUM (message);

    camel_medium_add_header (medium, "X-Scalix-Class", "IPM.DistList");

    doc = xmlNewDoc ((const char *) "1.0");
    root = xmlNewNode (NULL, (const char *) "distlist");

    uid = e_contact_get_const (contact, E_CONTACT_UID);

    if (uid == NULL) {
        uid = camel_header_msgid_generate ();
        e_contact_list_set_uid (contact, uid);
        is_new = TRUE;
    }

    camel_mime_message_set_message_id (message, uid);

    /* Set Subject */
    if ((name = e_contact_get_const (contact, E_CONTACT_FULL_NAME)) != NULL) {
        camel_mime_message_set_subject (message, name);
    }

    xmlNewTextChild (root, NULL, "subject", name);

    if (is_new) {
        /* default top-level properties for new lists */
        xmlNewTextChild (root, NULL, "dl_name", name);
        xmlNewTextChild (root, NULL, "message_class", "IPM.DistList");
        xmlNewTextChild (root, NULL, "sensitivity", "0");
    } else {
        /* existing entry -> merge MAPI xml */
        attr = e_contact_list_get_first_attr (contact, "X-SCALIX-MAPI");

        if (attr) {
            char *tstr = e_vcard_attribute_get_value (attr);
            xmlDocPtr mapi;

            mapi = xmlReadMemory (tstr, strlen (tstr), "contact.xml", NULL, 0);

            if (mapi != NULL) {
                xmlNodePtr mroot = xmlDocGetRootElement (mapi);
                xmlNodePtr niter;

                for (niter = mroot->children; niter != NULL;
                     niter = niter->next) {

                    /* ignore <members> and <dl_members> */
                    if (strcmp (niter->name, "dl_members")
                        || strcmp (niter->name, "members"))
                        continue;

                    node = xmlCopyNode (niter, 1);
                    xmlUnlinkNode (node);
                    xmlAddChild (root, node);
                }
            }
        }
    }

    for (iter = field_mapping; iter->xml_node_name; iter++) {
        if (iter->xml_to_contact != NULL && iter->sub_id > 0)
            continue;

        if (iter->contact_to_xml != NULL) {
            iter->contact_to_xml (contact, iter, root);
        } else {
            str = (xmlChar *) e_contact_get_const (contact, iter->e_field_id);
            xmlNewTextChild (root, NULL, iter->xml_node_name, str);
        }
    }

    /* render the <dl_members> and <members> sections */
    attrs = e_vcard_get_attributes (E_VCARD (contact));

    dl_members_root = xmlNewNode (NULL, (const char *) "dl_members");
    members_root = xmlNewNode (NULL, (const char *) "members");

    for (l = attrs; l; l = l->next) {
        EVCardAttribute *attr = NULL;
        const char *name;
        int type = TYPE_EMAIL;

        attr = l->data;
        name = e_vcard_attribute_get_name (attr);

        if (strcasecmp (name, EVC_EMAIL) == 0) {
            params = e_vcard_attribute_get_params (attr);
            GList *iter = NULL;
            xmlNodePtr dl_member, member;
            gboolean found_msg_format = FALSE;
            gboolean found_email_address = FALSE;

            dl_member = xmlNewNode (NULL, (const char *) "dl_member");
            member = xmlNewNode (NULL, (const char *) "member");

            /* determine type of entry */
            for (iter = params; iter; iter = iter->next) {
                EVCardAttributeParam *p = iter->data;

                if (strcmp
                    (e_vcard_attribute_param_get_name (p),
                     EVC_X_DEST_CONTACT_UID) == 0) {
                    char *v = e_contact_list_get_first_param_value (p);

                    /* ugly but the only way to distinguish contacts and system entries */
                    type = !strncmp (v, "cn=", 3) ? TYPE_SYSTEM : TYPE_CONTACT;
                }
            }

            if (type == TYPE_CONTACT) {
                xmlNewTextChild (member, NULL, "contact_type", "contact");
                xmlNewProp (member, "class", "contact");

                for (iter = params; iter; iter = iter->next) {
                    param = iter->data;
                    const char *name = e_vcard_attribute_param_get_name (param);

                    if (strcmp (name, EVC_X_DEST_CONTACT_UID) == 0) {
                        xmlNewTextChild (member, NULL, "contact_ref",
                                         e_contact_list_get_first_param_value
                                         (param));
                    } else if (strcmp (name, "X-SCALIX-PDL-STORE-RECORD-KEY") ==
                               0) {
                        xmlNewTextChild (member, NULL, "store_record_key",
                                         e_contact_list_get_first_param_value
                                         (param));
                    } else if (strcmp (name, "X-SCALIX-PDL-SX-MS-VER") == 0) {
                        xmlNewTextChild (member, NULL, "sx_ms_ver",
                                         e_contact_list_get_first_param_value
                                         (param));
                    } else if (strcmp (name, "X-SCALIX-PDL-ADDRESSTYPE") == 0) {
                        xmlNewTextChild (dl_member, NULL, "type",
                                         e_contact_list_get_first_param_value
                                         (param));
                    } else if (strcmp (name, EVC_X_DEST_NAME) == 0) {
                        xmlNewTextChild (dl_member, NULL, "name",
                                         e_contact_list_get_first_param_value
                                         (param));
                    } else if (strcmp (name, EVC_X_DEST_EMAIL) == 0) {
                        xmlNewTextChild (dl_member, NULL, "address",
                                         e_contact_list_get_first_param_value
                                         (param));
                        found_email_address = TRUE;
                    } else if (strcmp (name, EVC_X_DEST_HTML_MAIL) == 0) {
                        found_msg_format = TRUE;
                        char *v = e_contact_list_get_first_param_value (param);

                        if (strcmp (v, "TRUE") == 0) {
                            xmlNewTextChild (dl_member, NULL, "msg_format",
                                             "rtf");
                        } else {
                            xmlNewTextChild (dl_member, NULL, "msg_format",
                                             "plain_text");
                        }
                    }
                }

                if (found_email_address == FALSE) {
                    /* skip this entry if no email address was found */
                    continue;
                }

                if (found_msg_format == FALSE) {
                    xmlNewTextChild (dl_member, NULL, "msg_format",
                                     "outlook_decides");
                }

                if (is_new) {
                    xmlNewTextChild (dl_member, NULL, "type", "SMTP");
                }
                //xmlNewTextChild (dl_member, NULL, "address", e_vcard_attribute_get_value (attr));
            } else if (type == TYPE_SYSTEM) {
                xmlNodePtr address_node;

                xmlNewTextChild (member, NULL, "src_dir", "SYSTEM");
                xmlNewProp (member, "class", "directory");
                xmlNewTextChild (dl_member, NULL, "msg_format",
                                 "outlook_decides");
                xmlNewTextChild (dl_member, NULL, "type", "OPENMAIL");

                for (iter = params; iter; iter = iter->next) {
                    param = iter->data;
                    const char *name = e_vcard_attribute_param_get_name (param);

                    if (strcmp (name, EVC_X_DEST_EMAIL) == 0) {
                        /* fill in rfc822 address; server will lookup omAddress for us */
                        address_node =
                            xmlNewTextChild (member, NULL, "address", "");
                        xmlNewProp (address_node, "rfc822",
                                    e_contact_list_get_first_param_value
                                    (param));
                        address_node =
                            xmlNewTextChild (dl_member, NULL, "address", "");
                        xmlNewProp (address_node, "rfc822",
                                    e_contact_list_get_first_param_value
                                    (param));
                        found_email_address = TRUE;
                    } else if (strcmp (name, EVC_X_DEST_NAME) == 0) {
                        xmlNewTextChild (dl_member, NULL, "name",
                                         e_contact_list_get_first_param_value
                                         (param));
                    }
                }

                if (found_email_address == FALSE) {
                    /* skip this entry if no email address was found */
                    continue;
                }
            } else {
                xmlNewProp (member, "class", "by_value");
                xmlNewProp (member, "sub_class", "2");

                for (iter = params; iter; iter = iter->next) {
                    param = iter->data;
                    const char *name = e_vcard_attribute_param_get_name (param);

                    if (strcmp (name, EVC_X_DEST_NAME) == 0) {
                        xmlNewTextChild (dl_member, NULL, "name",
                                         e_contact_list_get_first_param_value
                                         (param));
                        xmlNewTextChild (member, NULL, "display",
                                         e_contact_list_get_first_param_value
                                         (param));
                    } else if (strcmp (name, EVC_X_DEST_HTML_MAIL) == 0) {
                        found_msg_format = TRUE;
                        char *v = e_contact_list_get_first_param_value (param);

                        if (strcmp (v, "TRUE") == 0) {
                            xmlNewTextChild (dl_member, NULL, "msg_format",
                                             "rtf");
                            xmlNewTextChild (member, NULL, "msg_format", "rtf");
                        } else {
                            xmlNewTextChild (dl_member, NULL, "msg_format",
                                             "plain_text");
                            xmlNewTextChild (member, NULL, "msg_format",
                                             "plain_text");
                        }
                    } else if (strcmp (name, "X-SCALIX-PDL-ADDRESSTYPE") == 0) {
                        xmlNewTextChild (dl_member, NULL, "type",
                                         e_contact_list_get_first_param_value
                                         (param));
                        xmlNewTextChild (dl_member, NULL, "address_type",
                                         e_contact_list_get_first_param_value
                                         (param));
                    }
                }

                if (found_msg_format == FALSE) {
                    xmlNewTextChild (dl_member, NULL, "msg_format",
                                     "outlook_decides");
                    xmlNewTextChild (member, NULL, "msg_format",
                                     "outlook_decides");
                }

                xmlNewTextChild (dl_member, NULL, "address",
                                 e_vcard_attribute_get_value (attr));
                xmlNewTextChild (member, NULL, "address",
                                 e_vcard_attribute_get_value (attr));

                if (is_new) {
                    xmlNewTextChild (member, NULL, "display",
                                     e_vcard_attribute_get_value (attr));
                    xmlNewTextChild (member, NULL, "address_type", "SMTP");
                    xmlNewTextChild (dl_member, NULL, "name",
                                     e_vcard_attribute_get_value (attr));
                    xmlNewTextChild (dl_member, NULL, "type", "SMTP");
                }
            }

            xmlAddChild (dl_members_root, dl_member);
            xmlAddChild (members_root, member);
            num_entries++;
        }
    }

    if (num_entries > 0) {
        xmlAddChild (root, dl_members_root);
        xmlAddChild (root, members_root);
    }

    /* render XML */
    buf = xmlAllocOutputBuffer (NULL);
    xmlNodeDumpOutput (buf, doc, root, 0, 1, NULL);
    xmlOutputBufferFlush (buf);

    len = buf->buffer->use;

    data = xmlStrndup (buf->buffer->content, buf->buffer->use);

    xmlOutputBufferClose (buf);

    camel_mime_part_set_content (CAMEL_MIME_PART (message), data, len,
                                 "application/scalix-properties");

    return message;
}

gboolean
scalix_contact_list_init_from_mime_message (ScalixObject * object,
                                            CamelMimeMessage * msg)
{
    char *mime_type;
    ScalixObjectIface *iface;
    GByteArray *data;
    CamelStream *stream;
    CamelDataWrapper *content;
    CamelMimePart *part;
    CamelMultipart *multipart;
    int i, num_parts;
    gboolean result = FALSE;

    iface = SCALIX_OBJECT_GET_IFACE (object);

    part = CAMEL_MIME_PART (msg);
    content = camel_medium_get_content_object (CAMEL_MEDIUM (part));

    if (content == NULL)
        return FALSE;

    mime_type = camel_content_type_simple (content->mime_type);

    if (CAMEL_IS_MULTIPART (content)) {
        multipart = (CamelMultipart *) content;
        num_parts = (int) camel_multipart_get_number (multipart);

        for (i = 0; i < num_parts; i++) {
            part = camel_multipart_get_part (multipart, i);
            content = camel_medium_get_content_object (CAMEL_MEDIUM (part));
            mime_type = camel_content_type_simple (content->mime_type);

            if (g_str_equal (mime_type, "application/scalix-properties")) {
                data = g_byte_array_new ();
                stream = camel_stream_mem_new_with_byte_array (data);
                camel_data_wrapper_decode_to_stream (content, stream);
                result = scalix_contact_list_set_xml_data (object, data);
            } else if (g_str_equal (mime_type, "multipart/alternative")) {
                // TBD
            } else {
                g_print ("XXXXX Unhandled mime part: %s\n", mime_type);
            }
        }
    } else {
        if (g_str_equal (mime_type, "application/scalix-properties")) {
            data = g_byte_array_new ();
            stream = camel_stream_mem_new_with_byte_array (data);
            camel_data_wrapper_decode_to_stream (content, stream);
            result = scalix_contact_list_set_xml_data (object, data);
        }
    }

    return result;
}

/* ************************************************************************** */
ScalixContactList *
scalix_contact_list_new (const char *vcard)
{
    ScalixContactList *list;

    list = g_object_new (SCALIX_TYPE_CONTACT_LIST, NULL);
    e_vcard_construct (E_VCARD (list), vcard);

    return list;
}

GList *
scalix_contact_list_get_fields ()
{
    EBookMapField *iter;
    const gchar *field_name;
    GList *fields = NULL;

    for (iter = field_mapping; iter->e_field_id != 0; iter++) {
        if (iter->xml_to_contact != NULL && iter->sub_id != 1)
            continue;

        field_name = e_contact_field_name (iter->e_field_id);
        fields = g_list_append (fields, g_strdup (field_name));
    }

    return fields;
}

void
scalix_contact_list_set (ScalixContactList * list, const char *key,
                         const char *val)
{
    EVCardAttribute *attr;

    attr = e_contact_list_get_first_attr (E_CONTACT (list), key);

    if (attr) {
        e_vcard_attribute_remove_values (attr);
        if (val) {
            e_vcard_attribute_add_value (attr, val);
        } else {
            e_vcard_remove_attribute (E_VCARD (list), attr);
        }
    } else if (val) {
        e_vcard_add_attribute_with_value (E_VCARD (list),
                                          e_vcard_attribute_new (NULL, key),
                                          val);
    }

}

gboolean
scalix_contact_list_get (ScalixContactList * list, const char *key, char **val)
{
    EVCardAttribute *attr;

    attr = e_contact_list_get_first_attr (E_CONTACT (list), key);

    if (attr) {
        *val = e_vcard_attribute_get_value (attr);
        return *val != NULL;
    } else {
        return FALSE;
    }
}

/* ************************************************************************** */
