/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifndef SCALIX_CONTACT_LIST_H
#define SCALIX_CONTACT_LIST_H

#include <glib.h>

#include <libebook/e-vcard.h>
#include <libebook/e-contact.h>

G_BEGIN_DECLS
#define SCALIX_TYPE_CONTACT_LIST              (scalix_contact_list_get_type ())
#define SCALIX_CONTACT_LIST                   (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_CONTACT_LIST, ScalixContactList))
#define SCALIX_CONTACT_LIST_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_CONTACT_LIST, ScalixContactListClass))
#define SCALIX_IS_CONTACT_LIST(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_CONTACT_LIST))
#define SCALIX_IS_CONTACT_LIST_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_CONTACT_LIST))
#define SCALIX_CONTACT_LIST_GET_PRIVATE(obj)  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_CONTACT_LIST, ScalixContactListPrivate))
typedef struct _ScalixContactList ScalixContactList;
typedef struct _ScalixContactListClass ScalixContactListClass;

struct _ScalixContactList {
    EContact parent;
};

struct _ScalixContactListClass {
    EContactClass parent_class;
};

GType scalix_contact_list_get_type (void);
ScalixContactList *scalix_contact_list_new (const char *vcard);
GList *scalix_contact_list_get_fields (void);

G_END_DECLS
#endif /* SCALIX_CONTACT_LIST_H */
