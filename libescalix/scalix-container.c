/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#include "scalix-version.h"

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-data-server-util.h>
#else
#include <libedataserver/e-util.h>
#endif

#include <camel/camel.h>
#include <camel/camel-folder.h>
#include <camel/camel-store.h>
#include <camel/camel-offline-store.h>

#include "scalix-container.h"
#include "scalix-object.h"
#include "scalix-oc-entry.h"
#include "scalix-object-cache.h"
#include "scalix-marshal.h"
#include "scalix-debug.h"
#include "scalix-camel-session.h"
#include "scalix-utils.h"
#include "scalix-server-info.h"

#include <string.h>

/* This is _ONLY_ included for defining  CAMEL_SCALIX_FOLDER_ARG_NEED_SYNCH
 * and such alike
 * DO NOT use any function from camel-scalix* direclty ! */
#include <camel/camel-scalix-utils.h>
#include <camel/camel-scalix-folder.h>
#include <camel/camel-scalix-store.h>

/* container refresh time in seconds */
#define DEFAULT_REFRESH_TIME 60

/* Logging */
#include <glog/glog.h>
GLOG_CATEGORY (slcon, "container", GLOG_FORMAT_FG_BLUE, "Scalix container");
GLOG_CATEGORY (slsyn, "synch", GLOG_FORMAT_FG_GREEN, "Synchronization");
GLOG_CATEGORY (slslv, "slave", GLOG_FORMAT_FG_MAGENTA, "Synchronization Salve");

/* Background synchronization Slave */
typedef enum {

    SLAVE_SHOULD_SLEEP,
    SLAVE_SHOULD_WORK,
    SLAVE_SHOULD_DIE
} SynchSlaveCommand;

struct _ScalixContainerPrivate {

    /* gobject stuff */
    gboolean dispose_has_run;

    CamelURL *url;

    /* Camel objects */
    CamelStore *store;
    CamelFolder *folder;
    CamelFolder *cfolder;

    /* cache object */
    ScalixObjectCache *cache;
    CamelInternetAddress *user_email;

    GMutex *state_lock;
    ContainerState state;

    char *local_attachment_store;
    char *cache_dir;

    gboolean in_sync;
    gboolean create;
    guint32 type;

    /* Synch Slave */
    GThread *synch_slave;
    GCond *synch_cond;
    GMutex *synch_lock;
    SynchSlaveCommand slave_cmd;
    GTimeVal refresh_time;
    gboolean do_synch;

};

enum {
    PROP_0 = 0,
    PROP_URI,
    PROP_ONLINE,
    PROP_PATH,
    PROP_USER,
    PROP_PASS,
    PROP_OWNER_EMAIL,
    PROP_TYPE,
    PROP_CREATE,
    PROP_STATE,
    PROP_ITEMS
};

enum {
    CONTAINER_OBJECT_ADDED_SIGNAL,
    CONTAINER_OBJECT_CHANGED_SIGNAL,
    CONTAINER_OBJECT_REMOVED_SIGNAL,
    CONTAINER_LAST_SIGNAL
};

static guint container_signals[CONTAINER_LAST_SIGNAL] = { 0 };

static GObjectClass *parent_class = NULL;

/* ************************************************************************** */

static GObject *scalix_container_constructor (GType type,
                                              guint n_cprops,
                                              GObjectConstructParam * cprops);

static void scalix_container_set_property (GObject * object,
                                           guint prop_id,
                                           const GValue * value,
                                           GParamSpec * pspec);

static void scalix_container_get_property (GObject * object,
                                           guint prop_id,
                                           GValue * value, GParamSpec * pspec);

/* Internal functions */
static gboolean go_online (ScalixContainer * container);
static gboolean go_offline (ScalixContainer * container);
static gboolean container_refresh_internal (ScalixContainer * container);
static gpointer synch_slave_loop (gpointer data);
static void slave_hibernate (ScalixContainer * container);
static void slave_wakeup (ScalixContainer * container);
static void slave_die (ScalixContainer * container);

static void set_error_from_cex (ScalixContainer * container,
                                CamelException * ex);
#define store_is_online(__store) (CAMEL_OFFLINE_STORE (__store)->state == CAMEL_OFFLINE_STORE_NETWORK_AVAIL)

/* ************************************************************************** */

/* GObject Stuff */

G_DEFINE_TYPE (ScalixContainer, scalix_container, G_TYPE_OBJECT);

GQuark
scalix_container_error_quark (void)
{
    static GQuark quark = 0;

    if (quark == 0) {
        quark = g_quark_from_static_string ("scalix-container-error-quark");
    }

    return quark;
}

static void
scalix_container_dispose (GObject * object)
{
    ScalixContainer *container;
    ScalixContainerPrivate *priv;

    container = SCALIX_CONTAINER (object);
    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    if (priv->dispose_has_run) {
        return;
    }

    priv->dispose_has_run = TRUE;

    /* Set container to error state to
     * make sure nobody is using it during
     * disposal */
    priv->state = CONTAINER_STATE_ERROR;

    GLOG_CAT_DEBUG (&slcon, "Killing slave");
    slave_die (container);

    g_free (priv->local_attachment_store);
    g_free (priv->cache_dir);

    if (priv->folder) {
        camel_object_unref (priv->folder);
    }

    if (priv->cfolder) {
        camel_object_unref (priv->cfolder);
    }

    if (priv->store) {
        camel_object_unref (priv->store);
    }

    if (priv->user_email) {
        camel_object_unref (priv->user_email);
    }

    if (priv->url) {
        camel_url_free (priv->url);
    }

    priv->folder = NULL;
    priv->store = NULL;

    if (priv->cache) {
        g_object_unref (priv->cache);
    }

    if (G_OBJECT_CLASS (parent_class)->dispose)
        (*G_OBJECT_CLASS (parent_class)->dispose) (object);
}

static void
scalix_container_finalize (GObject * object)
{
    ScalixContainer *container;
    ScalixContainerPrivate *priv;

    container = SCALIX_CONTAINER (object);
    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_thread_join (priv->synch_slave);

    GLOG_CAT_DEBUG (&slcon, "Freeing lock");
    g_mutex_free (priv->state_lock);
    priv->state_lock = NULL;
    GLOG_CAT_DEBUG (&slcon, "done");

    if (G_OBJECT_CLASS (parent_class)->finalize)
        (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
scalix_container_class_init (ScalixContainerClass * klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);

    g_type_class_add_private (klass, sizeof (ScalixContainerPrivate));

    gobject_class->constructor = scalix_container_constructor;
    gobject_class->dispose = scalix_container_dispose;
    gobject_class->finalize = scalix_container_finalize;
    gobject_class->set_property = scalix_container_set_property;
    gobject_class->get_property = scalix_container_get_property;

    /* Properties */
    g_object_class_install_property (gobject_class,
                                     PROP_URI,
                                     g_param_spec_string ("uri",
                                                          "Uri of the Container",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE |
                                                          G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (gobject_class,
                                     PROP_ONLINE,
                                     g_param_spec_boolean ("online",
                                                           "Container is online?",
                                                           "",
                                                           FALSE,
                                                           G_PARAM_READABLE |
                                                           G_PARAM_WRITABLE |
                                                           G_PARAM_CONSTRUCT));

    g_object_class_install_property (gobject_class,
                                     PROP_PATH,
                                     g_param_spec_string ("path",
                                                          "Path to the Container",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_USER,
                                     g_param_spec_string ("username",
                                                          "Username to use",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_PASS,
                                     g_param_spec_string ("password",
                                                          "Password to use",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_OWNER_EMAIL,
                                     g_param_spec_string ("owner-email",
                                                          "Email Address ",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_TYPE,
                                     g_param_spec_string ("type",
                                                          "container type",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_CREATE,
                                     g_param_spec_boolean ("only_if_exists",
                                                           "only open if exists",
                                                           "",
                                                           TRUE,
                                                           G_PARAM_READABLE |
                                                           G_PARAM_WRITABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_STATE,
                                     g_param_spec_int ("state",
                                                       "container state",
                                                       "",
                                                       0,
                                                       CONTAINER_STATE_ERROR,
                                                       CONTAINER_STATE_ERROR,
                                                       G_PARAM_READABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_ITEMS,
                                     g_param_spec_uint ("items",
                                                        "item count",
                                                        "",
                                                        0,
                                                        G_MAXUINT32,
                                                        0, G_PARAM_READABLE));

    /* Signals */

    container_signals[CONTAINER_OBJECT_ADDED_SIGNAL] =
        g_signal_new ("object_added",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_CLEANUP,
                      G_STRUCT_OFFSET (ScalixContainerClass, object_added),
                      NULL,
                      NULL,
                      scalix_g_cclosure_marshal_VOID__OBJECT,
                      G_TYPE_NONE, 1, G_TYPE_OBJECT);

    container_signals[CONTAINER_OBJECT_REMOVED_SIGNAL] =
        g_signal_new ("object_removed",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_CLEANUP,
                      G_STRUCT_OFFSET (ScalixContainerClass, object_removed),
                      NULL,
                      NULL,
                      scalix_g_cclosure_marshal_VOID__OBJECT,
                      G_TYPE_NONE, 1, G_TYPE_OBJECT);

    container_signals[CONTAINER_OBJECT_CHANGED_SIGNAL] =
        g_signal_new ("object_changed",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_CLEANUP,
                      G_STRUCT_OFFSET (ScalixContainerClass, object_changed),
                      NULL,
                      NULL,
                      scalix_g_cclosure_marshal_VOID__OBJECT_OBJECT,
                      G_TYPE_NONE, 2, G_TYPE_OBJECT, G_TYPE_OBJECT);

    /* klass->changed = container_changed; */
}

static void
scalix_container_init (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;
    GThread *slave;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    priv->state = CONTAINER_STATE_OFFLINE;
    priv->dispose_has_run = FALSE;
    priv->state_lock = g_mutex_new ();
    priv->synch_cond = g_cond_new ();
    priv->synch_lock = g_mutex_new ();
    priv->in_sync = FALSE;

    priv->slave_cmd = SLAVE_SHOULD_SLEEP;
    priv->refresh_time.tv_usec = 0;
    priv->refresh_time.tv_sec = DEFAULT_REFRESH_TIME;

    slave = g_thread_create (synch_slave_loop, container, TRUE, NULL);

    if (slave == NULL) {
        priv->state = CONTAINER_STATE_ERROR;
    }

    priv->synch_slave = slave;
}

char *
scalix_container_get_local_attachment_store (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    return priv->local_attachment_store;
}

static void
scalix_container_set_property (GObject * object,
                               guint prop_id,
                               const GValue * value, GParamSpec * pspec)
{
    ScalixContainerPrivate *priv;
    const char *text_uri;
    const char *type;
    CamelURL *url, *url2;
    char *uri;
    gboolean online;

    priv = SCALIX_CONTAINER_GET_PRIVATE (SCALIX_CONTAINER (object));

    switch (prop_id) {

        /* Property is read only, so no need to check
           if priv->url is already set */
    case PROP_URI:
        text_uri = g_value_get_string (value);
	url = camel_url_new (text_uri, NULL);

	/* work-around buggy _to_string not omiting the auth part */
	url2 = camel_url_copy (url);
	camel_url_set_authmech (url2, NULL);
	uri = camel_url_to_string (url2, CAMEL_URL_HIDE_ALL);
	GLOG_CAT_DEBUG (&slcon, "Container-URI: %s", uri);
	camel_url_free (url2);

	priv->cache_dir = path_from_uri (uri);
        priv->url = url;

        /* set the local attachment store */
        priv->local_attachment_store = g_build_path (G_DIR_SEPARATOR_S,
                                                     priv->cache_dir, "attach",
						     NULL);
	g_free (uri);
        break;

    case PROP_ONLINE:
        online = g_value_get_boolean (value);
        scalix_container_set_online (SCALIX_CONTAINER (object), online);
        break;

    case PROP_USER:
        camel_url_set_user (priv->url, g_value_get_string (value));
        break;

    case PROP_PASS:
        camel_url_set_passwd (priv->url, g_value_get_string (value));
        break;

    case PROP_TYPE:
        type = g_value_get_string (value);

        if (g_str_equal (type, "Calendar")) {

            priv->type = CAMEL_SCALIX_FOLDER_CALENDAR;

        } else if (g_str_equal (type, "Contacts")) {

            priv->type = CAMEL_SCALIX_FOLDER_CONTACT;

        } else {

            priv->type = 0;
        }

        break;

    case PROP_CREATE:
        priv->create = !g_value_get_boolean (value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }

}

static void
scalix_container_get_property (GObject * object,
                               guint prop_id,
                               GValue * value, GParamSpec * pspec)
{
    ScalixContainerPrivate *priv;
    char *text_uri;
    char *str;
    char *path;
    gboolean online;
    guint items;

    priv = SCALIX_CONTAINER_GET_PRIVATE (SCALIX_CONTAINER (object));

    switch (prop_id) {

    case PROP_URI:
        if (priv->url == NULL) {
            g_value_set_static_string (value, "Inavild URL");
            return;
        }

        text_uri = camel_url_to_string (priv->url, CAMEL_URL_HIDE_PASSWORD);
        g_value_take_string (value, text_uri);
        break;

    case PROP_PATH:
        path = priv->url->path;

        if (path == NULL || path[0] == '\0'
            || ((strlen (path) == 1) && path[0] == '/')) {
            g_value_set_string (value, NULL);
            break;
        }

        /* Skip leading / */
        if (path[0] == '/') {
            path++;
        }

        if (path[strlen (path) - 1] == '/') {
            path = g_strndup (path, strlen (path) - 1);
        }

        g_value_set_string (value, path);
        break;

    case PROP_ONLINE:
        online = store_is_online (priv->store);
        g_value_set_boolean (value, online);
        break;

    case PROP_USER:
        g_value_set_string (value, priv->url->user);
        break;

    case PROP_PASS:
        g_value_set_string (value, priv->url->passwd);
        break;

    case PROP_OWNER_EMAIL:

        camel_object_get (priv->store, NULL,
                          CAMEL_SCALIX_STORE_USER_EMAIL, &str, NULL);

        g_value_set_string (value, str);
        break;

    case PROP_ITEMS:
        online = store_is_online (priv->store);

        if (priv->folder == NULL) {
            g_value_set_uint (value, 0);
            break;
        }

        if (online == TRUE) {
            CamelException ex;

            camel_exception_init (&ex);
            camel_folder_refresh_info (priv->folder, &ex);
            /* we dont care to much about that result,
             * should we? */
        }

        camel_object_get (priv->folder, NULL,
                          CAMEL_FOLDER_ARG_VISIBLE, &items, NULL);

        g_value_set_uint (value, items);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);

    }
}

static GObject *
scalix_container_constructor (GType type,
                              guint n_cprops, GObjectConstructParam * cprops)
{
    ScalixContainer *container;
    ScalixContainerPrivate *priv;
    CamelException ex;
    CamelSession *session;
    CamelService *service;
    GObject *object;
    char *text_uri;

    /* call parent constructor first */
    object = parent_class->constructor (type, n_cprops, cprops);

    container = SCALIX_CONTAINER (object);
    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    text_uri = camel_url_to_string (priv->url, 0);

    /* Open the cache here */
    priv->cache = scalix_object_cache_open (priv->cache_dir);

    if (priv->cache == NULL) {
        g_critical ("Could not open cache! PANIC!\n");
        priv->state = CONTAINER_STATE_ERROR;
    }

    /* Create the local attachment store direcotry */
    g_mkdir_with_parents (priv->local_attachment_store, 0700);

    /* Get the camel store */
    camel_exception_init (&ex);
    session = scalix_camel_session_get_default ();

    GLOG_CAT_DEBUG (&slcon, "Trying to get service for (%s) [session: %p]",
                    text_uri, session);

    service = camel_session_get_service (session,
                                         text_uri, CAMEL_PROVIDER_STORE, &ex);

    if (service == NULL || camel_exception_is_set (&ex)) {
        set_error_from_cex (container, &ex);
        GLOG_CAT_ERROR (&slcon, "Error during get_service");
        priv->state = CONTAINER_STATE_ERROR;
    }

    priv->store = CAMEL_STORE (service);

    g_free (text_uri);
    return object;
}

/* ************************************************************************** */

/* Container itself retalted functions */

static void
set_error_from_cex (ScalixContainer * container, CamelException * ex)
{
    ScalixContainerPrivate *priv;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    priv->state = CONTAINER_STATE_ERROR;
    g_object_notify (G_OBJECT (container), "state");

    GLOG_CAT_ERROR (&slcon, "!!!!! CAMEL ERROR !!! Error (%s)",
                    camel_exception_get_description (ex));
}

static gboolean
go_online (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;
    CamelFolder *folder;
    CamelException ex;
    char *path;
    guint32 flags;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    if (priv->state == CONTAINER_STATE_ONLINE) {
        return TRUE;
    }

    if (priv->state != CONTAINER_STATE_OFFLINE) {
        /* that means we are in error state */
        return FALSE;
    }

    camel_exception_init (&ex);

    GLOG_CAT_DEBUG (&slcon, "Trying to connect to server %p", priv->store);

    camel_service_connect (CAMEL_SERVICE (priv->store), &ex);

    GLOG_CAT_DEBUG (&slcon, "Connected");

    if (camel_exception_is_set (&ex)) {
        set_error_from_cex (container, &ex);
        GLOG_CAT_ERROR (&slcon, "Error during connect\n");
        return FALSE;
    }

    if (priv->folder != NULL) {
        /* do a refresh_folder_info ? */
        slave_wakeup (container);

        priv->state = CONTAINER_STATE_ONLINE;
        g_object_notify (G_OBJECT (container), "state");
        return TRUE;
    }

    priv->cfolder = camel_store_get_folder (priv->store,
                                            "#ScalixControl", 0, &ex);

    g_object_get (container, "path", &path, NULL);

    if (path == NULL) {
        priv->state = CONTAINER_STATE_ERROR;
        g_object_notify (G_OBJECT (container), "state");
        return FALSE;
    }

    if (priv->create == TRUE) {
        flags = CAMEL_STORE_FOLDER_CREATE | priv->type;
    } else {
        flags = 0;
    }

    folder = camel_store_get_folder (priv->store, path, flags, &ex);

    g_free (path);

    if (camel_exception_is_set (&ex)) {
        set_error_from_cex (container, &ex);
        return FALSE;
    }

    priv->folder = folder;

    slave_wakeup (container);

    priv->state = CONTAINER_STATE_ONLINE;
    g_object_notify (G_OBJECT (container), "state");
    return TRUE;
}

static gboolean
go_offline (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;
    CamelException ex;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    slave_hibernate (container);

    camel_exception_init (&ex);

    camel_offline_store_set_network_state (CAMEL_OFFLINE_STORE (priv->store),
                                           CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL,
                                           &ex);

    if (camel_exception_is_set (&ex)) {
        return FALSE;
    }

    camel_service_disconnect (CAMEL_SERVICE (priv->store), TRUE, &ex);

    if (camel_exception_is_set (&ex)) {
        return FALSE;
    }

    priv->state = CONTAINER_STATE_OFFLINE;
    g_object_notify (G_OBJECT (container), "state");

    return TRUE;
}

/* ************************************************************************** */

/* utility functions */

static void
message_set_from (ScalixContainer * container, CamelMimeMessage * message)
{
    ScalixContainerPrivate *priv;

    if (camel_mime_message_get_from (message)) {
        return;
    }

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    if (priv->user_email == NULL) {
        char *user_name;
        char *email_str;

        user_name = email_str = NULL;

        camel_object_get (priv->store, NULL,
                          CAMEL_SCALIX_STORE_USER_NAME, &user_name,
                          CAMEL_SCALIX_STORE_USER_EMAIL, &email_str, NULL);

        priv->user_email = camel_internet_address_new ();

        g_assert (user_name && email_str);

        camel_internet_address_add (priv->user_email, user_name, email_str);
    }

    camel_object_ref (priv->user_email);
    camel_mime_message_set_from (message, priv->user_email);
}

/* ************************************************************************** */

/* direct server functions (no locking done here! Protect with external lock) */

static ScalixObject *
server_get_object (ScalixContainer * container, int iuid)
{
    ScalixContainerPrivate *priv;
    CamelException ex;
    CamelMimeMessage *message;
    ScalixObject *object;
    char iuid_str[1024];

    camel_exception_init (&ex);
    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    GLOG_CAT_DEBUG (&slcon, "Getting object from server (iuid: %d)", iuid);

    g_snprintf (iuid_str, sizeof (iuid_str), "%d", iuid);

    object = NULL;
    message = camel_folder_get_message (priv->folder, iuid_str, &ex);

    if (message == NULL || camel_exception_is_set (&ex)) {
        //scalix_debug (SCALIX_CONTAINER, "Could not get message %s", id);      
        goto out;
    }

    object = scalix_object_new_from_message (message,
                                             priv->local_attachment_store);

    camel_object_unref (message);

  out:
    return object;
}

static gboolean
server_remove_id (ScalixContainer * container, int iuid)
{
    ScalixContainerPrivate *priv;
    CamelException ex;
    gboolean res;
    char iuid_str[1024];

    res = FALSE;

    if (iuid == 0) {
        GLOG_CAT_WARNING (&slcon, "Tyring to remove iuid 0 message");
        /* FIXME: return TRUE here? */
        return FALSE;
    }

    g_snprintf (iuid_str, sizeof (iuid_str), "%d", iuid);

    camel_exception_init (&ex);
    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    GLOG_CAT_DEBUG (&slcon, "iuid to be removed %d %s", iuid, iuid_str);
    res = camel_folder_delete_message (priv->folder, iuid_str);

    if (res && !camel_exception_is_set (&ex)) {
        res = TRUE;
    } else {
        res = FALSE;
    }

    return res;
}

static int
server_put_object (ScalixContainer * container,
                   ScalixObject * object, int iuid, gboolean use_body_store)
{
    ScalixContainerPrivate *priv;
    CamelException ex;
    CamelMimeMessage *message;
    CamelFolderSummary *summary;
    gboolean res;
    CamelMessageInfo *mi;
    char *imap_uid;
    char iuid_str[1024];

    GLOG_CAT_DEBUG (&slsyn, "server_put_object iuid: %d", iuid);

    res = TRUE;

    camel_exception_init (&ex);
    priv = SCALIX_CONTAINER_GET_PRIVATE (container);
    summary = priv->folder->summary;

    message = scalix_object_to_mime_message (object);

    if (message == NULL) {
        return -1;
    }

    mi = camel_message_info_new (NULL);
    camel_message_info_set_flags (mi, CAMEL_MESSAGE_SEEN, ~0);

    if (!camel_mime_message_get_from (message)) {
        message_set_from (container, message);
    }

    if (iuid) {
        g_snprintf (iuid_str, sizeof (iuid_str), "%d", iuid);
    } else {
        use_body_store = FALSE;
    }

    /* For body store command */
    if (use_body_store) {
        GLOG_CAT_DEBUG (&slsyn, "Using body store (iuid: %s)", iuid_str);
        camel_medium_add_header (CAMEL_MEDIUM (message),
                                 "X-SCALIX-STORE-UID", iuid_str);
    }

    camel_folder_append_message (priv->folder, message, mi, &imap_uid, &ex);

    if (!camel_exception_is_set (&ex)) {

        if (iuid && !use_body_store) {
            GLOG_CAT_DEBUG (&slsyn, "!body_store, deleting :%s", iuid_str);
            camel_folder_delete_message (priv->folder, iuid_str);
        }

        iuid = strtol (imap_uid, (char **) NULL, 10);

    } else {
        iuid = -1;
        g_free (imap_uid);
    }

    camel_message_info_free (mi);
    camel_object_unref (message);

    return iuid;
}

/* ************************************************************************** */

/* Container synchronazition && refreshes */

/*
 * Sync offline changes to the server and vice versa
 * 
 * There are 11 cases: 
 * 
 *    UC = unchanged, M = modified, D = deleted
 * 
 *        \ server
 *         \    UC  |  M  |  D  |
 * local      +---------------------
 *        UC  |  1  |  2  |  3  |
 *       -----|---------------------
 *         M  |  4  |  5  |  6  |
 *       -----|---------------------
 *         D  |  7  |  8  |  9  |
 *            |     |     |     |
 * 
 *		 10 = object new on server
 *       11 = object new on client
 * */

static gboolean
container_sync_internal (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;
    ScalixObject *object, *old_object;
    GPtrArray *ids;
    GHashTable *iuids;
    EIterator *iter;
    char *ouid;
    int iuid_max_cache;
    int *iuid_store = NULL;
    int *ip;
    int i, iuid;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    GLOG_CAT_DEBUG (&slsyn, "Synching...");

    ids = camel_folder_get_uids (priv->folder);

    g_object_get (priv->cache, "iuid-max", &iuid_max_cache, NULL);

    /* Handle [1], [2], [5], [8], [10] */
    for (i = (ids->len - 1); i > -1; i--) {
        int flags;
        ScalixOCEntry *entry;

        iuid = strtol (ids->pdata[i], (char **) NULL, 10);
        GLOG_CAT_DEBUG (&slsyn, "check (s -> c): %d", iuid);
        /* [1] cought here */
        if (iuid <= iuid_max_cache) {
            GLOG_CAT_DEBUG (&slsyn, "s -> c imap uids < %d", iuid_max_cache);
            break;
        }

        object = server_get_object (container, iuid);

        if (G_UNLIKELY (object == NULL)) {
            continue;
        }

        g_object_get (object, "uid", &ouid, NULL);
        GLOG_CAT_DEBUG (&slsyn, "new: uid %s", ouid);

        if (G_UNLIKELY (ouid == NULL)) {
            GLOG_CAT_ERROR (&slsyn, "Object does not have a UID, skipping");
            g_object_unref (object);
            continue;
        }

        entry = scalix_object_cache_get_entry (priv->cache, ouid, TRUE);

        old_object = NULL;
        /* entry == NULL => [10] (put in cache then) */
        if (entry != NULL) {

            g_object_get (entry, "flags", &flags, "object", &old_object, NULL);

            if (flags == SC_OBJECT_REMOVED) {
                /* Conflict [8] */
                /* Fixme: compare last modified */
            } else if (flags == SC_OBJECT_MODIFIED) {
                /* Conflict [5] */

                /* Fixme: compare last modified */
            }
            /* else flags == 0 => [2] */
            g_object_unref (entry);
        }

        if (old_object != NULL) {
            g_signal_emit (container,
                           container_signals[CONTAINER_OBJECT_CHANGED_SIGNAL],
                           0, old_object, object);

            g_object_unref (old_object);
        } else {
            g_signal_emit (container,
                           container_signals[CONTAINER_OBJECT_ADDED_SIGNAL],
                           0, object);
        }

        scalix_object_cache_put (priv->cache, object, iuid);
        g_object_unref (object);
    }

    ip = NULL;
    if (i > -1) {
        iuid_store = ip = g_new0 (int, i + 1);
    }

    iuids = g_hash_table_new (g_int_hash, g_int_equal);

    while (i > -1) {
        *ip = strtol (ids->pdata[i], (char **) NULL, 10);
        g_hash_table_insert (iuids, ip, iuid_store);
        ip++;
        i--;
    }

    GLOG_CAT_DEBUG (&slsyn, "checking local cache now");

    iter = scalix_object_cache_get_iterator (priv->cache);

    /* Handle [3], [4], [6], [7], [9], [11] */

    while (e_iterator_is_valid (iter)) {
        ScalixOCEntry *entry;
        int flags;

        entry = SCALIX_OC_ENTRY (e_iterator_get (iter));

        iuid = flags = 0;
        g_object_get (entry, "flags", &flags, "imap-uid", &iuid, NULL);

        if (iuid == 0) {
            /* [11] */
            object = NULL;
            g_object_get (entry, "object", &object, NULL);

            if (object != NULL) {
                iuid = server_put_object (container, object, 0, FALSE);
                /* update the iuid */
                scalix_object_cache_put (priv->cache, object, iuid);
            }

            GLOG_CAT_DEBUG (&slsyn, "%d [11]", iuid);

            g_object_unref (entry);
            e_iterator_next (iter);
            continue;

        } else if (iuid > iuid_max_cache) {
            /* ([2], [5], [8]) */
            /* nothing to do here */
            GLOG_CAT_DEBUG (&slsyn, "%d [2],[5],[8]", iuid);
            g_object_unref (entry);
            e_iterator_next (iter);
            continue;
        }

        ip = g_hash_table_lookup (iuids, &iuid);

        ouid = NULL;
        g_object_get (entry, "object-uid", &ouid, NULL);

        ///////////////////////////////////////////////
        switch (flags & SC_OBJECT_STATE_MASK) {

            /* [3], ([1]) lookup ! */
        case 0:
            GLOG_CAT_DEBUG (&slsyn, "%s [3] or [1]", ouid);
            /* [3] */
            if (ip == NULL) {
                object = NULL;
                g_object_get (entry, "object", &object, NULL);
                g_signal_emit (container,
                               container_signals
                               [CONTAINER_OBJECT_REMOVED_SIGNAL], 0, object);

                scalix_object_cache_remove_entry (priv->cache, ouid);
            }
            break;

            /* [4], [6] lookup ! */
        case SC_OBJECT_MODIFIED:
            /* [6] */
            if (ip == NULL) {
                GLOG_CAT_DEBUG (&slsyn, "%s [4]", ouid);
                object = NULL;
                g_object_get (entry, "object", &object, NULL);
                g_signal_emit (container,
                               container_signals
                               [CONTAINER_OBJECT_REMOVED_SIGNAL], 0, object);
                scalix_object_cache_remove_entry (priv->cache, ouid);

            } else {
                /* [4] */
                GLOG_CAT_DEBUG (&slsyn, "%s [6]", ouid);
                object = NULL;
                g_object_get (entry, "object", &object, NULL);

                if (object != NULL) {
                    gboolean use_bstore = (flags & SC_OBJECT_BSOTRE);

                    iuid =
                        server_put_object (container, object, iuid, use_bstore);

                    /* update the iuid */
                    scalix_object_cache_put (priv->cache, object, iuid);
                }
            }
            break;

            /* [7], [9] lookup ! */
        case SC_OBJECT_REMOVED:
            GLOG_CAT_DEBUG (&slsyn, "%s [7] or [9]", ouid);
            if (ip != NULL) {
                server_remove_id (container, iuid);
            }

            scalix_object_cache_remove_entry (priv->cache, ouid);
            break;

        default:
            g_assert_not_reached ();
        }

        g_object_unref (entry);
        g_free (ouid);
        e_iterator_next (iter);
    }

    g_object_unref (iter);

    priv->in_sync = TRUE;
    GLOG_CAT_DEBUG (&slsyn, "done");
    return TRUE;
}

static gboolean
container_refresh_internal (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;
    CamelException ex;
    int need_synch;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    if (priv->state != CONTAINER_STATE_ONLINE || priv->folder == NULL) {
        return FALSE;
    }

    g_mutex_lock (priv->state_lock);

    if (priv->in_sync == FALSE) {
        g_mutex_unlock (priv->state_lock);
        return TRUE;
    }

    GLOG_CAT_INFO (&slcon, "Checking if synch is needed");

    camel_exception_init (&ex);
    camel_folder_refresh_info (priv->folder, &ex);

    if (camel_exception_is_set (&ex)) {
        GLOG_CAT_WARNING (&slcon, "Could not refresh folder");
        return FALSE;
    }

    /* FIXME: check for errors here */
    need_synch = 0;
    camel_object_get (priv->folder, &ex,
                      CAMEL_SCALIX_FOLDER_ARG_NEED_SYNCH, &need_synch, NULL);

    if (need_synch == TRUE) {
        GLOG_CAT_INFO (&slcon, "Synch needed ...");
        container_sync_internal (container);
    }

    g_mutex_unlock (priv->state_lock);

    return TRUE;
}

static gpointer
synch_slave_loop (gpointer data)
{
    ScalixContainer *container;
    ScalixContainerPrivate *priv;

    container = SCALIX_CONTAINER (data);
    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->synch_lock);
    GLOG_CAT_DEBUG (&slslv, "Starting Synch-Slave loop");

    while (G_UNLIKELY (priv->slave_cmd != SLAVE_SHOULD_DIE)) {
        GTimeVal alarm_cstate_lock;

        if (priv->slave_cmd == SLAVE_SHOULD_SLEEP) {
            /* just sleep until we get woken up again */
            GLOG_CAT_DEBUG (&slslv, "Going to hibernate ...");
            g_cond_wait (priv->synch_cond, priv->synch_lock);
            GLOG_CAT_DEBUG (&slslv, "Waking up ...");
            /* check if we should die, work or sleep again */
            continue;
        }

        /* Ok here we go, do some real work 
         * Synch it baby one more time ...
         */
        GLOG_CAT_INFO (&slslv, "Going to work ...");
        container_refresh_internal (container);
        GLOG_CAT_INFO (&slslv, "Work done.");

        /* puhh that was hard, get some rest :) */
        g_get_current_time (&alarm_cstate_lock);

        GLOG_CAT_DEBUG (&slslv, "Need some rest. Sleeping until %d",
                        alarm_cstate_lock.tv_sec);

        alarm_cstate_lock.tv_sec += priv->refresh_time.tv_sec;
        g_cond_timed_wait (priv->synch_cond,
                           priv->synch_lock, &alarm_cstate_lock);

    }

    GLOG_CAT_DEBUG (&slslv, "No they killed Kenny, you bastard!");
    /* we got killed ... */
    g_mutex_unlock (priv->synch_lock);
    g_mutex_free (priv->synch_lock);
    return NULL;
}

static void
slave_hibernate (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->synch_lock);
    priv->slave_cmd = SLAVE_SHOULD_SLEEP;
    g_mutex_unlock (priv->synch_lock);

}

static void
slave_wakeup (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->synch_lock);
    priv->slave_cmd = SLAVE_SHOULD_WORK;
    g_cond_signal (priv->synch_cond);
    g_mutex_unlock (priv->synch_lock);
}

static void
slave_die (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->synch_lock);
    priv->slave_cmd = SLAVE_SHOULD_DIE;
    g_cond_signal (priv->synch_cond);
    g_mutex_unlock (priv->synch_lock);
}

/* ************************************************************************** */

/* Public interface */

/* ************************************************************************** */

ScalixContainer *
scalix_container_open (const char *uri)
{
    ScalixContainer *container;

    container = g_object_new (SCALIX_TYPE_CONTAINER, "uri", uri, NULL);

    return container;
}

gboolean
scalix_container_remove (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;
    CamelException ex;
    gboolean res = TRUE;

    g_return_val_if_fail (container != NULL, FALSE);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->state_lock);

    if (priv->state == CONTAINER_STATE_ERROR) {
        g_mutex_unlock (priv->state_lock);
        return FALSE;
    }

    camel_exception_init (&ex);
    camel_store_delete_folder (priv->store,
                               camel_folder_get_full_name (priv->folder), &ex);

    if (camel_exception_is_set (&ex)) {
        set_error_from_cex (container, &ex);
        res = FALSE;
    } else {
        /* remove local data */
        res = scalix_object_cache_truncate (priv->cache);
    }

    g_mutex_unlock (priv->state_lock);
    return res;
}

gboolean
scalix_container_set_online (ScalixContainer * container, gboolean online)
{
    ScalixContainerPrivate *priv;
    gboolean res;

    g_return_val_if_fail (container != NULL, FALSE);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);
    res = TRUE;

    g_mutex_lock (priv->state_lock);

    if (priv->state == CONTAINER_STATE_ERROR) {
        res = FALSE;
        goto out;
    }

    if (online == TRUE) {
        if (priv->state == CONTAINER_STATE_ONLINE) {
            goto out;
        }

        res = go_online (container);

    } else {
        if (priv->state == CONTAINER_STATE_OFFLINE) {
            goto out;
        }

        res = go_offline (container);
    }

  out:
    g_mutex_unlock (priv->state_lock);
    return res;
}

gboolean
scalix_container_sync (ScalixContainer * container)
{
    ScalixContainerPrivate *priv;
    gboolean res;

    g_return_val_if_fail (container != NULL, FALSE);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->state_lock);

    if (priv->state != CONTAINER_STATE_ONLINE) {
        g_mutex_unlock (priv->state_lock);
        /* FIXME should we return TRUE here? */
        return FALSE;
    }

    res = container_sync_internal (container);

    g_mutex_unlock (priv->state_lock);
    return res;
}

/* ************************************************************************** */

ScalixObject *
scalix_container_get_object (ScalixContainer * container, const char *id)
{
    ScalixContainerPrivate *priv;
    ScalixObject *object;

    g_return_val_if_fail (container != NULL, NULL);
    g_return_val_if_fail (id != NULL, NULL);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    if (priv->state == CONTAINER_STATE_ERROR) {
        return NULL;
    }

    object = scalix_object_cache_get (priv->cache, id);

    return object;
}

ScalixObject *
scalix_container_refresh_object (ScalixContainer * container, const char *id)
{
    ScalixContainerPrivate *priv;
    ScalixOCEntry *entry;
    ScalixObject *object;
    gboolean res;
    int iuid;

    g_return_val_if_fail (container != NULL, NULL);
    g_return_val_if_fail (id != NULL, NULL);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->state_lock);

    if (priv->state == CONTAINER_STATE_ERROR) {
        g_mutex_unlock (priv->state_lock);
        return NULL;
    }

    GLOG_CAT_DEBUG (&slcon, "scalix_container_refresh_object uid: %s", id);
    entry = scalix_object_cache_get_entry (priv->cache, id, TRUE);

    if (entry == NULL) {
        g_mutex_unlock (priv->state_lock);
        return NULL;
    }

    if (priv->state == CONTAINER_STATE_OFFLINE) {
        object = NULL;
        g_object_get (entry, "object", &object, NULL);
        g_object_unref (entry);
        g_mutex_unlock (priv->state_lock);
        return object;
    }

    g_object_get (entry, "imap-uid", &iuid, NULL);
    g_object_unref (entry);

    object = server_get_object (container, iuid);

    scalix_object_cache_remove_entry (priv->cache, id);

    if (object == NULL) {
        g_mutex_unlock (priv->state_lock);
        return NULL;
    }

    res = scalix_object_cache_put (priv->cache, object, iuid);

    if (res == FALSE) {
        g_object_unref (object);
        object = NULL;
    }

    g_mutex_unlock (priv->state_lock);
    return object;
}

gboolean
scalix_container_add_object (ScalixContainer * container, ScalixObject * object)
{
    ScalixContainerPrivate *priv;
    int iuid;
    gboolean res;

    g_return_val_if_fail (container != NULL, FALSE);
    g_return_val_if_fail (object != NULL, FALSE);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    res = TRUE;
    iuid = 0;

    g_mutex_lock (priv->state_lock);

    if (priv->state == CONTAINER_STATE_ERROR) {
        g_mutex_unlock (priv->state_lock);
        return FALSE;
    }

    if (priv->state == CONTAINER_STATE_ONLINE) {
        iuid = server_put_object (container, object, 0, FALSE);
        if (iuid == -1) {
            g_mutex_unlock (priv->state_lock);
            return FALSE;
        }
    }

    res = scalix_object_cache_put (priv->cache, object, iuid);
    g_mutex_unlock (priv->state_lock);

    return res;
}

gboolean
scalix_container_update_object (ScalixContainer * container,
                                ScalixObject * object, gboolean use_bstore)
{
    ScalixContainerPrivate *priv;
    ScalixOCEntry *entry;
    int iuid;
    char *ouid;
    gboolean res;

    g_return_val_if_fail (container != NULL, FALSE);
    g_return_val_if_fail (object != NULL, FALSE);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);
    iuid = 0;
    ouid = NULL;

    g_mutex_lock (priv->state_lock);

    if (priv->state == CONTAINER_STATE_ERROR) {
        g_mutex_unlock (priv->state_lock);
        return FALSE;
    }

    g_object_get (object, "uid", &ouid, NULL);
    GLOG_CAT_DEBUG (&slcon, "scalix_container_update_object uid: %s", ouid);
    entry = scalix_object_cache_get_entry (priv->cache, ouid, FALSE);

    if (entry != NULL) {
        g_object_get (entry, "imap-uid", &iuid, NULL);
        g_object_unref (entry);
    }

    if (priv->state == CONTAINER_STATE_ONLINE) {

        iuid = server_put_object (container, object, iuid, use_bstore);

        if (iuid == -1) {
            g_mutex_unlock (priv->state_lock);
            return FALSE;
        }
    }

    res = scalix_object_cache_put (priv->cache, object, iuid);

    if (priv->state == CONTAINER_STATE_OFFLINE) {
        int flags = SC_OBJECT_MODIFIED;

        if (use_bstore) {
            flags |= SC_OBJECT_BSOTRE;
        }

        scalix_object_cache_set_flags (priv->cache, ouid, flags);
    }

    g_mutex_unlock (priv->state_lock);
    g_free (ouid);

    return TRUE;
}

gboolean
scalix_container_remove_id (ScalixContainer * container, const char *ouid)
{
    ScalixContainerPrivate *priv;
    gboolean res;

    g_return_val_if_fail (container != NULL, FALSE);
    g_return_val_if_fail (ouid != NULL, FALSE);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->state_lock);

    if (priv->state == CONTAINER_STATE_ERROR) {
        g_mutex_unlock (priv->state_lock);
        return FALSE;
    }

    if (priv->state == CONTAINER_STATE_ONLINE) {
        ScalixOCEntry *entry;
        int iuid;

        entry = scalix_object_cache_get_entry (priv->cache, ouid, FALSE);

        if (entry != NULL) {
            g_object_get (entry, "imap-uid", &iuid, NULL);
            g_object_unref (entry);
        }

        res = server_remove_id (container, iuid);

        if (res == FALSE) {
            g_mutex_unlock (priv->state_lock);
            return FALSE;
        }

        res = scalix_object_cache_remove_entry (priv->cache, ouid);
    } else {
        res = scalix_object_cache_set_flags (priv->cache,
                                             ouid, SC_OBJECT_REMOVED);
    }

    g_mutex_unlock (priv->state_lock);
    return res;
}

gboolean
scalix_container_remove_object (ScalixContainer * container,
                                ScalixObject * object)
{
    gboolean res;
    char *ouid;

    g_return_val_if_fail (object != NULL, FALSE);

    g_object_get (object, "uid", &ouid, NULL);
    res = scalix_container_remove_id (container, ouid);
    g_free (ouid);

    return res;
}

gboolean
scalix_container_foreach (ScalixContainer * container,
                          GFunc func, gpointer user_data)
{
    EIterator *iter;
    ScalixContainerPrivate *priv;

    g_return_val_if_fail (container != NULL, FALSE);
    g_return_val_if_fail (func != NULL, FALSE);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    if (priv->state == CONTAINER_STATE_ERROR) {
        return FALSE;
    }

    iter = scalix_object_cache_get_iterator (priv->cache);

    GLOG_CAT_DEBUG (&slcon, "foreach start");

    while (e_iterator_is_valid (iter)) {
        ScalixOCEntry *entry;
        ScalixObject *object;

        object = NULL;
        entry = SCALIX_OC_ENTRY (e_iterator_get (iter));

        if (entry == NULL) {
            e_iterator_next (iter);
            continue;
        }

        g_object_get (entry, "object", &object, NULL);

        if (object == NULL) {
            g_object_unref (entry);
            e_iterator_next (iter);
            continue;
        }

        (*func) (object, user_data);

        g_object_unref (object);
        g_object_unref (entry);

        e_iterator_next (iter);
    }

    GLOG_CAT_DEBUG (&slcon, "foreach finished");
    g_object_unref (iter);

    return TRUE;
}

/* Free-busy stuff */
void
scalix_container_set_freebusy (ScalixContainer * container, const char *request)
{
    ScalixContainerPrivate *priv;
    CamelMimeMessage *message;
    CamelMessageInfo *mi;
    CamelException ex;
    char *imap_uid;

    g_return_if_fail (container != NULL);
    g_return_if_fail (request != NULL);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    if (priv->cfolder == NULL || request == NULL) {
        return;
    }

    if (priv->state != CONTAINER_STATE_ONLINE) {
        return;
    }

    camel_exception_init (&ex);

    message = camel_mime_message_new ();
    camel_mime_part_set_content (CAMEL_MIME_PART (message),
                                 request,
                                 strlen (request),
                                 "text/calendar; method=PUBLISH; charset=UTF-8");

    mi = camel_message_info_new (NULL);

    camel_folder_append_message (priv->cfolder, message, mi, &imap_uid, &ex);
}

char *
scalix_container_get_freebusy (ScalixContainer * container, const char *request)
{
    ScalixContainerPrivate *priv;
    CamelMimeMessage *message;
    CamelDataWrapper *content;
    CamelMedium *medium;
    GByteArray *data;
    CamelStream *stream;
    CamelException ex;
    char *out;

    g_return_val_if_fail (container != NULL, NULL);
    g_return_val_if_fail (request != NULL, NULL);

    priv = SCALIX_CONTAINER_GET_PRIVATE (container);

    g_mutex_lock (priv->state_lock);

    if (priv->state != CONTAINER_STATE_ONLINE) {
        g_mutex_unlock (priv->state_lock);
        return NULL;
    }

    camel_exception_init (&ex);

    message = camel_folder_get_message (priv->cfolder, request, &ex);

    if (message == NULL) {
        g_mutex_unlock (priv->state_lock);
        return NULL;
    }

    GLOG_CAT_DEBUG (&slcon, "decoding vb response");
    data = g_byte_array_new ();
    medium = CAMEL_MEDIUM (CAMEL_MIME_PART (message));
    content = camel_medium_get_content_object (medium);
    stream = camel_stream_mem_new_with_byte_array (data);
    camel_data_wrapper_decode_to_stream (content, stream);
    camel_object_unref (message);
    out = (char *) data->data;

    GLOG_CAT_DEBUG (&slcon, "VBResponse: %s", out != NULL ? out : "(null)");

    g_byte_array_free (data, FALSE);

    g_mutex_unlock (priv->state_lock);
    return out;
}
