/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifndef SCALIX_APPOINTMENT_H
#define SCALIX_APPOINTMENT_H

#include <glib-object.h>
#include <libecal/e-cal-component.h>

G_BEGIN_DECLS
#define SCALIX_TYPE_APPOINTMENT             (scalix_appointment_get_type ())
#define SCALIX_APPOINTMENT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_APPOINTMENT, ScalixAppointment))
#define SCALIX_APPOINTMENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_APPOINTMENT, ScalixAppointmentClass))
#define SCALIX_IS_APPOINTMENT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_APPOINTMENT))
#define SCALIX_IS_APPOINTMENT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_APPOINTMENT))
#define SCALIX_APPOINTMENT_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_APPOINTMENT, ScalixAppointmentPrivate))
typedef struct _ScalixAppointment ScalixAppointment;
typedef struct _ScalixAppointmentClass ScalixAppointmentClass;
typedef struct _ScalixAppointmentPrivate ScalixAppointmentPrivate;

struct _ScalixAppointment {
    ECalComponent object;
};

struct _ScalixAppointmentClass {
    ECalComponentClass parent_class;
};

#define X_SCALIX_IMAP_UID             "X-SCALIX-IMAP-UID"
#define X_SCALIX_MSG_ID               "X-SCALIX-MESSAGE-ID"
#define X_SCALIX_ID_HASH              "X-SCALIX-MESSAGE-ID-HASH"
#define X_SCALIX_MEETING_CORRELATE_ID "X-SCALIX-MEETING-CORRELATE-ID"

GType scalix_appointment_get_type (void);

ScalixAppointment *scalix_appointment_new (const char *calobj);

void scalix_appointment_set (ScalixAppointment * comp,
                             const char *key, const char *value);

gboolean scalix_appointment_get (ScalixAppointment * comp,
                                 const char *key, char **value);

void scalix_appointment_unset (ScalixAppointment * comp, const char *key);

G_END_DECLS
#endif /* _SCALIX_APPOINTMENT_H */
