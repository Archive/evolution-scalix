/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifndef SCALIX_OC_ENTRY_H
#define SCALIX_OC_ENTRY_H

#include <glib-object.h>

G_BEGIN_DECLS
#define SCALIX_TYPE_OC_ENTRY          		(scalix_oc_entry_get_type ())
#define SCALIX_OC_ENTRY(obj)              	(G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_OC_ENTRY, ScalixOCEntry))
#define SCALIX_OC_ENTRY_CLASS(klass)      	(G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_OC_ENTRY, ScalixOCEntryClass))
#define SCALIX_IS_OC_ENTRY(obj)           	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_OC_ENTRY))
#define SCALIX_IS_OC_ENTRY_CLASS(klass)   	(G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_OC_ENTRY))
#define SCALIX_OC_ENTRY_GET_PRIVATE(obj) 	(G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_OC_ENTRY, ScalixOCEntryPrivate))
typedef struct _ScalixOCEntry ScalixOCEntry;
typedef struct _ScalixOCEntryClass ScalixOCEntryClass;
typedef struct _ScalixOCEntryPrivate ScalixOCEntryPrivate;

struct _ScalixOCEntry {
    GObject parent;
};

struct _ScalixOCEntryClass {
    GObjectClass parent_class;
};

GType scalix_oc_entry_get_type (void);

G_END_DECLS
#endif /* SCALIX_OC_ENTRY_H */
