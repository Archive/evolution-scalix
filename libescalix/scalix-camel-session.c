/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include <glib/gi18n.h>

#include <gtk/gtk.h>
#include <libedataserverui/e-passwords.h>
#include <camel/camel-url.h>
#include <camel/camel-service.h>
#include "scalix-camel-session.h"

#include <glog/glog.h>

GLOG_CATEGORY (slpass, "pass", GLOG_FORMAT_FG_BLACK, "Password managemnt");

static CamelSession *default_session = NULL;

static char *
create_key (CamelService * service)
{
    CamelURL *url;
    char *key;
    char *user;
    char *authmech;

    url = service->url;

    if (url->protocol == NULL || url->host == NULL || url->user == NULL) {
        return NULL;
    }

    user = camel_url_encode (url->user, ":;@/");

    if (url->authmech != NULL) {
        authmech = camel_url_encode (url->authmech, ":@/");
    } else {
        authmech = NULL;
    }

    if (url->port) {
        key = g_strdup_printf ("%s://%s%s%s@%s:%d/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host, url->port);
    } else {
        key = g_strdup_printf ("%s://%s%s%s@%s/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host);
    }

    g_free (user);
    g_free (authmech);

    return key;
}

static char *
get_password (CamelSession * session,
              CamelService * service,
              const char *domain,
              const char *prompt,
              const char *item, guint32 flags, CamelException * ex)
{
    char *key;
    char *ret;

    GLOG_CAT_DEBUG (&slpass, "Querying password: item: %s, domain: %s",
                    item ? item : "(NULL)", domain ? domain : "(NULL)");

    key = create_key (service);
    ret = e_passwords_get_password ("Mail", key);

    GLOG_CAT_INFO (&slpass, "Pass storage returned: key: %s  pass: %s",
                   key ? key : "(NULL)", ret ? "******" : "(NULL)");
    g_free (key);

    if (ret == NULL || (flags & CAMEL_SESSION_PASSWORD_REPROMPT)) {

        camel_exception_set (ex,
                             CAMEL_EXCEPTION_USER_CANCEL,
                             _("Could not obtain password!\n"));

        return NULL;
    }

    return ret;
}

static void
forget_password (CamelSession * session, CamelService * service,
                 const char *domain, const char *item, CamelException * ex)
{

}

static gboolean
alert_user (CamelSession * session, CamelSessionAlertType type,
            const char *prompt, gboolean cancel)
{

	GLOG_CAT_DEBUG (&slpass, "UserAlert: %d, propt: %s",
					type, prompt ? prompt : "(NULL)");

    return TRUE;
}

static CamelFilterDriver *
get_filter_driver (CamelSession * session, const char *type,
                   CamelException * ex)
{
    return NULL;
}

static void
class_init (ScalixCamelSessionClass * camel_scalix_session_class)
{
    CamelSessionClass *camel_session_class =
        CAMEL_SESSION_CLASS (camel_scalix_session_class);

    camel_session_class->get_password = get_password;
    camel_session_class->forget_password = forget_password;
    camel_session_class->alert_user = alert_user;
    camel_session_class->get_filter_driver = get_filter_driver;

}

CamelType
scalix_camel_session_get_type (void)
{
    static CamelType type = CAMEL_INVALID_TYPE;

    if (type == CAMEL_INVALID_TYPE) {
        type = camel_type_register (camel_session_get_type (),
                                    "ScalixCamelSession",
                                    sizeof (ScalixCamelSession),
                                    sizeof (ScalixCamelSessionClass),
                                    (CamelObjectClassInitFunc) class_init,
                                    NULL, NULL, NULL);
    }

    return type;
}

CamelSession *
scalix_camel_session_new (const char *path)
{
    CamelSession *session;

    session = CAMEL_SESSION (camel_object_new (SCALIX_CAMEL_SESSION_TYPE));

    camel_session_construct (session, path);

    return session;
}

static GStaticMutex scs_mutex = G_STATIC_MUTEX_INIT;

CamelSession *
scalix_camel_session_get_default ()
{

    g_static_mutex_lock (&scs_mutex);

    if (default_session == NULL) {
        char *path;

        path = g_build_filename (g_get_home_dir (),
                                 ".evolution", "scalix", NULL);

        default_session = scalix_camel_session_new (path);

        g_free (path);
    }

    g_static_mutex_unlock (&scs_mutex);

    return default_session;
}

void
scalix_camel_session_set_default (CamelSession * session)
{
    g_static_mutex_lock (&scs_mutex);
    default_session = session;
    g_static_mutex_unlock (&scs_mutex);
}

void
scalix_camel_session_init (const char *path)
{
    /* FIXME noop REMOVE ME! */
}

/*  ex: set ts=4:  */
