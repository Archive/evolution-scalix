/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include "scalix.h"
#include "scalix-version.h"
#include "scalix-camel-session.h"

#include <camel/camel.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <glog/glog.h>

static gpointer
_libescalix_init (gpointer data)
{
    char *path;
    gboolean init_camel;

    init_camel = GPOINTER_TO_UINT (data);

    /* do we need this? */
    if (!g_thread_supported ()) {
        g_thread_init (NULL);
    }

    if (init_camel) {
        path = g_build_filename (g_get_home_dir (),
                                 ".evolution", "scalix", NULL);

        if (camel_init (path, TRUE)) {
            return GUINT_TO_POINTER (FALSE);
        }

        g_free (path);

        camel_provider_init ();
    }

    LIBXML_TEST_VERSION;

    glog_init ();

    GLOG_DEBUG ("All set!");
#if EAPI_CHECK_VERSION (2,4)
    GLOG_DEBUG ("Using 2.4 API");
#endif

    return GUINT_TO_POINTER (TRUE);
}

gboolean
libescalix_init (gboolean init_camel)
{
    static GOnce initialize = G_ONCE_INIT;

    g_once (&initialize, _libescalix_init, GUINT_TO_POINTER (init_camel));

    return GPOINTER_TO_UINT (initialize.retval);
}
