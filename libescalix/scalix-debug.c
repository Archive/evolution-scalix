/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include <scalix-debug.h>

#ifdef SCALIX_DEBUG

#include <stdio.h>

static int debug_modules = 0;

#define SCALIX_DEBUG_MODULES_TYPE (scalix_modules_get_type ())
GType scalix_modules_get_type (void);

GType
scalix_modules_get_type (void)
{
    static GType type = 0;

    if (type == 0) {
        static const GFlagsValue values[] = {
            {SCALIX_MAIN, "SCALIX_MAIN", "main"},
            //{ SCALIX_DATA_SERVER,                   "SCALIX_DATA_SERVER",           "data-server" },
            //{ SCALIX_CONNECTION,                    "SCALIX_CONNECTION",            "connection" },
            //{ SCALIX_CONTAINER,                     "SCALIX_CONTAINER",             "container" },
            //{ SCALIX_CALENDAR_BACKEND,  "SCALIX_CALENDAR_BACKEND",      "calendar-backend" },
            //{ SCALIX_CONNECTION,                        "SCALIX_CONNECTION",            "connection" },
            //{ SCALIX_CALENDAR_CACHE,            "SCALIX_CALENDAR_CACHE",        "calendar-cache" },
            //{ SCALIX_ADDRESSBOOK,               "SCALIX_ADDRESSBOOK",           "addressbook" },
            //{ SCALIX_ADDRESSBOOK_IDMAP,         "SCALIX_ADDRESSBOOK_IDMAP", "addressbook-idmap" },
            //{ SCALIX_ITEM,                              "SCALIX_ITEM",                          "item"},
            {SCALIX_OBJECT, "SCALIX_OBJECT", "sobject"},
            {SCALIX_ALL, "SCALIX_ALL", "all"},
            {SCALIX_NONE, "SCALIX_NONE", "none"},
            {0, NULL, NULL}
        };

        type = g_flags_register_static ("ScalixModules", values);
    }

    return type;
}

void
scalix_debug_init (int default_modules)
{
    const char *debug_env;
    char **modules, **iter;
    GFlagsClass *fclass;

    debug_modules |= default_modules;

    if ((debug_env = g_getenv ("SCALIX_DEBUG")) == NULL)
        return;

    modules = g_strsplit (debug_env, ",", -1);

    fclass = G_FLAGS_CLASS (g_type_class_ref (SCALIX_DEBUG_MODULES_TYPE));

    scalix_debug (SCALIX_MAIN, "Activating debuging modules");

    for (iter = modules; *iter; iter++) {
        GFlagsValue *val;

        val = g_flags_get_value_by_nick (fclass, *iter);

        if (val == NULL)
            continue;

        scalix_debug (SCALIX_MAIN, "\t%s ... activated", val->value_nick);
        debug_modules |= val->value;
    }
    scalix_debug (SCALIX_MAIN, "done");
    g_strfreev (modules);

}

/*  0	Reset all attributes
	1	Bright
	2	Dim
	4	Underscore	
	5	Blink
	7	Reverse
	8	Hidden

	Foreground Colors

	30	Black
	31	Red
	32	Green
	33	Yellow
	34	Blue
	35	Magenta
	36	Cyan
	37	White

	Background Colors
	
	40	Black
	41	Red
	42	Green
	43	Yellow
	44	Blue
	45	Magenta
	46	Cyan
	47	White
*/

#define TERM_RESET 		"\E[0m"
#define TERM_DIM 		"\E[2m"

void
_scalix_debug (int module, const char *func, const char *fmt, ...)
{
    va_list args;
    gchar *out;
    GFlagsClass *fclass;
    GFlagsValue *val;

    if (!(module & debug_modules))
        return;

    g_assert (fmt);
    va_start (args, fmt);
    out = g_strdup_vprintf (fmt, args);

    fclass = G_FLAGS_CLASS (g_type_class_ref (SCALIX_DEBUG_MODULES_TYPE));
    val = g_flags_get_first_value (fclass, module);

    g_print (TERM_DIM "[%p]" TERM_RESET " [%-15.15s] (%-20.20s) %s\n",
             g_thread_self (), val ? val->value_nick : "-", func, out);

    g_free (out);
    va_end (args);
}

const char *
scalix_debug_register_module (const char *name, const char *file)
{
    g_print ("register module name: %s file: %s\n", name, file);
    return file;
}

#endif
