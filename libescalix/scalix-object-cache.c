/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>

#ifdef HAVE_DYNAMIC_LIBDB
#include <db.h>
#if DB_VERSION_MAJOR != 4
#error Wrong db version
#endif
#else
#include "db-local.h"
#endif

/* Function signature changed in 2.4 : http://www.bostic.com/docs/bdb_current/ref/upgrade.4.3/err.html */
#if DB_VERSION_MINOR >= 3
#define ERROR_REPORT_3ARGS 1
#endif

#include "scalix-object.h"
#include "scalix-object-cache.h"
#include "scalix-utils.h"
#include "scalix-oc-entry.h"
#include "scalix-version.h"

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-data-server-util.h>
#else
#include <libedataserver/e-util.h>
#endif

#include <string.h>

#define SCKEY_PREFIX "##ScalixCacheInfo"

#include <glog/glog.h>
GLOG_CATEGORY (sloc, "object-cache", GLOG_FORMAT_FG_YELLOW, "Object cache"); 

/* private data */

struct _ScalixObjectCachePrivate {
	char *cache_dir;
	/* Disc storage */
	DB       *db;
	DB       *db_iuid;
	DB_ENV   *db_env;
	int       iuid_max;
	guint32   sequence;
	gboolean  loaded;
};

/*  gobject  */

enum {
	PROP_0,
	PROP_DIR,
	PROP_IUID_MAX,
	PROP_LOADED
};

static GObjectClass *parent_class = NULL;

static void scalix_object_cache_set_property (GObject         *object,
											  guint            prop_id,
											  const GValue    *value,
											  GParamSpec      *pspec);
											  
static void scalix_object_cache_get_property (GObject         *object,
										 	  guint            prop_id,
											  GValue          *value,
											  GParamSpec      *pspec);


G_DEFINE_TYPE (ScalixObjectCache, 
			   scalix_object_cache, 
			   G_TYPE_OBJECT);

static void
scalix_object_cache_finalize (GObject *object)
{
	ScalixObjectCache        *cache;
	ScalixObjectCachePrivate *priv;
	DB                       *db;
	int                       db_res;
	char                     *fp;

	cache = SCALIX_OBJECT_CACHE (object);
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	
	if (priv->db != NULL) {
		db = priv->db;
       
		db_res = db->close (db, 0);
		
		if (db_res != 0) {
			GLOG_CAT_WARNING (&sloc, "Error while closing db (%s)\n", 
							  db_strerror (db_res));	
		}
	}
	
	
	if (priv->db_iuid != NULL) {
		db = priv->db_iuid;
		
		db_res = db->close (db, 0);
		
		if (db_res != 0) {
			GLOG_CAT_WARNING (&sloc, "Error while closing db (%s)\n", 
							  db_strerror (db_res));	
		}
	}
	
	
	if (priv->db_env) {
		priv->db_env->close (priv->db_env, 0);
	}

	fp = g_build_filename (priv->cache_dir, ".delete_me", NULL);
	
	if (g_file_test (fp, G_FILE_TEST_EXISTS)) {
		scalix_recursive_delete (priv->cache_dir);
	}
	
	g_free (fp);
	g_free (priv->cache_dir);
	
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
scalix_object_cache_class_init (ScalixObjectCacheClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);	
	
	g_type_class_add_private (klass, sizeof (ScalixObjectCachePrivate));

	gobject_class->finalize     = scalix_object_cache_finalize;
	gobject_class->set_property = scalix_object_cache_set_property;
	gobject_class->get_property = scalix_object_cache_get_property;
	
	/* properties */
	g_object_class_install_property (gobject_class,
									 PROP_DIR,
						 			 g_param_spec_string ("dir",
								 			  			  "dir to cache items for",
											  			  "",
											  			  NULL,
											  			  G_PARAM_READABLE |
											  			  G_PARAM_WRITABLE |
														  G_PARAM_CONSTRUCT_ONLY));
	
	g_object_class_install_property (gobject_class,
						 PROP_IUID_MAX,
						 g_param_spec_int ("iuid-max",
								 		   "max iuid",
										   "",
										   G_MININT,
										   G_MAXINT,
										   0,
										   G_PARAM_READABLE ));
										   
	
	g_object_class_install_property (gobject_class, 
								 PROP_LOADED,
								 g_param_spec_boolean ("loaded",
										 			   "is the cache loaded",
													   "",
													   FALSE,
													   G_PARAM_READABLE));

	
}

static void
scalix_object_cache_init (ScalixObjectCache *cache)
{
	ScalixObjectCachePrivate *priv;	
	
	priv           = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	priv->sequence = 0;
	priv->loaded   = FALSE;
}


/* db helper functions */

#ifdef ERROR_REPORT_3ARGS
static void
db_error_report (const DB_ENV *db_env, const char *buf1, const char *buf2)
{
	GLOG_CAT_ERROR (&sloc, "libdb error: %s", buf2);
}
#else
static void
db_error_report (const char *buf1, char *buf2)
{
	GLOG_CAT_ERROR (&sloc, "libdb error: %s", buf2);
}
#endif
/* database utilities */

static void
int_to_dbt (DBT *dbt, int *i)
{
	memset (dbt, 0, sizeof (*dbt));
	dbt->data = i;
	dbt->size = sizeof (int);
	dbt->flags = DB_DBT_USERMEM;
}

static void
string_to_dbt (DBT *dbt, const char *str)
{
	memset (dbt, 0, sizeof (*dbt));
	dbt->data = (void *) str;
	dbt->size = strlen (str) + 1;
	dbt->flags = DB_DBT_USERMEM;
}

/* 
 * We store data in the database like this
 * 
 * imap uid (int), type (int), flags (int) , <DATA>
 * 
 */

static int
get_imap_uid (DB *db, const DBT *pkey, const DBT *pdata, DBT *skey)
{
	int iuid;

	if (pdata->size < sizeof (int)) {
		return -1;	
	}
	
	/* Don't index Metadata */
	if (g_str_has_prefix ((char *) pkey->data, SCKEY_PREFIX)) {
		return DB_DONOTINDEX;
	}
	
	iuid = *((int *)((char *) pdata->data));
	
	if (iuid == 0) {
		return DB_DONOTINDEX;	
	}
	
	GLOG_CAT_DEBUG (&sloc, "Extracted %d as iuid index", iuid);
	
	/* Zero out the struct */
	memset (skey, 0, sizeof (DBT));
	
	skey->data = (void *) ((char *) pdata->data);
	skey->size = sizeof (int);
	skey->flags = DB_DBT_USERMEM;
	
	return 0;	
}

static gboolean
decode_dbt_data (DBT *dbt, int *type, int *iuid, int *flags, char **data)
{
	char *buf;
	
	if (dbt->size < ((2 * sizeof (int)))) {
		return FALSE;	
	}
	
	buf = dbt->data;
	
	*iuid = *((int *) buf);
	buf += sizeof (int);
	
	*type = *((int *) buf);
	buf += sizeof (int);
	
	*flags = *((int *) buf);
	buf += sizeof (int);
	
	*data = buf; 	
	
	return TRUE;
}

static gboolean
delete_database (DB_ENV *db_env, const char *path)
{
	GDir  *db_dir;
	const  gchar *name;
	int    ret;
	
	GLOG_CAT_ERROR (&sloc, "Database panic, deleting and retrying!");
	
	db_env->close (db_env, 0);
	db_dir = g_dir_open (path, 0, NULL);
	
	if (db_dir == NULL) {
		GLOG_CAT_ERROR (&sloc, "Could not open directory %s", path);
		return FALSE;
	}
	
	ret = 0;
	
	while ((name = g_dir_read_name (db_dir)) != NULL) {
		gchar *filepath;
		
		/* only kill "*.db" and "log.*" */
		if (g_str_has_suffix (name, ".db") || 
		    g_str_has_prefix (name, "log.")) {
			filepath = g_build_filename (path, name, NULL);
			GLOG_CAT_DEBUG (&sloc, "unlinking %s", filepath);
			ret += g_unlink (filepath);
			g_free (filepath);
		}
	}
	
	g_dir_close (db_dir);
	return ret == 0;
}


static gboolean
open_cache (ScalixObjectCache *cache, const char *path)
{
	ScalixObjectCachePrivate *priv;
	int db_res, open_flags;
	DB *db, *iuid_db;
	DB_ENV *db_env;
	DBT key, data;
	char *main_db_file;
	char *iuid_db_file;
	guint loop;
	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	loop = 0;

	db_res = db_env_create (&db_env, 0);
	
	if (db_res != 0) {
		g_warning ("db_env_create failed with %d", db_res);	
		return FALSE;
	}


retry_open:
		db_res = db_env->open (db_env, path, DB_CREATE | 
											 DB_INIT_MPOOL |
											 DB_INIT_TXN |
		                                     DB_PRIVATE | 
		                                     DB_THREAD |
		                                     DB_RECOVER, 0);
		
		if (loop == 0 && db_res == DB_RUNRECOVERY) {
			
			loop++; /* let's not do that over and over again */
		
			delete_database (db_env, path);
			goto retry_open;
		
		} else if (db_res != 0) {
		
				db_env->close (db_env, 0);
				GLOG_CAT_ERROR (&sloc, "db_env_open failed with %d (%s)", 
								db_res, db_strerror (db_res));
				return FALSE;
	
	}

	db_env->set_errcall (db_env, db_error_report);
	
	/* create database handles */
	db_res = db_create (&db, db_env, 0);
	
	if (db_res != 0) {
		g_warning ("Could not open db handle!\n");
		return FALSE;	
	}
	
	db_res = db_create (&iuid_db, db_env, 0);
	
	if (db_res != 0) {
		g_warning ("Could not open imap index db handle!\n");
		return FALSE;	
	}
	
	/* open the main database */
	main_db_file = g_build_filename (path, "main.db", NULL);
	
	GLOG_CAT_INFO (&sloc, "Main db file location %s", main_db_file);
	
	open_flags = DB_THREAD | DB_CREATE | DB_AUTO_COMMIT;
	
	db_res = db->open (db, NULL, main_db_file, 
						NULL, DB_HASH, open_flags, 0666);
	
	g_free (main_db_file);
						
	if (db_res != 0) {
		g_warning ("Could not open main db file!");
		return FALSE;
	}
	
	/* the imap uid index database */
	iuid_db_file = g_build_filename (path, "imap_uid_index.db", NULL);
	db_res = iuid_db->open (iuid_db, NULL, iuid_db_file, 
						NULL, DB_HASH, open_flags, 0666);

	if (db_res != 0) {
		g_warning ("Could not open db file %s", iuid_db_file);
		return FALSE;
	}

	g_free (iuid_db_file);
	
	GLOG_CAT_DEBUG (&sloc, "Associating secondary database!");
	db->associate (db, NULL, iuid_db, get_imap_uid, DB_AUTO_COMMIT);
	GLOG_CAT_DEBUG (&sloc, "Done!");
	priv->db = db;
	priv->db_iuid = iuid_db;
	priv->db_env = db_env;
	
	/* Read the iuid_max info from the database */
	string_to_dbt (&key, SCKEY_PREFIX "#iuid_max");
	memset (&data, 0, sizeof (DBT));
	data.flags = DB_DBT_MALLOC;
	
	db_res = db->get (db, NULL, &key, &data, 0);
	
	if (db_res == 0 && data.size >= sizeof (int)) {
		
		priv->iuid_max = *((int *) data.data);
		
		GLOG_CAT_INFO (&sloc, "iuid_max: %d", priv->iuid_max);
		
	} else if (db_res == DB_NOTFOUND) {
		priv->iuid_max = 0;
		
	} else {
		priv->iuid_max = 0;
		GLOG_CAT_ERROR (&sloc, "Error while fetching cache info!");	
	}
	
	return TRUE;	
}



static void
scalix_object_cache_set_property (GObject         *object,
								  guint            prop_id,
								  const GValue    *value,
								  GParamSpec      *pspec)
{
	ScalixObjectCache        *cache;
	ScalixObjectCachePrivate *priv;
	int                       result;

	cache = SCALIX_OBJECT_CACHE (object);	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (object);


	switch (prop_id) {
		
		case PROP_DIR:
            		priv->cache_dir = g_value_dup_string (value);
			result  = g_mkdir_with_parents (priv->cache_dir, 0700);
			
			if (result != 0) {
				g_critical ("Could not create folder hierachy\n");
				break;	
			}
			
			GLOG_CAT_DEBUG (&sloc, "Trying to open cache with path %s", 
							priv->cache_dir);
							
			priv->loaded = open_cache (cache, priv->cache_dir);
			
			break;
				
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
	
}

static void
scalix_object_cache_get_property (GObject         *object,
								  guint            prop_id,
								  GValue          *value,
								  GParamSpec      *pspec)
{
	ScalixObjectCache        *cache;
	ScalixObjectCachePrivate *priv;

	cache = SCALIX_OBJECT_CACHE (object);	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (object);


	switch (prop_id) {
		
		case PROP_IUID_MAX:
			g_value_set_int (value, priv->iuid_max);
			break;
		
		case PROP_LOADED:
			g_value_set_boolean (value, priv->loaded);
			break;
		
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

/* public interfaces */

ScalixObjectCache *
scalix_object_cache_open (const char *cache_dir)
{
	ScalixObjectCache *cache;
	gboolean loaded;
	cache = g_object_new (SCALIX_TYPE_OBJECT_CACHE, "dir", cache_dir, NULL);	
	
	loaded = FALSE;
	g_object_get (cache, "loaded", &loaded, NULL);
	
	if (loaded == FALSE) {
		g_object_unref (cache);
		cache = NULL;	
	}
	
	return cache;
}

gboolean
scalix_object_cache_truncate (ScalixObjectCache *cache)
{
    ScalixObjectCachePrivate *priv;
    DB *db;
    int db_res;
    u_int32_t count; 
    
    priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
    db = priv->db;
    
    db_res = db->truncate (db, NULL, &count, DB_AUTO_COMMIT);
    
    return db_res == 0;
}

ScalixObject *
scalix_object_cache_get (ScalixObjectCache *cache, const char *uid)
{
	ScalixObjectCachePrivate *priv;
	DB *db;
	int db_res;
	DBT  	dbt_id, dbt_data;
	ScalixObject *object;
	int type;
	int iuid;
	int flags;
	char *data;
	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	db = priv->db;
	
	g_return_val_if_fail (db != NULL, NULL);
	g_return_val_if_fail (uid != NULL, NULL);
	
	type = iuid = 0;
	data = NULL;
	
	string_to_dbt (&dbt_id, uid);
	memset (&dbt_data, 0, sizeof (DBT));
	
	dbt_data.flags = DB_DBT_MALLOC;
	
	db_res = db->get (db, NULL, &dbt_id, &dbt_data, 0);
	
	if (db_res != 0) {
		return NULL;	
	}
	
	decode_dbt_data (&dbt_data, &type, &iuid, &flags, &data);
	
	/* deserialize object here */
	object = scalix_object_new_by_type (type);
	
	if (object == NULL) {
		free (dbt_data.data);
		return NULL;	
	}
	
	if (! scalix_object_deserialize (object, data)) {
		g_object_unref (object);
		object = NULL;
	}
	
	free (dbt_data.data);
	return object;	
}

ScalixOCEntry *
scalix_object_cache_get_entry (ScalixObjectCache *cache,
							   const char *uid,
							   gboolean get_object)
{
	ScalixObjectCachePrivate *priv;
	ScalixOCEntry *entry;
	DB *db;
	int db_res;
	DBT dbt_id, dbt_data;
	int type;
	int iuid;
	int flags = 0;
	char *data;
	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	db = priv->db;
	
	g_return_val_if_fail (db != NULL, NULL);
	g_return_val_if_fail (uid != NULL, NULL);
	
	type = iuid = 0;
	data = NULL;

	if (uid == NULL) {
		return NULL;	
	}
	
	string_to_dbt (&dbt_id, uid);
	
	memset (&dbt_data, 0, sizeof (DBT));
	dbt_data.flags = DB_DBT_MALLOC;
	
	g_assert (db->get != NULL);
	db_res = db->get (db, NULL, &dbt_id, &dbt_data, 0);
	
	if (db_res != 0) {
		return NULL;	
	}
	
	decode_dbt_data (&dbt_data, &type, &iuid, &flags, &data);
	
	entry = g_object_new (SCALIX_TYPE_OC_ENTRY,
						  "object-uid", uid,
						  "ipm-type", type, 
						  "imap-uid", iuid,
						  "flags", flags,
						  "object-data", data,
						  NULL); 
	
	free (dbt_data.data);
	
	return entry;				
}

gboolean
scalix_object_cache_put (ScalixObjectCache *cache, 
						 ScalixObject      *object,
						 int                iuid)
{
	ScalixObjectCachePrivate *priv;
	DB *db;
	DBT  dbt_id, dbt_data;
	DB_TXN *txn;
	DB_ENV *env;
	int db_res;
	char *ouid;
	int type;
	int flags;
	size_t data_size;
	size_t offset;
	char *data, *payload;
	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	g_return_val_if_fail (priv->db != NULL, FALSE);
	g_return_val_if_fail (object != NULL, FALSE);
	
	flags = 0;
	db_res = 0;
	db = priv->db;
	env = priv->db_env;
	
	ouid = NULL;
	g_object_get (object, "uid", &ouid, NULL);
	
	if (ouid == NULL) {
		GLOG_CAT_WARNING (&sloc, "Trying to put object in cache without uid");
		return FALSE;	
	}
	
	g_object_get (object, "ipm-type", &type, NULL);
	
	payload = scalix_object_serialize (object);
	
	GLOG_CAT_DEBUG (&sloc, "Putting object %p (uid: %s)", 
						   object, ouid ? ouid : "null");
	
	string_to_dbt (&dbt_id, ouid);
	
	/* pack data into memory */
	data_size = (sizeof (int) * 3) + strlen (payload) + 1;
	
	data = g_malloc (data_size);
	
	memcpy (data, &iuid, sizeof (iuid));
	offset = sizeof (iuid);
	memcpy (data + offset, &type, sizeof (type));
	offset += sizeof (type);
	memcpy (data + offset, &flags, sizeof (flags));
	offset += sizeof (flags);
	memcpy (data + offset, payload, strlen (payload) + 1);
	
	dbt_data.data = (void *) data;
	dbt_data.size = data_size;
	dbt_data.flags = DB_DBT_USERMEM;
		
	db_res = env->txn_begin (env, NULL, &txn, DB_TXN_SYNC);
	
	if (db_res != 0) {
		GLOG_CAT_WARNING (&sloc, "Couldn't initate transaction (%s)", 
						  db_strerror (db_res));	
	}
	
	db_res = db->put (db, txn, &dbt_id, &dbt_data, 0);
	
	if (db_res != 0) {
		GLOG_CAT_WARNING (&sloc, "Could not add object to cache");
		/* FIXME abort transcaction */
		return FALSE;	
	}
	
	if (priv->iuid_max < iuid) {
		priv->iuid_max = iuid;
	
		/* pack up the cache info */
	
		string_to_dbt (&dbt_id, SCKEY_PREFIX "#iuid_max");
		int_to_dbt (&dbt_data, &iuid);
	
		db_res = db->put (db, txn, &dbt_id, &dbt_data, 0);
	
		if (db_res != 0) {
			GLOG_CAT_WARNING (&sloc, "Couldn't update iuid_max (%s)", 
							  db_strerror (db_res));	
		}
		
		GLOG_CAT_DEBUG (&sloc, "iuid_max: %d", priv->iuid_max);
	}
	
	txn->commit (txn, 0);	
	
	g_free (ouid);
	g_free (data);
	g_free (payload);
	
	if (db_res != 0) {
		GLOG_CAT_WARNING (&sloc, "Could not commit transaction");
		return FALSE;	
	}
		
	return TRUE;
}

gboolean
scalix_object_cache_set_flags (ScalixObjectCache *cache, 
							   const char        *uid,
							   int                flags)
{
	ScalixObjectCachePrivate *priv;
	DB *db;
	int db_res;
	DBT  key, data;
	
	GLOG_CAT_DEBUG (&sloc, "Setting flags (%x) for uid (%s) ", 
					(unsigned int) flags, uid ? uid : "null");
	
	if (uid == NULL) {
		return TRUE;	
	}
	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	g_return_val_if_fail (priv->db != NULL, FALSE);
	
	db = priv->db;
	
	string_to_dbt (&key, uid);
	int_to_dbt (&data, &flags);
	data.flags |= DB_DBT_PARTIAL;
	data.dlen = sizeof (int);
	data.doff = 2 * sizeof (int);
	
	db_res = db->put (db, NULL, &key, &data, DB_AUTO_COMMIT);
	
	if (db_res != 0) {
		GLOG_CAT_WARNING (&sloc, "Could not mark entry (%s)", 
						  db_strerror (db_res));	
	}
	
	return db_res == 0;
}


gboolean
scalix_object_cache_remove_entry (ScalixObjectCache *cache, 
								  const char        *uid)
{
	ScalixObjectCachePrivate *priv;
	DB *db;
	int db_res;
	DBT  dbt_id;
	
	GLOG_CAT_DEBUG (&sloc, "Removing uid (%s) from cache", uid ? uid : "null");
	
	if (uid == NULL) {
		return TRUE;	
	}
	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	g_return_val_if_fail (priv->db != NULL, FALSE);	
	db = priv->db;
	
	string_to_dbt (&dbt_id, uid);
	
	db_res = db->del (db, NULL, &dbt_id, DB_AUTO_COMMIT);
	
	if (db_res != 0) {
		GLOG_CAT_WARNING (&sloc, "Could not remove db entry %s", 
						  db_strerror (db_res));	
	}
	
	return db_res == 0;
}


char *
scalix_object_cache_ouid_lookup (ScalixObjectCache *cache, int iuid)
{
	ScalixObjectCachePrivate *priv;
	DB *db;
	int db_res;
	DBT  key, pkey, data;
	char *ouid;
	
	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	g_return_val_if_fail (priv->db != NULL, NULL);
		
	db = priv->db_iuid;
	
	int_to_dbt (&key, &iuid);
	memset (&data, 0, sizeof (DBT));
	memset (&pkey, 0, sizeof (DBT));
	
	pkey.flags = DB_DBT_MALLOC;
	data.flags = DB_DBT_MALLOC | DB_DBT_PARTIAL;
	data.dlen  = 0;
	/* TODO: try out 0 as dlen */
	
	db_res = db->pget (db, NULL, &key, &pkey, &data, 0);
	
	if (db_res != 0) {
		return NULL;	
	}
	
	ouid = g_strdup (pkey.data);
	free (pkey.data);
	
	return ouid;
}


/* ************************************************************************** */

#define SCALIX_TYPE_OC_ITERATOR         (scalix_oc_iterator_get_type ())
#define SCALIX_OC_ITERATOR(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
										 SCALIX_TYPE_OC_ITERATOR, 			\
										 ScalixOCIterator))

GType scalix_oc_iterator_get_type 	    (void);

static GObjectClass *iter_parent_class = NULL;


typedef struct _ScalixOCIterator ScalixOCIterator;
typedef struct _ScalixOCIteratorClass ScalixOCIteratorClass;

struct _ScalixOCIterator {
	EIterator      parent;

	DBC    *dbc;
	DB_TXN *txn;
	
	int     db_flags;
	
	ScalixOCEntry *entry;
	
	gboolean is_valid;
};


struct _ScalixOCIteratorClass {
	EIteratorClass parent_class;
	
};


G_DEFINE_TYPE (ScalixOCIterator, 
			   scalix_oc_iterator, 
			   E_TYPE_ITERATOR);

static gboolean     
ociter_is_valid (EIterator  *iterator)
{
	ScalixOCIterator *ociter;
	
	ociter = SCALIX_OC_ITERATOR (iterator);
	
	return ociter->dbc != NULL && ociter->is_valid;
}


static gboolean
ociter_next (EIterator  *iterator)
{
	DBC *cursor;
	DBT id, data;
	ScalixOCIterator *ociter;
	ScalixOCEntry    *entry;
	int db_res;
	int type = 0;
	int flags = 0;
	int iuid = 0;
	char *ostr = NULL;
	
	g_return_val_if_fail (iterator != NULL, FALSE);
	
	ociter = SCALIX_OC_ITERATOR (iterator);
	cursor = ociter->dbc;
	
	/* FIXME: GOTO?? */
retry:	
	memset (&data, 0, sizeof (DBT));
	memset (&id, 0, sizeof (DBT));
	
	data.flags = DB_DBT_MALLOC;
	id.flags   = DB_DBT_MALLOC;
	
	db_res = cursor->c_get (cursor, &id, &data, DB_NEXT);

	if (db_res == DB_NOTFOUND) {
		ociter->is_valid = FALSE;
		return FALSE;	
	}

	if (g_str_has_prefix ((char *) id.data, SCKEY_PREFIX)) {
		
		free (id.data);
		free (data.data);
		
		goto retry;
	}

	decode_dbt_data (&data, &type, &iuid, &flags, &ostr);

	entry = g_object_new (SCALIX_TYPE_OC_ENTRY,
						  "object-uid", id.data,
						  "ipm-type", type, 
						  "imap-uid", iuid,
						  "flags", flags,
						  "object-data", ostr,
						  NULL); 

	free (id.data);
	
	if (ociter->entry != NULL) {
		g_object_unref (ociter->entry);
	}
	
	free (data.data);	
	ociter->entry = entry;
	
	return TRUE;
}

static const void *
ociter_get (EIterator  *iterator)
{
	ScalixOCIterator *ociter;
	
	ociter = SCALIX_OC_ITERATOR (iterator);
	
	return ociter ? g_object_ref (ociter->entry) : NULL;
}

static void
ociter_finalize (GObject *object)
{
	ScalixOCIterator *ociter;
	
	ociter = SCALIX_OC_ITERATOR (object);
	
	if (ociter->dbc != NULL) {
		ociter->dbc->c_close (ociter->dbc);	
	}
	
	if (ociter->txn != NULL) {
		ociter->txn->commit (ociter->txn, 0);		
	}
		
	if (ociter->entry != NULL) {
		g_object_unref (ociter->entry);	
	}
	
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
scalix_oc_iterator_class_init (ScalixOCIteratorClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	EIteratorClass *iterator_class;

	iter_parent_class = g_type_class_peek_parent (klass);	
	
	iterator_class = E_ITERATOR_CLASS (klass);

	iterator_class->get		   = ociter_get;
	iterator_class->next       = ociter_next;
	iterator_class->is_valid   = ociter_is_valid;

	gobject_class->finalize    = ociter_finalize;

}

static void
scalix_oc_iterator_init (ScalixOCIterator *iter)
{
	iter->entry = NULL;
	iter->is_valid = FALSE;	
}


EIterator *
scalix_object_cache_get_iterator (ScalixObjectCache *cache)
{
	ScalixObjectCachePrivate *priv;
	ScalixOCIterator *iter;
	DB     *db;
	DBC    *dbc;
	DB_TXN *txn;
	DB_ENV *env;
	int db_res;

	priv = SCALIX_OBJECT_CACHE_GET_PRIVATE (cache);
	
	db_res = 0;
	db  = priv->db;
	env = priv->db_env;
	
	iter = g_object_new (SCALIX_TYPE_OC_ITERATOR, NULL);
	
	if (db == NULL) {
		iter->is_valid = FALSE;
		return E_ITERATOR (iter);	
	}
	
	db_res = env->txn_begin (env, NULL, &txn, DB_TXN_SYNC);
	
	if (db_res != 0) {
		g_warning ("Couldn't initate transaction (%s)\n", db_strerror (db_res));	
	}
	
	db_res = db->cursor (db, txn, &dbc, 0);
	
	if (db_res != 0) {
		g_warning ("Could not acquirce db cursor (%s)\n", db_strerror (db_res));
		return NULL;
	}
	
	iter->dbc = dbc;
	iter->txn = txn;
	iter->db_flags = 0;
	iter->is_valid = TRUE;
	
	ociter_next (E_ITERATOR (iter));
	
	return E_ITERATOR (iter);
}












