/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <glib.h>
#include <glib/gstdio.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <glog/glog.h>

#include "scalix-utils.h"

/* This function is stolen from E-D-S */
char *
path_from_uri (const char *uri)
{
    char *mangled_uri, *path;
    int i;

    /* mangle the URI to not contain invalid characters */
    mangled_uri = g_strdup (uri);

    for (i = 0; i < strlen (mangled_uri); i++) {
        switch (mangled_uri[i]) {
        case ':':
        case '/':
            mangled_uri[i] = '_';
        }
    }

    /* generate the file name */
    path = g_build_path (G_DIR_SEPARATOR_S,
                         g_get_home_dir (),
                         ".evolution", "cache", "scalix", mangled_uri, NULL);

    GLOG_DEBUG ("path [from uri: %s] = %s", uri, path);
    /* free memory */
    g_free (mangled_uri);

    return path;
}

/* Stolen from camel. Totally stolen! */

/* From RFC 2396 2.4.3, the characters that should always be encoded */
static const char url_encoded_char[] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,     /* 0x00 - 0x0f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,     /* 0x10 - 0x1f */
    1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,     /*  ' ' - '/'  */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0,     /*  '0' - '?'  */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,     /*  '@' - 'O'  */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0,     /*  'P' - '_'  */
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,     /*  '`' - 'o'  */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1,     /*  'p' - 0x7f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
};

void
g_string_append_url_encoded (GString * str,
                             const char *in, const char *extra_enc_chars)
{
    const unsigned char *s = (const unsigned char *) in;

    while (*s) {
        if (url_encoded_char[*s] ||
            (extra_enc_chars && strchr (extra_enc_chars, *s)))
            g_string_append_printf (str, "%%%02x", (int) *s++);
        else
            g_string_append_c (str, *s++);
    }
}

void
e_uri_set_path (EUri * uri, const char *path)
{
    GString *str;

    g_return_if_fail (uri != NULL);

    if (path == NULL || !strlen (path)) {
        g_free (uri->path);
        uri->path = NULL;
        return;
    }

    str = g_string_new ("");

    if (path[strlen (path) - 1] != '/') {
        g_string_append_c (str, '/');
    }

    /* FIXME: url encode, decode? */
    str = g_string_append (str, path);
    uri->path = str->str;
    g_string_free (str, FALSE);
}

gboolean
scalix_parse_version_string (const char *version,
                             guint * major, guint * minor, guint * micro)
{
    gchar **sa;
    guint len;

    if (version == NULL) {
        return FALSE;
    }

    sa = g_strsplit (version, ".", 0);

    if (sa == NULL || *sa == NULL || major == NULL) {
        return FALSE;
    }

    len = g_strv_length (sa);

    if (major && minor && micro) {
        len = MIN (len, 3);
    } else if (major && minor) {
        len = MIN (len, 2);
    } else {
        len = 1;
    }

    switch (len) {

    case 3:
        *micro = atoi (sa[2]);
        /* FALL */

    case 2:
        *minor = atoi (sa[1]);
        /* FALL */

    case 1:
        *major = atoi (sa[0]);
        break;

    default:
        return FALSE;
    }

    g_strfreev (sa);
    return TRUE;
}

gboolean
scalix_check_min_server_version (char *version_string)
{
    guint smajor, sminor, smicro;
    guint rmajor, rminor, rmicro;
    gboolean res;

    res = scalix_parse_version_string (version_string,
                                       &smajor, &sminor, &smicro);

    res &= scalix_parse_version_string (SERVER_VERSION_REQ,
                                        &rmajor, &rminor, &rmicro);

    if (res != TRUE) {
        return res;
    }

    res = (smajor >= rmajor && sminor >= rminor && rmicro >= rmicro);

    return res;
}

static int
hex_to_int (char c)
{
    return c >= '0' && c <= '9' ? c - '0'
         : c >= 'A' && c <= 'F' ? c - 'A' + 10
         : c >= 'a' && c <= 'f' ? c - 'a' + 10 : -1;
}

static int
unescape_character (const char *scanner)
{
    int first_digit;
    int second_digit;

    first_digit = hex_to_int (*scanner++);
    if (first_digit < 0) {
        return -1;
    }

    second_digit = hex_to_int (*scanner++);
    if (second_digit < 0) {
        return -1;
    }

    return (first_digit << 4) | second_digit;
}

#define HEX_ESCAPE '%'

gboolean
g_string_unescape (GString * string, const char *illegal_characters)
{
    const char *in;
    char *out;
    gint character;

    if (string == NULL) {
        return FALSE;
    }

    for (in = out = string->str; *in != '\0'; in++) {
        character = *in;
        if (*in == HEX_ESCAPE) {
            character = unescape_character (in + 1);

            /* Check for an illegal character. We consider '\0' illegal here. */
            if (character <= 0
                || (illegal_characters != NULL
                    && strchr (illegal_characters, (char) character) != NULL)) {
                return FALSE;
            }
            in += 2;
        }
        *out++ = (char) character;
    }

    *out = '\0';
    return TRUE;
}

void
scalix_recursive_delete (const char *path)
{
    GDir *dh;
    const char *name;

    if (!g_file_test (path, G_FILE_TEST_IS_DIR)) {
        g_unlink (path);
        return;
    }

    dh = g_dir_open (path, 0, NULL);

    while ((name = g_dir_read_name (dh))) {
        char *fp;

        if (g_str_equal (name, ".") || g_str_equal (name, "..")) {
            continue;
        }

        fp = g_build_filename (path, name, NULL);
        scalix_recursive_delete (fp);
        g_free (fp);
    }

    if (dh != NULL) {
        g_dir_close (dh);
    }

    g_rmdir (path);
}
