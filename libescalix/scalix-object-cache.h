/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifndef SCALIX_OBJECT_CACHE_H
#define SCALIX_OBJECT_CACHE_H

#include <glib-object.h>

#include <libescalix/scalix-object.h>
#include <libescalix/scalix-oc-entry.h>
#include <libedataserver/e-iterator.h>

G_BEGIN_DECLS
#define SCALIX_TYPE_OBJECT_CACHE          		(scalix_object_cache_get_type ())
#define SCALIX_OBJECT_CACHE(obj)              	(G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_OBJECT_CACHE, ScalixObjectCache))
#define SCALIX_OBJECT_CACHE_CLASS(klass)      	(G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_OBJECT_CACHE, ScalixObjectCacheClass))
#define SCALIX_IS_OBJECT_CACHE(obj)           	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_OBJECT_CACHE))
#define SCALIX_IS_OBJECT_CACHE_CLASS(klass)   	(G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_OBJECT_CACHE))
#define SCALIX_OBJECT_CACHE_GET_PRIVATE(obj) 	(G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_OBJECT_CACHE, ScalixObjectCachePrivate))
typedef struct _ScalixObjectCache ScalixObjectCache;
typedef struct _ScalixObjectCacheClass ScalixObjectCacheClass;
typedef struct _ScalixObjectCachePrivate ScalixObjectCachePrivate;

struct _ScalixObjectCache {
    GObject parent;
};

struct _ScalixObjectCacheClass {
    GObjectClass parent_class;
};

enum {
    SC_OBJECT_ADDED = (1 << 0),
    SC_OBJECT_MODIFIED = (2 << 0),
    SC_OBJECT_REMOVED = (3 << 0),
    SC_OBJECT_STATE_MASK = (7 << 0),
    SC_OBJECT_BSOTRE = (1 << 4)
};

GType scalix_object_cache_get_type (void);

ScalixObjectCache *scalix_object_cache_open (const char *uri);
ScalixObject *scalix_object_cache_get (ScalixObjectCache * cache,
                                       const char *uid);

ScalixOCEntry *scalix_object_cache_get_entry (ScalixObjectCache * cache,
                                              const char *uid,
                                              gboolean get_object);

gboolean scalix_object_cache_put (ScalixObjectCache * cache,
                                  ScalixObject * object, int iuid);

gboolean scalix_object_cache_set_flags (ScalixObjectCache * cache,
                                        const char *uid, int flags);

gboolean scalix_object_cache_remove_entry (ScalixObjectCache * cache,
                                           const char *uid);

char *scalix_object_cache_ouid_lookup (ScalixObjectCache * cache, int iuid);

EIterator *scalix_object_cache_get_iterator (ScalixObjectCache * cache);

gboolean scalix_object_cache_truncate (ScalixObjectCache * cache);

G_END_DECLS
#endif /* SCALIX_OBJECT_CACHE_H */
