/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>

#include <camel/camel.h>
#include <camel/camel-file-utils.h>

#include <libecal/e-cal-util.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <stdarg.h>
#include <string.h>

#include "scalix-version.h"

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-data-server-util.h>
#else
#include <libedataserver/e-util.h>
#endif

#include "scalix-appointment.h"
#include "scalix-object.h"

/* ************************************************************************** */

/* Prototyes */

static void scalix_appointment_finalize (GObject * object);

static char *scalix_appointment_to_string (ScalixObject * object);
static gboolean scalix_appointment_from_string (ScalixObject * object,
                                                const char *string);
gboolean scalix_appointment_init_from_mime_message (ScalixObject * object,
                                                    CamelMimeMessage * msg);

CamelMimeMessage *scalix_appointment_to_mime_message (ScalixObject * object);

static char *serialize (ScalixObject * object);
static gboolean deserialize (ScalixObject * object, const char *string);
static ScalixObject *iclone (ScalixObject * object);

/* ************************************************************************** */

/* GObject related stuff */

/* Private data */

struct _ScalixAppointmentPrivate {
    icaltimezone *timezone;
    GSList *exceptions;
};

static void
scalix_appointment_get_property (GObject * object,
                                 guint prop_id,
                                 GValue * value, GParamSpec * pspec);

static void
scalix_appointment_set_property (GObject * object,
                                 guint prop_id,
                                 const GValue * value, GParamSpec * pspec);

enum {
    PROP_0,
    PROP_ICAL,
    PROP_TIMEZONE,
    PROP_EXCEPTIONS,
    /* overriden (ScalixObject) */
    PROP_UID,
    PROP_IPM_TYPE
};

static void
scalix_appointment_object_iface_init (ScalixObjectIface * iface)
{
    iface->to_string = scalix_appointment_to_string;
    iface->init_from_mime_message = scalix_appointment_init_from_mime_message;
    iface->to_mime_message = scalix_appointment_to_mime_message;
    iface->serialize = serialize;
    iface->deserialize = deserialize;
    iface->clone = iclone;
}

G_DEFINE_TYPE_EXTENDED (ScalixAppointment,
                        scalix_appointment,
                        E_TYPE_CAL_COMPONENT,
                        0,
                        G_IMPLEMENT_INTERFACE (SCALIX_TYPE_OBJECT,
                                               scalix_appointment_object_iface_init));

static void
scalix_appointment_init (ScalixAppointment * comp)
{
    ECalComponent *ecomp;
    ScalixAppointmentPrivate *priv;

    priv = SCALIX_APPOINTMENT_GET_PRIVATE (comp);

    ecomp = E_CAL_COMPONENT (comp);

    e_cal_component_set_new_vtype (ecomp, E_CAL_COMPONENT_EVENT);

    priv->exceptions = NULL;
}

static void
scalix_appointment_finalize (GObject * object)
{
    ScalixAppointmentPrivate *priv;
    GSList *iter;

    priv = SCALIX_APPOINTMENT_GET_PRIVATE (SCALIX_APPOINTMENT (object));

    if (priv->timezone) {
        icaltimezone_free (priv->timezone, 1);
    }

    for (iter = priv->exceptions; iter; iter = iter->next) {
        icalcomponent_free (iter->data);
    }

    g_slist_free (priv->exceptions);
}

static void
scalix_appointment_class_init (ScalixAppointmentClass * klass)
{
    GObjectClass *gobject_class;

    gobject_class = (GObjectClass *) klass;

    gobject_class->finalize = scalix_appointment_finalize;
    gobject_class->set_property = scalix_appointment_set_property;
    gobject_class->get_property = scalix_appointment_get_property;

    g_type_class_add_private (klass, sizeof (ScalixAppointmentPrivate));

    g_object_class_install_property (gobject_class,
                                     PROP_ICAL,
                                     g_param_spec_string ("ical",
                                                          "the inner ical object",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_TIMEZONE,
                                     g_param_spec_pointer ("timezone",
                                                           "associated timezone if any",
                                                           "",
                                                           G_PARAM_READABLE |
                                                           G_PARAM_WRITABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_EXCEPTIONS,
                                     g_param_spec_pointer ("exceptions",
                                                           "associated exceptions if any",
                                                           "",
                                                           G_PARAM_READABLE));

    /* overriden (ScalixObject) */
    g_object_class_override_property (gobject_class, PROP_UID, "uid");
    g_object_class_override_property (gobject_class, PROP_IPM_TYPE, "ipm-type");

}

ScalixAppointment *
scalix_appointment_new (const char *calobj)
{
    ScalixAppointment *comp;
    icalcomponent *icalcomp = NULL;
    ECalComponent *ecomp;

    if (calobj != NULL) {
        icalcomp = icalparser_parse_string (calobj);

        if (icalcomp == NULL) {
            return NULL;
        }

    }

    comp = g_object_new (SCALIX_TYPE_APPOINTMENT, NULL);
    ecomp = E_CAL_COMPONENT (comp);

    if (icalcomp != NULL) {
        e_cal_component_set_icalcomponent (ecomp, icalcomp);
    }

    return comp;
}

/* ************************************************************************** */

/* Properties */

static void
scalix_appointment_set_property (GObject * object,
                                 guint prop_id,
                                 const GValue * value, GParamSpec * pspec)
{
    ScalixAppointment *comp;
    const char *str;
    ScalixAppointmentPrivate *priv;

    comp = SCALIX_APPOINTMENT (object);

    priv = SCALIX_APPOINTMENT_GET_PRIVATE (comp);

    switch (prop_id) {

    case PROP_ICAL:
        str = g_value_get_string (value);
        scalix_appointment_from_string (SCALIX_OBJECT (object), str);
        break;

    case PROP_TIMEZONE:
        if (priv->timezone)
            icaltimezone_free (priv->timezone, 1);
        priv->timezone = g_value_get_pointer (value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
scalix_appointment_get_property (GObject * object,
                                 guint prop_id,
                                 GValue * value, GParamSpec * pspec)
{
    ScalixAppointment *app;
    char *str;
    const char *uid;
    ScalixAppointmentPrivate *priv;

    priv = SCALIX_APPOINTMENT_GET_PRIVATE (SCALIX_APPOINTMENT (object));

    app = SCALIX_APPOINTMENT (object);

    switch (prop_id) {

    case PROP_ICAL:
        str = scalix_appointment_to_string (SCALIX_OBJECT (object));
        g_value_take_string (value, str);
        break;

    case PROP_TIMEZONE:
        g_value_set_pointer (value, priv->timezone);
        break;

    case PROP_EXCEPTIONS:
        g_value_set_pointer (value, priv->exceptions);
        break;

    case PROP_UID:
        e_cal_component_get_uid (E_CAL_COMPONENT (app), &uid);
        g_value_set_string (value, uid);
        break;

    case PROP_IPM_TYPE:
        g_value_set_int (value, IPM_APPOINTMENT);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);

    }
}

/* ************************************************************************** */

/* Interface implementation */

static char *
scalix_appointment_to_string (ScalixObject * object)
{
    ScalixAppointment *app;
    char *str;

    app = SCALIX_APPOINTMENT (object);
    e_cal_component_commit_sequence (E_CAL_COMPONENT (app));

    str = e_cal_component_get_as_string (E_CAL_COMPONENT (app));

    return str;
}

static gboolean
scalix_appointment_from_string (ScalixObject * object, const char *string)
{
    icalcomponent *icomp, *subcomp;

    icomp = icalparser_parse_string (string);
    if (icalcomponent_isa (icomp) == ICAL_VCALENDAR_COMPONENT) {
        subcomp = icalcomponent_get_first_component (icomp,
                                                     ICAL_VEVENT_COMPONENT);
        icalcomponent_remove_component (icomp, subcomp);
        icalcomponent_free (icomp);
        icomp = subcomp;

    }

    e_cal_component_set_icalcomponent (E_CAL_COMPONENT (object), icomp);

    return TRUE;
}

static char *
save_attachment (CamelMimePart * part, const char *uid,
                 const char *attachment_store)
{
    CamelDataWrapper *content;
    CamelStream *stream = NULL;
    CamelStreamFs *stream_fs;
    char *attach_file = NULL;
    char *file_url = NULL;
    const char *mime_filename;

    content = camel_medium_get_content_object (CAMEL_MEDIUM (part));
    mime_filename = camel_mime_part_get_filename (part);

    attach_file = g_strdup_printf ("%s/%s-%s",
                                   attachment_store, uid, mime_filename);

    stream = camel_stream_fs_new_with_name (attach_file,
                                            O_RDWR | O_CREAT | O_TRUNC, 0600);

    if (stream == NULL)
        goto end;

    stream_fs = (CamelStreamFs *) stream;
    camel_data_wrapper_decode_to_stream (content, stream);
    file_url = g_strdup_printf ("file://%s", attach_file);

  end:
    g_free (attach_file);
    camel_object_unref (stream);

    return file_url;
}

static gboolean
set_ical_from_mime_part (CamelMimePart * part, ScalixObject * object)
{
    CamelDataWrapper *content;
    CamelStream *stream;
    GByteArray *data;
    icalcomponent *icomp = NULL;
    icalcomponent *subcomp = NULL;
    ScalixAppointmentPrivate *priv;

    priv = SCALIX_APPOINTMENT_GET_PRIVATE (SCALIX_APPOINTMENT (object));

    content = camel_medium_get_content_object (CAMEL_MEDIUM (part));

    data = g_byte_array_new ();
    stream = camel_stream_mem_new_with_byte_array (data);
    camel_data_wrapper_decode_to_stream (content, stream);

    if (data == NULL || data->data == NULL) {
        g_print ("Found corrupt ical data\n");
        return FALSE;
    }

    icomp = icalparser_parse_string ((const char *) data->data);

    if (icalcomponent_isa (icomp) != ICAL_VCALENDAR_COMPONENT) {
        icalcomponent_free (icomp);
        return FALSE;
    }

    /* Grab the timezone if any */
    subcomp = icalcomponent_get_first_component (icomp,
                                                 ICAL_VTIMEZONE_COMPONENT);
    if (subcomp != NULL) {
        icaltimezone *zone;

        zone = icaltimezone_new ();
        icaltimezone_set_component (zone, subcomp);
        priv->timezone = zone;
    }

    /* Grab the main VEVENT */
    subcomp = icalcomponent_get_first_component (icomp, ICAL_VEVENT_COMPONENT);

    e_cal_component_set_icalcomponent (E_CAL_COMPONENT (object), subcomp);

    return TRUE;
}

gboolean
scalix_appointment_init_from_mime_message (ScalixObject * object,
                                           CamelMimeMessage * msg)
{
    const char *msgid;
    const char *attachment_store;
    const char *mime_type, *alternative_mime_type;
    const char *content_disposition;
    const char *mime_filename;
    const char *cid;
    const char *cuid;
    const char *attachment_file_url;
    GSList *attachments = NULL;
    GSList *attachments_new = NULL;
    GSList *siter = NULL;
    CamelMimePart *part, *alternative;
    CamelMultipart *multipart, *nested_multipart;
    CamelDataWrapper *content, *alternative_content;
    int i, j, num_parts, num_alternatives;
    gboolean found_ical = FALSE;

    attachment_store =
        g_object_get_data (G_OBJECT (object), "attachment-store");

    part = CAMEL_MIME_PART (msg);
    content = camel_medium_get_content_object (CAMEL_MEDIUM (part));

    if (content == NULL) {
        return FALSE;
    }

    mime_type = camel_content_type_simple (content->mime_type);

    if (CAMEL_IS_MULTIPART (content)) {
        multipart = (CamelMultipart *) content;
        num_parts = (int) camel_multipart_get_number (multipart);

        for (i = 0; i < num_parts; i++) {
            part = camel_multipart_get_part (multipart, i);
            content = camel_medium_get_content_object (CAMEL_MEDIUM (part));
            mime_type = camel_content_type_simple (content->mime_type);
            mime_filename = camel_mime_part_get_filename (part);
            content_disposition = camel_mime_part_get_disposition (part);

            if (CAMEL_IS_MULTIPART (content)) {
                nested_multipart = (CamelMultipart *) content;
                num_alternatives =
                    (int) camel_multipart_get_number (nested_multipart);
                for (j = 0; j < num_alternatives; j++) {
                    alternative =
                        camel_multipart_get_part (nested_multipart, j);
                    alternative_content =
                        camel_medium_get_content_object (CAMEL_MEDIUM
                                                         (alternative));
                    alternative_mime_type =
                        camel_content_type_simple (alternative_content->
                                                   mime_type);
                    if (g_str_equal (alternative_mime_type, "text/calendar")
                        && found_ical == FALSE) {
                        if (set_ical_from_mime_part (alternative, object) ==
                            TRUE) {
                            e_cal_component_get_uid (E_CAL_COMPONENT (object),
                                                     &cuid);
                            e_cal_component_get_attachment_list (E_CAL_COMPONENT
                                                                 (object),
                                                                 &attachments);
                            /* we are only interested in the first ical body part */
                            found_ical = TRUE;
                        }
                    }
                }
            } else if (g_str_equal (mime_type, "text/calendar")
                       && found_ical == FALSE) {
                if (set_ical_from_mime_part (part, object) == TRUE) {
                    e_cal_component_get_uid (E_CAL_COMPONENT (object), &cuid);
                    e_cal_component_get_attachment_list (E_CAL_COMPONENT
                                                         (object),
                                                         &attachments);
                    /* we are only interested in the first ical body part */
                    found_ical = TRUE;
                }
            } else if (content_disposition
                       && strcmp (content_disposition, "attachment") == 0) {
                cid = camel_mime_part_get_content_id (part);
                if (cid != NULL) {
                    for (siter = attachments; siter; siter = siter->next) {
                        if (strstr (siter->data, cid) ==
                            (char *) (siter->data) + 4) {
                            attachment_file_url =
                                save_attachment (part, cuid, attachment_store);
                            if (attachment_file_url != NULL) {
                                attachments_new =
                                    g_slist_append (attachments_new,
                                                    g_strdup
                                                    (attachment_file_url));
                            }
                        }
                    }
                }
            } else {
                g_print ("XXXXX Unhandled mime part: %s\n", mime_type);
            }
        }
    } else {
        /* just a simple ical message */
        if (g_str_equal (mime_type, "text/calendar")) {
            set_ical_from_mime_part (part, object);
        }
    }

    if (attachments_new != NULL) {
        e_cal_component_set_attachment_list (E_CAL_COMPONENT (object),
                                             attachments_new);
    }

    msgid = camel_mime_message_get_message_id (msg);

    scalix_appointment_unset (SCALIX_APPOINTMENT (object), X_SCALIX_MSG_ID);

    scalix_appointment_set (SCALIX_APPOINTMENT (object),
                            X_SCALIX_MSG_ID, msgid);

    return TRUE;
}

static char *
get_file_contents (const char *filename, int *size)
{
    int fd, len = 0;
    struct stat sb;
    char *file_contents = NULL;

    fd = open (filename, O_RDONLY);
    if (fd < 0) {
        goto end;
    }
    if (fstat (fd, &sb) < 0) {
        goto end;
    }
    len = sb.st_size;

    if (len > 0) {
        file_contents = g_malloc0 (len);

        if (camel_read (fd, file_contents, len) < 0) {
            g_free (file_contents);
            file_contents = NULL;
            goto end;
        }

        *size = len;
    }

  end:
    close (fd);
    return file_contents;
}

CamelMimeMessage *
scalix_appointment_to_mime_message (ScalixObject * object)
{
    CamelMimeMessage *message;
    CamelMultipart *multipart;
    CamelMimePart *part;
    CamelMedium *medium;
    CamelStream *stream;
    CamelDataWrapper *wrapper;
    ECalComponentDateTime dtstart, dtend;
    ECalComponent *comp;
    ECalComponentText text;
    icalcomponent_kind kind;
    icalcomponent *icalcomp, *toplevel_comp;
    icaltimezone *zone = NULL;
    GSList *attachment_list = NULL;
    GSList *attachment_list_new = NULL;
    GSList *siter = NULL;
    GList *part_list = NULL;
    GList *iter = NULL;
    char *msgid;
    char *str, *meeting_status;
    const char *ouid = NULL;
    char *file_contents = NULL;
    char *full_path, *filename, *mime_filename;
    char *cid;
    int size;

    g_object_get (SCALIX_APPOINTMENT (object), "timezone", &zone, NULL);

    comp = E_CAL_COMPONENT (scalix_object_clone (object));
    message = camel_mime_message_new ();
    medium = CAMEL_MEDIUM (message);

    camel_medium_add_header (medium, "X-Scalix-Class", "IPM.Appointment");

    /* Preserve msg id if there is already one */
    if (scalix_appointment_get (SCALIX_APPOINTMENT (comp),
                                X_SCALIX_MSG_ID, &msgid)) {

        scalix_appointment_unset (SCALIX_APPOINTMENT (comp), X_SCALIX_MSG_ID);

    } else {
        msgid = camel_header_msgid_generate ();
    }

    camel_mime_message_set_message_id (message, msgid);

    /* subject */
    e_cal_component_get_summary (comp, &text);

    if (text.value != NULL) {
        camel_mime_message_set_subject (message, text.value);
    }

    /* start day */
    e_cal_component_get_dtstart (comp, &dtstart);

    if (!icaltime_get_timezone (*dtstart.value))
        icaltime_set_timezone (dtstart.value,
                               icaltimezone_get_builtin_timezone_from_tzid
                               (dtstart.tzid));

    /* end day */
    e_cal_component_get_dtend (comp, &dtend);

    if (!icaltime_get_timezone (*dtend.value))
        icaltime_set_timezone (dtend.value,
                               icaltimezone_get_builtin_timezone_from_tzid
                               (dtend.tzid));

    /* set From: and Sender: */
    if (e_cal_component_has_organizer (comp)) {
        ECalComponentOrganizer organizer;

        e_cal_component_get_organizer (comp, &organizer);
        if (!strncasecmp (organizer.value, "MAILTO:", 7)) {
            camel_medium_add_header (medium, "Sender", organizer.value + 7);
            camel_medium_add_header (medium, "From", organizer.value + 7);
        }
    }

    /* set the appropriate recipient headers from the recipient table */
    if (e_cal_component_has_attendees (comp)
        && e_cal_component_has_organizer (comp)) {
        GSList *iter, *attendees = NULL;
        CamelInternetAddress *recipients_to = NULL;
        CamelInternetAddress *recipients_cc = NULL;

        meeting_status = "1";

        e_cal_component_get_attendee_list (comp, &attendees);

        for (iter = attendees; iter; iter = iter->next) {
            ECalComponentAttendee *attendee = iter->data;
            const char *mail = NULL;

            /* attendee entries must start with MAILTO: */
            if (strncasecmp (attendee->value, "MAILTO:", 7)) {
                continue;
            }

            mail = attendee->value + 7;

            if (attendee->role == ICAL_ROLE_REQPARTICIPANT) {
                if (recipients_to == NULL) {
                    recipients_to = camel_internet_address_new ();
                }
                camel_internet_address_add (recipients_to, attendee->cn, mail);
            } else if (attendee->role == ICAL_ROLE_OPTPARTICIPANT) {
                if (recipients_cc == NULL) {
                    recipients_cc = camel_internet_address_new ();
                }
                camel_internet_address_add (recipients_cc, attendee->cn, mail);
            } else {
                continue;
            }
        }

        if (recipients_to != NULL) {
            camel_mime_message_set_recipients (message, "To", recipients_to);
            camel_object_unref (recipients_to);
        }

        if (recipients_cc != NULL) {
            camel_mime_message_set_recipients (message, "Cc", recipients_cc);
            camel_object_unref (recipients_cc);
        }
    } else {
        meeting_status = "0";
    }

    /* Clear properties */
    scalix_appointment_unset (SCALIX_APPOINTMENT (comp), X_SCALIX_IMAP_UID);
    /* Render the text/calendar  */
    e_cal_component_commit_sequence (comp);
    icalcomp = e_cal_component_get_icalcomponent (comp);

    kind = icalcomponent_isa (icalcomp);
    if (kind != ICAL_VCALENDAR_COMPONENT) {
        /* If its not a VCALENDAR, make it one to simplify below */
        toplevel_comp = e_cal_util_new_top_level ();
        icalcomponent_add_component (toplevel_comp, icalcomp);
        icalcomp = toplevel_comp;
    }

    /* set METHOD to PUSBLISH */
    icalcomponent_set_method (icalcomp, ICAL_METHOD_PUBLISH);

    /* Add the VTIMEZONE components for start- and/or end-times */
    if (zone) {
        icalcomponent_add_component (icalcomp,
                                     icaltimezone_get_component (zone));
    } else if (dtstart.tzid) {
        icalcomponent_add_component (icalcomp,
                                     icaltimezone_get_component
                                     (icaltimezone_get_builtin_timezone_from_tzid
                                      (dtstart.tzid)));
    }

    if (dtstart.tzid && dtend.tzid && strcmp (dtstart.tzid, dtend.tzid) != 0) {
        icalcomponent_add_component (icalcomp,
                                     icaltimezone_get_component
                                     (icaltimezone_get_builtin_timezone_from_tzid
                                      (dtend.tzid)));
    }

    /* FIXME: do we leek icalcomponents here? */

    if (e_cal_component_has_attachments (comp)) {

        multipart = camel_multipart_new ();
        camel_multipart_set_boundary (multipart, NULL);

        e_cal_component_get_uid (comp, &ouid);
        e_cal_component_get_attachment_list (comp, &attachment_list);

        for (siter = attachment_list; siter; siter = siter->next) {

            if (siter->data == NULL)
                continue;

            if (strstr (siter->data, "file://") != siter->data)
                continue;

            full_path = ((char *) siter->data) + strlen ("file://");
            filename = g_strrstr (full_path, "/") + 1;
            mime_filename = filename + strlen (ouid) + 1;

            size = 0;
            file_contents = get_file_contents (full_path, &size);

            if (file_contents == NULL)
                continue;

            stream = camel_stream_mem_new_with_buffer (file_contents, size);
            wrapper = camel_data_wrapper_new ();
            camel_data_wrapper_construct_from_stream (wrapper, stream);
            camel_object_unref (stream);

            part = camel_mime_part_new ();
            camel_medium_set_content_object (CAMEL_MEDIUM (part), wrapper);
            camel_mime_part_set_filename (part, mime_filename);
            camel_mime_part_set_encoding (part, CAMEL_TRANSFER_ENCODING_BASE64);

            cid = camel_header_msgid_generate ();
            camel_mime_part_set_content_id (part, cid);
            camel_mime_part_set_description (part, mime_filename);
            camel_mime_part_set_disposition (part, "attachment");
            part_list = g_list_append (part_list, part);

            attachment_list_new = g_slist_append (attachment_list_new,
                                                  g_strdup_printf ("CID:%s",
                                                                   cid));
            g_free (cid);
        }

        e_cal_component_set_attachment_list (comp, attachment_list_new);
        str = icalcomponent_as_ical_string (icalcomp);

        part = camel_mime_part_new ();

        camel_mime_part_set_content (part,
                                     str,
                                     strlen (str),
                                     "text/calendar; method=PUBLISH; charset=UTF-8");

        part_list = g_list_prepend (part_list, part);

        for (iter = part_list; iter; iter = iter->next) {
            part = (CamelMimePart *) iter->data;
            camel_multipart_add_part (multipart, part);
            camel_object_unref (part);
        }

        camel_medium_set_content_object (CAMEL_MEDIUM (message),
                                         CAMEL_DATA_WRAPPER (multipart));
        camel_object_unref (multipart);

        g_slist_free (attachment_list);
        g_slist_free (attachment_list_new);
        g_list_free (part_list);

    } else {
        str = icalcomponent_as_ical_string (icalcomp);

        camel_mime_part_set_content (CAMEL_MIME_PART (message),
                                     str,
                                     strlen (str),
                                     "text/calendar; method=PUBLISH; charset=UTF-8");
    }

    scalix_appointment_set (SCALIX_APPOINTMENT (object),
                            X_SCALIX_MSG_ID, msgid);

    return message;
}

/* FIXME: check for ical leaks in the next two functions */

static char *
serialize (ScalixObject * object)
{
    ScalixAppointmentPrivate *priv;
    icalcomponent *icomp, *toplevel_comp;
    ECalComponent *comp;
    GSList *iter;
    char *result = NULL;

    priv = SCALIX_APPOINTMENT_GET_PRIVATE (SCALIX_APPOINTMENT (object));

    scalix_appointment_unset (SCALIX_APPOINTMENT (object),
                              "X-SCALIX-DESCRIPTION-CHANGED");

    comp = E_CAL_COMPONENT (object);

    e_cal_component_commit_sequence (comp);
    icomp = e_cal_component_get_icalcomponent (comp);

    if (icomp) {
        toplevel_comp = e_cal_util_new_top_level ();
        /* add timezone if one exists */
        if (priv->timezone) {
            icalcomponent_add_component (toplevel_comp,
                                         icalcomponent_new_clone
                                         (icaltimezone_get_component
                                          (priv->timezone)));
        }
        /* add the main vevent */
        icalcomponent_add_component (toplevel_comp,
                                     icalcomponent_new_clone (icomp));
        /* add exceptions if they exist */
        for (iter = priv->exceptions; iter; iter = iter->next) {
            icalcomponent_add_component (toplevel_comp,
                                         icalcomponent_new_clone ((icalcomponent*) iter->data));
        }
        result = icalcomponent_as_ical_string (toplevel_comp);

        if (result != NULL) {
            result = g_strdup (result);
        }

        icalcomponent_free (toplevel_comp);
    }

    
    return result;
}

static gboolean
deserialize (ScalixObject * object, const char *string)
{
    ScalixAppointmentPrivate *priv;
    icalcomponent *icomp, *subcomp;
    icalcomponent_kind kind;
    icaltimezone *zone;

    priv = SCALIX_APPOINTMENT_GET_PRIVATE (SCALIX_APPOINTMENT (object));

    icomp = icalparser_parse_string (string);

    if (icalcomponent_isa (icomp) != ICAL_VCALENDAR_COMPONENT) {
        g_warning ("No VCALENAR while deserializing! (%s)\n", string);
        icalcomponent_free (icomp);
        return FALSE;
    }

    /* Grab the timezone if any */
    subcomp = icalcomponent_get_first_component (icomp,
                                                 ICAL_VTIMEZONE_COMPONENT);
    if (subcomp != NULL) {
        zone = icaltimezone_new ();
        icalcomponent_remove_component (icomp, subcomp);
        if (icaltimezone_set_component (zone, subcomp))
            priv->timezone = zone;
    }

    kind = ICAL_VEVENT_COMPONENT;
    /* Grab the main VEVENT */
    subcomp = icalcomponent_get_first_component (icomp, kind);

    if (subcomp == NULL) {
        icalcomponent_free (icomp);
        return FALSE;
    }

    icalcomponent_remove_component (icomp, subcomp);
    e_cal_component_set_icalcomponent (E_CAL_COMPONENT (object), subcomp);
    e_cal_component_commit_sequence (E_CAL_COMPONENT (object));

    priv->exceptions = NULL;
    while ((subcomp = icalcomponent_get_next_component (icomp, kind))) {
        icalcomponent_remove_component (icomp, subcomp);
        priv->exceptions = g_slist_prepend (priv->exceptions, subcomp);
    }

    icalcomponent_free (icomp);

    return TRUE;
}

static ScalixObject *
iclone (ScalixObject * object)
{
    char *str;
    ScalixObject *cloned;

    g_return_val_if_fail (object != NULL, NULL);

    e_cal_component_commit_sequence (E_CAL_COMPONENT (object));
    str = e_cal_component_get_as_string (E_CAL_COMPONENT (object));

    g_assert (str != NULL);

    cloned = SCALIX_OBJECT (scalix_appointment_new (str));

    g_free (str);

    return cloned;
}

/* ************************************************************************** */

/* Convinience functions */

void
scalix_appointment_set (ScalixAppointment * comp,
                        const char *key, const char *value)
{
    icalcomponent *icomp;
    icalproperty *icalprop;

    e_cal_component_commit_sequence (E_CAL_COMPONENT (comp));

    icomp = e_cal_component_get_icalcomponent (E_CAL_COMPONENT (comp));
    icalprop = icalproperty_new_x (value);
    icalproperty_set_x_name (icalprop, key);
    icalcomponent_add_property (icomp, icalprop);

}

gboolean
scalix_appointment_get (ScalixAppointment * comp, const char *key, char **value)
{
    icalcomponent *icomp;
    icalproperty *icalprop;
    const char *x_val;

    icomp = e_cal_component_get_icalcomponent (E_CAL_COMPONENT (comp));
    icalprop = icalcomponent_get_first_property (icomp, ICAL_X_PROPERTY);

    while (icalprop) {
        const char *x_name;

        x_name = icalproperty_get_x_name (icalprop);
        x_val = icalproperty_get_x (icalprop);

        if (!strcmp (x_name, key)) {
            break;
        }

        icalprop = icalcomponent_get_next_property (icomp, ICAL_X_PROPERTY);
    }

    if (icalprop) {
        *value = g_strdup (x_val);
        return TRUE;
    }

    *value = NULL;

    return FALSE;
}

void
scalix_appointment_unset (ScalixAppointment * comp, const char *key)
{
    icalcomponent *icomp;
    icalproperty *icalprop;
    const char *x_val;
    GSList *list, *iter;

    list = NULL;
    icomp = e_cal_component_get_icalcomponent (E_CAL_COMPONENT (comp));
    icalprop = icalcomponent_get_first_property (icomp, ICAL_X_PROPERTY);

    while (icalprop) {
        const char *x_name;

        x_name = icalproperty_get_x_name (icalprop);
        x_val = icalproperty_get_x (icalprop);

        if (!strcmp (x_name, key)) {
            list = g_slist_prepend (list, icalprop);
        }

        icalprop = icalcomponent_get_next_property (icomp, ICAL_X_PROPERTY);
    }

    for (iter = list; iter; iter = iter->next) {
        icalprop = iter->data;
        icalcomponent_remove_property (icomp, icalprop);
        icalproperty_free (icalprop);
    }

    return;
}
