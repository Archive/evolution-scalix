/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors:	Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */ 

#include <camel/camel.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "scalix-object.h"
#include "scalix-server-info.h"

/* private data*/

struct _ScalixServerInfoPrivate {
    /* gobject*/
    gboolean dispose_has_run;
    /* members */
    char *name;
    char *version;
    char *display_name;
    char *smtp_address;
    char *auth_id;
    CamelInternetAddress *email;
};

/* prototypes */

static void scalix_server_info_set_property (GObject         *object,
											 guint            prop_id,
											 const GValue    *value,
											 GParamSpec      *pspec);

static void scalix_server_info_get_property (GObject         *object,
											 guint            prop_id,
											 GValue          *value,
											 GParamSpec      *pspec);


static void	object_iface_init 				(ScalixObjectIface *iface);

/* gobject */

static GObjectClass *parent_class = NULL;

enum {
	PROP_0,
	PROP_NAME,
	PROP_VERSION,
	PROP_DNAME,
	PROP_SMTP_ADDRESS,
	PROP_AUTH_ID,
	PROP_EMAIL,
	/*overriden (ScalixObject)*/
	PROP_UID,
	PROP_IPM_TYPE
};

G_DEFINE_TYPE_EXTENDED (ScalixServerInfo, 
						scalix_server_info, 
						G_TYPE_OBJECT,
						0,
						G_IMPLEMENT_INTERFACE (SCALIX_TYPE_OBJECT,
											   object_iface_init));


static void
scalix_server_info_dispose (GObject *object)
{
	ScalixServerInfoPrivate *priv;

	priv = SCALIX_SERVER_INFO_GET_PRIVATE (object);

		
	if (priv->dispose_has_run) {
		return;
	}

	priv->dispose_has_run = TRUE;

	g_free (priv->name);
	g_free (priv->version);
	g_free (priv->display_name);
	g_free (priv->smtp_address);
	g_free (priv->auth_id);
	
	if (priv->email != NULL) {
		camel_object_unref (priv->email);
		priv->email = NULL;
	}
}


static void
scalix_server_info_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize)
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
scalix_server_info_class_init (ScalixServerInfoClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	g_type_class_add_private (klass, sizeof (ScalixServerInfoPrivate));

	gobject_class->dispose		= scalix_server_info_dispose;
	gobject_class->finalize		= scalix_server_info_finalize;
	gobject_class->set_property = scalix_server_info_set_property;
	gobject_class->get_property = scalix_server_info_get_property;
	

	/* properties */
	g_object_class_install_property (gobject_class, 
									 PROP_NAME,
									 g_param_spec_string ("name",
											 			  "Server name",
														  "",
														  NULL,
														  G_PARAM_READABLE |
														  G_PARAM_WRITABLE));

	g_object_class_install_property (gobject_class, 
									 PROP_VERSION,
									 g_param_spec_string ("version",
											 			  "Server Version",
														  "",
														  NULL,
														  G_PARAM_READABLE |
														  G_PARAM_WRITABLE));

	g_object_class_install_property (gobject_class, 
									 PROP_DNAME,
									 g_param_spec_string ("display-name",
											 			  "Display name of the user",
														  "",
														  NULL,
														  G_PARAM_READABLE |
														  G_PARAM_WRITABLE));

	g_object_class_install_property (gobject_class, 
									 PROP_SMTP_ADDRESS,
									 g_param_spec_string ("smtp-address",
											 			  "SMTP address of the user",
														  "",
														  NULL,
														  G_PARAM_READABLE |
														  G_PARAM_WRITABLE));

	g_object_class_install_property (gobject_class, 
									 PROP_AUTH_ID,
									 g_param_spec_string ("auth-id",
											 			  "Auth id of the user",
														  "",
														  NULL,
														  G_PARAM_READABLE |
														  G_PARAM_WRITABLE));


	g_object_class_install_property (gobject_class, 
									 PROP_EMAIL,
									 g_param_spec_pointer ("email",
											 			   "Email (CamelAddress)",
														   "",
														   G_PARAM_READABLE));

	g_object_class_override_property (gobject_class,
									  PROP_UID,
									  "uid");
									  
	g_object_class_override_property (gobject_class,
									  PROP_IPM_TYPE,
									  "ipm-type");
}


static void
scalix_server_info_init (ScalixServerInfo *si)
{
	ScalixServerInfoPrivate *priv;
	
	priv = SCALIX_SERVER_INFO_GET_PRIVATE (si);
}


/* properties */

static void
scalix_server_info_set_property (GObject         *object,
								 guint            prop_id,
								 const GValue    *value,
								 GParamSpec      *pspec)
{
	ScalixServerInfoPrivate *priv;
	
	priv = SCALIX_SERVER_INFO_GET_PRIVATE (object);

	switch (prop_id) {

		case PROP_NAME:
			g_free (priv->name);
			priv->name = g_value_dup_string (value);
			break;

		case PROP_VERSION:
			g_free (priv->version);
			priv->version = g_value_dup_string (value);
			break;

		case PROP_DNAME:
			g_free (priv->display_name);
			priv->display_name = g_value_dup_string (value); 
			break;

		case PROP_SMTP_ADDRESS:
			g_free (priv->smtp_address);
			priv->smtp_address = g_value_dup_string (value);
			break;
			
		case PROP_AUTH_ID:
			g_free (priv->auth_id);
			priv->auth_id = g_value_dup_string (value);
			break;
			
		case PROP_UID:
			break;
			
		case PROP_IPM_TYPE:
			break;
			
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);	
	}
		
}
		
static void
scalix_server_info_get_property (GObject         *object,
							     guint            prop_id,
								 GValue          *value,
								 GParamSpec      *pspec)
{
	ScalixServerInfoPrivate *priv;

	priv = SCALIX_SERVER_INFO_GET_PRIVATE (object);

	switch (prop_id) {

		case PROP_NAME:
			g_value_set_string (value, priv->name);
			break;

		case PROP_VERSION:
			g_value_set_string (value, priv->version);
			break;

		case PROP_DNAME:
			g_value_set_string (value, priv->display_name);
			break;

		case PROP_SMTP_ADDRESS:
			g_value_set_string (value, priv->smtp_address);
			break;
			
		case PROP_AUTH_ID:
			g_value_set_string (value, priv->auth_id);
			break;
			
		case PROP_EMAIL:
			
			if (priv->email == NULL) {
				priv->email = camel_internet_address_new ();

				camel_internet_address_add (priv->email, 
											priv->display_name, 
											priv->smtp_address);
			}

			camel_object_ref (priv->email);
			g_value_set_pointer (value, priv->email);	
			break;

		case PROP_UID:
			g_value_set_static_string (value, "ServerInfo");
			break;
			
		case PROP_IPM_TYPE:
			g_value_set_int (value, 0);
			break;
			
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);	
	}
}

/* interface implementation */

static char *				scalix_server_info_to_string 	(ScalixObject     *object);
static CamelMimeMessage * 	to_mime_message 				(ScalixObject     *object);
gboolean					init_from_mime_message			(ScalixObject 	  *object, 
						 									 CamelMimeMessage *message);

static void
object_iface_init (ScalixObjectIface *iface)
{
	iface->to_string               = scalix_server_info_to_string;
	iface->init_from_mime_message  = init_from_mime_message;
	iface->to_mime_message         = to_mime_message;
}

static char *
scalix_server_info_to_string (ScalixObject *object)
{
	ScalixServerInfo *info = SCALIX_SERVER_INFO (object);
	ScalixServerInfoPrivate *priv = SCALIX_SERVER_INFO_GET_PRIVATE (info);
	GString *str = g_string_new ("-----------------\n");	
	char *res;
	
	g_string_append_printf (str, "name: \t%s\n", priv->name);
	g_string_append_printf (str, "version:\t%s\n", priv->version);
	g_string_append_printf (str, "display-name\t%s\n", priv->display_name);
	g_string_append_printf (str, "smtp-address:\t%s\n", priv->smtp_address);
	g_string_append_printf (str, "auth-id\t%s\n", priv->auth_id);
	g_string_append_printf (str, "-----------------\n");
	
	res = str->str;
	g_string_free (str, FALSE);
	
	return res;
}

static CamelMimeMessage * 
to_mime_message (ScalixObject *object)
{
	CamelMimeMessage *message;
	CamelMedium      *medium;
	GString			 *string;
	ScalixServerInfo *info; 
	ScalixServerInfoPrivate *priv;

	info	  = SCALIX_SERVER_INFO (object);
	priv	  = SCALIX_SERVER_INFO_GET_PRIVATE (info); 
	message   = camel_mime_message_new ();
	medium    = CAMEL_MEDIUM (message);


	camel_medium_add_header (medium, "X-Scalix-Class", "[Serializied]ScalixServerInfo");
	camel_mime_message_set_message_id (message, "ServerInfo");

	string = g_string_new ("<?xml version=\"1.0\" ?>\n");
	
	g_string_append_printf (string, "<ScalixServerInfo>\n");
	g_string_append_printf (string, "\t<name>%s</name>\n", priv->name);
	g_string_append_printf (string, "\t<version>%s</version>\n", priv->version);
	g_string_append_printf (string, "\t<display_name>%s</display_name>\n", priv->display_name);
	g_string_append_printf (string, "\t<smtp_address>%s</smtp_address>\n", priv->smtp_address);
	g_string_append_printf (string, "\t<auth_id>%s</auth_id>\n", priv->auth_id);
	g_string_append_printf (string, "</ScalixServerInfo>\n");

	camel_mime_part_set_content (CAMEL_MIME_PART (message), string->str, string->len, 
                                 "gobject/ScalixServerInfo");

	/*FIXME free the whole string ??*/
	g_string_free (string, FALSE);

	return message;
}

gboolean
init_from_mime_message  (ScalixObject *object, 
						 CamelMimeMessage *message)
{
	CamelDataWrapper *co;
	GByteArray *data;
	CamelStream *stream;
	xmlDocPtr    doc; 
    xmlNodePtr   root, iter;
	ScalixServerInfo *info; 
	ScalixServerInfoPrivate *priv;
	xmlChar *content;
	
	info	  = SCALIX_SERVER_INFO (object);
	priv	  = SCALIX_SERVER_INFO_GET_PRIVATE (info); 
	
	co = camel_medium_get_content_object (CAMEL_MEDIUM (message));

	if (co == NULL) {
			return FALSE;
	}
	
	data = g_byte_array_new ();
	stream = camel_stream_mem_new_with_byte_array (data);
	camel_data_wrapper_decode_to_stream (co, stream);

	doc = xmlReadMemory (data->data, data->len, "ScalixServerInfo.xml", NULL, XML_PARSE_NOBLANKS);

	if (doc == NULL) {
        return FALSE;
    }

    if ((root = xmlDocGetRootElement (doc)) == NULL) {
        return FALSE;
	}

	iter = root->children;
	
	/* name */
	content = xmlNodeGetContent (iter);
	priv->name = g_strdup ((char *) content);
	xmlFree (content);
	iter = iter->next;
	
	/* version */
	content = xmlNodeGetContent (iter);
	priv->version = g_strdup ((char *) content);
	xmlFree (content);
	iter = iter->next;

	/* display_name */
	content = xmlNodeGetContent (iter);
	priv->display_name = g_strdup ((char *) content);
	xmlFree (content);
	iter = iter->next;

	/* smtp_address */
	content = xmlNodeGetContent (iter);
	priv->smtp_address = g_strdup ((char *) content);
	xmlFree (content);
	iter = iter->next;

	/* auth_id */
	content = xmlNodeGetContent (iter);
	priv->auth_id = g_strdup ((char *) content);
	xmlFree (content);
	iter = iter->next;

	xmlFreeDoc (doc);
	
	return TRUE;
}


