/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifndef E_CAL_BACKEND_SCALIX_H
#define E_CAL_BACKEND_SCALIX_H

#include <libedata-cal/e-cal-backend-sync.h>

G_BEGIN_DECLS
#define E_TYPE_CAL_BACKEND_SCALIX            (e_cal_backend_scalix_get_type ())
#define E_CAL_BACKEND_SCALIX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), E_TYPE_CAL_BACKEND_SCALIX, ECalBackendScalix))
#define E_CAL_BACKEND_SCALIX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), E_TYPE_CAL_BACKEND_SCALIX, ECalBackendScalixClass))
#define E_IS_CAL_BACKEND_SCALIX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), E_TYPE_CAL_BACKEND_SCALIX))
#define E_IS_CAL_BACKEND_SCALIX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), E_TYPE_CAL_BACKEND_SCALIX))
#define E_CAL_BACKEND_SCALIX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), E_TYPE_CAL_BACKEND_SCALIX, ECalBackendScalixClass))
#define E_CAL_BACKEND_SCALIX_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), E_TYPE_CAL_BACKEND_SCALIX, ECalBackendScalixPrivate))
typedef struct _ECalBackendScalix ECalBackendScalix;
typedef struct _ECalBackendScalixClass ECalBackendScalixClass;
typedef struct _ECalBackendScalixPrivate ECalBackendScalixPrivate;

struct _ECalBackendScalix {
    ECalBackendSync parent;
};

struct _ECalBackendScalixClass {
    ECalBackendSyncClass parent_class;
};

GType e_cal_backend_scalix_get_type (void);

gboolean e_cal_backend_scalix_complete_cal_component (ECalBackendScalix * cbs,
                                                      ECalComponent ** comp);

G_END_DECLS
#endif /* E_CAL_BACKEND_SCALIX_H */
