/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#include <libecal/e-cal-util.h>
#include "fb-utils.h"

enum {
    FREE = 0,
    TENTATIVE = 1,
    BUSY = 2,
    OOF = 3
};

struct FBEntry {
    icaltimetype start;
    icaltimetype end;
    short fbtype;
};

static gint
fb_entry_compare (gconstpointer a, gconstpointer b)
{
    icaltimetype dtstart_a = ((struct FBEntry *) a)->start;
    icaltimetype dtstart_b = ((struct FBEntry *) b)->start;

    return icaltime_compare (dtstart_a, dtstart_b);
}

static gint
itt_compare (gconstpointer a, gconstpointer b)
{
    return icaltime_compare (*(icaltimetype *) a, *(icaltimetype *) b);
}

icalcomponent *
merge_freebusy_data (icalcomponent * vfb)
{
    GArray *fb_entries;
    GArray *overlapping_fields;
    GArray *fb_entries_new;
    GArray *fb_times;
    GArray *merged_fb_entries;
    GString *period_string;
    GList *list = NULL;
    GList *iter = NULL;
    icalproperty *prop;
    icalparameter *param;
    gboolean overlap;
    struct FBEntry fb_entry;
    struct FBEntry fb_entry_a;
    struct FBEntry fb_entry_b;
    struct icalperiodtype fb;
    icalparameter_fbtype fbtype;
    struct icalperiodtype ipt;
    int i, j, len;
    char *temp;

    fb_entries = g_array_new (FALSE, FALSE, sizeof (struct FBEntry));
    fb_entries_new = g_array_new (FALSE, FALSE, sizeof (struct FBEntry));

    for (prop = icalcomponent_get_first_property (vfb, ICAL_FREEBUSY_PROPERTY);
         prop != NULL;
         prop = icalcomponent_get_next_property (vfb, ICAL_FREEBUSY_PROPERTY)) {

        fb = icalproperty_get_freebusy (prop);
        param = icalproperty_get_first_parameter (prop, ICAL_FBTYPE_PARAMETER);
        fb_entry.start = fb.start;
        fb_entry.end = fb.end;

        fbtype = icalparameter_get_fbtype (param);

        switch (fbtype) {
        case ICAL_FBTYPE_FREE:
            fb_entry.fbtype = FREE;
            break;
        case ICAL_FBTYPE_BUSYTENTATIVE:
            fb_entry.fbtype = TENTATIVE;
            break;
        case ICAL_FBTYPE_BUSY:
            fb_entry.fbtype = BUSY;
            break;
        case ICAL_FBTYPE_BUSYUNAVAILABLE:
            fb_entry.fbtype = OOF;
            break;
        default:
            fb_entry.fbtype = FREE;
        }

        g_array_append_val (fb_entries, fb_entry);
        list = g_list_append (list, prop);
    }

    for (iter = list; iter; iter = iter->next) {
        prop = iter->data;
        icalcomponent_remove_property (vfb, prop);
        icalproperty_free (prop);
    }

    g_list_free (list);
    g_array_sort (fb_entries, fb_entry_compare);

    while (fb_entries->len > 0) {
        fb_entry_a = g_array_index (fb_entries, struct FBEntry, 0);

        fb_entries = g_array_remove_index (fb_entries, 0);

        overlapping_fields =
            g_array_new (FALSE, FALSE, sizeof (struct FBEntry));

        for (i = 0; i < fb_entries->len; i++) {
            fb_entry_b = g_array_index (fb_entries, struct FBEntry, i);

            overlap =
                !((icaltime_compare (fb_entry_a.end, fb_entry_b.start) < 1)
                  || (icaltime_compare (fb_entry_a.start, fb_entry_b.end) >
                      -1));

            if (overlap == TRUE) {
                overlapping_fields =
                    g_array_append_val (overlapping_fields, fb_entry_b);
                fb_entries = g_array_remove_index (fb_entries, i);
                i--;
            }
        }

        if (overlapping_fields->len == 0) {
            fb_entries_new = g_array_append_val (fb_entries_new, fb_entry_a);
            continue;
        }

        overlapping_fields =
            g_array_append_val (overlapping_fields, fb_entry_a);

        fb_times = g_array_new (FALSE, FALSE, sizeof (struct icaltimetype));

        for (i = 0; i < overlapping_fields->len; i++) {
            gboolean found = FALSE;
            fb_entry = g_array_index (overlapping_fields, struct FBEntry, i);

            for (j = 0; j < fb_times->len; j++) {
                if (icaltime_compare
                    (fb_entry.start,
                     g_array_index (fb_times, icaltimetype, j)) == 0) {
                    found = TRUE;
                    break;
                }
            }

            if (found == FALSE) {
                fb_times = g_array_append_val (fb_times, fb_entry.start);
            }

            found = FALSE;

            for (j = 0; j < fb_times->len; j++) {
                if (icaltime_compare
                    (fb_entry.end,
                     g_array_index (fb_times, icaltimetype, j)) == 0) {
                    found = TRUE;
                    break;
                }
            }

            if (found == FALSE) {
                fb_times = g_array_append_val (fb_times, fb_entry.end);
            }
        }

        g_array_sort (fb_times, itt_compare);

        merged_fb_entries = g_array_new (FALSE, FALSE, sizeof (struct FBEntry));

        for (i = 0, len = fb_times->len - 1; i < len; i++) {
            fbtype = TENTATIVE;
            icaltimetype fbd_times_a =
                g_array_index (fb_times, icaltimetype, i);
            icaltimetype fbd_times_b =
                g_array_index (fb_times, icaltimetype, i + 1);
            for (j = 0; j < overlapping_fields->len; j++) {
                fb_entry =
                    g_array_index (overlapping_fields, struct FBEntry, j);
                overlap = !((icaltime_compare (fb_entry.end, fbd_times_a) < 1)
                            || (icaltime_compare (fb_entry.start, fbd_times_b) >
                                -1));
                if (overlap == TRUE) {
                    if (fb_entry.fbtype > fbtype) {
                        fbtype = fb_entry.fbtype;
                    }
                }
            }
            fb_entry.start = fbd_times_a;
            fb_entry.end = fbd_times_b;
            fb_entry.fbtype = fbtype;
            merged_fb_entries =
                g_array_append_val (merged_fb_entries, fb_entry);
        }

        for (i = 0, len = merged_fb_entries->len - 1; i < len; i++) {
            fb_entry_a = g_array_index (merged_fb_entries, struct FBEntry, i);
            fb_entry_b =
                g_array_index (merged_fb_entries, struct FBEntry, i + 1);
            if (icaltime_compare (fb_entry_a.end, fb_entry_b.start) == 0
                && fb_entry_a.fbtype == fb_entry_b.fbtype) {
                fb_entry.start = fb_entry_a.start;
                fb_entry.end = fb_entry_b.end;
                fb_entry.fbtype = fb_entry_a.fbtype;
                merged_fb_entries = g_array_remove_index (merged_fb_entries, i);
                merged_fb_entries = g_array_remove_index (merged_fb_entries, i);
                merged_fb_entries =
                    g_array_insert_val (merged_fb_entries, i, fb_entry);
                i--;
            }
        }

        for (i = 0; i < merged_fb_entries->len; i++) {
            fb_entry = g_array_index (merged_fb_entries, struct FBEntry, i);

            fb_entries_new = g_array_append_val (fb_entries_new, fb_entry);
        }

        g_array_free (overlapping_fields, TRUE);
        overlapping_fields = NULL;
        g_array_free (merged_fb_entries, TRUE);
        merged_fb_entries = NULL;
        g_array_free (fb_times, TRUE);
        fb_times = NULL;
    }

    g_array_sort (fb_entries_new, fb_entry_compare);

    /* finally build an icalcomponent again */
    for (i = 0; i < fb_entries_new->len; i++) {
        fb_entry = g_array_index (fb_entries_new, struct FBEntry, i);

        if (fb_entry.fbtype == FREE)
            param = icalparameter_new_fbtype (ICAL_FBTYPE_FREE);
        else if (fb_entry.fbtype == TENTATIVE)
            param = icalparameter_new_fbtype (ICAL_FBTYPE_BUSYTENTATIVE);
        else if (fb_entry.fbtype == BUSY)
            param = icalparameter_new_fbtype (ICAL_FBTYPE_BUSY);
        else if (fb_entry.fbtype == OOF)
            param = icalparameter_new_fbtype (ICAL_FBTYPE_BUSYUNAVAILABLE);
        else
            param = NULL;       /* FIXME (carsten) */

        temp = (char *)icaltime_as_ical_string (fb_entry.start);
        period_string = g_string_new (temp);
#ifdef HANDLE_LIBICAL_MEMORY
        g_free (temp);
#endif
        period_string = g_string_append (period_string, "/");
        temp = (char *)icaltime_as_ical_string (fb_entry.end);
        period_string = g_string_append (period_string, temp);
#ifdef HANDLE_LIBICAL_MEMORY
        g_free (temp);
#endif
        ipt = icalperiodtype_from_string (period_string->str);
        g_string_free (period_string, TRUE);

        prop = icalproperty_new_freebusy (ipt);
        icalproperty_add_parameter (prop, param);
        icalcomponent_add_property (vfb, prop);
    }

    g_array_free (fb_entries, TRUE);
    g_array_free (fb_entries_new, TRUE);

    return vfb;
}
