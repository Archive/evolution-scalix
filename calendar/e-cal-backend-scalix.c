/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors:	Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n-lib.h>

#include <libecal/e-cal-recur.h>
#include <libecal/e-cal-util.h>
#include <libecal/e-cal-time-util.h>

#include <libedata-cal/e-cal-backend.h>
#include <libedata-cal/e-cal-backend-util.h>
#include <libedata-cal/e-cal-backend-sexp.h>

#include <libedataserver/e-xml-hash-utils.h>

#include <libescalix/scalix-version.h>
#include <libescalix/scalix-object.h>
#include <libescalix/scalix-container.h>
#include <libescalix/scalix-appointment.h>

#include "e-cal-backend-scalix.h"
#include "fb-utils.h"

/* Logging */
#include <glog/glog.h>

GLOG_CATEGORY (slcal, "cal-backend", GLOG_FORMAT_FG_RED, "Calendar Backend");

/* private data */

struct _ECalBackendScalixPrivate {
    ScalixContainer *container;
    CalMode mode;
    icaltimezone *default_tz;
    GHashTable *timezones;
    gboolean read_only;
    icalcomponent *freebusy_data;
    guint fb_update_timeout_id;
    guint fb_is_updating;
};

static gboolean update_freebusy_cb (ECalBackendSync * backend);
static void extract_timezones (gpointer data, gpointer user_data);
static ECalBackendSyncStatus add_timezone (ECalBackendSync * backend,
                                           EDataCal * cal, const char *tzobj);
static gboolean sanitize_component (ECalBackendSync * backend, ECalComponent *comp);

/* *************************************************************************/

static void
notify_error (ECalBackendSync * backend, const char *msg)
{
    e_cal_backend_notify_error (E_CAL_BACKEND (backend),
                                _("Could not open Container"));
}

#if 0
static gboolean
g_str_equal_null (const str * a, const str * b)
{
    if ((a == NULL || b == NULL) && a != b) {
        return FALSE;
    }

    return g_str_equal (a, b);
}
#endif

/* *************************************************************************/

static void
container_object_changed_cb (ScalixContainer * container,
                             ScalixObject * old_object,
                             ScalixObject * object, gpointer data)
{
    ECalBackend *backend;
    char *s_old, *s_new;

    GLOG_CAT_DEBUG (&slcal, "object changed cb");

    backend = E_CAL_BACKEND (data);

    s_old = e_cal_component_get_as_string (E_CAL_COMPONENT (old_object));
    s_new = e_cal_component_get_as_string (E_CAL_COMPONENT (object));

    e_cal_backend_notify_object_modified (backend, s_old, s_new);

    g_free (s_old);
    g_free (s_new);
}

static void
container_object_added_cb (ScalixContainer * container,
                           ScalixObject * object, gpointer data)
{
    ECalBackend *backend;
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    char *str;

    GLOG_CAT_DEBUG (&slcal, "object added cb");
    backend = E_CAL_BACKEND (data);
    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    extract_timezones (object, priv->timezones);

    str = e_cal_component_get_as_string (E_CAL_COMPONENT (object));
    e_cal_backend_notify_object_created (backend, str);
    g_free (str);
}

static void
container_object_removed_cb (ScalixContainer * container,
                             ScalixObject * object, gpointer data)
{
    ECalBackend *backend;
    ECalComponent *comp;
    char *str;
#if EAPI_CHECK_VERSION (2,6)
    ECalComponentId *id; 
#else
    const char *id;
#endif
    
    GLOG_CAT_DEBUG (&slcal, "object removed cb");
    backend = E_CAL_BACKEND (data);

    comp = E_CAL_COMPONENT (object);
    
#if EAPI_CHECK_VERSION (2,6)
    id = e_cal_component_get_id (comp);
#else
    e_cal_component_get_uid (comp, &id);
#endif
    
    str = e_cal_component_get_as_string (E_CAL_COMPONENT (object));
    e_cal_backend_notify_object_removed (backend, id, str, NULL);

#if EAPI_CHECK_VERSION (2,6)  
    e_cal_component_free_id (id);
#endif
    
    g_free (str);
}

/* *************************************************************************/

static ECalBackendSyncStatus
is_read_only (ECalBackendSync * backend, EDataCal * cal, gboolean * read_only)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ECalBackendSyncStatus result;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    if (priv->container == NULL || priv->read_only == TRUE) {
        *read_only = TRUE;
    } else {
        *read_only = FALSE;
    }

    result = GNOME_Evolution_Calendar_Success;

    return result;
}

static ECalBackendSyncStatus
get_static_capabilities (ECalBackendSync * backend,
                         EDataCal * cal, char **capabilities)
{
    *capabilities = g_strdup (CAL_STATIC_CAPABILITY_ORGANIZER_MUST_ATTEND ","
                              CAL_STATIC_CAPABILITY_NO_EMAIL_ALARMS ","
                              CAL_STATIC_CAPABILITY_NO_TASK_ASSIGNMENT ","
                              CAL_STATIC_CAPABILITY_NO_THISANDFUTURE ","
                              CAL_STATIC_CAPABILITY_NO_THISANDPRIOR ","
                              CAL_STATIC_CAPABILITY_REMOVE_ALARMS);
    /*CAL_STATIC_CAPABILITY_SAVE_SCHEDULES */

    return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
get_ldap_attribute (ECalBackendSync * backend, EDataCal * cal, char **attribute)
{
    *attribute = NULL;

    return GNOME_Evolution_Calendar_Success;
}

static gboolean
is_loaded (ECalBackend * backend)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    GLOG_CAT_INFO (&slcal, "is loaded!");

    return priv->container != NULL;
}

static ECalBackendSyncStatus
get_cal_address (ECalBackendSync * backend, EDataCal * cal, char **address)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    *address = NULL;

    if (priv->container == NULL) {
        return GNOME_Evolution_Calendar_OtherError;
    }

    g_object_get (priv->container, "owner-email", address, NULL);

    GLOG_CAT_DEBUG (&slcal, "owner email: %s",
                    *address != NULL ? *address : "(null)");

    return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
get_default_object (ECalBackendSync * backend, EDataCal * cal, char **object)
{

    ECalComponent *comp;

    comp = e_cal_component_new ();

    switch (e_cal_backend_get_kind (E_CAL_BACKEND (backend))) {

    case ICAL_VEVENT_COMPONENT:
        e_cal_component_set_new_vtype (comp, E_CAL_COMPONENT_EVENT);
        break;

    default:
        g_object_unref (comp);
        return GNOME_Evolution_Calendar_ObjectNotFound;
    }

    *object = e_cal_component_get_as_string (comp);
    g_object_unref (comp);

    return GNOME_Evolution_Calendar_Success;
}

static gboolean
sexp_is_search (const char *sexp)
{
    return !g_str_equal (sexp, "#t");
}

typedef struct {
    GList *obj_list;
    gboolean search_needed;
    gboolean return_objects;
    ECalBackendSExp *sexp;
    ECalBackend *backend;
    icaltimezone *default_zone;
} ScanContext;

static void
scan_objects (gpointer data, gpointer user_data)
{
    ScanContext *sctx = user_data;
    ECalComponent *comp;
    char *object;

    if ((comp = (ECalComponent *) data) == NULL) {
        g_warning ("data == NULL while scanning");
        return;
    }

    if (!E_IS_CAL_COMPONENT (comp)) {
        g_warning ("E_IS_CAL_COMPONENT failed while scanning");
        return;
    }

    if (!sctx->search_needed ||
        (e_cal_backend_sexp_match_comp (sctx->sexp, comp, sctx->backend))) {

        if (sctx->return_objects == TRUE) {
            sctx->obj_list =
                g_list_append (sctx->obj_list, g_object_ref (comp));
        } else {
            object = e_cal_component_get_as_string (comp);
            sctx->obj_list = g_list_append (sctx->obj_list, object);
        }
    }
}

static ECalBackendSyncStatus
get_object_list (ECalBackendSync * backend,
                 EDataCal * cal, const char *sexp_string, GList ** objects)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ECalBackendSyncStatus result;
    ECalBackendSExp *sexp;
    ScanContext sctx;
    gboolean res;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);
    result = GNOME_Evolution_Calendar_Success;
    sexp = e_cal_backend_sexp_new (sexp_string);

    if (sexp == NULL) {
        return GNOME_Evolution_Calendar_InvalidQuery;
    }

    GLOG_CAT_DEBUG (&slcal, "get_object_list: %s",
                    sexp_string != NULL ? sexp_string : "(null)");

    sctx.search_needed = sexp_is_search (sexp_string);
    sctx.obj_list = NULL;
    sctx.backend = E_CAL_BACKEND (backend);
    sctx.sexp = sexp;
    sctx.return_objects = FALSE;

    res = scalix_container_foreach (priv->container, scan_objects, &sctx);

    GLOG_CAT_DEBUG (&slcal, "Finished (res: %d)", res);

    *objects = sctx.obj_list;

    if (!res) {
        result = GNOME_Evolution_Calendar_OtherError;
    }

    return result;
}

static void
start_query (ECalBackend * backend, EDataCalView * view)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ECalBackendSyncStatus result;
    ECalBackendSExp *sexp;
    ScanContext sctx;
    const char *sexp_string;
    gboolean res;

    result = GNOME_Evolution_Calendar_Success;
    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    sexp_string = e_data_cal_view_get_text (view);
    GLOG_CAT_DEBUG (&slcal, "start_query: %s",
                    sexp_string != NULL ? sexp_string : "(null)");

    sexp = e_cal_backend_sexp_new (sexp_string);

    if (sexp == NULL) {
        e_data_cal_view_notify_done (view,
                                     GNOME_Evolution_Calendar_InvalidQuery);
        return;
    }

    sctx.search_needed = sexp_is_search (sexp_string);
    sctx.obj_list = NULL;
    sctx.backend = E_CAL_BACKEND (backend);
    sctx.sexp = sexp;
    sctx.return_objects = FALSE;

    res = scalix_container_foreach (priv->container, scan_objects, &sctx);

    if (res && sctx.obj_list != NULL) {
        e_data_cal_view_notify_objects_added (view, sctx.obj_list);
    }

    GLOG_CAT_DEBUG (&slcal, "Query resulted in %d objects (res: %d)",
                    sctx.obj_list != NULL ? g_list_length (sctx.obj_list) :
                    0, res);

    if (!res) {
        result = GNOME_Evolution_Calendar_OtherError;
    }

    e_data_cal_view_notify_done (view, result);
}

static ECalBackendSyncStatus
get_object (ECalBackendSync * backend,
            EDataCal * cal, const char *uid, const char *rid, char **object)
{
    ECalComponent *comp;
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    comp = E_CAL_COMPONENT (scalix_container_get_object (priv->container, uid));

    if (comp == NULL) {
        *object = NULL;
        return GNOME_Evolution_Calendar_ObjectNotFound;
    }

    *object = e_cal_component_get_as_string (comp);

    g_object_unref (comp);

    return GNOME_Evolution_Calendar_Success;
}

static gboolean
free_busy_instance (ECalComponent * comp,
                    time_t instance_start, time_t instance_end, gpointer data)
{
    icalcomponent *vfb = data;
    icalproperty *prop;
    icalparameter *param;
    struct icalperiodtype ipt;
    icaltimezone *utc_zone;

    utc_zone = icaltimezone_get_utc_timezone ();

    ipt.start = icaltime_from_timet_with_zone (instance_start, FALSE, utc_zone);
    ipt.end = icaltime_from_timet_with_zone (instance_end, FALSE, utc_zone);
    ipt.duration = icaldurationtype_null_duration ();

    /* add busy information to the vfb component */
    prop = icalproperty_new (ICAL_FREEBUSY_PROPERTY);
    icalproperty_set_freebusy (prop, ipt);

    param = icalparameter_new_fbtype (ICAL_FBTYPE_BUSY);
    icalproperty_add_parameter (prop, param);

    icalcomponent_add_property (vfb, prop);

    return TRUE;
}

static icaltimezone *
resolve_tzid (const char *tzid, gpointer user_data)
{
    icalcomponent *vcalendar_comp = user_data;

    if (!tzid || !tzid[0])
        return NULL;
    else if (!strcmp (tzid, "UTC"))
        return icaltimezone_get_utc_timezone ();

    return icalcomponent_get_timezone (vcalendar_comp, tzid);
}

static icalcomponent *
create_user_free_busy (ECalBackendSync * backend,
                       const char *address,
                       const char *cn, time_t start, time_t end)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    icalcomponent *vfb;
    icaltimezone *utc_zone;
    char *query, *iso_start, *iso_end;
    GList *icomps;
    GList *iter = NULL;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    /* create the (unique) VFREEBUSY object that we'll return */
    vfb = icalcomponent_new_vfreebusy ();
    if (address != NULL) {
        icalproperty *prop;
        icalparameter *param;

        prop = icalproperty_new_organizer (address);
        if (prop != NULL && cn != NULL) {
            param = icalparameter_new_cn (cn);
            icalproperty_add_parameter (prop, param);
        }
        if (prop != NULL) {
            icalcomponent_add_property (vfb, prop);
        }
    }

    utc_zone = icaltimezone_get_utc_timezone ();
    icalcomponent_set_dtstart (vfb,
                               icaltime_from_timet_with_zone (start, FALSE,
                                                              utc_zone));
    icalcomponent_set_dtend (vfb,
                             icaltime_from_timet_with_zone (end, FALSE,
                                                            utc_zone));

    /* add all objects in the given interval */
    iso_start = isodate_from_time_t (start);
    iso_end = isodate_from_time_t (end);
    query =
        g_strdup_printf
        ("occur-in-time-range? (make-time \"%s\") (make-time \"%s\")",
         iso_start, iso_end);

    g_free (iso_start);
    g_free (iso_end);

    /* get all components */
    get_object_list (backend, NULL, query, &icomps);
    g_free (query);

    for (iter = icomps; iter; iter = iter->next) {
        ECalComponent *comp;
        icalproperty *prop;
        icalcomponent *icomp;

        comp = e_cal_component_new_from_string (iter->data);
        icomp = e_cal_component_get_icalcomponent (comp);

        if (icomp == NULL) {
            continue;
        }

        prop = icalcomponent_get_first_property (icomp, ICAL_TRANSP_PROPERTY);

        if (prop) {
            icalproperty_transp transp_val = icalproperty_get_transp (prop);

            if (transp_val == ICAL_TRANSP_TRANSPARENT ||
                transp_val == ICAL_TRANSP_TRANSPARENTNOCONFLICT)
                continue;
        }

        e_cal_recur_generate_instances (comp, start, end,
                                        free_busy_instance,
                                        vfb,
                                        resolve_tzid, icomp, priv->default_tz);
    }

    vfb = merge_freebusy_data (vfb);

    return vfb;
}

static void
update_freebusy (ECalBackendSync * backend)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    icalcomponent *vfb;
    gchar *account_address;
    time_t start;
    time_t end;
    char *calobj;
    icaltimetype dtstart;
    icaltimetype dtend;
    icaltimetype now;
    int publish_months;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    priv->fb_is_updating = TRUE;

    /* calculate start and end time */
    dtstart = icalcomponent_get_dtstart (priv->freebusy_data);
    dtend = icalcomponent_get_dtend (priv->freebusy_data);
    publish_months =
        ((dtend.year - dtstart.year) * 12) + (dtend.month - dtstart.month);

    /* if for some reason publish_months is 0 or less we set it to 2 */
    if (publish_months < 1) {
        publish_months = 2;
    }

    now = icaltime_today ();
    now.day = 1;
    start = icaltime_as_timet (now);
    now.month += publish_months;

    if (now.month > 12) {
        now.year = now.year + (now.month / 12);
        now.month = now.month % 12;
    }

    end = icaltime_as_timet (now);

    g_object_get (priv->container, "owner-email", &account_address, NULL);

    vfb = create_user_free_busy (backend, account_address, NULL, start, end);
    calobj = icalcomponent_as_ical_string (vfb);
    scalix_container_set_freebusy (priv->container, calobj);
    g_free (account_address);

#ifdef HANDLE_LIBICAL_MEMORY
    g_free (calobj);
#endif

    priv->fb_is_updating = FALSE;
}

static gboolean
update_freebusy_cb (ECalBackendSync * backend)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    if (priv->mode == CAL_MODE_LOCAL || priv->fb_is_updating == TRUE) {
        return TRUE;
    }

    update_freebusy (backend);

    return TRUE;
}

static gboolean
is_allday (ECalComponent * comp)
{
    ECalComponentDateTime dt;
    gboolean res;

    /* we only check dtstart */
    e_cal_component_get_dtstart (comp, &dt);
    res = (dt.value)->is_date;
    e_cal_component_free_datetime (&dt);

    return res;
}

static ECalBackendSyncStatus
create_object (ECalBackendSync * backend,
               EDataCal * cal, char **calobj, char **uid)
{
    ScalixAppointment *app;
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    icaltimezone *zone = NULL;
    const char *tuid;
    ECalComponent *comp;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    GLOG_CAT_DEBUG (&slcal, "CREATE_OBJECT %s",
                    *calobj != NULL ? *calobj : "(null)");
                    
    comp = e_cal_component_new_from_string (*calobj);
    
    if (comp == NULL) {
        return GNOME_Evolution_Calendar_OtherError;
    }
    
    if (!sanitize_component (backend, comp)) {
        g_object_unref (comp);
        return GNOME_Evolution_Calendar_OtherError;
    }
    
    *calobj = e_cal_component_get_as_string (comp);

    app = scalix_appointment_new (*calobj);

    if (!app)
        return GNOME_Evolution_Calendar_OtherError;

    g_object_get (app, "timezone", &zone, NULL);

    if (is_allday (E_CAL_COMPONENT (app)) && !zone) {
        /* need to add our current timezone here */
        g_object_set (app, "timezone", priv->default_tz, NULL);
    }

    if (!scalix_container_add_object (priv->container, SCALIX_OBJECT (app))) {
        g_object_unref (app);
        return GNOME_Evolution_Calendar_OtherError;
    }

    *calobj = e_cal_component_get_as_string (E_CAL_COMPONENT (app));

    e_cal_component_get_uid (E_CAL_COMPONENT (app), &tuid);
    *uid = g_strdup (tuid);

    return GNOME_Evolution_Calendar_Success;
}

static void
check_attachments (ECalComponent * comp, char *local_attachment_store)
{
    GSList *attachment_list = NULL;
    GSList *attachment_list_new = NULL;
    GSList *siter = NULL;
    char *old_path, *old_filename, *new_filename, *new_path;
    const char *uid;
    gchar *data;
    gsize data_length;

    e_cal_component_get_attachment_list (comp, &attachment_list);
    e_cal_component_get_uid (comp, &uid);

    for (siter = attachment_list; siter; siter = siter->next) {

        if (siter->data == NULL)
            continue;

        if (g_str_has_prefix (siter->data, "file://"))
            old_path = ((char *) siter->data) + strlen ("file://");
        else
            old_path = siter->data;

        if (g_str_has_prefix (old_path, local_attachment_store)) {
            /* already in the right place, we assume with the right name */
            attachment_list_new = g_slist_append (attachment_list_new,
                                                  g_strdup_printf ("file://%s",
                                                                   old_path));
        } else {
            /* need to copy to the local attachment store */
            old_filename = g_strrstr (old_path, "/");
            if (old_filename == NULL)
                new_filename = g_strdup_printf ("%s-%s", uid, old_path);
            else {
                old_filename++;
                new_filename = g_strdup_printf ("%s-%s", uid, old_filename);
            }

            new_path = g_build_path (G_DIR_SEPARATOR_S,
                                     local_attachment_store,
                                     new_filename, NULL);

            if (g_file_get_contents (old_path, &data, &data_length, NULL)) {
                if (g_file_set_contents (new_path, data, data_length, NULL)) {
                    attachment_list_new = g_slist_append (attachment_list_new,
                                                          g_strdup_printf
                                                          ("file://%s",
                                                           new_path));
                }
                g_free (data);
            }

            g_free (new_filename);
            g_free (new_path);
        }
    }

    e_cal_component_set_attachment_list (comp, attachment_list_new);
    g_slist_free (attachment_list);
}

static gboolean
attachments_changed (ECalComponent * old_comp, ECalComponent * new_comp)
{
    GSList *attachment_list_old = NULL;
    GSList *attachment_list_new = NULL;
    GSList *iter1 = NULL;
    GSList *iter2 = NULL;
    gboolean found;

    assert (old_comp != NULL && new_comp != NULL);

    e_cal_component_get_attachment_list (old_comp, &attachment_list_old);
    e_cal_component_get_attachment_list (new_comp, &attachment_list_new);

    /* do some simple tests first */

    /* both have empty attachment lists */
    if (!attachment_list_old && !attachment_list_new)
        return FALSE;

    /* one attachment list is empty, the other is not */
    if (!(attachment_list_old && attachment_list_new))
        return TRUE;

    /* attachment lists of different sizes */
    if (g_slist_length (attachment_list_old) !=
        g_slist_length (attachment_list_new))
        return TRUE;

    /* if we come here both components have attachment lists with
     * the same length, so so we need to compare the filenames */
    for (iter1 = attachment_list_new; iter1; iter1 = iter1->next) {
        found = FALSE;
        for (iter2 = attachment_list_old; iter2; iter2 = iter2->next) {
            if (g_str_equal (iter1->data, iter2->data)) {
                found = TRUE;
                break;
            }
        }
        if (!found)
            return TRUE;
    }

    return FALSE;
}

static gboolean
description_changed (ECalComponent * old_comp, ECalComponent * new_comp)
{
    GSList *text_list_old, *text_list_new;
    gboolean result;

    assert (old_comp != NULL && new_comp != NULL);

    e_cal_component_get_description_list (old_comp, &text_list_old);
    e_cal_component_get_description_list (new_comp, &text_list_new);

    // both are NULL, return FALSE
    if (!text_list_old && !text_list_new) {
        result = FALSE;
        // one of them is NULL, the other is not, which makes them different
    } else if ((!text_list_old && text_list_new) ||
               (text_list_old && !text_list_new)) {
        result = TRUE;
    } else {
        // both are non-NULL so we have to inspect the text
        GString *desc_old = g_string_new ("");
        ECalComponentText *pt_old = text_list_old->data;

        g_string_append (desc_old, pt_old->value);

        GString *desc_new = g_string_new ("");
        ECalComponentText *pt_new = text_list_new->data;

        g_string_append (desc_new, pt_new->value);

        result = (strcmp (desc_old->str, desc_new->str) != 0);

        g_string_free (desc_old, TRUE);
        g_string_free (desc_new, TRUE);
    }

    e_cal_component_free_text_list (text_list_old);
    e_cal_component_free_text_list (text_list_new);

    return result;
}

static ECalBackendSyncStatus
modify_object (ECalBackendSync * backend,
               EDataCal * cal,
               const char *calobj,
               CalObjModType mod, char **old_object, char **new_object)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ECalBackendSyncStatus res;
    ECalComponent *comp;
    ScalixAppointment *scomp;
    const char *uid;
    gboolean use_body_store = TRUE;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    GLOG_CAT_DEBUG (&slcal, "MODIFY_OBJECT %s",
                    calobj != NULL ? calobj : "(null)");

    scomp = scalix_appointment_new (calobj);

    if (scomp == NULL || !E_IS_CAL_COMPONENT (scomp)) {
        return GNOME_Evolution_Calendar_ObjectNotFound;
    }

    if (e_cal_component_is_instance (E_CAL_COMPONENT (scomp))) {
        /* As of version 2.4 Evolution does not really support
         * updates to single occurrences. */
        return GNOME_Evolution_Calendar_UnsupportedMethod;
    }

    e_cal_component_get_uid (E_CAL_COMPONENT (scomp), &uid);

    if (e_cal_component_has_attachments (E_CAL_COMPONENT (scomp))) {
        check_attachments (E_CAL_COMPONENT (scomp),
                           scalix_container_get_local_attachment_store (priv->container));
    }

    comp = E_CAL_COMPONENT (scalix_container_get_object (priv->container, uid));

    if (comp == NULL) {
        g_object_unref (scomp);
        return GNOME_Evolution_Calendar_ObjectNotFound;
    }
    
    if (!sanitize_component (backend, E_CAL_COMPONENT(scomp))) {
        g_object_unref (comp);
        g_object_unref (scomp);
        return GNOME_Evolution_Calendar_OtherError;
    }

    /* Check if attachments changed. If yes we have to write back the
     * whole message */
    if (attachments_changed (comp, E_CAL_COMPONENT (scomp))) {
        use_body_store = FALSE;
    }

    /* Check if the description changed. If yes add a flag that
     * indicates the server to regenerate the description.*/
    if (use_body_store) {
        if (description_changed (comp, E_CAL_COMPONENT (scomp))) {
            scalix_appointment_set (scomp, "X-SCALIX-DESCRIPTION-CHANGED", "TRUE");
        } else {
            scalix_appointment_set (scomp, "X-SCALIX-DESCRIPTION-CHANGED", "FALSE");
        }
    }

    if (scalix_container_update_object
        (priv->container, SCALIX_OBJECT (scomp), use_body_store)) {
        *old_object = e_cal_component_get_as_string (comp);
        *new_object = e_cal_component_get_as_string (E_CAL_COMPONENT (scomp));
        res = GNOME_Evolution_Calendar_Success;
    } else {
        res = GNOME_Evolution_Calendar_OtherError;
    }

    g_object_unref (comp);
    g_object_unref (scomp);

    return res;
}

static ECalBackendSyncStatus
remove_object (ECalBackendSync * backend,
               EDataCal * cal,
               const char *uid,
               const char *rid,
               CalObjModType mod, char **old_object, char **object)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ECalBackendSyncStatus result;
    ECalComponent *comp;
    ScalixObject *sobject;
    gboolean res = FALSE;

    g_return_val_if_fail (uid != NULL, GNOME_Evolution_Calendar_ObjectNotFound);

    *old_object = *object = NULL;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    GLOG_CAT_DEBUG (&slcal, "REMOVE_OBJECT: mod = %d; uid = %s; rid = %s",
                    mod, uid ? uid : "(null)", rid ? rid : "(null)");

    sobject = scalix_container_get_object (priv->container, uid);

    if (sobject == NULL) {
        return GNOME_Evolution_Calendar_ObjectNotFound;
    }

    comp = E_CAL_COMPONENT (sobject);

    /* fix mod, just in case */
    if (!rid || !*rid) {
        mod = CALOBJ_MOD_ALL;
    }

    switch (mod) {
    case CALOBJ_MOD_ALL:
        /* delete the whole series */
        *old_object = e_cal_component_get_as_string (comp);
        res = scalix_container_remove_id (priv->container, uid);
        break;
    case CALOBJ_MOD_THIS:
        /* delete a single instance */
        *old_object = e_cal_component_get_as_string (comp);
        e_cal_util_remove_instances (e_cal_component_get_icalcomponent (comp),
                                     icaltime_from_string (rid),
                                     CALOBJ_MOD_THIS);
        *object = e_cal_component_get_as_string (comp);
        res = scalix_container_update_object (priv->container, sobject, TRUE);
        break;
    default:
        /* everything else is not really supported */
        break;
    }

    g_object_unref (comp);

    if (!res) {
        result = GNOME_Evolution_Calendar_OtherError;
    } else {
        *object = NULL;
        result = GNOME_Evolution_Calendar_Success;
    }

    return result;
}

/* ************************************************************************** */

/* scheduling */

static ECalBackendSyncStatus
send_objects (ECalBackendSync * backend,
              EDataCal * cal,
              const char *calobj, GList ** users, char **modified_calobj)
{
    *modified_calobj = g_strdup (calobj);
    *users = NULL;

    return GNOME_Evolution_Calendar_Success;
}

static gboolean
add_exdate_for_instance (ScalixObject * instance, ScalixObject * object)
{
    GSList *exdates = NULL;
    GSList *iter = NULL;
    ECalComponentRange recurid;
    ECalComponentDateTime exdate;
    icaltimezone *zone = NULL;
    icaltimezone *utc;

    g_object_get (object, "timezone", &zone, NULL);

    if (e_cal_component_has_exdates (E_CAL_COMPONENT (object))) {
        e_cal_component_get_exdate_list (E_CAL_COMPONENT (object), &exdates);
    }

    e_cal_component_get_recurid (E_CAL_COMPONENT (instance), &recurid);
    exdate = recurid.datetime;

    if (zone && (exdate.value)->is_utc) {
        utc = icaltimezone_get_utc_timezone ();
        icaltimezone_convert_time (exdate.value, utc, zone);
        (exdate.value)->is_date = TRUE;
        exdate.tzid = NULL;
    }

    /* stop if this EXDATE value already exists */
    for (iter = exdates; iter; iter = iter->next) {
        ECalComponentDateTime *dt = (ECalComponentDateTime *) iter->data;

        if (!icaltime_compare (*(exdate.value), *(dt->value))) {
            return FALSE;
        }
    }

    exdates = g_slist_append (exdates, &exdate);
    e_cal_component_set_exdate_list (E_CAL_COMPONENT (object), exdates);

    return TRUE;
}

static int
get_own_partstat (ECalBackendSync * backend, ECalComponent * comp)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    GSList *attendees = NULL;
    GSList *iter = NULL;
    gchar *account_address;
    int partstat = 0;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    g_object_get (priv->container, "owner-email", &account_address, NULL);

    if (e_cal_component_has_attendees (comp)) {
        e_cal_component_get_attendee_list (comp, &attendees);
        for (iter = attendees; iter; iter = iter->next) {
            ECalComponentAttendee *attendee = iter->data;

            if (g_ascii_strncasecmp (attendee->value, "MAILTO:", 7)) {
                continue;
            }
            if (!strcasecmp (attendee->value + 7, account_address)) {
                partstat = attendee->status;
                break;
            }
        }
        g_slist_free (attendees);
    }

    g_free (account_address);

    return partstat;
}

static ECalBackendSyncStatus
receive_objects (ECalBackendSync * backend, EDataCal * cal, const char *calobj)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    icalcomponent *toplevel_comp, *icalcomp, *subcomp;
    icalcomponent_kind child_kind;
    icalproperty_method method;
    GList *iter = NULL;
    GList *comps = NULL;
    GList *del_comps = NULL;
    gboolean res;
    ECalBackendSyncStatus status;
    int partstat = 0;
    char *toplevel_comp_str = NULL;
    char *old_object_str = NULL;
    char *new_object_str = NULL;
    const char *uid;
    ScalixObject *old_object = NULL, *new_object;
    struct icaltimetype current;
    icaltimezone *zone = NULL;
    char *icalstring;
#if EAPI_CHECK_VERSION (2,6)
    ECalComponentId *id = NULL;
#endif
    
    GLOG_CAT_DEBUG (&slcal, "RECEIVE_OBJECTS %s",
                    calobj != NULL ? calobj : "(null)");

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    g_return_val_if_fail (calobj != NULL,
                          GNOME_Evolution_Calendar_InvalidObject);

    status = GNOME_Evolution_Calendar_Success;
    toplevel_comp = icalparser_parse_string (calobj);

    if (!toplevel_comp)
        return GNOME_Evolution_Calendar_InvalidObject;

    /* make sure we have a VCALENDAR comp and sanity check METHOD property */
    if (icalcomponent_isa (toplevel_comp) != ICAL_VCALENDAR_COMPONENT) {
        icalcomp = toplevel_comp;
        toplevel_comp = e_cal_util_new_top_level ();
        if (icalcomponent_get_method (icalcomp) == ICAL_METHOD_CANCEL)
            icalcomponent_set_method (toplevel_comp, ICAL_METHOD_CANCEL);
        else
            icalcomponent_set_method (toplevel_comp, ICAL_METHOD_PUBLISH);
        icalcomponent_add_component (toplevel_comp, icalcomp);
    } else {
        if (!icalcomponent_get_first_property
            (toplevel_comp, ICAL_METHOD_PROPERTY))
            icalcomponent_set_method (toplevel_comp, ICAL_METHOD_PUBLISH);
    }

    /* extract the timezones */
    subcomp =
        icalcomponent_get_first_component (toplevel_comp,
                                           ICAL_VTIMEZONE_COMPONENT);
    while (subcomp) {
        icalstring = icalcomponent_as_ical_string (subcomp);
        add_timezone (backend, cal, icalstring);
        subcomp =
            icalcomponent_get_next_component (toplevel_comp,
                                              ICAL_VTIMEZONE_COMPONENT);
#ifdef HANDLE_LIBICAL_MEMORY
        g_free (icalstring);
#endif
    }

    /* create a list of components we can remove */
    subcomp =
        icalcomponent_get_first_component (toplevel_comp, ICAL_ANY_COMPONENT);
    while (subcomp) {
        child_kind = icalcomponent_isa (subcomp);
        if (child_kind != e_cal_backend_get_kind (E_CAL_BACKEND (backend))) {
            if (child_kind != ICAL_VTIMEZONE_COMPONENT)
                del_comps = g_list_prepend (del_comps, subcomp);
            subcomp =
                icalcomponent_get_next_component (toplevel_comp,
                                                  ICAL_ANY_COMPONENT);
            continue;
        }

        if (!icalcomponent_get_uid (subcomp)) {
            del_comps = g_list_prepend (del_comps, subcomp);
            continue;
        }

        subcomp =
            icalcomponent_get_next_component (toplevel_comp,
                                              ICAL_ANY_COMPONENT);
    }

    for (iter = del_comps; iter; iter = g_list_next (iter)) {
        subcomp = iter->data;
        icalcomponent_remove_component (toplevel_comp, subcomp);
        icalcomponent_free (subcomp);
    }

    g_list_free (del_comps);

    toplevel_comp_str = icalcomponent_as_ical_string (toplevel_comp);

    new_object = g_object_new (SCALIX_TYPE_APPOINTMENT, NULL);

    if (!scalix_object_deserialize
        (SCALIX_OBJECT (new_object), toplevel_comp_str)) {
        g_object_unref (new_object);
        status = GNOME_Evolution_Calendar_InvalidObject;
        goto error;
    }

    g_object_get (new_object, "timezone", &zone, NULL);

    if (is_allday (E_CAL_COMPONENT (new_object)) && !zone) {
        /* need to add our current timezone here */
        g_object_set (new_object, "timezone", priv->default_tz, NULL);
    }

    current = icaltime_from_timet (time (NULL), 0);
    e_cal_component_set_created (E_CAL_COMPONENT (new_object), &current);
    e_cal_component_set_last_modified (E_CAL_COMPONENT (new_object), &current);

    e_cal_component_get_uid (E_CAL_COMPONENT (new_object), &uid);

#if EAPI_CHECK_VERSION (2,6)
    id = e_cal_component_get_id (E_CAL_COMPONENT (new_object));
#endif
    
    old_object = scalix_container_get_object (priv->container, uid);

    if (old_object)
        old_object_str =
            e_cal_component_get_as_string (E_CAL_COMPONENT (old_object));

    if (icalcomponent_get_first_property (subcomp, ICAL_METHOD_PROPERTY))
        method = icalcomponent_get_method (subcomp);
    else
        method = icalcomponent_get_method (toplevel_comp);;

    switch (method) {

    case ICAL_METHOD_PUBLISH:
    case ICAL_METHOD_REQUEST:
    case ICAL_METHOD_REPLY:

        if (e_cal_component_is_instance (E_CAL_COMPONENT (new_object))) {
            if (old_object) {
                /* As of version 2.4 Evolution does not really support
                 * updates to single occurrences. */
                status = GNOME_Evolution_Calendar_UnsupportedMethod;
            }
        } else {
            /* check attachments first */
            if (e_cal_component_has_attachments (E_CAL_COMPONENT (new_object)))
                check_attachments (E_CAL_COMPONENT (new_object),
                                   scalix_container_get_local_attachment_store
                                   (priv->container));

            /* get our own partstat, we want to remove declined items */
            partstat = get_own_partstat (backend, E_CAL_COMPONENT (new_object));

            if (old_object) {
                if (partstat == ICAL_PARTSTAT_DECLINED) {
                    res =
                        scalix_container_remove_object (priv->container,
                                                        old_object);
                    if (res) {
                        e_cal_backend_notify_object_removed (E_CAL_BACKEND
                                                             (backend), 
#if EAPI_CHECK_VERSION (2,6)
							     id,
#else
							     uid,
#endif				     
							     old_object_str,
                                                             NULL);
                    } else {
                        status = GNOME_Evolution_Calendar_OtherError;
                    }
                } else {
                    res = scalix_container_update_object (priv->container,
                                                          SCALIX_OBJECT
                                                          (new_object), TRUE);
                    if (res) {
                        new_object_str =
                            e_cal_component_get_as_string (E_CAL_COMPONENT
                                                           (new_object));
                        e_cal_backend_notify_object_modified (E_CAL_BACKEND
                                                              (backend),
                                                              old_object_str,
                                                              new_object_str);
                    } else {
                        status = GNOME_Evolution_Calendar_OtherError;
                    }
                }
            } else {
                if (partstat != ICAL_PARTSTAT_DECLINED) {
                    res =
                        scalix_container_add_object (priv->container,
                                                     SCALIX_OBJECT
                                                     (new_object));
                    if (res) {
                        new_object_str =
                            e_cal_component_get_as_string (E_CAL_COMPONENT
                                                           (new_object));
                        e_cal_backend_notify_object_created (E_CAL_BACKEND
                                                             (backend),
                                                             new_object_str);
                    } else {
                        status = GNOME_Evolution_Calendar_OtherError;
                    }
                }
            }
        }

        break;

    case ICAL_METHOD_CANCEL:

        if (old_object) {
            if (e_cal_component_is_instance (E_CAL_COMPONENT (new_object))) {
                /* this is a cancellation of a single occurrence so
                 * we need to create an additional EXDATE property */
                if (add_exdate_for_instance (new_object, old_object)) {
                    res =
                        scalix_container_update_object (priv->container,
                                                        old_object, TRUE);
                    if (res) {
                        new_object_str =
                            e_cal_component_get_as_string (E_CAL_COMPONENT
                                                           (old_object));
                        e_cal_backend_notify_object_modified (E_CAL_BACKEND
                                                              (backend),
                                                              old_object_str,
                                                              new_object_str);
                    } else {
                        status = GNOME_Evolution_Calendar_OtherError;
                    }
                }
            } else {
                /* whole series was cancelled, so lets remove the item */
                res = scalix_container_remove_id (priv->container, uid);
                if (res) {
                    e_cal_backend_notify_object_removed (E_CAL_BACKEND
                                                         (backend),
#if EAPI_CHECK_VERSION (2,6)
							     id,
#else
							     uid,
#endif								 
                                                         old_object_str, NULL);
                } else {
                    status = GNOME_Evolution_Calendar_OtherError;
                }
            }
        }

        break;

    default:
        status = GNOME_Evolution_Calendar_UnsupportedMethod;
        break;
    }

  error:
    g_object_unref (old_object);
    g_list_free (comps);

#ifdef HANDLE_LIBICAL_MEMORY
    g_free (toplevel_comp_str);
#endif
    
#if EAPI_CHECK_VERSION (2,6)
    if (id != NULL) {
        e_cal_component_free_id (id);
    }
#endif
    
    return status;
}

static ECalBackendSyncStatus
get_freebusy (ECalBackendSync * backend,
              EDataCal * cal,
              GList * users, time_t start, time_t end, GList ** freebusy)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    icaltimezone *utc;
    icalcomponent *fb_comp, *subcomp;
    icaltimetype dtstart, dtend;
    icalcomponent_kind kind;
    char *request, *response;
    gchar *account_address;
    GList *iter;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    fb_comp = icalcomponent_new (ICAL_VFREEBUSY_COMPONENT);

    utc = icaltimezone_get_utc_timezone ();

    dtstart = icaltime_from_timet_with_zone (start, FALSE, utc);
    dtend = icaltime_from_timet_with_zone (end, FALSE, utc);

    /* We don't set a start or end time because we want all freebusy
     * data that the server has for that person. The number of months
     * that are covered is configurable in Outlook only. The covered
     * timeperiod is returned as dtstart/dtend in the response.
     */
    for (iter = users; iter; iter = g_list_next (iter)) {
        char *mail;
        icalproperty *prop;

        if (iter->data == NULL) {
            continue;
        }

        GLOG_CAT_DEBUG (&slcal, "GET_FREEBUSY: %s", (char *) iter->data);

        mail = g_strconcat ("MAILTO:", (char *) iter->data, NULL);
        prop = icalproperty_new_attendee (mail);

        /* REVIEW: free mail string here? */

        if (prop != NULL) {
            icalcomponent_add_property (fb_comp, prop);
        }

    }

    request = icalcomponent_as_ical_string (fb_comp);

    response = scalix_container_get_freebusy (priv->container, request);
    icalcomponent_free (fb_comp);

#ifdef HANDLE_LIBICAL_MEMORY
    g_free (request);
#endif

    if (response == NULL) {
        return GNOME_Evolution_Calendar_OtherError;
    }

    fb_comp = icalparser_parse_string (response);

    g_free (response);

    if (fb_comp == NULL) {
        return GNOME_Evolution_Calendar_OtherError;
    }

    kind = ICAL_VFREEBUSY_COMPONENT;
    subcomp = icalcomponent_get_first_component (fb_comp, kind);

    *freebusy = NULL;

    g_object_get (priv->container, "owner-email", &account_address, NULL);

    while (subcomp) {
        char *entry;
        icalproperty *prop;
        icalvalue *value;
        const char *attendee;

        prop =
            icalcomponent_get_first_property (subcomp, ICAL_ATTENDEE_PROPERTY);
        if (prop != NULL) {
            value = icalproperty_get_value (prop);
            if (value != NULL) {
                attendee = icalvalue_get_string (value);
                if (g_ascii_strncasecmp ("MAILTO:", attendee, 7) == 0) {
                    if (g_ascii_strcasecmp (attendee + 7, account_address) == 0) {
                        /* it's us so store the freebusy data in the backend */
                        if (priv->freebusy_data != NULL) {
                            icalcomponent_free (priv->freebusy_data);
                        }
                        priv->freebusy_data = icalcomponent_new_clone (subcomp);
                    }
                }
            }
        }

        entry = icalcomponent_as_ical_string (subcomp);
        *freebusy = g_list_prepend (*freebusy, g_strdup (entry));
        subcomp = icalcomponent_get_next_component (fb_comp, kind);

#ifdef HANDLE_LIBICAL_MEMORY
        g_free (entry);
#endif
    }

    g_free (account_address);
    icalcomponent_free (fb_comp);

    return GNOME_Evolution_Calendar_Success;
}

/* ************************************************************************** */

/* timezone reltated functions */

static ECalBackendSyncStatus
get_timezone (ECalBackendSync * backend, EDataCal * cal,
              const char *tzid, char **object)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    icaltimezone *zone;
    icalcomponent *icalcomp;
    char *out;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    if (!g_ascii_strcasecmp (tzid, "UTC")) {
        zone = icaltimezone_get_utc_timezone ();
    } else if ((out = g_hash_table_lookup (priv->timezones, tzid))) {
        *object = g_strdup (out);
        return GNOME_Evolution_Calendar_Success;
    } else {

        zone = icaltimezone_get_builtin_timezone_from_tzid (tzid);

        if (!zone) {
            GLOG_CAT_WARNING (&slcal, "Could not resove timezone! (%s)\n",
                              tzid);
            return GNOME_Evolution_Calendar_ObjectNotFound;
        }
    }

    icalcomp = icaltimezone_get_component (zone);

    if (!icalcomp) {
        GLOG_CAT_WARNING (&slcal, "Could not resove timezone!\n");
        return GNOME_Evolution_Calendar_InvalidObject;
    }

    out = icalcomponent_as_ical_string (icalcomp);
    *object = g_strdup (out);
#ifdef HANDLE_LIBICAL_MEMORY
    g_free (out);
#endif

    return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
add_timezone (ECalBackendSync * backend, EDataCal * cal, const char *tzobj)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ECalBackendSyncStatus res;
    icalcomponent *tz_comp;
    const char *tzid = NULL;
    char *zstr = NULL;
    icaltimezone *zone;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    res = GNOME_Evolution_Calendar_InvalidObject;

    tz_comp = icalparser_parse_string (tzobj);

    if (tz_comp) {
        if (icalcomponent_isa (tz_comp) == ICAL_VTIMEZONE_COMPONENT) {
            zone = icaltimezone_new ();
            if (icaltimezone_set_component (zone, tz_comp)) {
                tzid = icaltimezone_get_tzid (zone);
                zstr =
                    icalcomponent_as_ical_string (icaltimezone_get_component
                                                  (zone));

                if (tzid && zstr) {
                    g_hash_table_insert (priv->timezones,
                                         g_strdup (tzid), g_strdup (zstr));
                    res = GNOME_Evolution_Calendar_Success;
                }

#ifdef HANDLE_LIBICAL_MEMORY
                g_free (zstr);
#endif
            }
            icaltimezone_free (zone, TRUE);
        }
    }

    return res;
}

static ECalBackendSyncStatus
set_default_timezone (ECalBackendSync * backend,
                      EDataCal * cal, const char *tzid)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    priv->default_tz = icaltimezone_get_builtin_timezone_from_tzid (tzid);

    return GNOME_Evolution_Calendar_Success;
}

static void
extract_timezones (gpointer data, gpointer user_data)
{
    GHashTable *timezones;
    icaltimezone *zone = NULL;
    const char *tzid = NULL;
    char *zstr = NULL;

    timezones = (GHashTable *) user_data;
    g_assert (data != NULL && SCALIX_IS_APPOINTMENT (data));

    g_object_get (data, "timezone", &zone, NULL);

    if (zone == NULL) {
        return;
    }

    tzid = icaltimezone_get_tzid (zone);
    zstr = icalcomponent_as_ical_string (icaltimezone_get_component (zone));

    if (tzid && zstr) {
        g_hash_table_insert (timezones, g_strdup (tzid), g_strdup (zstr));
    }

#ifdef HANDLE_LIBICAL_MEMORY
    g_free (zstr);
#endif
}

static gpointer
load_calender_thread (gpointer data)
{
    ECalBackendSync *backend;
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ScalixContainer *container;
    GList *freebusy_users = NULL;
    GList *freebusy_data = NULL;
    gchar *account_address;
    gboolean res;

    cbs = E_CAL_BACKEND_SCALIX (data);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);
    backend = E_CAL_BACKEND_SYNC (cbs);
    container = SCALIX_CONTAINER (priv->container);

    GLOG_CAT_INFO (&slcal, "Setting container to online state");

#if EAPI_CHECK_VERSION (2,4)
    e_cal_backend_notify_view_progress (E_CAL_BACKEND (cbs),
                                        _("Connecting to server..."), 10);
#endif
    /* We are chaning state, we are read only during that period */
    /* e_cal_backend_notify_readonly (backend, TRUE); */
    res = scalix_container_set_online (container, TRUE);

    if (res != TRUE) {
        notify_error (backend, _("Could not open Container"));
        /* e_cal_backend_notify_readonly (backend, TRUE); */
        GLOG_CAT_ERROR (&slcal, "Error setting container to online state");
        return NULL;
    }
#if EAPI_CHECK_VERSION (2,4)
    e_cal_backend_notify_view_progress (E_CAL_BACKEND (cbs),
                                        _("Synchronizing ..."), 20);
#endif

    scalix_container_sync (container);

    /* get the freebusy data */
    g_object_get (priv->container, "owner-email", &account_address, NULL);
    freebusy_users = g_list_append (freebusy_users, account_address);

#if EAPI_CHECK_VERSION (2,4)
    e_cal_backend_notify_view_progress (E_CAL_BACKEND (cbs),
                                        _("Updating free-busy data ..."), 90);
#endif
    get_freebusy (backend, NULL, freebusy_users, 0, 0, &freebusy_data);

    g_list_free (freebusy_data);
    g_free (account_address);

    GLOG_CAT_INFO (&slcal, "Done setting container to online state");
    priv->read_only = FALSE;

#if EAPI_CHECK_VERSION (2,4)
    e_cal_backend_notify_view_done (E_CAL_BACKEND (cbs),
                                    GNOME_Evolution_Calendar_Success);
    e_cal_backend_notify_readonly (E_CAL_BACKEND (cbs), FALSE);
#endif

    return NULL;
}

static void
change_mode (ECalBackendScalix * cbs, gboolean online)
{
    ECalBackendScalixPrivate *priv;

    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    /* Let's be read only during mode changes */
    priv->read_only = TRUE;

    if (online) {
        g_thread_create (load_calender_thread, cbs, FALSE, NULL);
    } else {
        scalix_container_set_online (priv->container, FALSE);
        priv->read_only = FALSE;
    }
}

static ECalBackendSyncStatus
open_calendar (ECalBackendSync * backend,
               EDataCal * cal,
               gboolean only_if_exists,
               const char *username, const char *password)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    ECalBackendSyncStatus result;
    ESource *source;
    ScalixContainer *container;
    char *uri;
    const char *prop_default;
    const char *prop_ssl;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    result = GNOME_Evolution_Calendar_Success;

    if (priv->container != NULL) {
        return GNOME_Evolution_Calendar_Success;
    }

    source = e_cal_backend_get_source (E_CAL_BACKEND (backend));
    uri = e_source_get_uri (source);

    if ((prop_ssl = e_source_get_property (source, "use_ssl"))) {
		CamelURL *url;

		url = camel_url_new (uri, NULL);
		camel_url_set_param (url, "use_ssl", prop_ssl);
		g_free (uri);
		uri = camel_url_to_string (url, 0);
    }


    GLOG_CAT_INFO (&slcal, "Opening container (%s)",
                   uri != NULL ? uri : "(null)");
    container = scalix_container_open (uri);

    g_free (uri);

    if (container == NULL) {
        notify_error (backend, _("Could not open Container"));
        return GNOME_Evolution_Calendar_OtherError;
    }

    priv->container = container;

    if (only_if_exists == FALSE) {
        GLOG_CAT_DEBUG (&slcal, "Only if exists == FALSE");
        g_object_set (container, "only_if_exists", FALSE,
                      "type", "Calendar", NULL);
    }

    /* build timezone informations */
    scalix_container_foreach (container, extract_timezones, priv->timezones);

    /* Set up signals to catch container updates */
    g_signal_connect (container, "object_added",
                      G_CALLBACK (container_object_added_cb), (gpointer) cbs);

    g_signal_connect (container, "object_removed",
                      G_CALLBACK (container_object_removed_cb), (gpointer) cbs);

    g_signal_connect (container, "object_changed",
                      G_CALLBACK (container_object_changed_cb), (gpointer) cbs);

    /* Open the connection and synch in the background */
    if (priv->mode == CAL_MODE_REMOTE) {
        change_mode (cbs, TRUE);
    }

    /* schedule freebusy updates every 15 minutes for the main Calendar */
    prop_default = e_source_get_property (source, "default");
    if (prop_default != NULL && strcmp (prop_default, "TRUE") == 0) {
        GLOG_CAT_DEBUG (&slcal, "Scheduling freebusy for main calendar");
        priv->fb_update_timeout_id =
            g_timeout_add (15 * 60000, (GSourceFunc) update_freebusy_cb,
                           backend);
    }

    GLOG_CAT_INFO (&slcal, "Opening calendar done!");
    return result;
}

static ECalBackendSyncStatus
remove_calendar (ECalBackendSync * backend, EDataCal * cal)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    if (scalix_container_remove (priv->container)) {
        return GNOME_Evolution_Calendar_Success;
    } else {
        return GNOME_Evolution_Calendar_OtherError;
    }
}

static void
set_mode (ECalBackend * backend, CalMode mode)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;
    GNOME_Evolution_Calendar_CalListener_SetModeStatus status;
    gboolean res;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);
    status = GNOME_Evolution_Calendar_CalListener_MODE_SET;
    res = TRUE;

    GLOG_CAT_DEBUG (&slcal, "Set mode!");

    /* nothing changed */
    if (priv->mode == mode) {
        e_cal_backend_notify_mode (backend, status, cal_mode_to_corba (mode));
        return;
    }

    if (priv->container) {
        switch (mode) {

        case CAL_MODE_REMOTE:
            change_mode (cbs, TRUE);
            break;

        case CAL_MODE_LOCAL:
            change_mode (cbs, FALSE);
            break;

        default:
            GLOG_CAT_WARNING (&slcal, "unsupported mode!");
            e_cal_backend_notify_mode (backend,
                                       GNOME_Evolution_Calendar_CalListener_MODE_NOT_SUPPORTED,
                                       cal_mode_to_corba (mode));
        }

    }

    priv->mode = mode;
    e_cal_backend_notify_mode (backend, status, cal_mode_to_corba (mode));
}

static CalMode
get_mode (ECalBackend * backend)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    return priv->mode;
}

static icaltimezone *
internal_get_default_timezone (ECalBackend * backend)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (backend);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    return priv->default_tz;
}

static icaltimezone *
internal_get_timezone (ECalBackend * backend, const char *tzid)
{
    icaltimezone *zone;

    zone = icaltimezone_get_builtin_timezone_from_tzid (tzid);

    if (!zone) {
        return icaltimezone_get_utc_timezone ();
    }

    return zone;
}

static gboolean
sanitize_component (ECalBackendSync * backend, ECalComponent * comp)
{
    ECalComponentDateTime dtstart, dtend;
    icaltimezone *zone, *default_zone;
    gboolean res = TRUE;

    GLOG_CAT_DEBUG (&slcal, "SANITIZE_COMPONENT");

    default_zone = internal_get_default_timezone ((ECalBackend *) backend);

    e_cal_component_get_dtstart (comp, &dtstart);
    e_cal_component_get_dtend (comp, &dtend);

    if (dtstart.value == NULL || dtend.value == NULL
        || (dtstart.value)->is_date != (dtend.value)->is_date) {
        /* oops, something is terribly wrong here */
        res = FALSE;
    } else if ((dtstart.value)->is_date) {
        /* allday event, always use our default timezone */
        if (dtstart.tzid)
            g_free ((char *) dtstart.tzid);
        if (dtend.tzid)
            g_free ((char *) dtend.tzid);
        dtstart.tzid = g_strdup (icaltimezone_get_tzid (default_zone));
        dtend.tzid = g_strdup (icaltimezone_get_tzid (default_zone));
    } else {
        
        if (dtstart.tzid) {
            zone = internal_get_timezone ((ECalBackend *) backend, dtstart.tzid);
            if (!zone) {
                g_free ((char *) dtstart.tzid);
                dtstart.tzid = g_strdup (icaltimezone_get_tzid (default_zone));
                e_cal_component_set_dtstart (comp, &dtstart);
            }
        }

        if (dtend.tzid) {
            zone = internal_get_timezone ((ECalBackend *) backend, dtend.tzid);
            if (!zone) {
                g_free ((char *) dtend.tzid);
                dtend.tzid = g_strdup (icaltimezone_get_tzid (default_zone));
                e_cal_component_set_dtend (comp, &dtend);
            }
        }

        /* always make sure that dtstart and dtend are in the same timezone */
        if (dtstart.tzid && dtend.tzid && strcmp (dtstart.tzid, dtend.tzid)) {
            GLOG_CAT_DEBUG (&slcal,
                            "SANITIZE_COMPONENT: detected different TZs for dtstart and dtend");
            zone =
                internal_get_timezone ((ECalBackend *) backend, dtstart.tzid);
            icaltimezone_convert_time (dtend.value,
                                       internal_get_timezone ((ECalBackend *)
                                                              backend,
                                                              dtend.tzid),
                                       zone);
            g_free ((char *) dtend.tzid);
            dtend.tzid = g_strdup (icaltimezone_get_tzid (zone));
            e_cal_component_set_dtend (comp, &dtend);
        }
    }

    e_cal_component_free_datetime (&dtstart);
    e_cal_component_free_datetime (&dtend);

    e_cal_component_abort_sequence (comp);

    return res;
}



/* *************************************************************************/

/* gobject stuff */

G_DEFINE_TYPE (ECalBackendScalix,
               e_cal_backend_scalix, E_TYPE_CAL_BACKEND_SYNC);

static void
e_cal_backend_scalix_init (ECalBackendScalix * cbs)
{

    ECalBackendScalixPrivate *priv;

    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    priv->timezones = g_hash_table_new (g_str_hash, g_str_equal);
    priv->container = NULL;
    priv->freebusy_data = NULL;
    priv->read_only = TRUE;

    e_cal_backend_sync_set_lock (E_CAL_BACKEND_SYNC (cbs), TRUE);
}

static void
e_cal_backend_scalix_dispose (GObject * object)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (object);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    if (priv->fb_update_timeout_id > 0) {
        g_source_remove (priv->fb_update_timeout_id);
    }
}

static void
e_cal_backend_scalix_finalize (GObject * object)
{
    ECalBackendScalix *cbs;
    ECalBackendScalixPrivate *priv;

    cbs = E_CAL_BACKEND_SCALIX (object);
    priv = E_CAL_BACKEND_SCALIX_GET_PRIVATE (cbs);

    g_hash_table_destroy (priv->timezones);
    g_object_unref (priv->container);
    icalcomponent_free (priv->freebusy_data);
}

static void
e_cal_backend_scalix_class_init (ECalBackendScalixClass * klass)
{
    GObjectClass *object_class;
    ECalBackendClass *backend_class = E_CAL_BACKEND_CLASS (klass);
    ECalBackendSyncClass *sync_class = E_CAL_BACKEND_SYNC_CLASS (klass);

    g_type_class_add_private (klass, sizeof (ECalBackendScalixPrivate));

    object_class = (GObjectClass *) klass;

#if 0
    sync_class->get_alarm_email_address_sync = get_alarm_email_address;
    sync_class->discard_alarm_sync = discard_alarm;
    sync_class->get_changes_sync = get_changes;
#endif

    backend_class->internal_get_default_timezone =
        internal_get_default_timezone;
    backend_class->internal_get_timezone = internal_get_timezone;
    backend_class->is_loaded = is_loaded;
    backend_class->set_mode = set_mode;
    backend_class->get_mode = get_mode;
    backend_class->start_query = start_query;

    sync_class->get_cal_address_sync = get_cal_address;
    sync_class->open_sync = open_calendar;
    sync_class->remove_sync = remove_calendar;
    sync_class->is_read_only_sync = is_read_only;
    sync_class->get_static_capabilities_sync = get_static_capabilities;
    sync_class->add_timezone_sync = add_timezone;
    sync_class->set_default_timezone_sync = set_default_timezone;
    sync_class->get_object_sync = get_object;
    sync_class->get_object_list_sync = get_object_list;
    sync_class->create_object_sync = create_object;
    sync_class->modify_object_sync = modify_object;
    sync_class->remove_object_sync = remove_object;

    sync_class->receive_objects_sync = receive_objects;
    sync_class->send_objects_sync = send_objects;
    sync_class->get_freebusy_sync = get_freebusy;

    sync_class->get_default_object_sync = get_default_object;
    sync_class->get_timezone_sync = get_timezone;
    sync_class->get_ldap_attribute_sync = get_ldap_attribute;

    object_class->dispose = e_cal_backend_scalix_dispose;
    object_class->finalize = e_cal_backend_scalix_finalize;
}
