/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors:	Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-object.h>
#include <libedata-cal/e-cal-backend-factory.h>

#include <libescalix/scalix.h>

#include "e-cal-backend-scalix.h"

/* types */

typedef struct {
    ECalBackendFactory parent_object;
} ECalBackendScalixFactory;

typedef struct {
    ECalBackendFactoryClass parent_class;
} ECalBackendScalixFactoryClass;

/* prototypes */

void eds_module_initialize (GTypeModule * module);
void eds_module_shutdown (void);
void eds_module_list_types (const GType ** types, int *num_types);

/* gobject */

static void
e_cal_backend_scalix_factory_instance_init (ECalBackendScalixFactory * factory)
{
}

static const char *
_get_protocol (ECalBackendFactory * factory)
{
    return "scalix";
}

static ECalBackend *
_events_new_backend (ECalBackendFactory * factory, ESource * source)
{
    return g_object_new (E_TYPE_CAL_BACKEND_SCALIX,
                         "source", source, "kind", ICAL_VEVENT_COMPONENT, NULL);
}

static icalcomponent_kind
_events_get_kind (ECalBackendFactory * factory)
{
    return ICAL_VEVENT_COMPONENT;
}

static void
events_backend_factory_class_init (ECalBackendScalixFactoryClass * klass)
{
    E_CAL_BACKEND_FACTORY_CLASS (klass)->get_protocol = _get_protocol;
    E_CAL_BACKEND_FACTORY_CLASS (klass)->get_kind = _events_get_kind;
    E_CAL_BACKEND_FACTORY_CLASS (klass)->new_backend = _events_new_backend;
}

static GType
events_backend_factory_get_type (GTypeModule * module)
{
    GType type;

    GTypeInfo info = {
        sizeof (ECalBackendScalixFactoryClass),
        NULL,                   /* base_class_init */
        NULL,                   /* base_class_finalize */
        (GClassInitFunc) events_backend_factory_class_init,
        NULL,                   /* class_finalize */
        NULL,                   /* class_data */
        sizeof (ECalBackend),
        0,                      /* n_preallocs */
        (GInstanceInitFunc) e_cal_backend_scalix_factory_instance_init
    };

    type = g_type_module_register_type (module,
                                        E_TYPE_CAL_BACKEND_FACTORY,
                                        "ECalBackendScalixEventsFactory",
                                        &info, 0);

    return type;
}

/* e-d-s module  */
static GType scalix_cal_types[1];

void
eds_module_initialize (GTypeModule * module)
{
    libescalix_init (TRUE);

    scalix_cal_types[0] = events_backend_factory_get_type (module);
}

void
eds_module_shutdown (void)
{
}

void
eds_module_list_types (const GType ** types, int *num_types)
{
    *types = scalix_cal_types;
    *num_types = 1;
}
