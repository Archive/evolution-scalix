/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef E_BOOK_BACKEND_SCALIX_H
#define E_BOOK_BACKEND_SCALIX_H

#include <libedata-book/e-book-backend-sync.h>

#define E_TYPE_BOOK_BACKEND_SCALIX              (e_book_backend_scalix_get_type ())
#define E_BOOK_BACKEND_SCALIX(o)                (G_TYPE_CHECK_INSTANCE_CAST ((o), E_TYPE_BOOK_BACKEND_SCALIX, EBookBackendScalix))
#define E_BOOK_BACKEND_SCALIX_CLASS(k)          (G_TYPE_CHECK_CLASS_CAST((k), E_TYPE_BOOK_BACKEND_SCALIX, EBookBackendScalixClass))
#define E_IS_BOOK_BACKEND_SCALIX(o)             (G_TYPE_CHECK_INSTANCE_TYPE ((o), E_TYPE_BOOK_BACKEND_SCALIX))
#define E_IS_BOOK_BACKEND_SCALIX_CLASS(k)       (G_TYPE_CHECK_CLASS_TYPE ((k), E_TYPE_BOOK_BACKEND_SCALIX))
#define E_BOOK_BACKEND_SCALIX_GET_CLASS(k)      (G_TYPE_INSTANCE_GET_CLASS ((obj), E_TYPE_BOOK_BACKEND_SCALIX, EBookBackendScalixClass))
#define E_BOOK_BACKEND_SCALIX_GET_PRIVATE(obj)  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), E_TYPE_BOOK_BACKEND_SCALIX, EBookBackendScalixPrivate))

typedef struct _EBookBackendScalix EBookBackendScalix;
typedef struct _EBookBackendScalixClass EBookBackendScalixClass;
typedef struct _EBookBackendScalixPrivate EBookBackendScalixPrivate;

struct _EBookBackendScalix {
    EBookBackendSync parent_object;
};

struct _EBookBackendScalixClass {
    EBookBackendSyncClass parent_class;
};

EBookBackend *e_book_backend_scalix_new (void);
GType e_book_backend_scalix_get_type (void);

#endif /* ! E_BOOK_BACKEND_SCALIX_H */
