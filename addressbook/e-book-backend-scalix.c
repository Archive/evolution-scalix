/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 *          Carsten Guenther <Carsten.Guenther@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n-lib.h>

#include <libebook/e-vcard.h>
#include <libebook/e-contact.h>

#include <libedataserver/e-sexp.h>
#include <libedata-book/e-book-backend-sexp.h>
#include <libedata-book/e-book-backend-summary.h>
#include <libedata-book/e-data-book.h>
#include <libedata-book/e-data-book-view.h>

#include <libescalix/scalix-container.h>
#include <libescalix/scalix-contact.h>
#include <libescalix/scalix-contact-list.h>

#include "e-book-backend-scalix.h"

/* Logging */
#include <glog/glog.h>

GLOG_CATEGORY (slbook, "book-backend", GLOG_FORMAT_FG_CYAN,
               "Addressbook backend");

struct _EBookBackendScalixPrivate {

    ScalixContainer *container;
    int mode;

};

/* ************************************************************************* */
#define query_is_search(__query) ! g_str_equal (__query, \
                                    "(contains \"x-evolution-any-field\" \"\")")

/* ************************************************************************* */

static void
container_object_changed_cb (ScalixContainer * container,
                             ScalixObject * old_object,
                             ScalixObject * object, gpointer data)
{
    EBookBackend *backend;

    backend = E_BOOK_BACKEND (data);

    if (!E_IS_CONTACT (object)) {
        g_warning ("Invalid object\n");
        return;
    }

    e_book_backend_notify_update (backend, E_CONTACT (object));
    e_book_backend_notify_complete (backend);
}

static void
container_object_added_cb (ScalixContainer * container,
                           ScalixObject * object, gpointer data)
{
    EBookBackend *backend;

    backend = E_BOOK_BACKEND (data);

    if (!E_IS_CONTACT (object)) {
        g_warning ("Invalid object\n");
        return;
    }

    e_book_backend_notify_update (backend, E_CONTACT (object));
    e_book_backend_notify_complete (backend);
}

static void
container_object_removed_cb (ScalixContainer * container,
                             ScalixObject * object, gpointer data)
{
    EBookBackend *backend;
    EContact *contact;
    const char *uid;

    backend = E_BOOK_BACKEND (data);

    if (!E_IS_CONTACT (object)) {
        g_warning ("Invalid object");
        return;
    }

    contact = E_CONTACT (object);
    uid = e_contact_get_const (contact, E_CONTACT_UID);

    g_assert (uid != NULL);

    e_book_backend_notify_remove (backend, uid);
    e_book_backend_notify_complete (backend);
}

/* ************************************************************************* */

static gpointer
go_online_thread (gpointer data)
{
    EBookBackend *backend;
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    gboolean res;

    bs = E_BOOK_BACKEND_SCALIX (data);
    backend = E_BOOK_BACKEND (bs);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    res = scalix_container_set_online (priv->container, TRUE);

    if (res == TRUE) {
        scalix_container_sync (priv->container);
    }

    GLOG_CAT_INFO (&slbook, "done (writable: %d)", res);

    return NULL;
}

static gboolean
go_online (EBookBackendScalix * bs)
{
    EBookBackendScalixPrivate *priv;
    EBookBackend *backend;
    gboolean res;

    backend = E_BOOK_BACKEND (bs);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);
    res = TRUE;

    g_thread_create (go_online_thread, bs, FALSE, NULL);

    return res;
}

static gboolean
go_offline (EBookBackendScalix * bs)
{
    EBookBackendScalixPrivate *priv;
    EBookBackend *backend;
    gboolean res;

    backend = E_BOOK_BACKEND (bs);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    res = scalix_container_set_online (priv->container, FALSE);

    return res;
}

/* ************************************************************************* */
static GNOME_Evolution_Addressbook_CallStatus
e_book_backend_scalix_load_source (EBookBackend * backend,
                                   ESource * source, gboolean only_if_exists)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    ScalixContainer *container;
    const char *prop_ssl;
    gchar *uri;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    if (priv->container != NULL) {
        /* Source already loaded! */
        return GNOME_Evolution_Addressbook_Success;
    }

    uri = e_source_get_uri (source);

    if ((prop_ssl = e_source_get_property (source, "use_ssl"))) {
		CamelURL *url;

		url = camel_url_new (uri, NULL);
		camel_url_set_param (url, "use_ssl", prop_ssl);
		g_free (uri);
		uri = camel_url_to_string (url, 0);
    }

    container = scalix_container_open (uri);
    g_free (uri);

    if (container == NULL) {
        return GNOME_Evolution_Addressbook_OtherError;
    }

    e_book_backend_set_is_loaded (E_BOOK_BACKEND (backend), TRUE);
    e_book_backend_set_is_writable (E_BOOK_BACKEND (backend), TRUE);

    if (only_if_exists == FALSE) {
        GLOG_CAT_DEBUG (&slbook, "Only if exists == FALSE");
        g_object_set (container, "only_if_exists", FALSE,
                      "type", "Contacts", NULL);
    }

    priv->container = container;

    // go online here?
    if (priv->mode == GNOME_Evolution_Addressbook_MODE_REMOTE) {
        go_online (bs);
    }

    g_signal_connect (container, "object_added",
                      G_CALLBACK (container_object_added_cb),
                      (gpointer) backend);

    g_signal_connect (container, "object_removed",
                      G_CALLBACK (container_object_removed_cb),
                      (gpointer) backend);

    g_signal_connect (container, "object_changed",
                      G_CALLBACK (container_object_changed_cb),
                      (gpointer) backend);

    return GNOME_Evolution_Addressbook_Success;
}

static EBookBackendSyncStatus
e_book_backend_scalix_authenticate_user (EBookBackendSync * backend,
                                         EDataBook * book,
                                         guint32 opid,
                                         const char *user,
                                         const char *passwd,
                                         const char *auth_method)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    if (priv->container == NULL) {
        return GNOME_Evolution_Addressbook_OtherError;
    }

    g_object_set (priv->container, "username", user, NULL);
    g_object_set (priv->container, "password", passwd, NULL);

    if (priv->mode == GNOME_Evolution_Addressbook_MODE_REMOTE) {
        go_online (bs);
    }

    /* check for errors here */
    return GNOME_Evolution_Addressbook_Success;
}

static void
e_book_backend_scalix_set_mode (EBookBackend * backend, int mode)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    gboolean online;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    GLOG_CAT_INFO (&slbook, "BOOK: set mode!");

    if (!e_book_backend_is_loaded (backend)) {
        priv->mode = mode;
        return;
    }

    online = (mode == GNOME_Evolution_Addressbook_MODE_REMOTE);

    if (online == TRUE) {
        online = go_online (bs);

    } else {
        online = !go_offline (bs);
    }

    e_book_backend_notify_connection_status (backend, online);
}

/* *************************************************************************  */

typedef struct {

    GList *obj_list;
    gboolean search_needed;
    gboolean return_objects;
    EBookBackend *backend;
    EBookBackendSExp *sexp;

} ScanContext;

static void
scan_objects (gpointer data, gpointer user_data)
{
    ScanContext *context;
    ScalixContact *contact;
    EBookBackendSExp *sexp;
    char *ostr;
    gboolean is_search;

    context = (ScanContext *) user_data;
    contact = SCALIX_CONTACT (data);
    sexp = context->sexp;
    is_search = context->search_needed;

    if (!is_search
        || e_book_backend_sexp_match_contact (sexp, E_CONTACT (contact))) {

        if (context->return_objects == TRUE) {
            context->obj_list =
                g_list_prepend (context->obj_list, g_object_ref (contact));
        } else {
            ostr = e_vcard_to_string (E_VCARD (contact), EVC_FORMAT_VCARD_30);
            context->obj_list = g_list_prepend (context->obj_list, ostr);
        }
    }
}

/* *************************************************************************  */

#define SCALIX_BOOK_VIEW_CLOSURE_ID "EBookBackendScalix.BookView::closure"

typedef struct {

    EBookBackendScalix *bs;
    GMutex *mutex;
    GCond *cond;
    GThread *thread;
    gboolean stopped;

} ScalixBackendSearchClosure;

static void
closure_destroy (ScalixBackendSearchClosure * closure)
{
    g_mutex_free (closure->mutex);
    g_cond_free (closure->cond);
    g_free (closure);
}

static ScalixBackendSearchClosure *
closure_init (EDataBookView * book_view, EBookBackendScalix * bs)
{
    ScalixBackendSearchClosure *closure;

    closure = g_new (ScalixBackendSearchClosure, 1);

    closure->bs = bs;
    closure->mutex = g_mutex_new ();
    closure->cond = g_cond_new ();
    closure->thread = NULL;
    closure->stopped = FALSE;

    g_object_set_data_full (G_OBJECT (book_view),
                            SCALIX_BOOK_VIEW_CLOSURE_ID,
                            closure, (GDestroyNotify) closure_destroy);

    return closure;
}

static ScalixBackendSearchClosure *
closure_get (EDataBookView * book_view)
{
    return g_object_get_data (G_OBJECT (book_view),
                              SCALIX_BOOK_VIEW_CLOSURE_ID);
}

static gpointer
book_view_thread (gpointer data)
{
    EBookBackendScalix *bs;
    gboolean stopped;
    const char *query;
    EDataBookView *book_view;
    EBookBackendScalixPrivate *priv;
    ScalixBackendSearchClosure *closure;
    GList *iter;
    ScanContext context;
    gboolean res;

    book_view = data;
    closure = closure_get (book_view);
    bs = closure->bs;
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);
    stopped = FALSE;

    bonobo_object_ref (book_view);

    if (priv->container == NULL) {
        e_data_book_view_notify_complete (book_view,
                                          GNOME_Evolution_Addressbook_AuthenticationRequired);

        bonobo_object_unref (book_view);
        return NULL;
    }

    query = e_data_book_view_get_card_query (book_view);
    context.sexp = e_book_backend_sexp_new (query);
    context.backend = E_BOOK_BACKEND (bs);
    context.obj_list = NULL;
    context.return_objects = TRUE;

    if (!(context.search_needed = query_is_search (query))) {
        e_data_book_view_notify_status_message (book_view, _("Loading..."));
    } else {
        e_data_book_view_notify_status_message (book_view, _("Searching..."));
    }

    g_mutex_lock (closure->mutex);
    g_cond_signal (closure->cond);
    g_mutex_unlock (closure->mutex);

    res = scalix_container_foreach (priv->container, scan_objects, &context);

    if (res == FALSE) {
        e_data_book_view_notify_complete (book_view,
                                          GNOME_Evolution_Addressbook_OtherError);
        bonobo_object_unref (book_view);
        return NULL;
    }

    for (iter = context.obj_list; iter; iter = iter->next) {

        g_mutex_lock (closure->mutex);
        stopped = closure->stopped;
        g_mutex_unlock (closure->mutex);

        if (stopped) {
            break;
        }

        e_data_book_view_notify_update (book_view, iter->data);
        g_object_unref (iter->data);
    }

    /*FIXME: we leek objects if stopped */
    g_list_free (context.obj_list);

    if (!stopped) {
        e_data_book_view_notify_complete (book_view,
                                          GNOME_Evolution_Addressbook_Success);
    }

    bonobo_object_unref (book_view);

    return NULL;
}

static void
e_book_backend_scalix_start_book_view (EBookBackend * backend,
                                       EDataBookView * book_view)
{
    ScalixBackendSearchClosure *closure;

    closure = closure_init (book_view, E_BOOK_BACKEND_SCALIX (backend));

    g_mutex_lock (closure->mutex);
    closure->thread = g_thread_create (book_view_thread, book_view, TRUE, NULL);
    g_cond_wait (closure->cond, closure->mutex);

    /* at this point we know the book view thread is actually running */
    g_mutex_unlock (closure->mutex);
}

static void
e_book_backend_scalix_stop_book_view (EBookBackend * backend,
                                      EDataBookView * book_view)
{
    ScalixBackendSearchClosure *closure = closure_get (book_view);
    gboolean need_join = FALSE;

    g_mutex_lock (closure->mutex);

    if (!closure->stopped) {
        need_join = TRUE;
    }

    closure->stopped = TRUE;

    g_mutex_unlock (closure->mutex);

    if (need_join) {
        g_thread_join (closure->thread);
    }
}

/* ************************************************************************** */

static EBookBackendSyncStatus
e_book_backend_scalix_create_contact (EBookBackendSync * backend,
                                      EDataBook * book,
                                      guint32 opid,
                                      const char *vcard, EContact ** contact)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    ScalixObject *object;
    EContact *econtact;
    gboolean res;
    char *uid;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    econtact = e_contact_new_from_vcard (vcard);
    gboolean is_list =
        GPOINTER_TO_INT (e_contact_get (econtact, E_CONTACT_IS_LIST));

    if (is_list == TRUE) {
        object = SCALIX_OBJECT (scalix_contact_list_new (vcard));
    } else {
        object = SCALIX_OBJECT (scalix_contact_new (vcard));
    }

    if (object == NULL) {
        return GNOME_Evolution_Addressbook_OtherError;
    }

    res = scalix_container_add_object (priv->container, object);

    if (res == FALSE) {
        g_object_unref (object);
        return GNOME_Evolution_Addressbook_OtherError;
    }

    g_object_get (object, "uid", &uid, NULL);
    g_object_unref (object);

    object = scalix_container_refresh_object (priv->container, uid);

    if (object == NULL) {
        return GNOME_Evolution_Addressbook_OtherError;
    }

    *contact = E_CONTACT (object);

    return GNOME_Evolution_Addressbook_Success;
}

static EBookBackendSyncStatus
e_book_backend_scalix_modify_contact (EBookBackendSync * backend,
                                      EDataBook * book,
                                      guint32 opid,
                                      const char *vcard, EContact ** contact)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    ScalixObject *object;
    gboolean res;
    EContact *econtact;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    econtact = e_contact_new_from_vcard (vcard);
    gboolean is_list =
        GPOINTER_TO_INT (e_contact_get (econtact, E_CONTACT_IS_LIST));

    if (is_list == TRUE) {
        object = SCALIX_OBJECT (scalix_contact_list_new (vcard));
    } else {
        object = SCALIX_OBJECT (scalix_contact_new (vcard));
    }

    if (object == NULL) {
        return GNOME_Evolution_Addressbook_OtherError;
    }

    /* REVIEW: do we have to ask the cache for the object here first?
     * (to "save" the mapi stuff?) */

    res = scalix_container_update_object (priv->container, object, TRUE);

    if (res == FALSE) {
        g_object_unref (object);
        return GNOME_Evolution_Addressbook_OtherError;
    }

    *contact = E_CONTACT (object);

    return GNOME_Evolution_Addressbook_Success;

}

static EBookBackendSyncStatus
e_book_backend_scalix_get_contact (EBookBackendSync * backend,
                                   EDataBook * book,
                                   guint32 opid, const char *uid, char **vcard)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    ScalixObject *object;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    object = scalix_container_get_object (priv->container, uid);

    if (object == NULL || !E_IS_VCARD (object)) {
        return GNOME_Evolution_Addressbook_ContactNotFound;
    }

    *vcard = e_vcard_to_string (E_VCARD (object), EVC_FORMAT_VCARD_30);
    g_object_unref (object);

    return GNOME_Evolution_Addressbook_Success;
}

static EBookBackendSyncStatus
e_book_backend_scalix_remove_contacts (EBookBackendSync * backend,
                                       EDataBook * book,
                                       guint32 opid,
                                       GList * id_list, GList ** removed_ids)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    EBookBackendSyncStatus result;
    GList *iter;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    *removed_ids = NULL;

    for (iter = id_list; iter; iter = g_list_next (iter)) {
        gboolean res;
        char *id;

        id = (char *) iter->data;

        res = scalix_container_remove_id (priv->container, id);

        if (res == TRUE) {
            *removed_ids = g_list_prepend (*removed_ids, id);
        }
    }

    if (*removed_ids != NULL) {
        result = GNOME_Evolution_Addressbook_Success;
    } else {
        result = GNOME_Evolution_Addressbook_OtherError;
    }

    return result;
}

static EBookBackendSyncStatus
e_book_backend_scalix_remove (EBookBackendSync * backend,
                              EDataBook * book, guint32 opid)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    if (scalix_container_remove (priv->container)) {
        return GNOME_Evolution_Addressbook_Success;
    } else {
        return GNOME_Evolution_Addressbook_OtherError;
    }
}

static EBookBackendSyncStatus
e_book_backend_scalix_get_contact_list (EBookBackendSync * backend,
                                        EDataBook * book,
                                        guint32 opid,
                                        const char *query, GList ** contacts)
{
    EBookBackendScalix *bs;
    EBookBackendScalixPrivate *priv;
    ScanContext context;

    bs = E_BOOK_BACKEND_SCALIX (backend);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (bs);

    context.sexp = e_book_backend_sexp_new (query);
    context.search_needed = query_is_search (query);
    context.backend = E_BOOK_BACKEND (bs);
    context.obj_list = NULL;
    context.return_objects = FALSE;

    scalix_container_foreach (priv->container, scan_objects, &context);

    *contacts = context.obj_list;

    if (*contacts == NULL) {
        return GNOME_Evolution_Addressbook_ContactNotFound;
    }

    return GNOME_Evolution_Addressbook_Success;
}

static char *
e_book_backend_scalix_get_static_capabilities (EBookBackend * backend)
{
    return g_strdup ("net,bulk-removes,do-initial-query,contact-lists");
}

static EBookBackendSyncStatus
e_book_backend_scalix_get_supported_fields (EBookBackendSync * backend,
                                            EDataBook * book,
                                            guint32 opid, GList ** fields_out)
{
    *fields_out = scalix_contact_get_fields ();
    return GNOME_Evolution_Addressbook_Success;
}

static EBookBackendSyncStatus
e_book_backend_scalix_get_required_fields (EBookBackendSync * backend,
                                           EDataBook * book,
                                           guint32 opid, GList ** fields)
{
    *fields = NULL;

    *fields =
        g_list_append (*fields,
                       g_strdup (e_contact_field_name (E_CONTACT_FILE_AS)));
    *fields =
        g_list_append (*fields,
                       g_strdup (e_contact_field_name (E_CONTACT_FULL_NAME)));
    *fields =
        g_list_append (*fields,
                       g_strdup (e_contact_field_name (E_CONTACT_FAMILY_NAME)));

    return GNOME_Evolution_Addressbook_Success;
}

static EBookBackendSyncStatus
e_book_backend_scalix_get_supported_auth_methods (EBookBackendSync * backend,
                                                  EDataBook * book,
                                                  guint32 opid,
                                                  GList ** methods)
{
    *methods = g_list_append (*methods, g_strdup ("plain/password"));
    return GNOME_Evolution_Addressbook_Success;
}

/* ************************************************************************ */

/* GObject Stuff */

G_DEFINE_TYPE (EBookBackendScalix,
               e_book_backend_scalix, E_TYPE_BOOK_BACKEND_SYNC);

EBookBackend *
e_book_backend_scalix_new (void)
{
    EBookBackendScalix *backend;

    backend = g_object_new (E_TYPE_BOOK_BACKEND_SCALIX, NULL);

    return E_BOOK_BACKEND (backend);
}

static void
e_book_backend_scalix_dispose (GObject * object)
{
    EBookBackendScalix *ebs;
    EBookBackendScalixPrivate *priv;

    ebs = E_BOOK_BACKEND_SCALIX (object);
    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (ebs);

    g_object_unref (priv->container);
}

static void
e_book_backend_scalix_class_init (EBookBackendScalixClass * klass)
{

    GObjectClass *object_class;
    EBookBackendSyncClass *sync_class;
    EBookBackendClass *backend_class;

    e_book_backend_scalix_parent_class = g_type_class_peek_parent (klass);

    g_type_class_add_private (klass, sizeof (EBookBackendScalixPrivate));

    object_class = G_OBJECT_CLASS (klass);
    backend_class = E_BOOK_BACKEND_CLASS (klass);
    sync_class = E_BOOK_BACKEND_SYNC_CLASS (klass);

    /* Set the virtual methods. */
    object_class->dispose = e_book_backend_scalix_dispose;

    backend_class->load_source = e_book_backend_scalix_load_source;
    backend_class->get_static_capabilities =
        e_book_backend_scalix_get_static_capabilities;
    backend_class->start_book_view = e_book_backend_scalix_start_book_view;
    backend_class->stop_book_view = e_book_backend_scalix_stop_book_view;
    backend_class->set_mode = e_book_backend_scalix_set_mode;
    sync_class->remove_sync = e_book_backend_scalix_remove;
    sync_class->authenticate_user_sync =
        e_book_backend_scalix_authenticate_user;
    sync_class->create_contact_sync = e_book_backend_scalix_create_contact;
    sync_class->get_contact_list_sync = e_book_backend_scalix_get_contact_list;
    sync_class->get_supported_fields_sync =
        e_book_backend_scalix_get_supported_fields;
    sync_class->remove_contacts_sync = e_book_backend_scalix_remove_contacts;
    sync_class->modify_contact_sync = e_book_backend_scalix_modify_contact;
    sync_class->get_contact_sync = e_book_backend_scalix_get_contact;
    sync_class->get_supported_auth_methods_sync =
        e_book_backend_scalix_get_supported_auth_methods;
    sync_class->get_required_fields_sync =
        e_book_backend_scalix_get_required_fields;
#if 0
    parent_class->get_changes = e_book_backend_scalix_get_changes;
#endif

}

static void
e_book_backend_scalix_init (EBookBackendScalix * backend)
{
    EBookBackendScalixPrivate *priv;

    priv = E_BOOK_BACKEND_SCALIX_GET_PRIVATE (backend);

    priv->mode = GNOME_Evolution_Addressbook_MODE_LOCAL;
    priv->container = NULL;
}
