/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include <config.h>
#include <glib.h>

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <libescalix/scalix-appointment.h>
#include <libescalix/scalix-object-cache.h>
#include <libescalix/scalix-oc-entry.h>

static gboolean verbose = FALSE;

static GOptionEntry entries[] = {
    {"verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, "Be verbose", NULL},
    {NULL}
};

typedef struct {
    char *name;
    int (*func) (ScalixObjectCache * cache, int argc, char **argv);
} Command;

/* ADD */

int
do_add (ScalixObjectCache * cache, int argc, char **argv)
{
    ScalixObject *object;
    gboolean res;
    FILE *in;
    char text[4096];
    int n, total;
    int iuid;

    iuid = atoi (argv[3]);

    memset (text, 0, sizeof (text));
    fread (text, sizeof (text), 1, stdin);

    if (verbose) {
        g_print ("got from stdin [%s]\n", text);
    }

    object = g_object_new (SCALIX_TYPE_APPOINTMENT, NULL);

    scalix_object_deserialize (object, text);

    res = scalix_object_cache_put (cache, object, iuid);

    if (res == FALSE) {
        g_print ("Could not add object\n");
    }

    return 0;
}

int
do_get (ScalixObjectCache * cache, int argc, char **argv)
{
    char *ouid = argv[3];
    ScalixObject *object;
    char *string;

    object = scalix_object_cache_get (cache, ouid);

    if (object == NULL) {
        g_print ("No such object or db error!");
    }

    string = scalix_object_serialize (object);

    g_print ("%s", string);

    g_free (string);

    return 0;
}

int
do_del (ScalixObjectCache * cache, int argc, char **argv)
{
    char *ouid = argv[3];
    ScalixObject *object;
    gboolean result;

    if (verbose) {
        g_print ("Trying to delete %s\n", ouid);
    }

    result = scalix_object_cache_remove_entry (cache, ouid);

    if (result == FALSE) {
        g_print ("Could not delete database entry\n");
    }

    return 0;
}

int
do_mdel (ScalixObjectCache * cache, int argc, char **argv)
{
    char *ouid = argv[3];
    ScalixObject *object;
    gboolean result;

    if (verbose) {
        g_print ("Trying to delete %s\n", ouid);
    }

    result = scalix_object_cache_set_flags (cache, ouid, SC_OBJECT_REMOVED);

    if (result == FALSE) {
        g_print ("Could not delete database entry\n");
    }

    return 0;
}

int
do_list (ScalixObjectCache * cache, int argc, char **argv)
{
    EIterator *iter;

    iter = scalix_object_cache_get_iterator (cache);

    while (e_iterator_is_valid (iter)) {
        ScalixOCEntry *entry;
        char *ouid;
        int ipm_type;
        int iuid;
        int flags;

        entry = SCALIX_OC_ENTRY (e_iterator_get (iter));

        g_object_get (entry,
                      "ipm-type", &ipm_type,
                      "object-uid", &ouid,
                      "imap-uid", &iuid, "flags", &flags, NULL);

        g_print ("%d %d %s %d\n", ipm_type, iuid, ouid, flags);

        e_iterator_next (iter);
    }

    g_object_unref (iter);

    return 0;
}

int
do_lookup (ScalixObjectCache * cache, int argc, char **argv)
{
    int iuid = atoi (argv[3]);
    char *ouid;

    ouid = scalix_object_cache_ouid_lookup (cache, iuid);

    g_print ("%d -> %s\n", iuid, ouid);

    return 0;
}

Command cmds[] = {
    {"add", do_add}
    ,
    {"get", do_get}
    ,
    {"del", do_del}
    ,
    {"mdel", do_mdel}
    ,
    {"list", do_list}
    ,
    {"lookup", do_lookup}
    ,
    {NULL, NULL}
};

int
main (int argc, char **argv)
{
    GOptionContext *octx;
    GError *error = NULL;
    char *uri;
    char *path;
    Command *iter;
    ScalixObjectCache *cache;

    octx = g_option_context_new ("Command line tool for scalix server");
    g_option_context_set_help_enabled (octx, TRUE);
    g_option_context_add_main_entries (octx, entries, GETTEXT_PACKAGE);
    g_option_context_parse (octx, &argc, &argv, &error);

    if (argc < 3) {
        g_print ("Wrong usage. See %s --help for help\n", argv[0]);
        exit (1);
    }

    g_type_init ();

    if (!g_thread_supported ()) {
        g_thread_init (NULL);
    }

    path = g_build_filename (g_get_home_dir (), ".evolution", "scalix", NULL);

    if (camel_init (path, FALSE)) {
        return FALSE;
    }

    camel_provider_init ();

    uri = argv[1];

    if (verbose) {
        g_print ("Scalix Cache command line tool\n");
    }

    cache = scalix_object_cache_open (uri);

    for (iter = cmds; iter->name != NULL; iter++) {
        if (g_str_equal (argv[2], iter->name)) {
            break;
        }
    }

    if (iter->name == NULL) {
        g_print ("Unkown command!\n");
        exit (2);
    }

    iter->func (cache, argc, argv);

    return 0;
}
