/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#include <config.h>
#include <glib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libescalix/scalix-container.h>
#include <libescalix/scalix-appointment.h>

#include <libedataserverui/e-passwords.h>

static gboolean verbose = FALSE;
static gboolean offline = FALSE;

static GOptionEntry entries[] = {
    {"verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, "Be verbose", NULL},
    {"offline", 'o', 0, G_OPTION_ARG_NONE, &offline, "Operate in offline mode",
     NULL},
    {NULL}
};

typedef struct {
    char *name;
    int (*func) (ScalixContainer * container, int argc, char **argv);
} Command;

/* SHOW */

static int
do_show (ScalixContainer * container, int argc, char **argv)
{
    char *uid = argv[3];
    char *string;
    ScalixObject *object;

    if (verbose) {
        g_print ("show for %s\n", uid);
    }

    object = scalix_container_get_object (container, uid);

    if (object == NULL) {
        g_print ("Could not get object\n");
        return -1;
    }

    string = scalix_object_to_string (object);

    if (verbose)
        g_print ("----------------------------\n");

    if (verbose) {
        char *iuid;

        g_object_get (object, "imap-uid", &iuid, NULL);
        g_print ("Object Imap-Uid: %s\n", iuid);
        g_free (iuid);
    }

    g_print ("%s\n", string);
    g_free (string);
    if (verbose)
        g_print ("----------------------------\n");
    return 0;
}

/* REMOVE */

static int
do_remove (ScalixContainer * container, int argc, char **argv)
{
    char *uid = argv[3];
    gboolean success;

    if (verbose) {
        g_print ("remove for %s\n", uid);
    }

    success = scalix_container_remove_id (container, uid);

    if (!success) {
        g_print ("Could not remove object\n");
    }

    return 0;
}

/* ADD */

static int
do_add (ScalixContainer * container, int argc, char **argv)
{
    ScalixAppointment *app;
    gboolean res;
    char text[4096];

    memset (text, 0, sizeof (text));
    fread (text, sizeof (text), 1, stdin);

    if (verbose) {
        g_print ("got from stdin [%s]\n", text);
    }

    app = g_object_new (SCALIX_TYPE_APPOINTMENT, NULL);

    g_object_set (app, "ical", text, NULL);

    res = scalix_container_add_object (container, SCALIX_OBJECT (app));

    if (res == FALSE) {
        g_print ("Could not add object\n");
        return -1;
    }

    return 0;
}

static void
container_object_changed_cb (ScalixContainer * container,
                             ScalixObject * old_object,
                             ScalixObject * object, gpointer data)
{
    char *s1, *s2;

    g_print ("object changed cb\n");

    /* */

    s1 = scalix_object_serialize (old_object);
    s2 = scalix_object_serialize (object);

    g_print ("old_object: %s\n ------ new_object: %s\n", s1, s2);

    g_free (s1);
    g_free (s2);

}

static void
container_object_added_cb (ScalixContainer * container,
                           ScalixObject * object, gpointer data)
{
    char *s1;

    g_print ("object added cb\n");

    s1 = scalix_object_serialize (object);

    g_print ("%s\n", s1);
    g_free (s1);
}

static void
container_object_removed_cb (ScalixContainer * container,
                             ScalixObject * object, gpointer data)
{
    char *s1;

    g_print ("object removed cb");

    s1 = scalix_object_serialize (object);

    g_print ("%s\n", s1);
    g_free (s1);
}

/* watch */
static int
do_watch (ScalixContainer * container, int argc, char **argv)
{

    GMainContext *ctx;

    ctx = g_main_context_default ();

    g_signal_connect (container, "object_added",
                      G_CALLBACK (container_object_added_cb), NULL);

    g_signal_connect (container, "object_removed",
                      G_CALLBACK (container_object_removed_cb), NULL);

    g_signal_connect (container, "object_changed",
                      G_CALLBACK (container_object_changed_cb), NULL);

    while (TRUE) {
        g_main_context_iteration (ctx, TRUE);
    }

    return 0;
}

/* UPDATE */

static int
do_update (ScalixContainer * container, int argc, char **argv)
{
    ScalixAppointment *app;
    gboolean res;
    char text[4096];
    char *uid = NULL;

    memset (text, 0, sizeof (text));
    fread (text, sizeof (text), 1, stdin);

    if (verbose) {
        g_print ("got from stdin [%s]\n", text);
    }

    app = g_object_new (SCALIX_TYPE_APPOINTMENT, NULL);

    scalix_object_deserialize (SCALIX_OBJECT (app), text);

    res = scalix_container_update_object (container, SCALIX_OBJECT (app), TRUE);

    if (res == FALSE) {
        g_print ("Could not update object\n");
    }

    if (verbose) {
        g_object_get (app, "imap-uid", &uid, NULL);
        g_print ("Update completed! New imap uid: %s\n", uid);
    }

    return 0;
}

/* SYNC */

static int
do_sync (ScalixContainer * container, int argc, char **argv)
{
    gboolean res;

    res = scalix_container_sync (container);

    return res;
}

static void
scan_objects (gpointer data, gpointer user_data)
{
    char *uid;

    ScalixObject *object = SCALIX_OBJECT (data);

    g_object_get (object, "uid", &uid, NULL);

    g_print ("%s\n", uid);
    g_free (uid);
}

static int
do_list (ScalixContainer * container, int argc, char **argv)
{

    scalix_container_foreach (container, scan_objects, NULL);

    return 0;
}

Command cmds[] = {

    {"show", do_show}
    ,
    {"remove", do_remove}
    ,
    {"add", do_add}
    ,
    {"watch", do_watch}
    ,
    {"update", do_update}
    ,
    {"sync", do_sync}
    ,
    {"list", do_list}
    ,
    {NULL, NULL}
};

static char *
create_key (CamelURL * url)
{
    char *key;
    char *user;
    char *authmech;

    if (url->protocol == NULL || url->host == NULL || url->user == NULL) {
        return NULL;
    }

    user = camel_url_encode (url->user, ":;@/");

    if (url->authmech != NULL) {
        authmech = camel_url_encode (url->authmech, ":@/");
    } else {
        authmech = NULL;
    }

    if (url->port) {
        key = g_strdup_printf ("%s://%s%s%s@%s:%d/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host, url->port);
    } else {
        key = g_strdup_printf ("%s://%s%s%s@%s/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host);
    }

    g_free (user);
    g_free (authmech);

    return key;
}

int
main (int argc, char **argv)
{
    GOptionContext *octx;
    GError *error = NULL;
    char *uri;
    char *path;
    ScalixContainer *container;
    Command *iter;

    octx = g_option_context_new ("Command line tool for scalix server");
    g_option_context_set_help_enabled (octx, TRUE);
    g_option_context_add_main_entries (octx, entries, GETTEXT_PACKAGE);
    g_option_context_parse (octx, &argc, &argv, &error);

    if (argc < 3) {
        g_print ("Wrong usage. See %s --help for help\n", argv[0]);
        exit (1);
    }

    g_thread_init (NULL);
    g_type_init ();
    libescalix_init (TRUE);

    uri = argv[1];

    container = scalix_container_open (uri);

    if (verbose) {
        char *uri;

        g_object_get (container, "uri", &uri, NULL);
        g_print ("uri: %s\n", uri);
    }

    if (!offline) {
        CamelURL *curl;
        char *key;

        /* set to container to online mode */
        e_passwords_set_online (TRUE);

        g_object_set (container, "online", TRUE, NULL);

    }

    for (iter = cmds; iter->name != NULL; iter++) {
        if (g_str_equal (argv[2], iter->name)) {
            break;
        }
    }

    if (iter->name == NULL) {
        g_print ("Unkown command!\n");
        exit (2);
    }

    iter->func (container, argc, argv);

    e_passwords_shutdown ();
    g_object_unref (container);

    return 0;
}
