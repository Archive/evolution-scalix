/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "camel-session.h"

static char *
get_password (CamelSession * session, const char *prompt, guint32 flags,
              CamelService * service, const char *item, CamelException * ex)
{
    return NULL;
}

static void
forget_password (CamelSession * session, CamelService * service,
                 const char *item, CamelException * ex)
{
}

static gboolean
alert_user (CamelSession * session, CamelSessionAlertType type,
            const char *prompt, gboolean cancel)
{
    return FALSE;
}

static CamelFilterDriver *
get_filter_driver (CamelSession * session, const char *type,
                   CamelException * ex)
{
    return NULL;
}

static void
class_init (ECamelScalixSessionClass * camel_scalix_session_class)
{
    CamelSessionClass *camel_session_class =
        CAMEL_SESSION_CLASS (camel_scalix_session_class);

    camel_session_class->get_password = get_password;
    camel_session_class->forget_password = forget_password;
    camel_session_class->alert_user = alert_user;
    camel_session_class->get_filter_driver = get_filter_driver;
}

CamelType
e_camel_scalix_session_get_type (void)
{
    static CamelType type = CAMEL_INVALID_TYPE;

    if (type == CAMEL_INVALID_TYPE) {
        type = camel_type_register (camel_session_get_type (),
                                    "CamelScalixSession",
                                    sizeof (ECamelScalixSession),
                                    sizeof (ECamelScalixSessionClass),
                                    (CamelObjectClassInitFunc) class_init,
                                    NULL, NULL, NULL);
    }

    return type;
}

CamelSession *
camel_scalix_session_new (const char *path)
{
    CamelSession *session;

    session = CAMEL_SESSION (camel_object_new (E_CAMEL_SCALIX_SESSION_TYPE));

    camel_session_construct (session, path);

    return session;
}
