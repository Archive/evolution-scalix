/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifndef __CAMEL_IMAP_STORE_SUMMARY_H__
#define __CAMEL_IMAP_STORE_SUMMARY_H__

#include <camel/camel-store-summary.h>
#include "camel-scalix-engine.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define CAMEL_SCALIX_STORE_SUMMARY(obj)         CAMEL_CHECK_CAST (obj, camel_scalix_store_summary_get_type (), CamelSCALIXStoreSummary)
#define CAMEL_SCALIX_STORE_SUMMARY_CLASS(klass) CAMEL_CHECK_CLASS_CAST (klass, camel_scalix_store_summary_get_type (), CamelSCALIXStoreSummaryClass)
#define CAMEL_IS_SCALIX_STORE_SUMMARY(obj)      CAMEL_CHECK_TYPE (obj, camel_scalix_store_summary_get_type ())

typedef struct _CamelSCALIXStoreSummary      CamelSCALIXStoreSummary;
typedef struct _CamelSCALIXStoreSummaryClass CamelSCALIXStoreSummaryClass;

typedef struct _CamelSCALIXStoreInfo CamelSCALIXStoreInfo;

enum {
	CAMEL_SCALIX_STORE_INFO_FULL_NAME = CAMEL_STORE_INFO_LAST,
	CAMEL_SCALIX_STORE_INFO_LAST,
};


struct _CamelFolderInfo;


struct _CamelSCALIXStoreInfo {
	CamelStoreInfo info;
};

struct _CamelSCALIXStoreSummary {
	CamelStoreSummary summary;
	
	struct _CamelSCALIXStoreSummaryPrivate *priv;
	
	/* header info */
	guint32 version;
	
	CamelSCALIXNamespaceList *namespaces;
	guint32 capa;
};

struct _CamelSCALIXStoreSummaryClass {
	CamelStoreSummaryClass summary_class;
};


CamelType camel_scalix_store_summary_get_type (void);

CamelSCALIXStoreSummary *camel_scalix_store_summary_new (void);

void camel_scalix_store_summary_set_capabilities (CamelSCALIXStoreSummary *s, guint32 capa);
void camel_scalix_store_summary_set_namespaces (CamelSCALIXStoreSummary *s, const CamelSCALIXNamespaceList *ns);

/* add the info to the cache if we don't already have it, otherwise do nothing */
void camel_scalix_store_summary_note_info (CamelSCALIXStoreSummary *s, struct _CamelFolderInfo *fi);

void camel_scalix_store_summary_unnote_info (CamelSCALIXStoreSummary *s, struct _CamelFolderInfo *fi);

struct _CamelFolderInfo *camel_scalix_store_summary_get_folder_info (CamelSCALIXStoreSummary *s, const char *top, guint32 flags);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_STORE_SUMMARY_H__ */
