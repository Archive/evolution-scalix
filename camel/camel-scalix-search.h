/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@novell.com>
 *           Michael Zucchi <notzed@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifndef __CAMEL_SCALIX_SEARCH_H__
#define __CAMEL_SCALIX_SEARCH_H__

#include <libedataserver/e-msgport.h>

#include <camel/camel-data-cache.h>
#include <camel/camel-folder-search.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define CAMEL_SCALIX_SEARCH_TYPE         (camel_scalix_search_get_type ())
#define CAMEL_SCALIX_SEARCH(obj)         CAMEL_CHECK_CAST (obj, camel_scalix_search_get_type (), CamelSCALIXSearch)
#define CAMEL_SCALIX_SEARCH_CLASS(klass) CAMEL_CHECK_CLASS_CAST (klass, camel_scalix_search_get_type (), CamelSCALIXSearchClass)
#define CAMEL_IS_SCALIX_SEARCH(obj)      CAMEL_CHECK_TYPE (obj, camel_scalix_search_get_type ())

typedef struct _CamelSCALIXSearch CamelSCALIXSearch;
typedef struct _CamelSCALIXSearchClass CamelSCALIXSearchClass;

struct _CamelSCALIXEngine;

struct _CamelSCALIXSearch {
	CamelFolderSearch parent_object;
	
	struct _CamelSCALIXEngine *engine;
	
	guint32 lastuid;	/* current 'last uid' for the folder */
	guint32 validity;	/* validity of the current folder */
	
	CamelDataCache *cache;	/* disk-cache for searches */
	
	/* cache of body search matches */
	EDList matches;
	GHashTable *matches_hash;
	unsigned int matches_count;
};

struct _CamelSCALIXSearchClass {
	CamelFolderSearchClass parent_class;
	
};


CamelType camel_scalix_search_get_type (void);

CamelFolderSearch *camel_scalix_search_new (struct _CamelSCALIXEngine *engine, const char *cachedir);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_SEARCH_H__ */
