/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Camel
 *  Copyright (C) 1999-2004 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifndef __CAMEL_SCALIX_SUMMARY_H__
#define __CAMEL_SCALIX_SUMMARY_H__

#include <sys/types.h>

#include <camel/camel-folder.h>
#include <camel/camel-folder-summary.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define CAMEL_TYPE_SCALIX_SUMMARY            (camel_scalix_summary_get_type ())
#define CAMEL_SCALIX_SUMMARY(obj)            (CAMEL_CHECK_CAST ((obj), CAMEL_TYPE_SCALIX_SUMMARY, CamelSCALIXSummary))
#define CAMEL_SCALIX_SUMMARY_CLASS(klass)    (CAMEL_CHECK_CLASS_CAST ((klass), CAMEL_TYPE_SCALIX_SUMMARY, CamelSCALIXSummaryClass))
#define CAMEL_IS_SCALIX_SUMMARY(obj)         (CAMEL_CHECK_TYPE ((obj), CAMEL_TYPE_SCALIX_SUMMARY))
#define CAMEL_IS_SCALIX_SUMMARY_CLASS(klass) (CAMEL_CHECK_CLASS_TYPE ((klass), CAMEL_TYPE_SCALIX_SUMMARY))
#define CAMEL_SCALIX_SUMMARY_GET_CLASS(obj)  (CAMEL_CHECK_GET_CLASS ((obj), CAMEL_TYPE_FOLDER_SUMMARY, CamelSCALIXSummaryClass))

typedef struct _CamelSCALIXMessageInfo CamelSCALIXMessageInfo;
typedef struct _CamelSCALIXMessageContentInfo CamelSCALIXMessageContentInfo;

typedef struct _CamelSCALIXSummary CamelSCALIXSummary;
typedef struct _CamelSCALIXSummaryClass CamelSCALIXSummaryClass;

enum {
   	CAMEL_SCALIX_MESSAGE_RECENT = 1 << 17,
   	CAMEL_SCALIX_MESSAGE_NJUNK  = 1 << 18,
   
	CAMEL_SCALIX_MESSAGE_LABEL1 = 1 << 19,
	CAMEL_SCALIX_MESSAGE_LABEL2 = 1 << 20,
	CAMEL_SCALIX_MESSAGE_LABEL3 = 1 << 21,
	CAMEL_SCALIX_MESSAGE_LABEL4 = 1 << 22,
	CAMEL_SCALIX_MESSAGE_LABEL5 = 1 << 23,
    	

	CAMEL_SCALIX_MESSAGE_LABEL_MASK = 31<<19

};

enum {
	CAMEL_SCALIX_SUMMARY_HAVE_MLIST = (1 << 8)
};

struct _CamelSCALIXMessageInfo {
	CamelMessageInfoBase info;
	
	guint32 server_flags;
};

struct _CamelSCALIXMessageContentInfo {
	CamelMessageContentInfo info;
	
};

struct _CamelSCALIXSummary {
	CamelFolderSummary parent_object;
	
	guint32 version;
	
	guint32 exists;
	guint32 recent;
	guint32 unseen;
	
	guint32 uidvalidity;
	
	guint uidvalidity_changed:1;
	guint update_flags:1;
};

struct _CamelSCALIXSummaryClass {
	CamelFolderSummaryClass parent_class;
	
};


CamelType camel_scalix_summary_get_type (void);

CamelFolderSummary *camel_scalix_summary_new (CamelFolder *folder);

void camel_scalix_summary_set_exists (CamelFolderSummary *summary, guint32 exists);
void camel_scalix_summary_set_recent (CamelFolderSummary *summary, guint32 recent);
void camel_scalix_summary_set_unseen (CamelFolderSummary *summary, guint32 unseen);
void camel_scalix_summary_set_uidnext (CamelFolderSummary *summary, guint32 uidnext);

void camel_scalix_summary_set_uidvalidity (CamelFolderSummary *summary, guint32 uidvalidity);

void camel_scalix_summary_expunge (CamelFolderSummary *summary, int seqid);

int camel_scalix_summary_flush_updates (CamelFolderSummary *summary, CamelException *ex);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_SUMMARY_H__ */
