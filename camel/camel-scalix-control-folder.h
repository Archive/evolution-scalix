
#ifndef CAMEL_SCALIX_CONTROL_FOLDER_H
#define CAMEL_SCALIX_CONTROL_FOLDER_H

#include <glib.h>
#include <camel/camel-folder.h>

G_BEGIN_DECLS

#define CAMEL_TYPE_SCALIX_CONTROL_FOLDER            (camel_scalix_cfolder_get_type ())
#define CAMEL_SCALIX_CONTROL_FOLDER(obj)            (CAMEL_CHECK_CAST ((obj), CAMEL_TYPE_SCALIX_CONTROL_FOLDER, CamelScalixControlFolder))
#define CAMEL_SCALIX_CONTROL_FOLDER_CLASS(klass)    (CAMEL_CHECK_CLASS_CAST ((klass), CAMEL_TYPE_SCALIX_CONTROL_FOLDER, CamelScalixControlFolderClass))
#define CAMEL_IS_SCALIX_CONTROL_FOLDER(obj)         (CAMEL_CHECK_TYPE ((obj), CAMEL_TYPE_SCALIX_CONTROL_FOLDER))
#define CAMEL_IS_SCALIX_CONTROL_FOLDER_CLASS(klass) (CAMEL_CHECK_CLASS_TYPE ((klass), CAMEL_TYPE_SCALIX_CONTROL_FOLDER))
#define CAMEL_SCALIX_CONTROL_FOLDER_GET_CLASS(obj)  (CAMEL_CHECK_GET_CLASS ((obj), CAMEL_TYPE_SCALIX_CONTROL_FOLDER, CamelScalixControlFolderClass))



typedef struct _CamelScalixControlFolder  CamelScalixControlFolder;
typedef struct _CamelScalixControlFolderClass   CamelScalixControlFolderClass;


struct _CamelScalixControlFolder {
	CamelFolder parent;

};


struct _CamelScalixControlFolderClass {
	CamelFolderClass parent_class;
};

CamelType    camel_scalix_cfolder_get_type (void);
CamelFolder *camel_scalix_cfolder_new (CamelStore *store);

G_END_DECLS

#endif /* CAMEL_SCALIX_CONTROL_FOLDER_H */
