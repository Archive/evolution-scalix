/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Camel
 *  Copyright (C) 1999-2004 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifndef __CAMEL_SCALIX_STORE_H__
#define __CAMEL_SCALIX_STORE_H__

#include <camel/camel-offline-store.h>
#include <libescalix/scalix-server-info.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define CAMEL_TYPE_SCALIX_STORE            (camel_scalix_store_get_type ())
#define CAMEL_SCALIX_STORE(obj)            (CAMEL_CHECK_CAST ((obj), CAMEL_TYPE_SCALIX_STORE, CamelSCALIXStore))
#define CAMEL_SCALIX_STORE_CLASS(klass)    (CAMEL_CHECK_CLASS_CAST ((klass), CAMEL_TYPE_SCALIX_STORE, CamelSCALIXStoreClass))
#define CAMEL_IS_SCALIX_STORE(obj)         (CAMEL_CHECK_TYPE ((obj), CAMEL_TYPE_SCALIX_STORE))
#define CAMEL_IS_SCALIX_STORE_CLASS(klass) (CAMEL_CHECK_CLASS_TYPE ((klass), CAMEL_TYPE_SCALIX_STORE))
#define CAMEL_SCALIX_STORE_GET_CLASS(obj)  (CAMEL_CHECK_GET_CLASS ((obj), CAMEL_TYPE_SCALIX_STORE, CamelSCALIXStoreClass))

#define CAMEL_SCALIX_STORE_SHOW_SFOLDER (1 << 16)

enum {
	CAMEL_SCALIX_STORE_ARG_FIRST  = CAMEL_OFFLINE_STORE_ARG_FIRST + 100,
	CAMEL_SCALIX_STORE_ARG_SERVER_VERSION,
	CAMEL_SCALIX_STORE_ARG_SERVER_NAME,
	CAMEL_SCALIX_STORE_ARG_USER_NAME,
	CAMEL_SCALIX_STORE_ARG_USER_AUTH_ID,
	CAMEL_SCALIX_STORE_ARG_USER_EMAIL,
	CAMEL_SCALIX_STORE_ARG_NEW_PW
};


#define CAMEL_SCALIX_STORE_SERVER_VERSION  (CAMEL_SCALIX_STORE_ARG_SERVER_VERSION | CAMEL_ARG_STR)
#define CAMEL_SCALIX_STORE_SERVER_NAME     (CAMEL_SCALIX_STORE_ARG_SERVER_NAME | CAMEL_ARG_STR)
#define CAMEL_SCALIX_STORE_USER_NAME       (CAMEL_SCALIX_STORE_ARG_USER_NAME | CAMEL_ARG_STR)
#define CAMEL_SCALIX_STORE_USER_AUTH_ID    (CAMEL_SCALIX_STORE_ARG_USER_AUTH_ID | CAMEL_ARG_STR)
#define CAMEL_SCALIX_STORE_USER_EMAIL      (CAMEL_SCALIX_STORE_ARG_USER_EMAIL | CAMEL_ARG_STR)
#define CAMEL_SCALIX_STORE_NEW_PW          (CAMEL_SCALIX_STORE_ARG_NEW_PW | CAMEL_ARG_STR)


typedef struct _CamelSCALIXStore CamelSCALIXStore;
typedef struct _CamelSCALIXStoreClass CamelSCALIXStoreClass;

struct _CamelSCALIXEngine;

struct _CamelSCALIXStore {
	CamelOfflineStore parent_object;
	
	struct _CamelSCALIXStoreSummary *summary;
	struct _CamelSCALIXEngine *engine;
	char *storage_path;

	char        *trash_full_name;
	
	/* Server and user information */
	char *server_name;
        char *server_version;
        char *user_name;
        char *user_email;
        char *user_auth_id;
};

struct _CamelSCALIXStoreClass {
	CamelOfflineStoreClass parent_class;
	
};

CamelType camel_scalix_store_get_type (void);
char * scalix_store_build_filename (void *store, const char *full_name);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_STORE_H__ */
