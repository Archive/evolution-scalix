/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Camel
 *  Copyright (C) 1999-2004 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <camel/camel-sasl.h>
#include <camel/camel-provider.h>
#include <camel/camel-i18n.h>

#include "camel-scalix-store.h"
#include "camel-scalix-extensions.h"

CamelProviderConfEntry scalix_conf_entries[] = {
	{ CAMEL_PROVIDER_CONF_SECTION_START, "mailcheck", NULL, N_("Checking for new mail"), "1" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "check_all", NULL, N_("C_heck for new messages in all folders"), "1" },
	{ CAMEL_PROVIDER_CONF_SECTION_END },

	{ CAMEL_PROVIDER_CONF_SECTION_START, "general", NULL, N_("Options") },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "filter", NULL, N_("_Apply filters to new messages in INBOX on this server"), "0" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "filter_junk", NULL, N_("Check new messages for _Junk contents"), "0" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "filter_junk_inbox", "filter_junk", N_("Only check for Junk messa_ges in the INBOX folder"), "0" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "sync_offline", NULL, N_("Automatically synchroni_ze remote mail locally"), "1" },
	{ CAMEL_PROVIDER_CONF_SECTION_END },
	{ CAMEL_PROVIDER_CONF_END }
};

static CamelProvider scalix_provider = {
	"scalix",
	N_("Scalix"),
	
	N_("For accessing Scalix servers."),
	
	"mail",
	
	CAMEL_PROVIDER_IS_REMOTE | CAMEL_PROVIDER_IS_SOURCE |
	CAMEL_PROVIDER_IS_STORAGE | CAMEL_PROVIDER_SUPPORTS_SSL,
	
	CAMEL_URL_NEED_USER | CAMEL_URL_NEED_HOST | CAMEL_URL_NEED_AUTH,
	
	scalix_conf_entries,
	
	/* ... */
};

CamelServiceAuthType camel_scalix_password_authtype = {
	N_("Password"),
	
	N_("This option will connect to the IMAPv4rev1 server using a "
	   "plaintext password."),
	
	"",
	TRUE
};


static void
add_hash (guint *hash, char *s)
{
	if (s)
		*hash ^= g_str_hash(s);
}

static guint
scalix_url_hash (gconstpointer key)
{
	const CamelURL *u = (CamelURL *)key;
	guint hash = 0;
	
	add_hash (&hash, u->user);
	add_hash (&hash, u->host);
	hash ^= u->port;
	
	return hash;
}

static int
check_equal (char *s1, char *s2)
{
	if (s1 == NULL) {
		if (s2 == NULL)
			return TRUE;
		else
			return FALSE;
	}
	
	if (s2 == NULL)
		return FALSE;
	
	return strcmp (s1, s2) == 0;
}

static int
scalix_url_equal (gconstpointer a, gconstpointer b)
{
	const CamelURL *u1 = a, *u2 = b;
	
	return check_equal (u1->protocol, u2->protocol)
		&& check_equal (u1->user, u2->user)
		&& check_equal (u1->host, u2->host)
		&& u1->port == u2->port;
}

void
camel_provider_module_init (void)
{
	CamelProvider *smtp_provider;
	CamelException ex;
	
	camel_exception_init (&ex);
	smtp_provider = camel_provider_get ("smtp://", &ex);

	if (camel_exception_is_set (&ex)) {
		g_warning ("Could not get smtp provider");
	}
	
	scalix_provider.object_types[CAMEL_PROVIDER_STORE] = camel_scalix_store_get_type ();
	scalix_provider.url_hash = scalix_url_hash;
	scalix_provider.url_equal = scalix_url_equal;
	scalix_provider.authtypes = camel_sasl_authtype_list (FALSE);
	scalix_provider.authtypes = g_list_prepend (scalix_provider.authtypes, &camel_scalix_password_authtype);

	if (smtp_provider != NULL) {
		scalix_provider.object_types[CAMEL_PROVIDER_TRANSPORT] =
					 smtp_provider->object_types [CAMEL_PROVIDER_TRANSPORT];
	}

	/* To avoid later (asynch) gconf calls */
	camel_scalix_get_evolution_version ();	

	bindtextdomain (GETTEXT_PACKAGE, SX_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	scalix_provider.translation_domain = GETTEXT_PACKAGE;
	
	camel_provider_register (&scalix_provider);
}
