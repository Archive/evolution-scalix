/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Camel
 *  Copyright (C) 1999-2004 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifndef __CAMEL_SCALIX_UTILS_H__
#define __CAMEL_SCALIX_UTILS_H__

#include <glib.h>

#include <camel/camel-exception.h>
#include <camel/camel-scalix-summary.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

/* SCALIX flag merging */
typedef struct {
	guint32 changed;
	guint32 bits;
} flags_diff_t;

void camel_scalix_flags_diff (flags_diff_t *diff, guint32 old, guint32 new);
guint32 camel_scalix_flags_merge (flags_diff_t *diff, guint32 flags);
guint32 camel_scalix_merge_flags (guint32 original, guint32 local, guint32 server);


struct _CamelFolderInfo;
struct _CamelSCALIXEngine;
struct _CamelSCALIXCommand;
struct _CamelFolderSummary;
struct _camel_scalix_token_t;
struct _CamelSCALIXStoreSummary;
struct _CamelSCALIXNamespaceList;
struct _CamelSCALIXNamespace;

struct _CamelFolderInfo *camel_scalix_build_folder_info_tree (GPtrArray *array, const char *top);

void camel_scalix_namespace_clear (struct _CamelSCALIXNamespace **ns);
struct _CamelSCALIXNamespaceList *camel_scalix_namespace_list_copy (const struct _CamelSCALIXNamespaceList *nsl);
void camel_scalix_namespace_list_free (struct _CamelSCALIXNamespaceList *nsl);

char camel_scalix_get_path_delim (struct _CamelSCALIXStoreSummary *s, const char *full_name);

int camel_scalix_get_uid_set (struct _CamelSCALIXEngine *engine, struct _CamelFolderSummary *summary, GPtrArray *infos, int cur, size_t linelen, char **set);

void camel_scalix_utils_set_unexpected_token_error (CamelException *ex, struct _CamelSCALIXEngine *engine, struct _camel_scalix_token_t *token);

int camel_scalix_parse_flags_list (struct _CamelSCALIXEngine *engine, guint32 *flags, CamelException *ex);

/* Note: make sure these don't clash with any bit flags in camel-store.h */
#define CAMEL_SCALIX_FOLDER_MARKED   (1 << 17)
#define CAMEL_SCALIX_FOLDER_UNMARKED (1 << 18)

#define CAMEL_SCALIX_SFOLDER_MASK    (7 << 19)
#define CAMEL_SCALIX_SFOLDER_BIT     (19)

#define CAMEL_SCALIX_FOLDER_CALENDAR (1 << 19)
#define CAMEL_SCALIX_FOLDER_CONTACT  (2 << 19)
#define CAMEL_SCALIX_FOLDER_NOTE     (3 << 19)
#define CAMEL_SCALIX_FOLDER_TASK     (4 << 19)
#define CAMEL_SCALIX_FOLDER_JOURNAL  (5 << 19)
#define CAMEL_SCALIX_FOLDER_SENT     (6 << 19)
#define CAMEL_SCALIX_FOLDER_DRAFTS   (7 << 19)

/* indicates that this folder is the default
   for it's type */
#define CAMEL_FOLDER_IS_DEFAULT    (1 << 24)

typedef struct {
	guint32 flags;
	char delim;
	char *name;
} camel_scalix_list_t;

int camel_scalix_untagged_list (struct _CamelSCALIXEngine *engine, struct _CamelSCALIXCommand *ic,
			       guint32 index, struct _camel_scalix_token_t *token, CamelException *ex);


enum {
	CAMEL_SCALIX_STATUS_UNKNOWN,
	CAMEL_SCALIX_STATUS_MESSAGES,
	CAMEL_SCALIX_STATUS_RECENT,
	CAMEL_SCALIX_STATUS_UIDNEXT,
	CAMEL_SCALIX_STATUS_UIDVALIDITY,
	CAMEL_SCALIX_STATUS_UNSEEN,
};

typedef struct _camel_scalix_status_attr {
	struct _camel_scalix_status_attr *next;
	guint32 type;
	guint32 value;
} camel_scalix_status_attr_t;

typedef struct {
	camel_scalix_status_attr_t *attr_list;
	char *mailbox;
} camel_scalix_status_t;

void camel_scalix_status_free (camel_scalix_status_t *status);

int camel_scalix_untagged_status (struct _CamelSCALIXEngine *engine, struct _CamelSCALIXCommand *ic,
				 guint32 index, struct _camel_scalix_token_t *token, CamelException *ex);
char * camel_scalix_folder_type_to_name (guint32 flags, const char *defname);

guint32     scalix_label_to_flags (CamelMessageInfo *info);
guint32     scalix_user_tag_to_flag (const char *tag);
const char *scalix_flag_to_user_tag (guint32 flag);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_UTILS_H__ */
