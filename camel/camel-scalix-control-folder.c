#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <camel/camel-exception.h>
#include <camel/camel-mime-message.h>
#include <camel/camel-folder.h>
#include <camel/camel-session.h>
#include <camel/camel-private.h>
#include <camel/camel-debug.h>
#include <camel/camel-i18n.h>

#include <camel/camel-file-utils.h>
#include <camel/camel-mime-message.h>
#include <camel/camel-stream-mem.h>
#include <camel/camel-stream-filter.h>
#include <camel/camel-mime-filter-crlf.h>

#include "camel-scalix-store.h"
#include "camel-scalix-engine.h"
#include "camel-scalix-command.h"
#include "camel-scalix-stream.h"
#include "camel-scalix-control-folder.h"
#include "camel-scalix-summary.h"

/* prototypes */

static CamelMimeMessage*  cfolder_get_message (CamelFolder *folder, 
											   const char *uid, 
											   CamelException *ex);

static void cfolder_append_message (CamelFolder *folder,
                                    CamelMimeMessage *message,
                                    const CamelMessageInfo *info,
                                    char **appended_uid,
                                    CamelException *ex);

static void	camel_scalix_cfolder_class_init (CamelScalixControlFolderClass *klass);
static void camel_scalix_cfolder_finalize (CamelObject *object);

/* camel object */

static CamelFolderClass *parent_class = NULL;

static void
camel_scalix_cfolder_class_init (CamelScalixControlFolderClass *klass)
{
	CamelFolderClass *folder_class = (CamelFolderClass *) klass;
	
	parent_class = (CamelFolderClass *) camel_type_get_global_classfuncs (CAMEL_FOLDER_TYPE);

	folder_class->get_message = cfolder_get_message;
	folder_class->append_message = cfolder_append_message;
}

static void
camel_scalix_cfolder_init (CamelScalixControlFolder *folder, 
						   CamelScalixControlFolderClass *klass)
{


}


static void
camel_scalix_cfolder_finalize (CamelObject *object)
{


		
}


CamelType
camel_scalix_cfolder_get_type (void)
{
	static CamelType type = 0;
	
	if (!type) {
		type = camel_type_register (camel_folder_get_type (),
					    "CamelScalixControlFolder",
					    sizeof (CamelScalixControlFolder),
					    sizeof (CamelScalixControlFolderClass),
					    (CamelObjectClassInitFunc) camel_scalix_cfolder_class_init,
					    NULL,
					    (CamelObjectInitFunc) camel_scalix_cfolder_init,
					    (CamelObjectFinalizeFunc) camel_scalix_cfolder_finalize);
	}
	
	return type;
}


static int
untagged_ical_freebusy (CamelSCALIXEngine *engine,
			CamelSCALIXCommand *ic,
			guint32 index, 
			camel_scalix_token_t *token, 
			CamelException *ex)
{
	CamelStream *stream;
	CamelStream *fstream;
	CamelMimeFilter *crlf;

	g_print ("UNTAGGED SETTING FREEBUSY\n");
	
	if (camel_scalix_engine_next_token (engine, token, ex) == -1) {
		return -1;
	}
	
	stream = ic->user_data;
	fstream = (CamelStream *) camel_stream_filter_new_with_stream (stream);
	crlf = camel_mime_filter_crlf_new (CAMEL_MIME_FILTER_CRLF_DECODE, CAMEL_MIME_FILTER_CRLF_MODE_CRLF_ONLY);
	camel_stream_filter_add ((CamelStreamFilter *) fstream, crlf);
	camel_object_unref (crlf);
			
	camel_stream_write_to_stream ((CamelStream *) engine->istream, fstream);
	camel_stream_flush (fstream);
	camel_object_unref (fstream);
	
	if (camel_scalix_engine_next_token (engine, token, ex) == -1) {
		return -1;
	}
	
	//camel_stream_write_to_stream ((CamelStream *) engine->istream, stream);
	//camel_stream_flush (stream);
	return 0;
}

static CamelMimeMessage *
get_freebusy (CamelFolder *folder, const char *request, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) folder->parent_store)->engine;
	CamelSCALIXCommand *ic;
	CamelStream *stream;
	CamelMimeMessage *message = NULL;
	GByteArray *buffer;
	int id;
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif	
	ic = camel_scalix_engine_queue (engine, NULL, 
					"X-GET-ICAL-FREEBUSY %S\r\n", 
					request);
	
	camel_scalix_command_register_untagged (ic, 
						"X-GET-ICAL-FREEBUSY", 
						untagged_ical_freebusy);
	
	buffer = g_byte_array_new ();
	stream = camel_stream_mem_new_with_byte_array (buffer);
	ic->user_data = stream;
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		goto done;
	}
	
	switch (ic->result) {
	case CAMEL_SCALIX_RESULT_OK:
		camel_stream_reset (stream);
		
		if (buffer->len == 0) {
			break;	
		}
		
		message = camel_mime_message_new ();
		camel_mime_part_set_content (CAMEL_MIME_PART (message),
					     (char *) buffer->data, 
					     buffer->len,
					     "text/calendar; method=PUBLISH; charset=UTF-8");
		
		
		break;
		
	case CAMEL_SCALIX_RESULT_NO:

		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Could not retrieve free-busy information"));
		break;
		
	case CAMEL_SCALIX_RESULT_BAD:
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Could not retrieve free-busy information: bad command"));
		break;
	}
	

done:
	camel_scalix_command_unref (ic);
	camel_object_unref (stream);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif
	return message;
}

static void
set_freebusy (CamelFolder *folder, const char *request, CamelException *ex)
{
    CamelSCALIXEngine *engine = ((CamelSCALIXStore *) folder->parent_store)->engine;
    CamelSCALIXCommand *ic;
    CamelStream *stream;
    GByteArray *buffer;
    int id;
    
#if EAPI_CHECK_VERSION (2, 10)
    CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
    CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif 
    ic = camel_scalix_engine_queue (engine, NULL, 
                                    "X-PUT-ICAL-FREEBUSY Calendar %S\r\n", 
                                    request);
    
    camel_scalix_command_register_untagged (ic, 
                                           "X-PUT-ICAL-FREEBUSY", 
                                            untagged_ical_freebusy);
    buffer = g_byte_array_new ();
    stream = camel_stream_mem_new_with_byte_array (buffer);

    ic->user_data = stream;
    
    while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
       ;
  
    if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
       camel_exception_xfer (ex, &ic->ex);
       goto done;
    }
  
    switch (ic->result) {
    case CAMEL_SCALIX_RESULT_OK:
       camel_stream_reset (stream);
       break;
     
   case CAMEL_SCALIX_RESULT_NO:

       camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
			     _("Could not store free-busy information"));
     break;
     
   case CAMEL_SCALIX_RESULT_BAD:
      camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
			    _("Could not store free-busy information: bad command"));
      break;
 }
  

done:

    camel_scalix_command_unref (ic);
    camel_object_unref (stream);
    
#if EAPI_CHECK_VERSION (2, 10)
    CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
    CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif
}

static CamelMimeMessage *
cfolder_get_message (CamelFolder *folder, const char *uid, CamelException *ex)
{
	CamelSCALIXStore *store;
	CamelMimeMessage *message;
	
	store = (CamelSCALIXStore *) folder->parent_store;
	
	message = NULL;
	
	if (g_str_has_prefix (uid, "BEGIN:VFREEBUSY")) {
		message = get_freebusy (folder, uid, ex);
		//g_print ("VFRequest: %s\n", uid);
	}
		
	return message;
}


static void
cfolder_append_message (CamelFolder *folder, CamelMimeMessage *message,
                        const CamelMessageInfo *info, char **appended_uid,
                        CamelException *ex)
{
    g_print ("cfolder_append_message\n");

    CamelSCALIXStore *store;
    CamelMimePart *part;
    CamelDataWrapper *content;
    GByteArray *data;
    CamelStream *stream;
 
    store = (CamelSCALIXStore *) folder->parent_store;
    
    part = CAMEL_MIME_PART (message);
    content = camel_medium_get_content_object (CAMEL_MEDIUM (part));

    data = g_byte_array_new ();
    stream = camel_stream_mem_new_with_byte_array (data);
    camel_data_wrapper_decode_to_stream (content, stream);
    
    if (data == NULL || data->data == NULL) {
        return;
    }

    /* terminating null-character */    
    g_byte_array_append (data, "", 1);
    
    set_freebusy (folder, data->data, ex);
    
    g_byte_array_free (data, TRUE);
}

CamelFolder *
camel_scalix_cfolder_new (CamelStore *store)
{
		CamelFolder *folder;
		CamelScalixControlFolder *cfolder;
		
		folder = (CamelFolder *) (cfolder = (CamelScalixControlFolder *) camel_object_new (CAMEL_TYPE_SCALIX_CONTROL_FOLDER));

		camel_folder_construct (folder, store, "ControlFolder", "cFolder");
		folder->summary = camel_scalix_summary_new (folder);

		return folder;
}

