/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@novell.com>
 *
 *  Copyright 2005 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifndef __CAMEL_SCALIX_ENGINE_H__
#define __CAMEL_SCALIX_ENGINE_H__

#include <stdarg.h>

#include <glib.h>

#include <camel/camel-stream.h>
#include <camel/camel-folder.h>
#include <camel/camel-session.h>
#include <camel/camel-list-utils.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define CAMEL_TYPE_SCALIX_ENGINE            (camel_scalix_engine_get_type ())
#define CAMEL_SCALIX_ENGINE(obj)            (CAMEL_CHECK_CAST ((obj), CAMEL_TYPE_SCALIX_ENGINE, CamelSCALIXEngine))
#define CAMEL_SCALIX_ENGINE_CLASS(klass)    (CAMEL_CHECK_CLASS_CAST ((klass), CAMEL_TYPE_SCALIX_ENGINE, CamelSCALIXEngineClass))
#define CAMEL_IS_SCALIX_ENGINE(obj)         (CAMEL_CHECK_TYPE ((obj), CAMEL_TYPE_SCALIX_ENGINE))
#define CAMEL_IS_SCALIX_ENGINE_CLASS(klass) (CAMEL_CHECK_CLASS_TYPE ((klass), CAMEL_TYPE_SCALIX_ENGINE))
#define CAMEL_SCALIX_ENGINE_GET_CLASS(obj)  (CAMEL_CHECK_GET_CLASS ((obj), CAMEL_TYPE_SCALIX_ENGINE, CamelSCALIXEngineClass))

typedef struct _CamelSCALIXEngine CamelSCALIXEngine;
typedef struct _CamelSCALIXEngineClass CamelSCALIXEngineClass;

struct _camel_scalix_token_t;
struct _CamelSCALIXCommand;
struct _CamelSCALIXFolder;
struct _CamelSCALIXStream;

typedef enum {
	CAMEL_SCALIX_ENGINE_DISCONNECTED,
	CAMEL_SCALIX_ENGINE_CONNECTED,
	CAMEL_SCALIX_ENGINE_PREAUTH,
	CAMEL_SCALIX_ENGINE_AUTHENTICATED,
	CAMEL_SCALIX_ENGINE_SELECTED,
} camel_scalix_engine_t;

typedef enum {
	CAMEL_SCALIX_LEVEL_UNKNOWN,
	CAMEL_SCALIX_LEVEL_IMAP4,
	CAMEL_SCALIX_LEVEL_IMAP4REV1
} camel_scalix_level_t;

enum {
	CAMEL_SCALIX_CAPABILITY_IMAP4            = (1 << 0),
	CAMEL_SCALIX_CAPABILITY_IMAP4REV1        = (1 << 1),
	CAMEL_SCALIX_CAPABILITY_STATUS           = (1 << 2),
	CAMEL_SCALIX_CAPABILITY_NAMESPACE        = (1 << 3),
	CAMEL_SCALIX_CAPABILITY_UIDPLUS          = (1 << 4),
	CAMEL_SCALIX_CAPABILITY_LITERALPLUS      = (1 << 5),
	CAMEL_SCALIX_CAPABILITY_LOGINDISABLED    = (1 << 6),
	CAMEL_SCALIX_CAPABILITY_STARTTLS         = (1 << 7),
	CAMEL_SCALIX_CAPABILITY_IDLE             = (1 << 8),
	CAMEL_SCALIX_CAPABILITY_QUOTA            = (1 << 9),
	CAMEL_SCALIX_CAPABILITY_ACL              = (1 << 10),
	CAMEL_SCALIX_CAPABILITY_MULTIAPPEND      = (1 << 11),
	CAMEL_SCALIX_CAPABILITY_UNSELECT         = (1 << 12),
	
	CAMEL_SCALIX_CAPABILITY_XGWEXTENSIONS    = (1 << 16),
        CAMEL_SCALIX_CAPABILITY_XGWMOVE          = (1 << 17),

	//CAMEL_SCALIX_CAPABILITY_XSCALIX_1     = (1 << 16),
	//CAMEL_SCALIX_CAPABILITY_XSCALIX_2     = (1 << 17),
	
	CAMEL_SCALIX_CAPABILITY_useful_lsub      = (1 << 30),
	CAMEL_SCALIX_CAPABILITY_utf8_search      = (1 << 31),
};

typedef enum {
	CAMEL_SCALIX_RESP_CODE_ALERT,
	CAMEL_SCALIX_RESP_CODE_BADCHARSET,
	CAMEL_SCALIX_RESP_CODE_CAPABILITY,
	CAMEL_SCALIX_RESP_CODE_PARSE,
	CAMEL_SCALIX_RESP_CODE_PERM_FLAGS,
	CAMEL_SCALIX_RESP_CODE_READONLY,
	CAMEL_SCALIX_RESP_CODE_READWRITE,
	CAMEL_SCALIX_RESP_CODE_TRYCREATE,
	CAMEL_SCALIX_RESP_CODE_UIDNEXT,
	CAMEL_SCALIX_RESP_CODE_UIDVALIDITY,
	CAMEL_SCALIX_RESP_CODE_UNSEEN,
	CAMEL_SCALIX_RESP_CODE_NEWNAME,
	CAMEL_SCALIX_RESP_CODE_APPENDUID,
	CAMEL_SCALIX_RESP_CODE_COPYUID,
	CAMEL_SCALIX_RESP_CODE_STOREUID,
	CAMEL_SCALIX_RESP_CODE_MOVEUID,
	CAMEL_SCALIX_RESP_CODE_XBADAUTH,
	CAMEL_SCALIX_RESP_CODE_XBADPASS,
	CAMEL_SCALIX_RESP_CODE_UNKNOWN,
} camel_scalix_resp_code_t;

typedef struct _CamelSCALIXRespCode {
	camel_scalix_resp_code_t code;
	union {
		guint32 flags;
		char *parse;
		guint32 uidnext;
		guint32 uidvalidity;
		guint32 unseen;
		char *newname[2];
		struct {
			guint32 uidvalidity;
			guint32 uid;
		} appenduid;
		struct {
			guint32 uidvalidity;
			char *srcset;
			char *destset;
		} copyuid;
	} v;
} CamelSCALIXRespCode;

enum {
	CAMEL_SCALIX_UNTAGGED_ERROR = -1,
	CAMEL_SCALIX_UNTAGGED_OK,
	CAMEL_SCALIX_UNTAGGED_NO,
	CAMEL_SCALIX_UNTAGGED_BAD,
	CAMEL_SCALIX_UNTAGGED_PREAUTH,
	CAMEL_SCALIX_UNTAGGED_HANDLED,
};

typedef struct _CamelSCALIXNamespace {
	struct _CamelSCALIXNamespace *next;
	char *path;
	char sep;
} CamelSCALIXNamespace;

typedef struct _CamelSCALIXNamespaceList {
	CamelSCALIXNamespace *personal;
	CamelSCALIXNamespace *other;
	CamelSCALIXNamespace *shared;
} CamelSCALIXNamespaceList;

enum {
	CAMEL_SCALIX_ENGINE_MAXLEN_LINE,
	CAMEL_SCALIX_ENGINE_MAXLEN_TOKEN
};

typedef gboolean (* CamelSCALIXReconnectFunc) (CamelSCALIXEngine *engine, CamelException *ex);

struct _CamelSCALIXEngine {
	CamelObject parent_object;
	
	CamelSCALIXReconnectFunc reconnect;
	gboolean reconnecting;
	
	CamelSession *session;
	CamelService *service;
	CamelURL *url;
	
	camel_scalix_engine_t state;
	camel_scalix_level_t level;
	guint32 capa;
	
	guint32 maxlen:31;
	guint32 maxlentype:1;
	
	CamelSCALIXNamespaceList namespaces;
	GHashTable *authtypes;                    /* supported authtypes */
	
	struct _CamelSCALIXStream *istream;
	CamelStream *ostream;
	
	unsigned char tagprefix;             /* 'A'..'Z' */
	unsigned int tag;                    /* next command tag */
	int nextid;
	
	struct _CamelSCALIXFolder *folder;    /* currently selected folder */
	
	CamelDList queue;                     /* queue of waiting commands */
	struct _CamelSCALIXCommand *current;
};

struct _CamelSCALIXEngineClass {
	CamelObjectClass parent_class;
	
	unsigned char tagprefix;
};


CamelType camel_scalix_engine_get_type (void);

CamelSCALIXEngine *camel_scalix_engine_new (CamelService *service, CamelSCALIXReconnectFunc reconnect);

/* returns 0 on success or -1 on error */
int camel_scalix_engine_take_stream (CamelSCALIXEngine *engine, CamelStream *stream, CamelException *ex);

void camel_scalix_engine_disconnect (CamelSCALIXEngine *engine);

int camel_scalix_engine_capability (CamelSCALIXEngine *engine, CamelException *ex);
int camel_scalix_engine_namespace (CamelSCALIXEngine *engine, CamelException *ex);

int camel_scalix_engine_select_folder (CamelSCALIXEngine *engine, CamelFolder *folder, CamelException *ex);

struct _CamelSCALIXCommand *camel_scalix_engine_queue (CamelSCALIXEngine *engine, CamelFolder *folder,
						     const char *format, ...);
struct _CamelSCALIXCommand *camel_scalix_engine_prequeue (CamelSCALIXEngine *engine, CamelFolder *folder,
							const char *format, ...);

void camel_scalix_engine_dequeue (CamelSCALIXEngine *engine, struct _CamelSCALIXCommand *ic);

int camel_scalix_engine_iterate (CamelSCALIXEngine *engine);


/* untagged response utility functions */
int camel_scalix_engine_handle_untagged_1 (CamelSCALIXEngine *engine, struct _camel_scalix_token_t *token, CamelException *ex);
void camel_scalix_engine_handle_untagged (CamelSCALIXEngine *engine, CamelException *ex);

/* stream wrapper utility functions */
int camel_scalix_engine_next_token (CamelSCALIXEngine *engine, struct _camel_scalix_token_t *token, CamelException *ex);
int camel_scalix_engine_line (CamelSCALIXEngine *engine, unsigned char **line, size_t *len, CamelException *ex);
int camel_scalix_engine_literal (CamelSCALIXEngine *engine, unsigned char **literal, size_t *len, CamelException *ex);
int camel_scalix_engine_nstring (CamelSCALIXEngine *engine, unsigned char **nstring, CamelException *ex);
int camel_scalix_engine_eat_line (CamelSCALIXEngine *engine, CamelException *ex);


/* response code stuff */
int camel_scalix_engine_parse_resp_code (CamelSCALIXEngine *engine, CamelException *ex);
void camel_scalix_resp_code_free (CamelSCALIXRespCode *rcode);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_ENGINE_H__ */
