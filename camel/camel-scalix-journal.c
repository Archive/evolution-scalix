/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <camel/camel-i18n.h>
#include <camel/camel-folder.h>
#include <camel/camel-file-utils.h>
#include <camel/camel-folder-summary.h>
#include <camel/camel-data-cache.h>

#include "camel-scalix-folder.h"
#include "camel-scalix-journal.h"


#define d(x) x


static void camel_scalix_journal_class_init (CamelSCALIXJournalClass *klass);
static void camel_scalix_journal_init (CamelSCALIXJournal *journal, CamelSCALIXJournalClass *klass);
static void camel_scalix_journal_finalize (CamelObject *object);

static void scalix_entry_free (CamelOfflineJournal *journal, CamelDListNode *entry);
static CamelDListNode *scalix_entry_load (CamelOfflineJournal *journal, FILE *in);
static int scalix_entry_write (CamelOfflineJournal *journal, CamelDListNode *entry, FILE *out);
static int scalix_entry_play (CamelOfflineJournal *journal, CamelDListNode *entry, CamelException *ex);


static CamelOfflineJournalClass *parent_class = NULL;


CamelType
camel_scalix_journal_get_type (void)
{
	static CamelType type = 0;
	
	if (!type) {
		type = camel_type_register (camel_offline_journal_get_type (),
					    "CamelSCALIXJournal",
					    sizeof (CamelSCALIXJournal),
					    sizeof (CamelSCALIXJournalClass),
					    (CamelObjectClassInitFunc) camel_scalix_journal_class_init,
					    NULL,
					    (CamelObjectInitFunc) camel_scalix_journal_init,
					    (CamelObjectFinalizeFunc) camel_scalix_journal_finalize);
	}
	
	return type;
}

static void
camel_scalix_journal_class_init (CamelSCALIXJournalClass *klass)
{
	CamelOfflineJournalClass *journal_class = (CamelOfflineJournalClass *) klass;
	
	parent_class = (CamelOfflineJournalClass *) camel_type_get_global_classfuncs (CAMEL_TYPE_OFFLINE_JOURNAL);
	
	journal_class->entry_free = scalix_entry_free;
	journal_class->entry_load = scalix_entry_load;
	journal_class->entry_write = scalix_entry_write;
	journal_class->entry_play = scalix_entry_play;
}

static void
camel_scalix_journal_init (CamelSCALIXJournal *journal, CamelSCALIXJournalClass *klass)
{
	journal->failed = g_ptr_array_new ();
}

static void
camel_scalix_journal_finalize (CamelObject *object)
{
	CamelSCALIXJournal *journal = (CamelSCALIXJournal *) object;
	int i;
	
	if (journal->failed) {
		for (i = 0; i < journal->failed->len; i++)
			camel_message_info_free (journal->failed->pdata[i]);
		g_ptr_array_free (journal->failed, TRUE);
	}
}

static void
scalix_entry_free (CamelOfflineJournal *journal, CamelDListNode *entry)
{
	CamelSCALIXJournalEntry *scalix_entry = (CamelSCALIXJournalEntry *) entry;
	
	g_free (scalix_entry->v.append_uid);
	g_free (scalix_entry);
}

static CamelDListNode *
scalix_entry_load (CamelOfflineJournal *journal, FILE *in)
{
	CamelSCALIXJournalEntry *entry;
	
	entry = g_malloc0 (sizeof (CamelSCALIXJournalEntry));
	
	if (camel_file_util_decode_uint32 (in, &entry->type) == -1)
		goto exception;
	
	switch (entry->type) {
	case CAMEL_SCALIX_JOURNAL_ENTRY_APPEND:
		if (camel_file_util_decode_string (in, &entry->v.append_uid) == -1)
			goto exception;
		
		break;
	default:
		goto exception;
	}
	
	return (CamelDListNode *) entry;
	
 exception:
	
	switch (entry->type) {
	case CAMEL_SCALIX_JOURNAL_ENTRY_APPEND:
		g_free (entry->v.append_uid);
		break;
	default:
		g_assert_not_reached ();
	}
	
	g_free (entry);
	
	return NULL;
}

static int
scalix_entry_write (CamelOfflineJournal *journal, CamelDListNode *entry, FILE *out)
{
	CamelSCALIXJournalEntry *scalix_entry = (CamelSCALIXJournalEntry *) entry;
	
	if (camel_file_util_encode_uint32 (out, scalix_entry->type) == -1)
		return -1;
	
	switch (scalix_entry->type) {
	case CAMEL_SCALIX_JOURNAL_ENTRY_APPEND:
		if (camel_file_util_encode_string (out, scalix_entry->v.append_uid))
			return -1;
		
		break;
	default:
		g_assert_not_reached ();
	}
	
	return 0;
}

static void
scalix_message_info_dup_to (CamelMessageInfoBase *dest, CamelMessageInfoBase *src)
{
	camel_flag_list_copy (&dest->user_flags, &src->user_flags);
	camel_tag_list_copy (&dest->user_tags, &src->user_tags);
	dest->date_received = src->date_received;
	dest->date_sent = src->date_sent;
	dest->flags = src->flags;
	dest->size = src->size;
}

static int
scalix_entry_play_append (CamelOfflineJournal *journal, CamelSCALIXJournalEntry *entry, CamelException *ex)
{
	CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) journal->folder;
	CamelFolder *folder = journal->folder;
	CamelMessageInfo *info, *real;
	CamelMimeMessage *message;
	CamelStream *stream;
	CamelException lex;
	char *uid = NULL;
	
	/* if the message isn't in the cache, the user went behind our backs so "not our problem" */
	if (!scalix_folder->cache || !(stream = camel_data_cache_get (scalix_folder->cache, "cache", entry->v.append_uid, ex)))
		goto done;
	
	message = camel_mime_message_new ();
	if (camel_data_wrapper_construct_from_stream ((CamelDataWrapper *) message, stream) == -1) {
		camel_object_unref (message);
		camel_object_unref (stream);
		goto done;
	}
	
	camel_object_unref (stream);
	
	if (!(info = camel_folder_summary_uid (folder->summary, entry->v.append_uid))) {
		/* info not in the summary, either because the summary
		 * got corrupted or because the previous time this
		 * journal was replay'd, it failed [1] */
		info = camel_message_info_new (NULL);
	}
	
	camel_exception_init (&lex);
	camel_folder_append_message (folder, message, info, &uid, &lex);
	camel_object_unref (message);
	
	if (camel_exception_is_set (&lex)) {
		/* Remove the message-info from the summary even if we fail or the next
		 * summary downsync will break because info indexes will be wrong */
		if (info->summary == folder->summary) {
			camel_folder_summary_remove (folder->summary, info);
			g_ptr_array_add (((CamelSCALIXJournal *) journal)->failed, info);
		} else {
			/* info wasn't in the summary to begin with */
			camel_folder_summary_remove_uid (folder->summary, entry->v.append_uid);
			camel_message_info_free (info);
		}
		
		camel_exception_xfer (ex, &lex);
		
		return -1;
	}
	
	if (uid != NULL && (real = camel_folder_summary_uid (folder->summary, uid))) {
		/* Copy the system flags and user flags/tags over to the real
		   message-info now stored in the summary to prevent the user
		   from losing any of this meta-data */
		scalix_message_info_dup_to ((CamelMessageInfoBase *) real, (CamelMessageInfoBase *) info);
	}
	
	camel_message_info_free (info);
	g_free (uid);
	
 done:
	
	camel_folder_summary_remove_uid (folder->summary, entry->v.append_uid);
	camel_data_cache_remove (scalix_folder->cache, "cache", entry->v.append_uid, NULL);
	
	return 0;
}

static int
scalix_entry_play (CamelOfflineJournal *journal, CamelDListNode *entry, CamelException *ex)
{
	CamelSCALIXJournalEntry *scalix_entry = (CamelSCALIXJournalEntry *) entry;
	
	switch (scalix_entry->type) {
	case CAMEL_SCALIX_JOURNAL_ENTRY_APPEND:
		return scalix_entry_play_append (journal, scalix_entry, ex);
	default:
		g_assert_not_reached ();
		return -1;
	}
}



CamelOfflineJournal *
camel_scalix_journal_new (CamelSCALIXFolder *folder, const char *filename)
{
	CamelOfflineJournal *journal;
	
	g_return_val_if_fail (CAMEL_IS_SCALIX_FOLDER (folder), NULL);
	
	journal = (CamelOfflineJournal *) camel_object_new (camel_scalix_journal_get_type ());
	camel_offline_journal_construct (journal, (CamelFolder *) folder, filename);
	
	return journal;
}


void
camel_scalix_journal_readd_failed (CamelSCALIXJournal *journal)
{
	CamelFolderSummary *summary = ((CamelOfflineJournal *) journal)->folder->summary;
	int i;
	
	for (i = 0; i < journal->failed->len; i++)
		camel_folder_summary_add (summary, journal->failed->pdata[i]);
	
	g_ptr_array_set_size (journal->failed, 0);
}


void
camel_scalix_journal_append (CamelSCALIXJournal *scalix_journal, CamelMimeMessage *message,
			    const CamelMessageInfo *mi, char **appended_uid, CamelException *ex)
{
	CamelOfflineJournal *journal = (CamelOfflineJournal *) scalix_journal;
	CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) journal->folder;
	CamelFolder *folder = (CamelFolder *) journal->folder;
	CamelSCALIXJournalEntry *entry;
	CamelMessageInfo *info;
	CamelStream *cache;
	guint32 nextuid;
	char *uid;
	
	if (scalix_folder->cache == NULL) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM,
				     _("Cannot append message in offline mode: cache unavailable"));
		return;
	}
	
	nextuid = camel_folder_summary_next_uid (folder->summary);
	uid = g_strdup_printf ("-%u", nextuid);
	
	if (!(cache = camel_data_cache_add (scalix_folder->cache, "cache", uid, ex))) {
		folder->summary->nextuid--;
		g_free (uid);
		return;
	}
	
	if (camel_data_wrapper_write_to_stream ((CamelDataWrapper *) message, cache) == -1
	    || camel_stream_flush (cache) == -1) {
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot append message in offline mode: %s"),
				      g_strerror (errno));
		camel_data_cache_remove (scalix_folder->cache, "cache", uid, NULL);
		folder->summary->nextuid--;
		camel_object_unref (cache);
		g_free (uid);
		return;
	}
	
	camel_object_unref (cache);
	
	entry = g_new (CamelSCALIXJournalEntry, 1);
	entry->type = CAMEL_SCALIX_JOURNAL_ENTRY_APPEND;
	entry->v.append_uid = uid;
	
	camel_dlist_addtail (&journal->queue, (CamelDListNode *) entry);
	
	info = camel_folder_summary_info_new_from_message (folder->summary, message);
	info->uid = g_strdup (uid);
	
	scalix_message_info_dup_to ((CamelMessageInfoBase *) info, (CamelMessageInfoBase *) mi);
	
	camel_folder_summary_add (folder->summary, info);
	
	if (appended_uid)
		*appended_uid = g_strdup (uid);
}
