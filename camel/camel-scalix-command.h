/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Camel
 *  Copyright (C) 1999-2004 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifndef __CAMEL_SCALIX_COMMAND_H__
#define __CAMEL_SCALIX_COMMAND_H__

#include <stdarg.h>

#include <glib.h>

#include <camel/camel-stream.h>
#include <camel/camel-exception.h>
#include <camel/camel-list-utils.h>
#include <camel/camel-data-wrapper.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

struct _CamelSCALIXEngine;
struct _CamelSCALIXFolder;
struct _camel_scalix_token_t;

typedef struct _CamelSCALIXCommand CamelSCALIXCommand;
typedef struct _CamelSCALIXLiteral CamelSCALIXLiteral;

typedef int (* CamelSCALIXPlusCallback) (struct _CamelSCALIXEngine *engine,
					CamelSCALIXCommand *ic,
					const unsigned char *linebuf,
					size_t linelen, CamelException *ex);

typedef int (* CamelSCALIXUntaggedCallback) (struct _CamelSCALIXEngine *engine,
					    CamelSCALIXCommand *ic,
					    guint32 index,
					    struct _camel_scalix_token_t *token,
					    CamelException *ex);

typedef void (* CamelSCALIXCommandReset) (CamelSCALIXCommand *ic, void *user_data);

enum {
	CAMEL_SCALIX_LITERAL_STRING,
	CAMEL_SCALIX_LITERAL_STREAM,
	CAMEL_SCALIX_LITERAL_WRAPPER,
};

struct _CamelSCALIXLiteral {
	int type;
	union {
		char *string;
		CamelStream *stream;
		CamelDataWrapper *wrapper;
	} literal;
};

typedef struct _CamelSCALIXCommandPart {
	struct _CamelSCALIXCommandPart *next;
	unsigned char *buffer;
	size_t buflen;
	
	CamelSCALIXLiteral *literal;
} CamelSCALIXCommandPart;

enum {
	CAMEL_SCALIX_COMMAND_QUEUED,
	CAMEL_SCALIX_COMMAND_ACTIVE,
	CAMEL_SCALIX_COMMAND_COMPLETE,
	CAMEL_SCALIX_COMMAND_ERROR,
};

enum {
	CAMEL_SCALIX_RESULT_NONE,
	CAMEL_SCALIX_RESULT_OK,
	CAMEL_SCALIX_RESULT_NO,
	CAMEL_SCALIX_RESULT_BAD,
};

struct _CamelSCALIXCommand {
	CamelDListNode node;
	
	struct _CamelSCALIXEngine *engine;
	
	unsigned int ref_count:26;
	unsigned int status:3;
	unsigned int result:3;
	int id;
	
	char *tag;
	
	GPtrArray *resp_codes;
	
	struct _CamelSCALIXFolder *folder;
	CamelException ex;
	
	/* command parts - logical breaks in the overall command based on literals */
	CamelSCALIXCommandPart *parts;
	
	/* current part */
	CamelSCALIXCommandPart *part;
	
	/* untagged handlers */
	GHashTable *untagged;
	
	/* '+' callback/data */
	CamelSCALIXPlusCallback plus;
	CamelSCALIXCommandReset reset;
	void *user_data;
};

CamelSCALIXCommand *camel_scalix_command_new (struct _CamelSCALIXEngine *engine, struct _CamelSCALIXFolder *folder,
					    const char *format, ...);
CamelSCALIXCommand *camel_scalix_command_newv (struct _CamelSCALIXEngine *engine, struct _CamelSCALIXFolder *folder,
					     const char *format, va_list args);

void camel_scalix_command_register_untagged (CamelSCALIXCommand *ic, const char *atom, CamelSCALIXUntaggedCallback untagged);

void camel_scalix_command_ref (CamelSCALIXCommand *ic);
void camel_scalix_command_unref (CamelSCALIXCommand *ic);

/* returns 1 when complete, 0 if there is more to do, or -1 on error */
int camel_scalix_command_step (CamelSCALIXCommand *ic);

void camel_scalix_command_reset (CamelSCALIXCommand *ic);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_COMMAND_H__ */
