/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Copyright 2005 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *  Authors: Jeffrey Stedfast <fejj@novell.com>
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gstdio.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include <camel/camel-sasl.h>
#include <camel/camel-utf8.h>
#include <camel/camel-stream-process.h>
#include <camel/camel-tcp-stream-raw.h>

#ifdef HAVE_SSL
#include <camel/camel-tcp-stream-ssl.h>
#endif

#include <camel-private.h>

#include <camel/camel-i18n.h>
#include <camel/camel-net-utils.h>

#include "camel-scalix-store.h"
#include "camel-scalix-engine.h"
#include "camel-scalix-folder.h"
#include "camel-scalix-stream.h"
#include "camel-scalix-command.h"
#include "camel-scalix-utils.h"
#include "camel-scalix-summary.h"
#include "camel-scalix-store-summary.h"
#include "camel-scalix-extensions.h"
#include "camel-scalix-control-folder.h"

#define d(x) x

static void camel_scalix_store_class_init (CamelSCALIXStoreClass *klass);
static void camel_scalix_store_init (CamelSCALIXStore *store, CamelSCALIXStoreClass *klass);
static void camel_scalix_store_finalize (CamelObject *object);

/* camel object methods */
static int  scalix_setv (CamelObject *object, CamelException *ex, CamelArgV *args);
static int  scalix_getv (CamelObject *object, CamelException *ex, CamelArgGetV *args);

/* service methods */
static void scalix_construct (CamelService *service, CamelSession *session,
			     CamelProvider *provider, CamelURL *url,
			     CamelException *ex);
static char *scalix_get_name (CamelService *service, gboolean brief);
static gboolean scalix_connect (CamelService *service, CamelException *ex);
static gboolean scalix_reconnect (CamelSCALIXEngine *engine, CamelException *ex);
static gboolean scalix_disconnect (CamelService *service, gboolean clean, CamelException *ex);
static GList *scalix_query_auth_types (CamelService *service, CamelException *ex);

/* store methods */
static CamelFolder *scalix_get_folder (CamelStore *store, const char *folder_name, guint32 flags, CamelException *ex);
static CamelFolder *scalix_get_trash (CamelStore *store, CamelException *ex);
static CamelFolderInfo *scalix_create_folder (CamelStore *store, const char *parent_name, const char *folder_name, CamelException *ex);
static void scalix_delete_folder (CamelStore *store, const char *folder_name, CamelException *ex);
static void scalix_rename_folder (CamelStore *store, const char *old_name, const char *new_name, CamelException *ex);
static CamelFolderInfo *scalix_get_folder_info (CamelStore *store, const char *top, guint32 flags, CamelException *ex);
static void scalix_subscribe_folder (CamelStore *store, const char *folder_name, CamelException *ex);
static void scalix_unsubscribe_folder (CamelStore *store, const char *folder_name, CamelException *ex);
static void scalix_noop (CamelStore *store, CamelException *ex);

static gboolean scalix_can_refresh_folder (CamelStore *store, CamelFolderInfo *info, CamelException *ex);

static CamelFolderInfo *scalix_create_special_folder (CamelStore *store, const char *parent_name, const char *folder_name, guint32 type, CamelException *ex);

static void change_password (CamelSCALIXStore *store, const char *str, CamelException *ex);


static CamelOfflineStoreClass *parent_class = NULL;


CamelType
camel_scalix_store_get_type (void)
{
	static CamelType type = 0;
	
	if (!type) {
		type = camel_type_register (camel_offline_store_get_type (),
					    "CamelSCALIXStore",
					    sizeof (CamelSCALIXStore),
					    sizeof (CamelSCALIXStoreClass),
					    (CamelObjectClassInitFunc) camel_scalix_store_class_init,
					    NULL,
					    (CamelObjectInitFunc) camel_scalix_store_init,
					    (CamelObjectFinalizeFunc) camel_scalix_store_finalize);
	}
	
	return type;
}

static guint
scalix_hash_folder_name (gconstpointer key)
{
	if (g_ascii_strcasecmp (key, "INBOX") == 0)
		return g_str_hash ("INBOX");
	else
		return g_str_hash (key);
}

static int
scalix_compare_folder_name (gconstpointer a, gconstpointer b)
{
	gconstpointer aname = a, bname = b;
	
	if (g_ascii_strcasecmp (a, "INBOX") == 0)
		aname = "INBOX";
	if (g_ascii_strcasecmp (b, "INBOX") == 0)
		bname = "INBOX";
	
	return g_str_equal (aname, bname);
}

static void
camel_scalix_store_class_init (CamelSCALIXStoreClass *klass)
{
	CamelObjectClass *camel_object_class = CAMEL_OBJECT_CLASS (klass);
	CamelServiceClass *service_class = (CamelServiceClass *) klass;
	CamelStoreClass *store_class = (CamelStoreClass *) klass;
	
	parent_class = (CamelOfflineStoreClass *) camel_type_get_global_classfuncs (CAMEL_TYPE_OFFLINE_STORE);
	
	camel_object_class->setv = scalix_setv;
	camel_object_class->getv = scalix_getv;
	
	service_class->construct        = scalix_construct;
	service_class->get_name         = scalix_get_name;
	service_class->connect          = scalix_connect;
	service_class->disconnect       = scalix_disconnect;
	service_class->query_auth_types = scalix_query_auth_types;
	
	store_class->hash_folder_name    = scalix_hash_folder_name;
	store_class->compare_folder_name = scalix_compare_folder_name;
	
	store_class->get_folder       = scalix_get_folder;
	store_class->get_trash        = scalix_get_trash;
	store_class->create_folder = scalix_create_folder;
	store_class->delete_folder = scalix_delete_folder;
	store_class->rename_folder = scalix_rename_folder;
	store_class->can_refresh_folder = scalix_can_refresh_folder;
	store_class->get_folder_info = scalix_get_folder_info;
	store_class->subscribe_folder = scalix_subscribe_folder;
	store_class->unsubscribe_folder = scalix_unsubscribe_folder;
	store_class->noop = scalix_noop;

	/* FIXME: implement folder_subscribed */
}

static void
camel_scalix_store_init (CamelSCALIXStore *store, CamelSCALIXStoreClass *klass)
{
	store->engine = NULL;
	store->summary = NULL;
}

static void
camel_scalix_store_finalize (CamelObject *object)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) object;

	camel_service_disconnect((CamelService *)store, TRUE, NULL);
	
	if (store->summary) {
		camel_store_summary_save ((CamelStoreSummary *) store->summary);
		camel_object_unref (store->summary);
	}
	
	if (store->engine)
		camel_object_unref (store->engine);
	
	g_free (store->storage_path);

	g_free (store->trash_full_name);
	
	g_free (store->server_name);
	g_free (store->server_version);
	g_free (store->user_name);
	g_free (store->user_email);
	g_free (store->user_auth_id);
}


static void
scalix_construct (CamelService   *service, 
		  CamelSession   *session, 
		  CamelProvider  *provider, 
		  CamelURL       *url, 
		  CamelException *ex)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) service;
	char       *buf;

	CAMEL_SERVICE_CLASS (parent_class)->construct (service, session, provider, url, ex);
	
	if (camel_exception_is_set (ex)) {
		return;
	}
		
	if (camel_url_get_param (url, "use_lsub")) {
		((CamelStore *) store)->flags |= CAMEL_STORE_SUBSCRIPTIONS;
	}

	/* We don't want vtrash mode */
	CAMEL_STORE (store)->flags &= ~CAMEL_STORE_VTRASH;

	store->storage_path = camel_session_get_storage_path (session, service, ex);
	store->engine = camel_scalix_engine_new (service, scalix_reconnect);
	
	/* setup/load the summary */
	buf = g_alloca (strlen (store->storage_path) + 32);
	sprintf (buf, "%s/.summary", store->storage_path);
	store->summary = camel_scalix_store_summary_new ();
	camel_store_summary_set_filename ((CamelStoreSummary *) store->summary, buf);
	
	/* store->check_all = camel_url_get_param (url, "check_all") == NULL; */

	buf = camel_url_to_string (service->url, CAMEL_URL_HIDE_ALL);
	url = camel_url_new (buf, NULL);
	g_free (buf);
	camel_store_summary_set_uri_base ((CamelStoreSummary *) store->summary, url);

	camel_url_free (url);
    
	camel_store_summary_load ((CamelStoreSummary *) store->summary);
}

static int
scalix_setv (CamelObject *object, CamelException *ex, CamelArgV *args)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) object;
	guint32 tag;
	int i;
	
	for (i = 0; i < args->argc; i++) {
		tag = args->argv[i].tag;
		
		/* make sure this is an arg we're supposed to handle */
		if ((tag & CAMEL_ARG_TAG) <= CAMEL_SCALIX_STORE_ARG_FIRST ||
		    (tag & CAMEL_ARG_TAG) >= CAMEL_SCALIX_STORE_ARG_FIRST + 100)
			continue;

		switch (tag) {
		
		case CAMEL_SCALIX_STORE_NEW_PW:
			change_password (store, args->argv[i].ca_str, ex);	
			break;
		
		case CAMEL_SCALIX_STORE_SERVER_VERSION:
		default:
			/* error?? */
			continue;
		}
		
		/* let our parent know that we've handled this arg */
		camel_argv_ignore (args, i);
	}
		
	return CAMEL_OBJECT_CLASS (parent_class)->setv (object, ex, args);
}

static int
scalix_getv (CamelObject *object, CamelException *ex, CamelArgGetV *args)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) object;
	guint32 tag;
	int i;
	
	for (i = 0; i < args->argc; i++) {
		tag = args->argv[i].tag;
		
		/* make sure this is an arg we're supposed to handle */
		if ((tag & CAMEL_ARG_TAG) <= CAMEL_SCALIX_STORE_ARG_FIRST ||
		    (tag & CAMEL_ARG_TAG) >= CAMEL_SCALIX_STORE_ARG_FIRST + 100)
			continue;
		
		switch (tag) {
		case CAMEL_SCALIX_STORE_SERVER_VERSION:			
			*args->argv[i].ca_str = store->server_version;
			break;

		case CAMEL_SCALIX_STORE_SERVER_NAME:
			*args->argv[i].ca_str = store->server_name;
			break;
			
		case CAMEL_SCALIX_STORE_USER_NAME:
			*args->argv[i].ca_str = store->user_name;
			break;

		case CAMEL_SCALIX_STORE_USER_AUTH_ID:
			*args->argv[i].ca_str = store->user_auth_id;
			break;

		case CAMEL_SCALIX_STORE_USER_EMAIL:
			*args->argv[i].ca_str = store->user_email;
			break;
			
		default:
			/* error? */
			break;
		}
	}
	
	return CAMEL_OBJECT_CLASS (parent_class)->getv (object, ex, args);
}

static char *
scalix_get_name (CamelService *service, gboolean brief)
{
	if (brief)
		return g_strdup_printf (_("IMAP server %s"), service->url->host);
	else
		return g_strdup_printf (_("IMAP service for %s on %s"),
					service->url->user, service->url->host);
}


enum {
	MODE_CLEAR,
	MODE_SSL,
	MODE_TLS,
};

#ifdef HAVE_SSL
#define SSL_PORT_FLAGS (CAMEL_TCP_STREAM_SSL_ENABLE_SSL2 | CAMEL_TCP_STREAM_SSL_ENABLE_SSL3)
#define STARTTLS_FLAGS (CAMEL_TCP_STREAM_SSL_ENABLE_TLS)
#endif

static gboolean
connect_to_server (CamelSCALIXEngine *engine, struct addrinfo *ai, int ssl_mode, CamelException *ex)
{
	CamelService *service = engine->service;
	CamelSockOptData sockopt;
	CamelStream *tcp_stream;
	int ret;
#ifdef HAVE_SSL
	CamelSCALIXCommand *ic;
	int id;
#endif


	if (ssl_mode != MODE_CLEAR) {
#ifdef HAVE_SSL
		if (ssl_mode == MODE_TLS) {
			tcp_stream = camel_tcp_stream_ssl_new_raw (service->session, service->url->host, STARTTLS_FLAGS);
		} else {
			tcp_stream = camel_tcp_stream_ssl_new (service->session, service->url->host, SSL_PORT_FLAGS);
		}
#else
		camel_exception_setv (ex, CAMEL_EXCEPTION_SERVICE_UNAVAILABLE,
				      _("Could not connect to %s: %s"),
				      service->url->host, _("SSL unavailable"));

		return FALSE;
#endif /* HAVE_SSL */
	} else {
		tcp_stream = camel_tcp_stream_raw_new ();
	}

	if ((ret = camel_tcp_stream_connect ((CamelTcpStream *) tcp_stream, ai)) == -1) {
		if (errno == EINTR)
			camel_exception_set (ex, CAMEL_EXCEPTION_USER_CANCEL,
					     _("Connection cancelled"));
		else
			camel_exception_setv (ex, CAMEL_EXCEPTION_SERVICE_UNAVAILABLE,
					      _("Could not connect to %s: %s"),
					      service->url->host,
					      g_strerror (errno));

		camel_object_unref (tcp_stream);

		return FALSE;
	}

	/* set some socket options to better tailor the connection to our needs */
	sockopt.option = CAMEL_SOCKOPT_NODELAY;
	sockopt.value.no_delay = TRUE;
	camel_tcp_stream_setsockopt ((CamelTcpStream *) tcp_stream, &sockopt);

	sockopt.option = CAMEL_SOCKOPT_KEEPALIVE;
	sockopt.value.keep_alive = TRUE;
	camel_tcp_stream_setsockopt ((CamelTcpStream *) tcp_stream, &sockopt);

	if (camel_scalix_engine_take_stream (engine, tcp_stream, ex) == -1)
		return FALSE;

	if (camel_scalix_engine_capability (engine, ex) == -1)
		return FALSE;

	camel_scalix_store_summary_set_capabilities (((CamelSCALIXStore *) service)->summary, engine->capa);

	if (ssl_mode != MODE_TLS) {
		/* we're done */
		return TRUE;
	}

#ifdef HAVE_SSL
	if (!(engine->capa & CAMEL_SCALIX_CAPABILITY_STARTTLS)) {
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Failed to connect to IMAP server %s in secure mode: %s"),
				      service->url->host, _("SSL negotiations failed"));

		return FALSE;
	}

	ic = camel_scalix_engine_prequeue (engine, NULL, "STARTTLS\r\n");
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;

	if (id == -1 || ic->result != CAMEL_SCALIX_RESULT_OK) {
		if (ic->result != CAMEL_SCALIX_RESULT_OK) {
			camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
					      _("Failed to connect to IMAP server %s in secure mode: %s"),
					      service->url->host, _("Unknown error"));
		} else {
			camel_exception_xfer (ex, &ic->ex);
		}

		camel_scalix_command_unref (ic);

		return FALSE;
	}

	camel_scalix_command_unref (ic);

	if (camel_tcp_stream_ssl_enable_ssl ((CamelTcpStreamSSL *) tcp_stream) == -1) {
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
		                      _("Failed to connect to IMAP server %s in secure mode: %s"),
		                      service->url->host, _("TLS negotiations failed"));
		camel_scalix_engine_disconnect (engine);
		return FALSE;
	}

	return TRUE;
#else
	camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
			      _("Failed to connect to IMAP server %s in secure mode: %s"),
			      service->url->host, _("SSL is not available in this build"));

	return FALSE;
#endif /* HAVE_SSL */
}

static struct {
	char *value;
	char *serv;
	char *port;
	int mode;
} ssl_options[] = {
	{ "",              "imaps", "993", MODE_SSL   },  /* really old (1.x) */
	{ "always",        "imaps", "993", MODE_SSL   },
	{ "when-possible", "imap",  "143", MODE_TLS   },
	{ "never",         "imap",  "143", MODE_CLEAR },
	{ NULL,            "imap",  "143", MODE_CLEAR },
};

static gboolean
connect_to_server_wrapper (CamelSCALIXEngine *engine, CamelException *ex)
{
	CamelService *service = engine->service;
	struct addrinfo *ai, hints;
	const char *ssl_mode;
	int mode, ret, i;
	const char *port;
	char *serv;

	if ((ssl_mode = camel_url_get_param (service->url, "use_ssl"))) {
		for (i = 0; ssl_options[i].value; i++)
			if (!strcmp (ssl_options[i].value, ssl_mode))
				break;
		mode = ssl_options[i].mode;
		serv = ssl_options[i].serv;
		port = ssl_options[i].port;
	} else {
		mode = MODE_CLEAR;
		serv = "imap";
		port = "143";
	}

	if (service->url->port) {
		serv = g_alloca (16);
		sprintf (serv, "%d", service->url->port);
		port = NULL;
	}

	memset (&hints, 0, sizeof (hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	ai = camel_getaddrinfo (service->url->host, serv, &hints, ex);
	if (ai == NULL && port != NULL && camel_exception_get_id(ex) != CAMEL_EXCEPTION_USER_CANCEL) {
		camel_exception_clear (ex);
		ai = camel_getaddrinfo (service->url->host, port, &hints, ex);
	}
	if (ai == NULL)
		return FALSE;

	if (!(ret = connect_to_server (engine, ai, mode, ex)) && mode == MODE_SSL) {
		camel_exception_clear (ex);
		ret = connect_to_server (engine, ai, MODE_TLS, ex);
	} else if (!ret && mode == MODE_TLS) {
		camel_exception_clear (ex);
		ret = connect_to_server (engine, ai, MODE_CLEAR, ex);
	}

	camel_freeaddrinfo (ai);

	return ret;
}

static int
sasl_auth (CamelSCALIXEngine *engine, CamelSCALIXCommand *ic, const unsigned char *linebuf, size_t linelen, CamelException *ex)
{
	/* Perform a single challenge iteration */
	CamelSasl *sasl = ic->user_data;
	char *challenge;
	
	if (camel_sasl_authenticated (sasl)) {
		camel_exception_setv (ex, CAMEL_EXCEPTION_SERVICE_CANT_AUTHENTICATE,
				      _("Cannot authenticate to IMAP server %s using the %s authentication mechanism"),
				      engine->url->host, engine->url->authmech);
		return -1;
	}
	
	while (isspace (*linebuf))
		linebuf++;
	
	if (*linebuf == '\0')
		linebuf = NULL;
	
	if (!(challenge = camel_sasl_challenge_base64 (sasl, (const char *) linebuf, ex)))
		return -1;
	
	d(fprintf (stderr, "sending : %s\r\n", challenge));
	
	if (camel_stream_printf (engine->ostream, "%s\r\n", challenge) == -1) {
		g_free (challenge);
		return -1;
	}
	
	g_free (challenge);
	
	if (camel_stream_flush (engine->ostream) == -1)
		return -1;
	
	return 0;
}

static int
scalix_try_authenticate (CamelSCALIXEngine *engine, gboolean reprompt, const char *errmsg, CamelException *ex)
{
	CamelService *service = engine->service;
	CamelSession *session = service->session;
	CamelServiceAuthType *mech = NULL;
	CamelSasl *sasl = NULL;
	CamelSCALIXCommand *ic;
	int id;

	if (service->url->authmech) {
		mech = g_hash_table_lookup (engine->authtypes,
					    service->url->authmech);
	}

	if ((!mech || (mech && mech->need_password)) && !service->url->passwd) {
		guint32 flags = CAMEL_SESSION_PASSWORD_SECRET;
		char *prompt;

		if (reprompt)
			flags |= CAMEL_SESSION_PASSWORD_REPROMPT;

		prompt = g_strdup_printf (_("%sPlease enter the IMAP password for %s on host %s"),
					  errmsg ? errmsg : "",
					  service->url->user,
					  service->url->host);

		service->url->passwd = camel_session_get_password (session, service, NULL, prompt, "password", flags, ex);

		g_free (prompt);

		if (!service->url->passwd)
			return FALSE;
	}

	if (service->url->authmech) {

		sasl = camel_sasl_new ("imap", mech->authproto, service);

		ic = camel_scalix_engine_prequeue (engine, NULL, "AUTHENTICATE %s\r\n", service->url->authmech);
		ic->plus = sasl_auth;
		ic->user_data = sasl;
	} else {
		ic = camel_scalix_engine_prequeue (engine, NULL, "LOGIN %S %S\r\n",
						  service->url->user, service->url->passwd);
	}

	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;

	if (sasl != NULL)
		camel_object_unref (sasl);

	if (id == -1 || ic->status == CAMEL_SCALIX_COMMAND_ERROR) {
		/* unrecoverable error */
		camel_exception_xfer (ex, &ic->ex);
		camel_scalix_command_unref (ic);

		return FALSE;
	}

	if (ic->result != CAMEL_SCALIX_RESULT_OK) {
		camel_scalix_command_unref (ic);

		/* try again */

		return TRUE;
	}

	camel_scalix_command_unref (ic);

	return FALSE;
}

static gboolean
scalix_reconnect (CamelSCALIXEngine *engine, CamelException *ex)
{
	CamelService *service = engine->service;
	CamelServiceAuthType *mech;
	gboolean reprompt = FALSE;
	char *errmsg = NULL;
	CamelException lex;

	if (!connect_to_server_wrapper (engine, ex))
		return FALSE;

	if (engine->state != CAMEL_SCALIX_ENGINE_AUTHENTICATED) {
#define CANT_USE_AUTHMECH (!(mech = g_hash_table_lookup (engine->authtypes, service->url->authmech)))
		if (service->url->authmech && CANT_USE_AUTHMECH) {
			/* Oops. We can't AUTH using the requested mechanism */
			camel_exception_setv (ex, CAMEL_EXCEPTION_SERVICE_CANT_AUTHENTICATE,
					      _("Cannot authenticate to IMAP server %s using %s"),
					      service->url->host, service->url->authmech);

			return FALSE;
		}

		camel_exception_init (&lex);

		if (camel_scalix_id (engine, ex) == -1)
			return FALSE;

		while (scalix_try_authenticate (engine, reprompt, errmsg, &lex)) {
			g_free (errmsg);
			errmsg = g_strdup (lex.desc);
			camel_exception_clear (&lex);
			g_free (service->url->passwd);
			service->url->passwd = NULL;
			reprompt = TRUE;
		}
		g_free (errmsg);

		if (camel_exception_is_set (&lex)) {
			camel_exception_xfer (ex, &lex);
			return FALSE;
		}

	}

	if (camel_scalix_id (engine, ex) == -1)
		return FALSE;

	if (camel_scalix_engine_namespace (engine, ex) == -1)
		return FALSE;

	camel_scalix_store_summary_set_namespaces (((CamelSCALIXStore *) service)->summary, &engine->namespaces);

	return TRUE;
}

static gboolean
scalix_connect (CamelService *service, CamelException *ex)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) service;
	gboolean retval;

	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL)
		return TRUE;

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (service, connect_lock);
#else
	CAMEL_SERVICE_LOCK (service, connect_lock);
#endif
	
	if (store->engine->state == CAMEL_SCALIX_ENGINE_DISCONNECTED)
		retval = scalix_reconnect (store->engine, ex);
	else
		retval = TRUE;

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (service, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (service, connect_lock);
#endif

	return retval;
}

static gboolean
scalix_disconnect (CamelService *service, gboolean clean, CamelException *ex)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) service;
	CamelSCALIXCommand *ic;
	int id;
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL)
		return TRUE;
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif
	if (clean && store->engine->state != CAMEL_SCALIX_ENGINE_DISCONNECTED) {
		ic = camel_scalix_engine_queue (store->engine, NULL, "LOGOUT\r\n");
		while ((id = camel_scalix_engine_iterate (store->engine)) < ic->id && id != -1)
			;
		
		camel_scalix_command_unref (ic);
	}

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
	
	return 0;
}

extern CamelServiceAuthType camel_scalix_password_authtype;

static GList *
scalix_query_auth_types (CamelService *service, CamelException *ex)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) service;
	CamelServiceAuthType *authtype;
	GList *sasl_types, *t, *next;
	gboolean connected;
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL)
		return NULL;

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif

	connected = connect_to_server_wrapper (store->engine, ex);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
	if (!connected)
		return NULL;
	
	sasl_types = camel_sasl_authtype_list (FALSE);
	for (t = sasl_types; t; t = next) {
		authtype = t->data;
		next = t->next;
		
		if (!g_hash_table_lookup (store->engine->authtypes, authtype->authproto)) {
			sasl_types = g_list_remove_link (sasl_types, t);
			g_list_free_1 (t);
		}
	}
	
	return g_list_prepend (sasl_types, &camel_scalix_password_authtype);
}

static char *
scalix_folder_utf7_name (CamelStore *store, const char *folder_name, char wildcard)
{
	char *real_name, *p;
	char sep = '\0';
	int len;
	
	if (*folder_name) {
		sep = camel_scalix_get_path_delim (((CamelSCALIXStore *) store)->summary, folder_name);
		
		if (sep != '/') {
			p = real_name = g_alloca (strlen (folder_name) + 1);
			strcpy (real_name, folder_name);
			while (*p != '\0') {
				if (*p == '/')
					*p = sep;
				p++;
			}
			
			folder_name = real_name;
		}
		
		real_name = camel_utf8_utf7 (folder_name);
	} else
		real_name = g_strdup ("");
	
	if (wildcard) {
		len = strlen (real_name);
		real_name = g_realloc (real_name, len + 3);
		
		if (len > 0)
			real_name[len++] = sep;
		
		real_name[len++] = wildcard;
		real_name[len] = '\0';
	}
	
	return real_name;
}

#define CAMEL_SCALIX_SFOLDER_MASK    (7 << 19)

static CamelFolder *
scalix_get_folder (CamelStore *store, const char *folder_name, guint32 flags, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelStoreSummary *summary;
	CamelFolder *folder = NULL;
	camel_scalix_list_t *list;
	CamelSCALIXCommand *ic;
	CamelFolderInfo *fi;
	GPtrArray *array;
	char *utf7_name;
	int create;
	int id, i;
	guint32 folder_flags;

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif
	/* Scalix Control Folder */
	if (g_str_equal (folder_name, "#ScalixControl")) {
		folder = camel_scalix_cfolder_new (store);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
		return folder;
	}

	folder_flags = 0;
	summary = CAMEL_STORE_SUMMARY (CAMEL_SCALIX_STORE (store)->summary);

	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		if ((flags & CAMEL_STORE_FOLDER_CREATE) != 0) {
			camel_exception_set (ex,
					     CAMEL_EXCEPTION_SYSTEM,
					     _("Cannot create IMAP folders in offline mode."));
		} else {
			CamelStoreInfo *si;
			/* TODO: test me */
			/* we need to retrive the folder flags from the
			 * summary */
			si = camel_store_summary_path (summary,
						       folder_name);

			if (si != NULL) {
				folder_flags = si->flags;
				camel_store_summary_info_free (summary, si);
			}

			folder = camel_scalix_folder_new (store,
							  folder_name,
							  folder_flags,
							  ex);

		}

#if EAPI_CHECK_VERSION (2, 10)
		CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
		CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
		return folder;
	}

	/* make sure the folder exists - try LISTing it? */
	utf7_name = scalix_folder_utf7_name (store, folder_name, '\0');
	ic = camel_scalix_engine_queue (engine, NULL, "LIST \"\" %S\r\n", utf7_name);
	camel_scalix_command_register_untagged (ic, "LIST", camel_scalix_untagged_list);
	ic->user_data = array = g_ptr_array_new ();
	g_free (utf7_name);

	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;

	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		camel_scalix_command_unref (ic);
		g_ptr_array_free (array, TRUE);
		goto done;
	}

	create = array->len == 0;
	for (i = 0; i < array->len; i++) {
		list = array->pdata[i];

		if (g_str_equal (list->name, folder_name)) {
			folder_flags = list->flags;
		}

		g_free (list->name);
		g_free (list);
	}

	g_ptr_array_free (array, TRUE);

	if (ic->result != CAMEL_SCALIX_RESULT_OK) {
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot get folder `%s' on IMAP server %s: Unknown"),
				      folder_name, ((CamelService *) store)->url->host);
		camel_scalix_command_unref (ic);
		goto done;
	}
	
	camel_scalix_command_unref (ic);
	
	if (create) {
		const char *basename;
		char *parent;
		int len;
		gboolean folder_is_special;
		
		if (!(flags & CAMEL_STORE_FOLDER_CREATE))
			goto done;
		
		if (!(basename = strrchr (folder_name, '/')))
			basename = folder_name;
		else
			basename++;
		
		len = basename > folder_name ? (basename - folder_name) - 1 : 0;
		parent = g_alloca (len + 1);
		memcpy (parent, folder_name, len);
		parent[len] = '\0';
		
		folder_is_special = (flags & CAMEL_SCALIX_SFOLDER_MASK);
		
		if (folder_is_special == TRUE) {
			fi = scalix_create_folder (store, parent, basename, ex);
		} else {
			fi = scalix_create_special_folder (store, 
					parent, 
					basename,
					flags,
					ex); 	
		}
		
		if (fi == NULL)
			goto done;
		
		camel_store_free_folder_info (store, fi);
	}
	
	folder = camel_scalix_folder_new (store, folder_name, folder_flags, ex);

 done:

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif	
	return folder;
}


static CamelFolder *
scalix_get_trash (CamelStore *camel_store, CamelException *ex)
{
	CamelSCALIXStore *store = (CamelSCALIXStore *) camel_store;
	CamelFolder *folder;

	g_assert (store->trash_full_name);

	folder = camel_store_get_folder (camel_store,
					 store->trash_full_name,
					 0,
					 ex);

	return folder;
}

static gboolean
scalix_check_foldername (const char *folder_name, char sep)
{
    const char *c;

    if (!folder_name)
        return FALSE;

    c = folder_name;
    while (*c != '\0') {
        if ((sep && *c == sep) || strchr ("%*", *c))
            return FALSE;
        c++;
    }

    return TRUE;
}


static CamelFolderInfo *
scalix_create_special_folder (CamelStore *store,
			      const char *parent_name,
			      const char *folder_name,
			      guint32 flags,
			      CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelFolderInfo *fi = NULL;
	CamelSCALIXCommand *ic;
	char *utf7_name;
	CamelURL *url;
	char *name;
	char *type_str;
	char sep;
	const char *c;
	int id;
	guint32 type;

	sep = camel_scalix_get_path_delim (((CamelSCALIXStore *) store)->summary, parent_name);

	if (!scalix_check_foldername (folder_name, sep)) {
		camel_exception_setv (ex, CAMEL_EXCEPTION_FOLDER_INVALID_PATH,
				      _("\"%s\" is not a valid foldername"), folder_name);
		return NULL;
	}

	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM, _("Cannot create folders in offline mode."));
		return NULL;
	}

	if (parent_name != NULL && *parent_name)
		name = g_strdup_printf ("%s/%s", parent_name, folder_name);
	else
		name = g_strdup (folder_name);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif
	utf7_name = scalix_folder_utf7_name (store, name, '\0');

	type = (flags & CAMEL_SCALIX_SFOLDER_MASK);

	if (type == CAMEL_SCALIX_FOLDER_CALENDAR) {
		type_str = "IPF.Appointment";
	} else if (type == CAMEL_SCALIX_FOLDER_CONTACT) {
		type_str = "IPF.Contact";
	} else {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM, _("Unknown folder type"));
		return NULL;
	}

	ic = camel_scalix_engine_queue (engine, NULL,
					"X-CREATE-SPECIAL %S %S\r\n",
					type_str, utf7_name);
	g_free (utf7_name);

	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;

	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		camel_scalix_command_unref (ic);
		g_free (name);
		goto done;
	}

	switch (ic->result) {
	case CAMEL_SCALIX_RESULT_OK:
		url = camel_url_copy (engine->url);
		url->path = g_strdup_printf ("/%s", name);

		c = strrchr (name, '/');

		fi = g_malloc0 (sizeof (CamelFolderInfo));
		fi->full_name = name;
		fi->name = g_strdup (c ? c + 1: name);
		fi->uri = camel_url_to_string (url, CAMEL_URL_HIDE_ALL);
		camel_url_free (url);
		fi->flags = 0;
		fi->unread = -1;
		fi->total = -1;

		//camel_scalix_store_summary_note_info (((CamelSCALIXStore *) store)->summary, fi);

		//camel_object_trigger_event (store, "folder_created", fi);
		break;
	case CAMEL_SCALIX_RESULT_NO:
		/* FIXME: would be good to save the NO reason into the err message */
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot create folder `%s': Invalid mailbox name"),
				      name);
		g_free (name);
		break;
	case CAMEL_SCALIX_RESULT_BAD:
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot create folder `%s': Bad command"),
				      name);
		g_free (name);
		break;
	default:
		g_assert_not_reached ();
	}

	camel_scalix_command_unref (ic);

 done:

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
	return fi;


	return NULL;
}


static CamelFolderInfo *
scalix_create_folder (CamelStore *store, const char *parent_name, const char *folder_name, CamelException *ex)
{
	/* FIXME: also need to deal with parent folders that can't
	 * contain subfolders - delete them and re-create with the
	 * proper hint */
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelFolderInfo *fi = NULL;
	CamelSCALIXCommand *ic;
	char *utf7_name;
	CamelURL *url;
	const char *c;
	char *name;
	char sep;
	int id;
	
	sep = camel_scalix_get_path_delim (((CamelSCALIXStore *) store)->summary, parent_name);
	
    if (!scalix_check_foldername (folder_name, sep)) {
         camel_exception_setv (ex, CAMEL_EXCEPTION_FOLDER_INVALID_PATH,
                   _("\"%s\" is not a valid foldername"), folder_name);
        return NULL;
    }
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM, _("Cannot create IMAP folders in offline mode."));
		return NULL;
	}
	
	if (parent_name != NULL && *parent_name)
		name = g_strdup_printf ("%s/%s", parent_name, folder_name);
	else
		name = g_strdup (folder_name);
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif

	utf7_name = scalix_folder_utf7_name (store, name, '\0');
	ic = camel_scalix_engine_queue (engine, NULL, "CREATE %S\r\n", utf7_name);
	g_free (utf7_name);
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		camel_scalix_command_unref (ic);
		g_free (name);
		goto done;
	}
	
	switch (ic->result) {
	case CAMEL_SCALIX_RESULT_OK:
		url = camel_url_copy (engine->url);
		url->path = g_strdup_printf ("/%s", name);
       
		c = strrchr (name, '/');
		
		fi = g_malloc0 (sizeof (CamelFolderInfo));
		fi->full_name = name;
		fi->name = g_strdup (c ? c + 1: name);
		fi->uri = camel_url_to_string (url, CAMEL_URL_HIDE_ALL);
		camel_url_free (url);
		fi->flags = 0;
		fi->unread = -1;
		fi->total = -1;
		
		camel_scalix_store_summary_note_info (((CamelSCALIXStore *) store)->summary, fi);
		
		camel_object_trigger_event (store, "folder_created", fi);
		break;
	case CAMEL_SCALIX_RESULT_NO:
		/* FIXME: would be good to save the NO reason into the err message */
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot create folder `%s': Invalid mailbox name"),
				      name);
		g_free (name);
		break;
	case CAMEL_SCALIX_RESULT_BAD:
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot create folder `%s': Bad command"),
				      name);
		g_free (name);
		break;
	default:
		g_assert_not_reached ();
	}
	
	camel_scalix_command_unref (ic);
	
 done:
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif	
	return fi;
}

static void
scalix_delete_folder (CamelStore *store, const char *folder_name, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelFolder *selected = (CamelFolder *) engine->folder;
	CamelSCALIXCommand *ic, *ic0 = NULL;
	CamelFolderInfo *fi;
	char *utf7_name;
	CamelURL *url;
	const char *p;
	int id;
	
	if (!g_ascii_strcasecmp (folder_name, "INBOX")) {
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot delete folder `%s': Special folder"),
				      folder_name);
		
		return;
	}
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM, _("Cannot delete IMAP folders in offline mode."));
		return;
	}
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif

	if (selected && !strcmp (folder_name, selected->full_name))
		ic0 = camel_scalix_engine_queue (engine, NULL, "CLOSE\r\n");
	
	utf7_name = scalix_folder_utf7_name (store, folder_name, '\0');
	ic = camel_scalix_engine_queue (engine, NULL, "DELETE %S\r\n", utf7_name);
	g_free (utf7_name);
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		if (ic0 && ic0->status != CAMEL_SCALIX_COMMAND_COMPLETE)
			camel_exception_xfer (ex, &ic0->ex);
		else
			camel_exception_xfer (ex, &ic->ex);
		
		if (ic0 != NULL)
			camel_scalix_command_unref (ic0);
		
		camel_scalix_command_unref (ic);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
		return;
	}
	
	if (ic0 != NULL)
		camel_scalix_command_unref (ic0);
	
	switch (ic->result) {
	case CAMEL_SCALIX_RESULT_OK:
		/* deleted */
		url = camel_url_copy (engine->url);
		url->path = g_strdup_printf ("/%s", folder_name);
       
		p = strrchr (folder_name, '/');
		
		fi = g_malloc0 (sizeof (CamelFolderInfo));
		fi->full_name = g_strdup (folder_name);
		fi->name = g_strdup (p ? p + 1: folder_name);
		fi->uri = camel_url_to_string (url, CAMEL_URL_HIDE_ALL);
		camel_url_free (url);
		fi->flags = 0;
		fi->unread = -1;
		fi->total = -1;
		
		camel_scalix_store_summary_unnote_info (((CamelSCALIXStore *) store)->summary, fi);
		
		camel_object_trigger_event (store, "folder_deleted", fi);
		
		camel_folder_info_free (fi);
		break;
	case CAMEL_SCALIX_RESULT_NO:
		/* FIXME: would be good to save the NO reason into the err message */
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot delete folder `%s': Invalid mailbox name"),
				      folder_name);
		break;
	case CAMEL_SCALIX_RESULT_BAD:
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot delete folder `%s': Bad command"),
				      folder_name);
		break;
	}
	
	camel_scalix_command_unref (ic);
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
}


static void
rename_folder_info (CamelSCALIXStore *store, const char *old_name, const char *new_name)
{
	int                i, count;
	CamelStoreInfo    *si;
	CamelStoreSummary *summary;
	int                olen = strlen (old_name);
	const char        *path;
	char              *npath;
	
	summary = CAMEL_STORE_SUMMARY (store->summary);
	count   = camel_store_summary_count (summary);
	
	for (i = 0; i < count; i++) {
		si = camel_store_summary_index (summary, i);
		
		if (si == NULL) {
			continue;
		}
		
		path = camel_store_info_path (summary, si);
		
		if (strncmp (path, old_name, olen) == 0) {
			
			if (strlen (path) > olen) {
				npath = g_strdup_printf("%s/%s", new_name, path + olen + 1);
			} else {
				npath = g_strdup (new_name);
			}
		
			camel_store_info_set_string (summary, si, CAMEL_STORE_INFO_PATH, npath);
			
			/* camel_store_info_set_string () frees si->uri but doesnt set it to 
			 * NULL, so we do it here. Not doing so might leed to double frees */
			si->uri = NULL;
		}
		
		camel_store_summary_info_free (summary, si);
	}
}


static void
scalix_rename_folder (CamelStore *store, const char *old_name, const char *new_name, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	char *old_uname, *new_uname;
	CamelSCALIXCommand *ic;
	CamelStoreInfo *si;
	int id;
	char *oldpath, *newpath;
    char sep;
	
	si = camel_store_summary_path (CAMEL_STORE_SUMMARY (CAMEL_SCALIX_STORE (store)->summary),
				       old_name);
	
	if (si != NULL) {
		gboolean is_system;

		is_system = si->flags & CAMEL_FOLDER_SYSTEM;
		camel_store_summary_info_free (CAMEL_STORE_SUMMARY (CAMEL_SCALIX_STORE (store)->summary), 
					       si);
		
		if (is_system) {
			camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
					      _("Cannot rename folder `%s' to `%s': Special folder"),
					      old_name, new_name);
			return;
		}
	}
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM, _("Cannot rename IMAP folders in offline mode."));
		return;
	}
	
    sep = camel_scalix_get_path_delim (((CamelSCALIXStore *) store)->summary, old_name);
    
    if (!scalix_check_foldername (new_name, 0)) {
         camel_exception_setv (ex, CAMEL_EXCEPTION_FOLDER_INVALID_PATH,
                   _("\"%s\" is not a valid foldername"), new_name);
        return;
    }

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif

	old_uname = scalix_folder_utf7_name (store, old_name, '\0');
	new_uname = scalix_folder_utf7_name (store, new_name, '\0');
	
	ic = camel_scalix_engine_queue (engine, 
					NULL,
					"RENAME %S %S\r\n",
					old_uname,
					new_uname);
	g_free (old_uname);
	g_free (new_uname);
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		camel_scalix_command_unref (ic);
#if EAPI_CHECK_VERSION (2, 10)
		CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
		CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
		return;
	}
	
	switch (ic->result) {
	case CAMEL_SCALIX_RESULT_OK:	
	
		oldpath = scalix_store_build_filename (CAMEL_SCALIX_STORE (store),
						       old_name);
		
		newpath = scalix_store_build_filename (CAMEL_SCALIX_STORE (store),
						       new_name);
		
		if (g_rename (oldpath, newpath) != 0) {
			g_warning ("Could not rename cache dir!");
		}

		g_free (oldpath);
		g_free (newpath);
		
		rename_folder_info (CAMEL_SCALIX_STORE (store), old_name, new_name);
		/* FIXME: need to update state on the renamed folder object */
		/* FIXME: need to update cached summary info too */
		break;
	case CAMEL_SCALIX_RESULT_NO:
		/* FIXME: would be good to save the NO reason into the err message */
		/* as a temp. fix for 8652 we comment this out 
                camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot rename folder `%s' to `%s': Invalid mailbox name"),
				      old_name, new_name);*/
		break;
	case CAMEL_SCALIX_RESULT_BAD:
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot rename folder `%s' to `%s': Bad command"),
				      old_name, new_name);
		break;
	}
	
	camel_scalix_command_unref (ic);
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
}

static int
list_sort (const camel_scalix_list_t **list0, const camel_scalix_list_t **list1)
{
	return strcmp ((*list0)->name, (*list1)->name);
}

static void
list_remove_duplicates (GPtrArray *array)
{
	camel_scalix_list_t *list, *last;
	int i;
	
	last = array->pdata[0];
	for (i = 1; i < array->len; i++) {
		list = array->pdata[i];
		if (!strcmp (list->name, last->name)) {
			g_ptr_array_remove_index (array, i--);
			last->flags |= list->flags;
			g_free (list->name);
			g_free (list);
		}
	}
}

static void
scalix_status (CamelStore      *store,
	       CamelFolderInfo *fi)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	camel_scalix_status_attr_t *attr, *next;
	camel_scalix_status_t *status;
	CamelSCALIXCommand *ic;
	GPtrArray *array;
	char *mailbox;
	int id, i;
	
	mailbox = scalix_folder_utf7_name (store, fi->full_name, '\0');
	ic = camel_scalix_engine_queue (engine, NULL, "STATUS %S (MESSAGES UNSEEN UIDNEXT)\r\n", mailbox);
	g_free (mailbox);
	
	camel_scalix_command_register_untagged (ic, "STATUS", camel_scalix_untagged_status);
	ic->user_data = array = g_ptr_array_new ();
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_scalix_command_unref (ic);
		g_ptr_array_free (array, TRUE);
		return;
	}
	
	for (i = 0; i < array->len; i++) {
		status = array->pdata[i];
		attr = status->attr_list;
		while (attr != NULL) {
			next = attr->next;
			
			if (attr->type == CAMEL_SCALIX_STATUS_MESSAGES) {
				fi->total = attr->value;
			} else if (attr->type == CAMEL_SCALIX_STATUS_UNSEEN) {
				fi->unread = attr->value;
			} else if (attr->type == CAMEL_SCALIX_STATUS_UIDNEXT) {
				g_print ("UIDNEXT: %d\n", attr->value);
			}
			
			g_free (attr);
			attr = next;
		}
		
		g_free (status->mailbox);
		g_free (status);
	}
	
	camel_scalix_command_unref (ic);
	g_ptr_array_free (array, TRUE);
}

static char *
scalix_uidvalidity (CamelStore      *store,
		    const char      *folder_name)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	camel_scalix_status_attr_t *attr, *next;
	camel_scalix_status_t *status;
	CamelSCALIXCommand *ic;
	GPtrArray *array;
	char *mailbox;
	int id, i;
	guint32 uid = 0;
	char *uid_str = NULL;
	
	mailbox = scalix_folder_utf7_name (store, folder_name, '\0');
	ic = camel_scalix_engine_queue (engine, NULL, "STATUS %S (UIDVALIDITY)\r\n", mailbox);
	g_free (mailbox);
	
	camel_scalix_command_register_untagged (ic, "STATUS", camel_scalix_untagged_status);
	ic->user_data = array = g_ptr_array_new ();
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_scalix_command_unref (ic);
		g_ptr_array_free (array, TRUE);
		return NULL;
	}
	
	for (i = 0; i < array->len; i++) {
		status = array->pdata[i];
		attr = status->attr_list;
		while (attr != NULL) {
			next = attr->next;
			
			if (attr->type == CAMEL_SCALIX_STATUS_UIDVALIDITY) {
				uid = attr->value;
			}
			
			g_free (attr);
			attr = next;
		}
		
		g_free (status->mailbox);
		g_free (status);
	}
	
	camel_scalix_command_unref (ic);
	g_ptr_array_free (array, TRUE);

	if (uid != 0) {
		uid_str = g_strdup_printf ("%" G_GUINT32_FORMAT, uid);
	}

	return uid_str;
}

static CamelFolderInfo *
scalix_build_folder_info (CamelStore *store, const char *top, guint32 flags, GPtrArray *array)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelFolder *folder = (CamelFolder *) engine->folder;
	camel_scalix_list_t *list;
	CamelFolderInfo *fi;
	char *name, *p;
	CamelURL *url;
	int i;
	gboolean folder_is_special;
	CamelException ex;
    	guint32 sfflags;
	char *fuid;
	    
	if (array->len == 0) {
		g_ptr_array_free (array, TRUE);
		return NULL;
	}
	
	camel_exception_init (&ex);
	
	g_ptr_array_sort (array, (GCompareFunc) list_sort);
	
	list_remove_duplicates (array);
	
	url = camel_url_copy (engine->url);
	
	if (!strcmp (top, "") && (flags & CAMEL_STORE_FOLDER_INFO_RECURSIVE)) {
		/* clear the folder-info cache */
		camel_store_summary_clear (CAMEL_STORE_SUMMARY (CAMEL_SCALIX_STORE (store)->summary));
	}
	
	for (i = 0; i < array->len; i++) {
		list = array->pdata[i];
     
		sfflags = list->flags & CAMEL_SCALIX_SFOLDER_MASK;
	       
		/* A special folder is everything that is marked as such BUT not
		   Drafts and Sent Items */	
		folder_is_special = sfflags && 
			            ! (sfflags == CAMEL_SCALIX_FOLDER_SENT || 
				       sfflags == CAMEL_SCALIX_FOLDER_DRAFTS);

		
		/* Do not show special folders if we should not */
		if (folder_is_special && (!(flags & CAMEL_SCALIX_STORE_SHOW_SFOLDER))) {
			g_free (list->name);
			g_free (list);
			g_ptr_array_remove_index (array, i--);
			continue;	
		}
		
		p = name = camel_utf7_utf8 (list->name);
     
		fi = g_malloc0 (sizeof (CamelFolderInfo));

		while (*p != '\0') {
			if (*p == list->delim)
				*p = '/';
			p++;
		}
		
		p = strrchr (name, '/');
		url->path = g_strdup_printf ("/%s", name);
      
		fi->full_name = name;

		/* include the uid for special folders */
		if (folder_is_special) {
			fuid = scalix_uidvalidity (store, name);

			if (fuid != NULL) {
				camel_url_set_param (url, "uid", fuid);
				g_free (fuid);
			}
			fi->uri = camel_url_to_string (url, CAMEL_URL_HIDE_PASSWORD |
						       	    CAMEL_URL_HIDE_AUTH);
		} else {
			fi->uri = camel_url_to_string (url, CAMEL_URL_HIDE_ALL);
		}

		fi->name = g_strdup (p ? p + 1: name);	
		fi->flags = list->flags;
		fi->unread = -1;
		fi->total = -1;
		
		if (!g_ascii_strcasecmp (fi->full_name, "INBOX")) { 
			fi->flags |= CAMEL_FOLDER_SYSTEM | CAMEL_FOLDER_TYPE_INBOX;
		}
		
		if (sfflags == CAMEL_SCALIX_FOLDER_SENT ||
		    sfflags == CAMEL_SCALIX_FOLDER_DRAFTS ||
		    fi->flags & CAMEL_FOLDER_SHARED_TO_ME) {
			fi->flags |= CAMEL_FOLDER_SYSTEM;
		}
		
		if (!g_ascii_strcasecmp (name, "INBOX")) {
			/* SELECTED folder, just get it from the folder */
			if ((folder && !strcmp (folder->full_name, fi->full_name))) {
				camel_folder_refresh_info (folder, &ex);
				camel_object_get (folder, 
						  NULL, 
						  CAMEL_FOLDER_TOTAL, 
						  &fi->total, 
						  CAMEL_FOLDER_UNREAD, 
						  &fi->unread, 
						  0);
				
			} else if (!(flags & CAMEL_STORE_FOLDER_INFO_FAST)) {
				scalix_status (store, fi);
			}
		}
	
		/* special trash handling */
		if ((fi->flags & CAMEL_FOLDER_TYPE_MASK) == CAMEL_FOLDER_TYPE_TRASH) {
			fi->flags |= CAMEL_FOLDER_SYSTEM;
			CAMEL_SCALIX_STORE (store)->trash_full_name = g_strdup (fi->full_name);
		}
		
		array->pdata[i] = fi;
	
		if (!folder_is_special)	{
			camel_scalix_store_summary_note_info (CAMEL_SCALIX_STORE (store)->summary, fi);
		}		

		p = camel_scalix_folder_type_to_name (fi->flags, NULL);	

		if (p != NULL) {
			g_free (fi->name);
			fi->name = p;
		}	
		
		g_free (list->name);
		g_free (list);
	}
	
	fi = camel_scalix_build_folder_info_tree (array, top);
	
	camel_url_free (url);
	
	g_ptr_array_free (array, TRUE);
	
	camel_store_summary_save ((CamelStoreSummary *) ((CamelSCALIXStore *) store)->summary);
	
	return fi;
}

static CamelFolderInfo *
scalix_get_folder_info (CamelStore *store, const char *top, guint32 flags, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelSCALIXCommand *ic, *ic0 = NULL, *ic1 = NULL;
	CamelFolderInfo *inbox = NULL, *fi = NULL;
	const char *base, *namespace;
	camel_scalix_list_t *list;
	GPtrArray *array;
	const char *cmd = "LIST"; /* always use LIST */
	char *pattern;
	char wildcard;
	int id, i;
    
	if (top == NULL)
		top = "";
	
	if (!(namespace = camel_url_get_param (((CamelService *) store)->url, "namespace")))
		namespace = "";
	
	if (!strcmp (top, ""))
		base = namespace;
	else
		base = top;
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif

#ifdef TRUE//USE_FOLDER_INFO_CACHE_LOGIC_FOR_SPEED
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL
	    || engine->state == CAMEL_SCALIX_ENGINE_DISCONNECTED) {
		fi = camel_scalix_store_summary_get_folder_info (((CamelSCALIXStore *) store)->summary, base, flags);
		if (base == namespace && *namespace) {
			inbox = camel_scalix_store_summary_get_folder_info (((CamelSCALIXStore *) store)->summary, "INBOX",
									   flags & ~CAMEL_STORE_FOLDER_INFO_RECURSIVE);
			if (inbox) {
				inbox->next = fi;
				fi = inbox;
			}
		}
		
		if (fi == NULL && ((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_AVAIL) {
			/* folder info hasn't yet been cached and the store hasn't been
			 * connected yet, but the network is available so we can connect
			 * and query the server. */
			goto check_online;
		}

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif

		return fi;
	}
#else
	/* this is the way the old imap code was meant to work (except it was broken and disregarded the
	 * NETWORK_UNAVAIL state and went online anyway if fi was NULL, but we won't be evil like that)
	 */
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		fi = camel_scalix_store_summary_get_folder_info (((CamelSCALIXStore *) store)->summary, base, flags);
		if (base == namespace && *namespace) {
			inbox = camel_scalix_store_summary_get_folder_info (((CamelSCALIXStore *) store)->summary, "INBOX",
									   flags & ~CAMEL_STORE_FOLDER_INFO_RECURSIVE);
			if (inbox) {
				inbox->next = fi;
				fi = inbox;
			}
		}

#if EAPI_CHECK_VERSION (2, 10)
		CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
		CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif

		return fi;
	}
#endif
	
 check_online:

	wildcard = (flags & CAMEL_STORE_FOLDER_INFO_RECURSIVE) ? '*' : '%';
	pattern = scalix_folder_utf7_name (store, base, wildcard);
	array = g_ptr_array_new ();
	
	if (base == namespace && *namespace) {
		/* Make sure to get INBOX: we always use LIST so the user sees his/her INBOX even
		   if it isn't subscribed and the user has enabled "Only show subscribed folders" */
		ic1 = camel_scalix_engine_queue (engine, NULL, "LIST \"\" INBOX\r\n");
		camel_scalix_command_register_untagged (ic1, "LIST", camel_scalix_untagged_list);
		ic1->user_data = array;
	}
	
	if (*top != '\0') {
		size_t len;
		char sep;
		
		len = strlen (pattern);
		sep = pattern[len - 2];
		pattern[len - 2] = '\0';
		
		ic0 = camel_scalix_engine_queue (engine, NULL, "%s \"\" %S\r\n", cmd, pattern);
		camel_scalix_command_register_untagged (ic0, cmd, camel_scalix_untagged_list);
		ic0->user_data = array;
		
		pattern[len - 2] = sep;
	}
	
	ic = camel_scalix_engine_queue (engine, NULL, "%s \"\" %S\r\n", cmd, pattern);
	camel_scalix_command_register_untagged (ic, cmd, camel_scalix_untagged_list);
	ic->user_data = array;
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		if (ic1 && ic1->status != CAMEL_SCALIX_COMMAND_COMPLETE)
			camel_exception_xfer (ex, &ic1->ex);
		else if (ic0 && ic0->status != CAMEL_SCALIX_COMMAND_COMPLETE)
			camel_exception_xfer (ex, &ic0->ex);
		else
			camel_exception_xfer (ex, &ic->ex);
		
		if (ic1 != NULL)
			camel_scalix_command_unref (ic1);
		
		if (ic0 != NULL)
			camel_scalix_command_unref (ic0);
		
		camel_scalix_command_unref (ic);
		
		for (i = 0; i < array->len; i++) {
			list = array->pdata[i];
			g_free (list->name);
			g_free (list);
		}
		
		g_ptr_array_free (array, TRUE);
		g_free (pattern);
		
		goto done;
	}
	
	if (ic1 != NULL)
		camel_scalix_command_unref (ic1);
	
	if (ic0 != NULL)
		camel_scalix_command_unref (ic0);
	
	if (ic->result != CAMEL_SCALIX_RESULT_OK) {
		camel_scalix_command_unref (ic);
		
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot get %s information for pattern `%s' on IMAP server %s: %s"),
				      cmd, pattern, engine->url->host, ic->result == CAMEL_SCALIX_RESULT_BAD ?
				      _("Bad command") : _("Unknown"));
		
		for (i = 0; i < array->len; i++) {
			list = array->pdata[i];
			g_free (list->name);
			g_free (list);
		}
		
		g_ptr_array_free (array, TRUE);
		
		g_free (pattern);
		
		goto done;
	}
	
	g_free (pattern);
	
	fi = scalix_build_folder_info (store, top, flags, array);
	
 done:
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif	
	return fi;
}

static void
scalix_subscribe_folder (CamelStore *store, const char *folder_name, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelSCALIXCommand *ic;
	CamelFolderInfo *fi;
	char *utf7_name;
	CamelURL *url;
	const char *p;
	int id;
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM, _("Cannot subscribe to IMAP folders in offline mode."));
		return;
	}
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif	

	utf7_name = scalix_folder_utf7_name (store, folder_name, '\0');
	ic = camel_scalix_engine_queue (engine, NULL, "SUBSCRIBE %S\r\n", utf7_name);
	g_free (utf7_name);
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		camel_scalix_command_unref (ic);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif

		return;
	}
	
	switch (ic->result) {
	case CAMEL_SCALIX_RESULT_OK:
		/* subscribed */
		url = camel_url_copy (engine->url);
        url->path = g_strdup_printf ("/%s", folder_name);
		
		p = strrchr (folder_name, '/');
		
		fi = g_malloc0 (sizeof (CamelFolderInfo));
		fi->full_name = g_strdup (folder_name);
		fi->name = g_strdup (p ? p + 1: folder_name);
		fi->uri = camel_url_to_string (url, CAMEL_URL_HIDE_ALL);
		camel_url_free (url);
		fi->flags = CAMEL_FOLDER_NOCHILDREN;
		fi->unread = -1;
		fi->total = -1;
		
		camel_scalix_store_summary_note_info (((CamelSCALIXStore *) store)->summary, fi);
		
		camel_object_trigger_event (store, "folder_subscribed", fi);
		camel_folder_info_free (fi);
		break;
	case CAMEL_SCALIX_RESULT_NO:
		/* FIXME: would be good to save the NO reason into the err message */
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot subscribe to folder `%s': Invalid mailbox name"),
				      folder_name);
		break;
	case CAMEL_SCALIX_RESULT_BAD:
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot subscribe to folder `%s': Bad command"),
				      folder_name);
		break;
	}
	
	camel_scalix_command_unref (ic);
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
}

static void
scalix_unsubscribe_folder (CamelStore *store, const char *folder_name, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelSCALIXCommand *ic;
	CamelFolderInfo *fi;
	char *utf7_name;
	CamelURL *url;
	const char *p;
	int id;
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM, _("Cannot unsubscribe from IMAP folders in offline mode."));
		return;
	}
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif	

	utf7_name = scalix_folder_utf7_name (store, folder_name, '\0');
	ic = camel_scalix_engine_queue (engine, NULL, "UNSUBSCRIBE %S\r\n", utf7_name);
	g_free (utf7_name);
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		camel_scalix_command_unref (ic);
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
		return;
	}
	
	switch (ic->result) {
	case CAMEL_SCALIX_RESULT_OK:
		/* unsubscribed */
		url = camel_url_copy (engine->url);
		url->path = g_strdup_printf ("/%s", folder_name);
       
		p = strrchr (folder_name, '/');
		
		fi = g_malloc0 (sizeof (CamelFolderInfo));
		fi->full_name = g_strdup (folder_name);
		fi->name = g_strdup (p ? p + 1: folder_name);
		fi->uri = camel_url_to_string (url, CAMEL_URL_HIDE_ALL);
		camel_url_free (url);
		fi->flags = 0;
		fi->unread = -1;
		fi->total = -1;
		
		camel_scalix_store_summary_unnote_info (((CamelSCALIXStore *) store)->summary, fi);
		
		camel_object_trigger_event (store, "folder_unsubscribed", fi);
		camel_folder_info_free (fi);
		break;
	case CAMEL_SCALIX_RESULT_NO:
		/* FIXME: would be good to save the NO reason into the err message */
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot unsubscribe from folder `%s': Invalid mailbox name"),
				      folder_name);
		break;
	case CAMEL_SCALIX_RESULT_BAD:
		camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
				      _("Cannot unsubscribe from folder `%s': Bad command"),
				      folder_name);
		break;
	}
	
	camel_scalix_command_unref (ic);
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
}

static void
scalix_noop (CamelStore *store, CamelException *ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	CamelFolder *folder = (CamelFolder *) engine->folder;
	CamelSCALIXCommand *ic;
	int id;
	
	if (((CamelOfflineStore *) store)->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL)
		return;
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif	
	if (folder) {
		camel_folder_sync (folder, FALSE, ex);
		if (camel_exception_is_set (ex)) {
#if EAPI_CHECK_VERSION (2, 10)
			CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
			CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
			return;
		}
	}
	
	ic = camel_scalix_engine_queue (engine, NULL, "NOOP\r\n");
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE)
		camel_exception_xfer (ex, &ic->ex);
	
	camel_scalix_command_unref (ic);
	
	if (folder && !camel_exception_is_set (ex))
		camel_scalix_summary_flush_updates (folder->summary, ex);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
	
}


static void 
change_password (CamelSCALIXStore *store, const char *str, CamelException *ex)
{
	CamelSCALIXCommand *ic;
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) store)->engine;
	gchar **sa;
	int id, retval = 0;
	char *oldkey, *newkey;

	sa = g_strsplit (str, "@", -1);

	if (sa == NULL || g_strv_length (sa) != 2) {
		camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM,
				     _("Internal Error %s"));
		g_strfreev (sa);
		return;
	}
	
	oldkey = g_strdup (sa[0]);
	newkey = g_strdup (sa[1]);
	
	camel_url_decode (oldkey);
	camel_url_decode (newkey);
	
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (store, connect_lock);
#endif

	ic = camel_scalix_engine_queue (engine, 
				       NULL, 
				       "X-SCALIX-PASSWORD %s %s\r\n",
				       oldkey,
				       newkey);
	
	while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
		;

	g_free (oldkey);
	g_free (newkey);
	g_strfreev (sa);
	
	if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
		camel_exception_xfer (ex, &ic->ex);
		retval = -1;
	}
	

	switch (ic->result) {
		
		case CAMEL_SCALIX_RESULT_OK:
			/* password changed! Huray! */
			break;

		case CAMEL_SCALIX_RESULT_NO:
			/* FIXME: needed? */
			if (camel_exception_is_set (&ic->ex)) {
				camel_exception_xfer (ex, &ic->ex);
			} else if (! (camel_exception_is_set (ex))){
				camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM,
						      _("Internal Error %s"));
			}
			break;
	

		default:
		case CAMEL_SCALIX_RESULT_BAD:
			/* haha server error, not ture, you can call me 
			 * a liar! */
			camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
					      _("Server error %s"));
			break;
	
	}
	
	camel_scalix_command_unref (ic);
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (store, connect_lock);
#endif
}

static gboolean
scalix_can_refresh_folder (CamelStore *store, CamelFolderInfo *info, CamelException *ex) 
{
	gboolean res; 

	res = CAMEL_STORE_CLASS(parent_class)->can_refresh_folder (store, info, ex) ||
		(camel_url_get_param (((CamelService *)store)->url, "check_all") != NULL);

	/*if (!res && !camel_exception_is_set (ex)) {
		CamelFolder *folder;

		folder = camel_store_get_folder (store, info->full_name, 0, ex); 
		if (folder && CAMEL_IS_IMAP_FOLDER (folder))
			res = CAMEL_IMAP_FOLDER (folder)->check_folder;
	}*/

	return res; 
}

