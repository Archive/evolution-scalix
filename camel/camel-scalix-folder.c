/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Camel
 *  Copyright (C) 1999-2004 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>

#include <glib.h>
#include <glib/gi18n-lib.h>

#include <camel/camel-private.h>
#include <camel/camel-file-utils.h>
#include <camel/camel-stream-filter.h>
#include <camel/camel-mime-filter-crlf.h>
#include <camel/camel-mime-message.h>
#include <camel/camel-multipart.h>
#include <camel/camel-stream-mem.h>
#include <camel/camel-utf8.h>

#include "camel-scalix-utils.h"
#include "camel-scalix-store.h"
#include "camel-scalix-engine.h"
#include "camel-scalix-folder.h"
#include "camel-scalix-journal.h"
#include "camel-scalix-stream.h"
#include "camel-scalix-command.h"
#include "camel-scalix-summary.h"
#include "camel-scalix-search.h"

#include <libescalix/scalix-version.h>

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-data-server-util.h>
#else
#include <libedataserver/e-util.h>
#endif

#define d(x) x

static void camel_scalix_folder_class_init (CamelSCALIXFolderClass * klass);
static void camel_scalix_folder_init (CamelSCALIXFolder * folder,
                                      CamelSCALIXFolderClass * klass);
static void camel_scalix_folder_finalize (CamelObject * object);

static int scalix_getv (CamelObject * object, CamelException * ex,
                        CamelArgGetV * args);
static int scalix_setv (CamelObject * object, CamelException * ex,
                        CamelArgV * args);

static void scalix_sync (CamelFolder * folder, gboolean expunge,
                         CamelException * ex);
static void scalix_refresh_info (CamelFolder * folder, CamelException * ex);
static void scalix_expunge (CamelFolder * folder, CamelException * ex);
static CamelMimeMessage *scalix_get_message (CamelFolder * folder,
                                             const char *uid,
                                             CamelException * ex);
static void scalix_append_message (CamelFolder * folder,
                                   CamelMimeMessage * message,
                                   const CamelMessageInfo * info,
                                   char **appended_uid, CamelException * ex);
static gboolean scalix_set_message_flags (CamelFolder * folder, const char *uid,
                                          guint32 flags, guint32 set);
static void scalix_transfer_messages_to (CamelFolder * src, GPtrArray * uids,
                                         CamelFolder * dest,
                                         GPtrArray ** transferred_uids,
                                         gboolean move, CamelException * ex);
static GPtrArray *scalix_search_by_expression (CamelFolder * folder,
                                               const char *expr,
                                               CamelException * ex);
static GPtrArray *scalix_search_by_uids (CamelFolder * folder, const char *expr,
                                         GPtrArray * uids, CamelException * ex);
static void scalix_search_free (CamelFolder * folder, GPtrArray * uids);
static void scalix_rename (CamelFolder * folder, const char *new);
/*
static char* scalix_get_filename (CamelFolder *folder, const char *uid, CamelException *ex);
*/

static CamelOfflineFolderClass *parent_class = NULL;

static GSList *scalix_folder_props = NULL;

static CamelProperty scalix_prop_list[] = {
	{ CAMEL_SCALIX_FOLDER_ENABLE_MLIST, "mlist_info", N_("Enable mailing-list detection required for some filter and search folder rules") },
	{ CAMEL_SCALIX_FOLDER_EXPIRE_ACCESS, "expire-access", N_("Expire cached messages that haven't been read in X seconds") },
	{ CAMEL_SCALIX_FOLDER_EXPIRE_AGE, "expire-age", N_("Expire cached messages older than X seconds") },
};

CamelType
camel_scalix_folder_get_type (void)
{
    static CamelType type = 0;

    if (!type) {
        type = camel_type_register (camel_offline_folder_get_type (),
                                    "CamelSCALIXFolder",
                                    sizeof (CamelSCALIXFolder),
                                    sizeof (CamelSCALIXFolderClass),
                                    (CamelObjectClassInitFunc)
                                    camel_scalix_folder_class_init, NULL,
                                    (CamelObjectInitFunc)
                                    camel_scalix_folder_init,
                                    (CamelObjectFinalizeFunc)
                                    camel_scalix_folder_finalize);
    }

    return type;
}

static void
camel_scalix_folder_class_init (CamelSCALIXFolderClass * klass)
{
    CamelFolderClass *folder_class = (CamelFolderClass *) klass;
    CamelObjectClass *object_class = (CamelObjectClass *) klass;
    int i;

    parent_class =
        (CamelOfflineFolderClass *)
        camel_type_get_global_classfuncs (CAMEL_OFFLINE_FOLDER_TYPE);

    if (scalix_folder_props == NULL) {
        for (i = 0; i < G_N_ELEMENTS (scalix_prop_list); i++) {
            scalix_prop_list[i].description =
                _(scalix_prop_list[i].description);
            scalix_folder_props =
                g_slist_prepend (scalix_folder_props, &scalix_prop_list[i]);
        }
    }

    object_class->getv = scalix_getv;
    object_class->setv = scalix_setv;

    folder_class->sync = scalix_sync;
    folder_class->refresh_info = scalix_refresh_info;
    folder_class->expunge = scalix_expunge;
    folder_class->get_message = scalix_get_message;
    folder_class->append_message = scalix_append_message;
    folder_class->set_message_flags = scalix_set_message_flags;
    folder_class->transfer_messages_to = scalix_transfer_messages_to;
    folder_class->search_by_expression = scalix_search_by_expression;
    folder_class->search_by_uids = scalix_search_by_uids;
    folder_class->search_free = scalix_search_free;
    folder_class->rename = scalix_rename;
    /*
    folder_class->get_filename = scalix_get_filename;
    */
}

static void
camel_scalix_folder_init (CamelSCALIXFolder * folder,
                          CamelSCALIXFolderClass * klass)
{
    ((CamelFolder *) folder)->folder_flags |=
        CAMEL_FOLDER_HAS_SUMMARY_CAPABILITY |
        CAMEL_FOLDER_HAS_SEARCH_CAPABILITY;

    folder->utf7_name = NULL;
    folder->cachedir = NULL;
    folder->journal = NULL;
    folder->search = NULL;
    folder->need_synch = FALSE;
    folder->is_special = FALSE;
    folder->total = 0;
    folder->uidnext = 0;
}

static void
camel_scalix_folder_finalize (CamelObject * object)
{
    CamelSCALIXFolder *folder = (CamelSCALIXFolder *) object;

    camel_object_unref (folder->search);

    camel_object_unref (folder->cache);

    if (folder->journal) {
        camel_offline_journal_write (folder->journal, NULL);
        camel_object_unref (folder->journal);
    }

    g_free (folder->utf7_name);
    g_free (folder->cachedir);
}

/*
static char*
scalix_get_filename (CamelFolder *folder, const char *uid, CamelException *ex)
{
    CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) folder;

    return camel_data_cache_get_filename (scalix_folder->cache, "cache", uid, ex);
}
*/

static int
scalix_getv (CamelObject *object, CamelException *ex, CamelArgGetV *args)
{
	CamelSCALIXFolder *folder = (CamelSCALIXFolder *) object;
	CamelArgGetV props;
	int i, count = 0;
	guint32 tag;

	for (i = 0; i < args->argc; i++) {
		CamelArgGet *arg = &args->argv[i];

		tag = arg->tag;

		switch (tag & CAMEL_ARG_TAG) {
		case CAMEL_OBJECT_ARG_PERSISTENT_PROPERTIES:
		case CAMEL_FOLDER_ARG_PROPERTIES:
			props.argc = 1;
			props.argv[0] = *arg;
			((CamelObjectClass *) parent_class)->getv (object, ex, &props);
			*arg->ca_ptr = g_slist_concat (*arg->ca_ptr, g_slist_copy (scalix_folder_props));
			break;
		case CAMEL_SCALIX_FOLDER_ARG_ENABLE_MLIST:
			*arg->ca_int = folder->enable_mlist;
			break;
		case CAMEL_SCALIX_FOLDER_ARG_EXPIRE_ACCESS:
			*arg->ca_int = folder->cache->expire_access;
			break;
		case CAMEL_SCALIX_FOLDER_ARG_EXPIRE_AGE:
			*arg->ca_int = folder->cache->expire_age;
			break;
		default:
			count++;
			continue;
		}

		arg->tag = (tag & CAMEL_ARG_TYPE) | CAMEL_ARG_IGNORE;
	}

	if (count)
		return ((CamelObjectClass *) parent_class)->getv (object, ex, args);

	return 0;
}

static int
scalix_setv (CamelObject *object, CamelException *ex, CamelArgV *args)
{
	CamelSCALIXFolder *folder = (CamelSCALIXFolder *) object;
	CamelDataCache *cache = folder->cache;
	gboolean save = FALSE;
	guint32 tag;
	int i;

	for (i = 0; i < args->argc; i++) {
		CamelArg *arg = &args->argv[i];

		tag = arg->tag;

		switch (tag & CAMEL_ARG_TAG) {
		case CAMEL_SCALIX_FOLDER_ARG_ENABLE_MLIST:
			if (folder->enable_mlist != arg->ca_int) {
				folder->enable_mlist = arg->ca_int;
				save = TRUE;
			}
			break;
		case CAMEL_SCALIX_FOLDER_ARG_EXPIRE_ACCESS:
			if (cache->expire_access != (time_t) arg->ca_int) {
				camel_data_cache_set_expire_access (cache, (time_t) arg->ca_int);
				save = TRUE;
			}
			break;
		case CAMEL_SCALIX_FOLDER_ARG_EXPIRE_AGE:
			if (cache->expire_age != (time_t) arg->ca_int) {
				camel_data_cache_set_expire_age (cache, (time_t) arg->ca_int);
				save = TRUE;
			}
			break;
		default:
			continue;
		}

		arg->tag = (tag & CAMEL_ARG_TYPE) | CAMEL_ARG_IGNORE;
	}

	if (save)
		camel_object_state_write (object);

	return ((CamelObjectClass *) parent_class)->setv (object, ex, args);
}

static char *
scalix_get_summary_filename (const char *path)
{
    /* /path/to/imap/summary */
    return g_build_filename (path, "summary", NULL);
}

static char *
scalix_get_journal_filename (const char *path)
{
    /* /path/to/imap/journal */
    return g_build_filename (path, "journal", NULL);
}

static char *
scalix_build_filename (const char *toplevel_dir, const char *full_name)
{
    const char *inptr = full_name;
    int subdirs = 0;
    char *path, *p;

    if (*full_name == '\0')
        return g_strdup (toplevel_dir);

    while (*inptr != '\0') {
        if (*inptr == '/')
            subdirs++;
        inptr++;
    }

    path =
        g_malloc (strlen (toplevel_dir) + (inptr - full_name) + (12 * subdirs) +
                  2);
    p = g_stpcpy (path, toplevel_dir);

    if (p[-1] != '/')
        *p++ = '/';

    inptr = full_name;
    while (*inptr != '\0') {
        while (*inptr != '/' && *inptr != '\0')
            *p++ = *inptr++;

        if (*inptr == '/') {
            p = g_stpcpy (p, "/subfolders/");
            inptr++;

            /* strip extranaeous '/'s */
            while (*inptr == '/')
                inptr++;
        }
    }

    *p = '\0';

    return path;
}

char *
scalix_store_build_filename (void *store, const char *full_name)
{
    CamelSCALIXStore *scalix_store = (CamelSCALIXStore *) store;
    char *toplevel_dir;
    char *path;

    toplevel_dir = g_strdup_printf ("%s/folders", scalix_store->storage_path);
    path = scalix_build_filename (toplevel_dir, full_name);
    g_free (toplevel_dir);

    return path;
}

static char *
get_utf7_name (const char *full_name, const char sep)
{
    char *utf7_name, *p;

    utf7_name = g_alloca (strlen (full_name) + 1);
    strcpy (utf7_name, full_name);

    if (sep != '/') {
        p = utf7_name;
        while (*p != '\0') {
            if (*p == '/')
                *p = sep;
            p++;
        }
    }

    utf7_name = camel_utf8_utf7 (utf7_name);

    return utf7_name;
}

CamelFolder *
camel_scalix_folder_new (CamelStore * store,
                         const char *full_name,
                         guint32 flags, CamelException * ex)
{
    CamelSCALIXFolder *scalix_folder;
    char *utf7_name, *name, *p;
    CamelFolder *folder;
    char *path;
    char sep;
    guint32 type;

    if (!(p = strrchr (full_name, '/')))
        p = (char *) full_name;
    else
        p++;

    name = g_alloca (strlen (p) + 1);
    strcpy (name, p);

    sep =
        camel_scalix_get_path_delim (((CamelSCALIXStore *) store)->summary,
                                     full_name);
    utf7_name = get_utf7_name (full_name, sep);

    folder = (CamelFolder *) (scalix_folder =
                              (CamelSCALIXFolder *)
                              camel_object_new (CAMEL_TYPE_SCALIX_FOLDER));

    p = camel_scalix_folder_type_to_name (flags, NULL);

    camel_folder_construct (folder, store, full_name, p ? p : name);
    scalix_folder->utf7_name = utf7_name;
    scalix_folder->flags = flags;

    if ((type = (flags & CAMEL_SCALIX_SFOLDER_MASK) != 0)) {
        if (type != CAMEL_SCALIX_FOLDER_SENT ||
            type != CAMEL_SCALIX_FOLDER_DRAFTS) {
            scalix_folder->is_special = TRUE;
        }
    }

    if ((type = (flags & CAMEL_FOLDER_TYPE_MASK)) != 0) {
        if (type == CAMEL_FOLDER_TYPE_TRASH) {
            scalix_folder->is_special = FALSE;
            scalix_folder->is_trash = TRUE;
        }
    }

    folder->summary = camel_scalix_summary_new (folder);
    scalix_folder->cachedir =
        scalix_store_build_filename (store, folder->full_name);
    g_mkdir_with_parents (scalix_folder->cachedir, 0777);

    scalix_folder->cache =
        camel_data_cache_new (scalix_folder->cachedir, 0, NULL);

    path = scalix_get_summary_filename (scalix_folder->cachedir);
    camel_folder_summary_set_filename (folder->summary, path);
    g_free (path);

    path = scalix_get_journal_filename (scalix_folder->cachedir);
    scalix_folder->journal = camel_scalix_journal_new (scalix_folder, path);
    g_free (path);

    path = g_build_filename (scalix_folder->cachedir, "cmeta", NULL);
    camel_object_set (folder, NULL, CAMEL_OBJECT_STATE_FILE, path, NULL);
    g_free (path);

    if (camel_object_state_read (folder) == -1) {
        /* set our defaults */
        scalix_folder->enable_mlist = FALSE;
    }

    if (!g_ascii_strcasecmp (full_name, "INBOX")) {
        if (camel_url_get_param (((CamelService *) store)->url, "filter"))
            folder->folder_flags |= CAMEL_FOLDER_FILTER_RECENT;
        if (camel_url_get_param (((CamelService *) store)->url, "filter_junk"))
            folder->folder_flags |= CAMEL_FOLDER_FILTER_JUNK;
    } else if (!camel_url_get_param (((CamelService *) store)->url, "filter_junk_inbox")) {
        if (camel_url_get_param (((CamelService *) store)->url, "filter_junk"))
            folder->folder_flags |= CAMEL_FOLDER_FILTER_JUNK;
    }

    scalix_folder->search =
        camel_scalix_search_new (((CamelSCALIXStore *) store)->engine,
                                 scalix_folder->cachedir);

    if (((CamelOfflineStore *) store)->state ==
        CAMEL_OFFLINE_STORE_NETWORK_AVAIL) {
        /* we don't care if the summary loading fails here */
        camel_folder_summary_load (folder->summary);

        if (camel_scalix_engine_select_folder
            (((CamelSCALIXStore *) store)->engine, folder, ex) == -1) {
            camel_object_unref (folder);
            folder = NULL;
        }

        if (folder
            && camel_scalix_summary_flush_updates (folder->summary, ex) == -1) {
            camel_object_unref (folder);
            folder = NULL;
        }
    } else {
        /* we *do* care if summary loading fails here though */
        if (camel_folder_summary_load (folder->summary) == -1) {
            camel_exception_setv (ex, CAMEL_EXCEPTION_FOLDER_INVALID_PATH,
                                  _("Cannot access folder `%s': %s"),
                                  full_name, g_strerror (ENOENT));

            camel_object_unref (folder);
            folder = NULL;
        }
    }

    return folder;
}

const char *
camel_scalix_folder_utf7_name (CamelSCALIXFolder * folder)
{
    return folder->utf7_name;
}

static struct {
    const char *name;
    guint32 flag;
} scalix_flags[] = {
    { "\\Answered", CAMEL_MESSAGE_ANSWERED},
    { "\\Deleted", CAMEL_MESSAGE_DELETED},
    { "\\Draft", CAMEL_MESSAGE_DRAFT}, 
    { "\\Flagged", CAMEL_MESSAGE_FLAGGED},
 /* { "$Forwarded",  CAMEL_MESSAGE_FORWARDED }, */
    {"\\Seen", CAMEL_MESSAGE_SEEN},
    { "junk",       CAMEL_MESSAGE_JUNK           },
    { "nonjunk",    CAMEL_SCALIX_MESSAGE_NJUNK   },
    { "$label1",    CAMEL_SCALIX_MESSAGE_LABEL1  },
    { "$label2",    CAMEL_SCALIX_MESSAGE_LABEL2  },
    { "$label3",    CAMEL_SCALIX_MESSAGE_LABEL3  },
    { "$label4",    CAMEL_SCALIX_MESSAGE_LABEL4  },
    { "$label5",    CAMEL_SCALIX_MESSAGE_LABEL5  },
};

static int
scalix_sync_flag (CamelFolder * folder, GPtrArray * infos, char onoff,
                  const char *flag, CamelException * ex)
{
    CamelSCALIXEngine *engine =
        ((CamelSCALIXStore *) folder->parent_store)->engine;
    CamelSCALIXCommand *ic;
    int i, id, retval = 0;
    char *set = NULL;

    for (i = 0; i < infos->len;) {
        i += camel_scalix_get_uid_set (engine, folder->summary, infos, i,
                                       30 + strlen (flag), &set);

        ic = camel_scalix_engine_queue (engine, folder,
                                        "UID STORE %s %cFLAGS.SILENT (%s)\r\n",
                                        set, onoff, flag);
        while ((id = camel_scalix_engine_iterate (engine)) < ic->id
               && id != -1) ;

        g_free (set);

        if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
            camel_exception_xfer (ex, &ic->ex);
            camel_scalix_command_unref (ic);

            return -1;
        }

        switch (ic->result) {
        case CAMEL_SCALIX_RESULT_NO:
            /* FIXME: would be good to save the NO reason into the err message */
            camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                                  _
                                  ("Cannot sync flags to folder `%s': Unknown error"),
                                  folder->full_name);
            retval = -1;
            break;
        case CAMEL_SCALIX_RESULT_BAD:
            camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                                  _
                                  ("Cannot sync flags to folder `%s': Bad command"),
                                  folder->full_name);
            retval = -1;
            break;
        }

        camel_scalix_command_unref (ic);

        if (retval == -1)
            return -1;
    }

    return 0;
}

static int
scalix_sync_changes (CamelFolder * folder, GPtrArray * sync,
                     CamelException * ex)
{
    CamelSCALIXMessageInfo *iinfo;
    GPtrArray *on_set, *off_set;
    CamelMessageInfo *info;
    flags_diff_t diff;
    int retval = 0;
    int i, j;

#if 0
    if (folder->permanent_flags == 0)
        return 0;
#endif

    on_set = g_ptr_array_new ();
    off_set = g_ptr_array_new ();

    /* construct commands to sync system and user flags */
    for (i = 0; i < G_N_ELEMENTS (scalix_flags); i++) {
        if (!(scalix_flags[i].flag & folder->permanent_flags))
            continue;

        for (j = 0; j < sync->len; j++) {
            iinfo = (CamelSCALIXMessageInfo *) (info = sync->pdata[j]);
            camel_scalix_flags_diff (&diff, iinfo->server_flags,
                                     iinfo->info.flags);
            if (diff.changed & scalix_flags[i].flag) {
                if (diff.bits & scalix_flags[i].flag) {
                    g_ptr_array_add (on_set, info);
                } else {
                    g_ptr_array_add (off_set, info);
                }
            }
        }

        if (on_set->len > 0) {
            if ((retval =
                 scalix_sync_flag (folder, on_set, '+', scalix_flags[i].name,
                                   ex)) == -1)
                break;

            g_ptr_array_set_size (on_set, 0);
        }

        if (off_set->len > 0) {
            if ((retval =
                 scalix_sync_flag (folder, off_set, '-', scalix_flags[i].name,
                                   ex)) == -1)
                break;

            g_ptr_array_set_size (off_set, 0);
        }
    }

    g_ptr_array_free (on_set, TRUE);
    g_ptr_array_free (off_set, TRUE);

    if (retval == -1)
        return -1;

    for (i = 0; i < sync->len; i++) {
        iinfo = (CamelSCALIXMessageInfo *) (info = sync->pdata[i]);
        iinfo->info.flags &= ~CAMEL_MESSAGE_FOLDER_FLAGGED;
        iinfo->server_flags = iinfo->info.flags & folder->permanent_flags;
    }

    return 0;
}

static void
scalix_sync (CamelFolder * folder, gboolean expunge, CamelException * ex)
{
	CamelSCALIXEngine *engine = ((CamelSCALIXStore *) folder->parent_store)->engine;
	CamelOfflineStore *offline = (CamelOfflineStore *) folder->parent_store;
	CamelSCALIXMessageInfo *iinfo;
	CamelMessageInfo *info;
	CamelSCALIXCommand *ic;
	flags_diff_t diff;
	GPtrArray *sync;
	int id, max, i;
	int retval;

	if (offline->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL)
		return;

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif

	/* gather a list of changes to sync to the server */
	if (folder->permanent_flags) {
		sync = g_ptr_array_new ();
		max = camel_folder_summary_count (folder->summary);
		for (i = 0; i < max; i++) {
			iinfo = (CamelSCALIXMessageInfo *) (info = camel_folder_summary_index (folder->summary, i));
			if (iinfo->info.flags & CAMEL_MESSAGE_FOLDER_FLAGGED) {
				camel_scalix_flags_diff (&diff, iinfo->server_flags, iinfo->info.flags);
				diff.changed &= folder->permanent_flags;

				/* weed out flag changes that we can't sync to the server */
				if (!diff.changed)
					camel_message_info_free (info);
				else
					g_ptr_array_add (sync, info);
			} else {
				camel_message_info_free (info);
			}
		}

		if (sync->len > 0) {
			retval = scalix_sync_changes (folder, sync, ex);

			for (i = 0; i < sync->len; i++)
				camel_message_info_free (sync->pdata[i]);

			g_ptr_array_free (sync, TRUE);

			if (retval == -1)
				goto done;
		} else {
			g_ptr_array_free (sync, TRUE);
		}
	}

	if (expunge && !((CamelSCALIXFolder *) folder)->read_only) {
		ic = camel_scalix_engine_queue (engine, folder, "EXPUNGE\r\n");
		while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1)
			;

		switch (ic->result) {
		case CAMEL_SCALIX_RESULT_OK:
			camel_scalix_summary_flush_updates (folder->summary, ex);
			break;
		case CAMEL_SCALIX_RESULT_NO:
			/* FIXME: would be good to save the NO reason into the err message */
			camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
			                      _("Cannot expunge folder `%s': Unknown error"),
			                      folder->full_name);
			break;
		case CAMEL_SCALIX_RESULT_BAD:
			camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
			                      _("Cannot expunge folder `%s': Bad command"),
			                      folder->full_name);
			break;
		}

		camel_scalix_command_unref (ic);
	} else {
		camel_scalix_summary_flush_updates (folder->summary, ex);
	}

	camel_folder_summary_save (folder->summary);

done:

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif
}

static void
scalix_expunge (CamelFolder * folder, CamelException * ex)
{

    /* trash mode: if we are a trash folder we have to 
     * mark every message as deleted first and then 
     * do the expunge */
    if (CAMEL_SCALIX_FOLDER (folder)->is_trash == TRUE) {
        GPtrArray *uids;
        int i, n;

        uids = camel_folder_get_uids (folder);
        n = uids != NULL ? uids->len : 0;

        for (i = 0; i < n; i++) {
            const char *uid;
            static const int dflag = CAMEL_MESSAGE_DELETED;

            uid = g_ptr_array_index (uids, i);

            /* since we have thing overwritten in this 
             * class use the parent_class' one */
            CAMEL_FOLDER_CLASS (parent_class)->set_message_flags (folder,
                                                                  uid,
                                                                  dflag, dflag);
        }
    }

    scalix_sync (folder, TRUE, ex);
}

    extern int camel_application_is_exiting;
static void
scalix_refresh_info (CamelFolder * folder, CamelException * ex)
{
    CamelSCALIXEngine *engine =
        ((CamelSCALIXStore *) folder->parent_store)->engine;
    CamelOfflineStore *offline = (CamelOfflineStore *) folder->parent_store;
    CamelFolder *selected = (CamelFolder *) engine->folder;
    CamelSCALIXFolder *sf = (CamelSCALIXFolder *) folder;
    camel_scalix_status_attr_t *attr, *next;
    camel_scalix_status_t *status;
    CamelSCALIXCommand *ic;
    GPtrArray *array;
    guint32 uidnext;
    guint32 total;
    guint32 type;
    int id;
    int i;

    if (offline->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL || camel_application_is_exiting)
        return;


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif


    type = (sf->flags & CAMEL_SCALIX_SFOLDER_MASK);

    if (folder == selected || type == CAMEL_SCALIX_FOLDER_CALENDAR
        || type == CAMEL_SCALIX_FOLDER_CONTACT) {
        ic = camel_scalix_engine_queue (engine, NULL,
                                        "STATUS %S (MESSAGES UIDNEXT)\r\n",
                                        sf->utf7_name);

        camel_scalix_command_register_untagged (ic,
                                                "STATUS",
                                                camel_scalix_untagged_status);

        ic->user_data = array = g_ptr_array_new ();

        while ((id = camel_scalix_engine_iterate (engine)) < ic->id
               && id != -1) ;

        if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
            camel_scalix_command_unref (ic);
            g_ptr_array_free (array, TRUE);
            goto done;
        }

        uidnext = total = 0;
        for (i = 0; i < array->len; i++) {
            status = array->pdata[i];
            attr = status->attr_list;
            while (attr != NULL) {
                next = attr->next;

                if (attr->type == CAMEL_SCALIX_STATUS_MESSAGES) {
                    total = attr->value;
                } else if (attr->type == CAMEL_SCALIX_STATUS_UIDNEXT) {
                    uidnext = attr->value;
                }

                g_free (attr);
                attr = next;
            }

            g_free (status->mailbox);
            g_free (status);
        }

        /* dont do it the first time! */
        if (sf->uidnext != 0 || sf->total != 0) {
            sf->need_synch = (sf->uidnext != uidnext || sf->total != total);
        }

        camel_scalix_summary_set_uidnext (folder->summary, uidnext);
        camel_scalix_summary_set_exists (folder->summary, total);

        sf->uidnext = uidnext;
        sf->total = total;

        camel_scalix_command_unref (ic);
        g_ptr_array_free (array, TRUE);

    } else {
        if (camel_scalix_engine_select_folder (engine, folder, ex) == -1)
            goto done;

        ((CamelSCALIXSummary *) folder->summary)->update_flags = TRUE;
    }

    camel_scalix_summary_flush_updates (folder->summary, ex);

  done:
#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif
}


static int
untagged_fetch (CamelSCALIXEngine * engine,
                CamelSCALIXCommand * ic,
                guint32 index,
                camel_scalix_token_t * token, CamelException * ex)
{
    CamelFolderSummary *summary = ((CamelFolder *) engine->folder)->summary;
    CamelStream *fstream, *stream = ic->user_data;
    CamelFolderChangeInfo *changes;
    CamelSCALIXMessageInfo *iinfo;
    CamelMessageInfo *info;
    CamelMimeFilter *crlf;
    guint32 flags;

    if (camel_scalix_engine_next_token (engine, token, ex) == -1)
        return -1;

    /* parse the FETCH response list */
    if (token->token != '(') {
        camel_scalix_utils_set_unexpected_token_error (ex, engine, token);
        return -1;
    }

    do {
        if (camel_scalix_engine_next_token (engine, token, ex) == -1)
            goto exception;

        if (token->token == ')' || token->token == '\n')
            break;

        if (token->token != CAMEL_SCALIX_TOKEN_ATOM)
            goto unexpected;

        if (!strcmp (token->v.atom, "BODY[")) {
            if (camel_scalix_engine_next_token (engine, token, ex) == -1)
                goto exception;

            if (token->token != ']')
                goto unexpected;

            if (camel_scalix_engine_next_token (engine, token, ex) == -1)
                goto exception;

            if (token->token != CAMEL_SCALIX_TOKEN_LITERAL)
                goto unexpected;

            fstream =
                (CamelStream *) camel_stream_filter_new_with_stream (stream);
            crlf =
                camel_mime_filter_crlf_new (CAMEL_MIME_FILTER_CRLF_DECODE,
                                            CAMEL_MIME_FILTER_CRLF_MODE_CRLF_ONLY);
            camel_stream_filter_add ((CamelStreamFilter *) fstream, crlf);
            camel_object_unref (crlf);

            camel_stream_write_to_stream ((CamelStream *) engine->istream,
                                          fstream);
            camel_stream_flush (fstream);
            camel_object_unref (fstream);
        } else if (!strcmp (token->v.atom, "UID")) {
            if (camel_scalix_engine_next_token (engine, token, ex) == -1)
                goto exception;

            if (token->token != CAMEL_SCALIX_TOKEN_NUMBER
                || token->v.number == 0)
                goto unexpected;
        } else if (!strcmp (token->v.atom, "FLAGS")) {
            /* even though we didn't request this bit of information, it might be
             * given to us if another client recently changed the flags... */
            if (camel_scalix_parse_flags_list (engine, &flags, ex) == -1)
                goto exception;

            if ((info = camel_folder_summary_index (summary, index - 1))) {
                iinfo = (CamelSCALIXMessageInfo *) info;
                iinfo->info.flags =
                    camel_scalix_merge_flags (iinfo->server_flags,
                                              iinfo->info.flags, flags);
                iinfo->server_flags = flags;

                changes = camel_folder_change_info_new ();
                camel_folder_change_info_change_uid (changes,
                                                     camel_message_info_uid
                                                     (info));
		
                camel_object_trigger_event (engine->folder, "folder_changed",
                                            changes);
                camel_folder_change_info_free (changes);

                camel_message_info_free (info);
            }
        } else {
            goto unexpected;	
        }
    } while (1);

    if (token->token != ')') {    
        goto unexpected;
    }

    return 0;

  unexpected:

    camel_scalix_utils_set_unexpected_token_error (ex, engine, token);

  exception:

    return -1;
}

static CamelMimeMessage *
scalix_get_message (CamelFolder * folder, const char *uid, CamelException * ex)
{
    CamelSCALIXEngine *engine =
        ((CamelSCALIXStore *) folder->parent_store)->engine;
    CamelOfflineStore *offline = (CamelOfflineStore *) folder->parent_store;
    CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) folder;
    CamelMimeMessage *message = NULL;
    CamelStream *stream, *cache;
    CamelSCALIXCommand *ic;
    int id;


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif


    if (scalix_folder->cache
        && (stream =
            camel_data_cache_get (scalix_folder->cache, "cache", uid, ex))) {
        message = camel_mime_message_new ();

        if (camel_data_wrapper_construct_from_stream
            ((CamelDataWrapper *) message, stream) == -1) {
            if (errno == EINTR) {

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif


                camel_exception_setv (ex, CAMEL_EXCEPTION_USER_CANCEL,
                                      _("User cancelled"));
                camel_object_unref (message);
                camel_object_unref (stream);
                return NULL;
            } else {
                camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                                      _("Cannot get message %s: %s"), uid,
                                      g_strerror (errno));
                camel_object_unref (message);
                message = NULL;
            }
        }

        camel_object_unref (stream);
    }

    if (message != NULL) {

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif


        return message;
    }

    if (offline->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif


        camel_exception_set (ex, CAMEL_EXCEPTION_SERVICE_UNAVAILABLE,
                             _
                             ("This message is not available in offline mode."));
        return NULL;
    }

    /* Note: While some hard-core IMAP extremists are probably
     * going to flame me for fetching entire messages here, it's
     * the *only* sure-fire way of working with all IMAP
     * servers. There are numerous problems with fetching
     * individual MIME parts from a good handful of IMAP servers
     * which makes this a pain to do the Right Way (tm). For
     * example: Courier-IMAP has "issues" parsing some multipart
     * messages apparently, because BODY responses are often
     * inaccurate. I'm also not very trusting of the free German
     * IMAP hosting either (such as mail.gmx.net and imap.web.de)
     * as they have proven themselves to be quite flakey wrt FETCH
     * requests (they seem to be written exclusively for
     * Outlook). Also, some IMAP servers such as GroupWise don't
     * store mail in MIME format and so must re-construct the
     * entire message in order to extract the requested part, so
     * it is *much* more efficient (generally) to just request the
     * entire message anyway. */
    ic = camel_scalix_engine_queue (engine, folder,
                                    "UID FETCH %s BODY.PEEK[]\r\n", uid);
    camel_scalix_command_register_untagged (ic, "FETCH", untagged_fetch);
    ic->user_data = stream = camel_stream_mem_new ();

    while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1) ;

    if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
        camel_exception_xfer (ex, &ic->ex);
        camel_scalix_command_unref (ic);
        camel_object_unref (stream);
        goto done;
    }

    switch (ic->result) {
    case CAMEL_SCALIX_RESULT_OK:
        camel_stream_reset (stream);
        message = camel_mime_message_new ();
        camel_data_wrapper_construct_from_stream ((CamelDataWrapper *) message,
                                                  stream);
        camel_stream_reset (stream);

        /* cache the message locally */
        if (scalix_folder->cache
            && (cache =
                camel_data_cache_add (scalix_folder->cache, "cache", uid,
                                      NULL))) {
            if (camel_stream_write_to_stream (stream, cache) == -1
                || camel_stream_flush (cache) == -1)
                camel_data_cache_remove (scalix_folder->cache, "cache", uid,
                                         NULL);
            camel_object_unref (cache);
        }

        break;
    case CAMEL_SCALIX_RESULT_NO:
        /* FIXME: would be good to save the NO reason into the err message */
        camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                              _
                              ("Cannot get message %s from folder `%s': No such message"),
                              uid, folder->full_name);
        break;
    case CAMEL_SCALIX_RESULT_BAD:
        camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                              _
                              ("Cannot get message %s from folder `%s': Bad command"),
                              uid, folder->full_name);
        break;
    }

    camel_scalix_command_unref (ic);

    camel_object_unref (stream);

  done:


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif


    return message;
}

static char *tm_months[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static void
scalix_append_message (CamelFolder * folder, CamelMimeMessage * message,
                       const CamelMessageInfo * info, char **appended_uid,
                       CamelException * ex)
{
    CamelSCALIXEngine *engine =
        ((CamelSCALIXStore *) folder->parent_store)->engine;
    CamelOfflineStore *offline = (CamelOfflineStore *) folder->parent_store;
    CamelSCALIXSummary *summary = (CamelSCALIXSummary *) folder->summary;
    const CamelSCALIXMessageInfo *iinfo = (const CamelSCALIXMessageInfo *) info;
    CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) folder;
    CamelSCALIXRespCode *resp;
    CamelSCALIXCommand *ic;
    CamelFolderInfo *fi;
    CamelException lex;
    const char *old_uid = NULL;
	guint32 merged_flags;
    char flags[100], *p;
    char date[50];
    struct tm tm;
    int id, i;

    if (appended_uid)
        *appended_uid = NULL;

    if (((CamelSCALIXFolder *) folder)->read_only) {
        camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                              _("Cannot append message to folder '%s': Folder is read-only"),
                              folder->full_name);
        return;
    }

    if (offline->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
        camel_scalix_journal_append ((CamelSCALIXJournal *) scalix_folder->
                                     journal, message, info, appended_uid, ex);
        return;
    }


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif


	merged_flags = iinfo->info.flags | scalix_label_to_flags ((CamelMessageInfo *)info);
	
    /* construct the option flags list */
    if (merged_flags & folder->permanent_flags) {
	 
        p = g_stpcpy (flags, " (");

        for (i = 0; i < G_N_ELEMENTS (scalix_flags); i++) {
            if ((merged_flags & scalix_flags[i].flag) & folder->
                permanent_flags) {
                p = g_stpcpy (p, scalix_flags[i].name);
                *p++ = ' ';
            }
        }

        p[-1] = ')';
        *p = '\0';
    } else {
        flags[0] = '\0';
    }

    /* construct the optional date_time string */
    if (iinfo->info.date_received > (time_t) 0) {
        int tzone;

#ifdef HAVE_LOCALTIME_R
        localtime_r (&iinfo->info.date_received, &tm);
#else
        memcpy (&tm, localtime (&iinfo->info.date_received), sizeof (tm));
#endif

#if defined (HAVE_TM_GMTOFF)
        tzone = -tm.tm_gmtoff;
#elif defined (HAVE_TIMEZONE)
        if (tm.tm_isdst > 0) {
#if defined (HAVE_ALTZONE)
            tzone = altzone;
#else /* !defined (HAVE_ALTZONE) */
            tzone = (timezone - 3600);
#endif
        } else {
            tzone = timezone;
        }
#else
#error Neither HAVE_TIMEZONE nor HAVE_TM_GMTOFF defined. Rerun autoheader, autoconf, etc.
#endif

        sprintf (date, " \"%02d-%s-%04d %02d:%02d:%02d %+05d\"",
                 tm.tm_mday, tm_months[tm.tm_mon], tm.tm_year + 1900,
                 tm.tm_hour, tm.tm_min, tm.tm_sec, tzone);
    } else {
        date[0] = '\0';
    }

  retry:

    /* Don't use APPEND UID because scalix server doesn't handle that */

    if ((old_uid =
         camel_medium_get_header (CAMEL_MEDIUM (message),
                                  "X-SCALIX-STORE-UID"))) {
        CamelFolder *selected = (CamelFolder *) engine->folder;
        CamelDataWrapper *data;
        CamelDataWrapper *content;
        CamelMimePart *part;

        if (folder != selected) {
            if (camel_scalix_engine_select_folder (engine, folder, ex) == -1) {
                goto out;
            }
        }

        content =
            camel_medium_get_content_object (CAMEL_MEDIUM
                                             (CAMEL_MIME_PART (message)));
        part = camel_multipart_get_part ((CamelMultipart *) content, 0);

        if (part != NULL) {
            // multi-part message
            data = camel_medium_get_content_object (CAMEL_MEDIUM (part));
        } else {
            // single-part message
            data = camel_medium_get_content_object (CAMEL_MEDIUM (message));
        }

        ic = camel_scalix_engine_queue (engine, folder,
                                        "UID STORE %s X-SCALIX-PROPS %L\r\n",
                                        old_uid, data);

    } else {
        ic = camel_scalix_engine_queue (engine, NULL, "APPEND %F%s%s %L\r\n",
                                        folder, flags, date, message);
    }

    while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1) ;

    if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
        camel_exception_xfer (ex, &ic->ex);
        camel_scalix_command_unref (ic);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif


        return;
    }

    switch (ic->result) {
    case CAMEL_SCALIX_RESULT_OK:
        if (!appended_uid || !(engine->capa & CAMEL_SCALIX_CAPABILITY_UIDPLUS))
            break;

        for (i = 0; i < ic->resp_codes->len; i++) {
            resp = ic->resp_codes->pdata[i];
            if (resp->code == CAMEL_SCALIX_RESP_CODE_APPENDUID) {
                if (resp->v.appenduid.uidvalidity == summary->uidvalidity)
                    *appended_uid =
                        g_strdup_printf ("%u", resp->v.appenduid.uid);
                break;
            } else if (resp->code == CAMEL_SCALIX_RESP_CODE_STOREUID) {
                *appended_uid = g_strdup_printf ("%u", resp->v.appenduid.uid);
            }
        }

        /* update the folder summary */
        if (old_uid != NULL) {
            CamelMessageInfo *mi =
                camel_folder_summary_uid (folder->summary, old_uid);
            if (mi != NULL) {
                CamelMessageInfo *clone = camel_message_info_clone (mi);

                clone->uid = g_strdup (*appended_uid);
                camel_folder_summary_add (folder->summary, clone);
            }
        } else {
            CamelMessageInfo *mi =
                camel_folder_summary_info_new_from_message (folder->summary,
                                                            message);

            mi->uid = g_strdup (*appended_uid);
            camel_folder_summary_add (folder->summary, mi);
        }

        break;
    case CAMEL_SCALIX_RESULT_NO:
        /* FIXME: can we give the user any more information? */
        camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                              _
                              ("Cannot append message to folder `%s': Unknown error"),
                              folder->full_name);

        for (i = 0; i < ic->resp_codes->len; i++) {
            resp = ic->resp_codes->pdata[i];
            if (resp->code == CAMEL_SCALIX_RESP_CODE_TRYCREATE) {
                char *parent_name, *p;

                parent_name = g_alloca (strlen (folder->full_name) + 1);
                if (!(p = strrchr (parent_name, '/')))
                    *parent_name = '\0';
                else
                    *p = '\0';

                if (!
                    (fi =
                     camel_store_create_folder (folder->parent_store,
                                                parent_name, folder->name,
                                                &lex))) {
                    camel_exception_clear (&lex);
                    break;
                }

                camel_store_free_folder_info (folder->parent_store, fi);
                camel_scalix_command_unref (ic);
                camel_exception_clear (ex);
                goto retry;
            }
        }

        break;
    case CAMEL_SCALIX_RESULT_BAD:
        camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                              _
                              ("Cannot append message to folder `%s': Bad command"),
                              folder->full_name);

        break;
    default:
        g_assert_not_reached ();
    }

    camel_scalix_command_unref (ic);

  out:

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif


}

struct move_to_trash_msg {

    CamelSessionThreadMsg msg;

    CamelFolder *source;
    CamelFolder *trash;
    GPtrArray *uids;

};

static void
move_to_trash_do (CamelSession * session, CamelSessionThreadMsg * mm)
{
    struct move_to_trash_msg *m;

    m = (struct move_to_trash_msg *) mm;

    camel_operation_start (NULL, _("Moving message to trash folder"));

    camel_folder_transfer_messages_to (m->source,
                                       m->uids, m->trash, NULL, TRUE, &mm->ex);

    if (camel_exception_is_set (&mm->ex)) {
        char *msg;

        msg = g_strdup_printf (_("Error while moving message to trash: %s"),
                               mm->ex.desc);

        camel_session_alert_user ((session),
                                  CAMEL_SESSION_ALERT_ERROR, msg, FALSE);
        g_free (msg);
    }

    camel_operation_end (NULL);
}

static void
move_to_trash_free (CamelSession * session, CamelSessionThreadMsg * mm)
{
    struct move_to_trash_msg *m;

    m = (struct move_to_trash_msg *) mm;

    camel_object_unref (m->source);
    camel_object_unref (m->trash);

    g_ptr_array_foreach (m->uids, (GFunc) g_free, NULL);
    g_ptr_array_free (m->uids, TRUE);

}

static CamelSessionThreadOps move_to_trash_ops = {

    move_to_trash_do,
    move_to_trash_free,

};

static void
move_to_trash (CamelService * service,
               CamelFolder * source, CamelFolder * trash, const char *uid)
{
    CamelSession *session = service->session;
    GPtrArray *array;
    struct move_to_trash_msg *m;

    array = g_ptr_array_sized_new (1);
    g_ptr_array_add (array, (gpointer) g_strdup (uid));

    if (FALSE) {
        m = camel_session_thread_msg_new (session, &move_to_trash_ops,
                                          sizeof (*m));

        camel_object_ref (source);
        m->source = source;

        camel_object_ref (trash);
        m->trash = trash;

        m->uids = array;
        camel_session_thread_queue (session, &m->msg, 0);

    } else {
        CamelException ex;

        camel_exception_init (&ex);
        camel_folder_transfer_messages_to (source,
                                           array, trash, NULL, TRUE, &ex);

        if (camel_exception_is_set (&ex)) {
            char *msg;

            msg = g_strdup_printf (_("Error while moving message to trash: %s"),
                                   ex.desc);

            camel_session_alert_user ((session),
                                      CAMEL_SESSION_ALERT_ERROR, msg, FALSE);
            g_free (msg);
        }

        g_ptr_array_foreach (array, (GFunc) g_free, NULL);
        g_ptr_array_free (array, TRUE);

    }

}

static gboolean
scalix_set_message_flags (CamelFolder * folder,
                          const char *uid, guint32 flags, guint32 set)
{
    CamelSCALIXStore *store;
    CamelFolder *trash_folder;
    CamelException ex;
    CamelSCALIXFolder *cfolder;
    gboolean kill;
    gboolean result;
    gboolean delete;
    guint32 type;

    store = CAMEL_SCALIX_STORE (folder->parent_store);
    cfolder = CAMEL_SCALIX_FOLDER (folder);
    delete = (flags & CAMEL_MESSAGE_DELETED && set & CAMEL_MESSAGE_DELETED);
    result = TRUE;
    type = (cfolder->flags & CAMEL_SCALIX_SFOLDER_MASK);

    /* if this is a calendar, contacts or the trash folder, kill the message */
    kill = (type == CAMEL_SCALIX_FOLDER_CALENDAR ||
            type == CAMEL_SCALIX_FOLDER_CONTACT || cfolder->is_trash);

    /* delete normally is paired with erasing the seen
     * flag, but we dont want that in the move case */
    if (delete == TRUE && kill == FALSE) {

        set &= ~(CAMEL_MESSAGE_DELETED | CAMEL_MESSAGE_SEEN);
        flags &= ~(CAMEL_MESSAGE_DELETED | CAMEL_MESSAGE_SEEN);
    }

    /* Set any remaining flags */
    if (flags != 0) {
        result = CAMEL_FOLDER_CLASS (parent_class)->set_message_flags (folder,
                                                                       uid,
                                                                       flags,
                                                                       set);
    }

    if (delete == FALSE) {
        /* No need to do anything else */
        return result;
    }

    camel_exception_init (&ex);

    if (CAMEL_SCALIX_FOLDER (folder)->is_trash == FALSE && kill == FALSE) {

        trash_folder = camel_store_get_folder (CAMEL_STORE (store),
                                               store->trash_full_name, 0, &ex);

        if (camel_exception_is_set (&ex)) {
            goto done;
        }

        move_to_trash (CAMEL_SERVICE (store), folder, trash_folder, uid);

        camel_object_unref (trash_folder);

    } else {
        /* EXPUNGE! */
        scalix_sync (folder, TRUE, &ex);
    }

  done:
    if (camel_exception_is_set (&ex)) {
        g_warning ("Error deleting message");
        camel_exception_clear (&ex);
        result = FALSE;
    }

    return result;
}

static int
info_uid_sort (const CamelMessageInfo ** info0, const CamelMessageInfo ** info1)
{
    guint32 uid0, uid1;

    uid0 = strtoul (camel_message_info_uid (*info0), NULL, 10);
    uid1 = strtoul (camel_message_info_uid (*info1), NULL, 10);

    if (uid0 == uid1)
        return 0;

    return uid0 < uid1 ? -1 : 1;
}

static void
scalix_transfer_messages_to (CamelFolder * src, GPtrArray * uids,
                             CamelFolder * dest, GPtrArray ** transferred_uids,
                             gboolean move, CamelException * ex)
{
    CamelSCALIXEngine *engine =
        ((CamelSCALIXStore *) src->parent_store)->engine;
    CamelOfflineStore *offline = (CamelOfflineStore *) src->parent_store;
    int i, n, id, dest_namelen;
    CamelMessageInfo *info;
    CamelSCALIXCommand *ic;
    GPtrArray *infos;
    char *set;

    if (transferred_uids) {
        *transferred_uids = NULL;
    }

    infos = g_ptr_array_new ();
    for (i = 0; i < uids->len; i++) {
        if (!(info = camel_folder_summary_uid (src->summary, uids->pdata[i])))
            continue;

        g_ptr_array_add (infos, info);
    }

    if (infos->len == 0) {
        g_ptr_array_free (infos, TRUE);
        return;
    }

    g_ptr_array_sort (infos, (GCompareFunc) info_uid_sort);


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (src->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (src->parent_store, connect_lock);
#endif


    /* check for offline operation */
    if (offline->state == CAMEL_OFFLINE_STORE_NETWORK_UNAVAIL) {
        CamelSCALIXJournal *journal =
            (CamelSCALIXJournal *) ((CamelSCALIXFolder *) dest)->journal;
        CamelMimeMessage *message;

        for (i = 0; i < infos->len; i++) {
            info = infos->pdata[i];

            if (!(message = scalix_get_message (src,
                                                camel_message_info_uid (info),
                                                ex))) {
                break;
            }

            camel_scalix_journal_append (journal, message, info, NULL, ex);
            camel_object_unref (message);

            if (camel_exception_is_set (ex))
                break;

            if (move) {
                /* FIXME offline trash handling */
                camel_folder_set_message_flags (src,
                                                camel_message_info_uid (info),
                                                CAMEL_MESSAGE_DELETED,
                                                CAMEL_MESSAGE_DELETED);
            }
        }

        goto done;
    }

    dest_namelen =
        strlen (camel_scalix_folder_utf7_name (CAMEL_SCALIX_FOLDER (dest)));

    for (i = 0; i < infos->len; i += n) {

        n = camel_scalix_get_uid_set (engine,
                                      src->summary,
                                      infos, i, 10 + dest_namelen, &set);

        if (move == TRUE) {
            ic = camel_scalix_engine_queue (engine,
                                            src,
                                            "UID X-MOVE %s %F\r\n", set, dest);
        } else {
            ic = camel_scalix_engine_queue (engine,
                                            src,
                                            "UID COPY %s %F\r\n", set, dest);
        }

        while ((id = camel_scalix_engine_iterate (engine)) < ic->id
               && id != -1) ;

        g_free (set);

        if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
            camel_exception_xfer (ex, &ic->ex);
            camel_scalix_command_unref (ic);
            g_free (set);
            goto done;
        }

        switch (ic->result) {

        case CAMEL_SCALIX_RESULT_NO:
            /* FIXME: would be good to save the NO reason into the err message */
            if (move) {
                camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                                      _
                                      ("Cannot move messages from folder `%s' to folder `%s': Unknown"),
                                      src->full_name, dest->full_name);
            } else {
                camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                                      _
                                      ("Cannot copy messages from folder `%s' to folder `%s': Unknown"),
                                      src->full_name, dest->full_name);
            }

            goto done;

        case CAMEL_SCALIX_RESULT_BAD:
            if (move) {
                camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                                      _
                                      ("Cannot move messages from folder `%s' to folder `%s': Bad command"),
                                      src->full_name, dest->full_name);
            } else {
                camel_exception_setv (ex, CAMEL_EXCEPTION_SYSTEM,
                                      _
                                      ("Cannot copy messages from folder `%s' to folder `%s': Bad command"),
                                      src->full_name, dest->full_name);
            }

            goto done;
        }

        camel_scalix_command_unref (ic);
    }

  done:

    for (i = 0; i < infos->len; i++) {
        camel_message_info_free (infos->pdata[i]);
    }

    g_ptr_array_free (infos, TRUE);


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (src->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (src->parent_store, connect_lock);
#endif


}

static GPtrArray *
scalix_search_by_expression (CamelFolder * folder, const char *expr,
                             CamelException * ex)
{
    CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) folder;
    GPtrArray *matches;

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif
    camel_folder_search_set_folder (scalix_folder->search, folder);
    matches =
        camel_folder_search_search (scalix_folder->search, expr, NULL, ex);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif



    return matches;
}

static GPtrArray *
scalix_search_by_uids (CamelFolder * folder, const char *expr, GPtrArray * uids,
                       CamelException * ex)
{
    CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) folder;
    GPtrArray *matches;

    if (uids->len == 0)
        return g_ptr_array_new ();

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif
    camel_folder_search_set_folder (scalix_folder->search, folder);
    matches =
        camel_folder_search_search (scalix_folder->search, expr, uids, ex);


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif


    return matches;
}

static void
scalix_search_free (CamelFolder * folder, GPtrArray * uids)
{
    CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) folder;

    g_return_if_fail (scalix_folder->search);

#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_LOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_LOCK (folder->parent_store, connect_lock);
#endif
    camel_folder_search_free_result (scalix_folder->search, uids);


#if EAPI_CHECK_VERSION (2, 10)
	CAMEL_SERVICE_REC_UNLOCK (folder->parent_store, connect_lock);
#else
	CAMEL_SERVICE_UNLOCK (folder->parent_store, connect_lock);
#endif
}


static void
scalix_rename (CamelFolder * folder, const char *new)
{
    CamelSCALIXFolder *scalix_folder = (CamelSCALIXFolder *) folder;
    CamelSCALIXStore *store = CAMEL_SCALIX_STORE (folder->parent_store);
    char sep;
    char *cachedir, *path;

    /* Let the parent class rename all full_name and name */
    CAMEL_FOLDER_CLASS (parent_class)->rename (folder, new);

    /* replace utf7 name with new one */
    g_free (scalix_folder->utf7_name);
    sep = camel_scalix_get_path_delim (store->summary, folder->full_name);

    scalix_folder->utf7_name = get_utf7_name (folder->full_name, sep);

    camel_object_unref (scalix_folder->journal);
    camel_object_unref (scalix_folder->cache);

    cachedir = scalix_store_build_filename (store, folder->full_name);
    g_free (scalix_folder->cachedir);
    scalix_folder->cachedir = cachedir;
    g_mkdir_with_parents (scalix_folder->cachedir, 0777);

    scalix_folder->cache = camel_data_cache_new (scalix_folder->cachedir,
                                                 0, NULL);

    path = scalix_get_summary_filename (scalix_folder->cachedir);
    camel_folder_summary_set_filename (folder->summary, path);
    camel_folder_summary_save (folder->summary);
    g_free (path);

    path = scalix_get_journal_filename (scalix_folder->cachedir);
    scalix_folder->journal = camel_scalix_journal_new (scalix_folder, path);
    g_free (path);

    path = g_build_filename (scalix_folder->cachedir, "cmeta", NULL);
    camel_object_set (folder, NULL, CAMEL_OBJECT_STATE_FILE, path, NULL);
    g_free (path);

}
