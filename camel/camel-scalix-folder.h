/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Camel
 *  Copyright (C) 1999-2004 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 */


#ifndef __CAMEL_SCALIX_FOLDER_H__
#define __CAMEL_SCALIX_FOLDER_H__

#include <camel/camel-store.h>
#include <camel/camel-folder.h>
#include <camel/camel-data-cache.h>
#include <camel/camel-offline-folder.h>
#include <camel/camel-offline-journal.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define CAMEL_TYPE_SCALIX_FOLDER            (camel_scalix_folder_get_type ())
#define CAMEL_SCALIX_FOLDER(obj)            (CAMEL_CHECK_CAST ((obj), CAMEL_TYPE_SCALIX_FOLDER, CamelSCALIXFolder))
#define CAMEL_SCALIX_FOLDER_CLASS(klass)    (CAMEL_CHECK_CLASS_CAST ((klass), CAMEL_TYPE_SCALIX_FOLDER, CamelSCALIXFolderClass))
#define CAMEL_IS_SCALIX_FOLDER(obj)         (CAMEL_CHECK_TYPE ((obj), CAMEL_TYPE_SCALIX_FOLDER))
#define CAMEL_IS_SCALIX_FOLDER_CLASS(klass) (CAMEL_CHECK_CLASS_TYPE ((klass), CAMEL_TYPE_SCALIX_FOLDER))
#define CAMEL_SCALIX_FOLDER_GET_CLASS(obj)  (CAMEL_CHECK_GET_CLASS ((obj), CAMEL_TYPE_SCALIX_FOLDER, CamelSCALIXFolderClass))

typedef struct _CamelSCALIXFolder CamelSCALIXFolder;
typedef struct _CamelSCALIXFolderClass CamelSCALIXFolderClass;

struct _CamelSCALIXJournal;

enum {
	CAMEL_SCALIX_FOLDER_ARG_ENABLE_MLIST = CAMEL_OFFLINE_FOLDER_ARG_LAST,
	CAMEL_SCALIX_FOLDER_ARG_NEED_SYNCH,
	//CAMEL_SCALIX_FOLDER_ARG_FREE_BUSY,
	CAMEL_SCALIX_FOLDER_ARG_EXPIRE_ACCESS,
	CAMEL_SCALIX_FOLDER_ARG_EXPIRE_AGE, 
	CAMEL_SCALIX_FOLDER_ARG_LAST = CAMEL_OFFLINE_FOLDER_ARG_LAST + 0x100
};

enum {
	CAMEL_SCALIX_FOLDER_ENABLE_MLIST = CAMEL_SCALIX_FOLDER_ARG_ENABLE_MLIST | CAMEL_ARG_BOO,
	CAMEL_SCALIX_FOLDER_EXPIRE_ACCESS = CAMEL_SCALIX_FOLDER_ARG_EXPIRE_ACCESS | CAMEL_ARG_INT,
	CAMEL_SCALIX_FOLDER_EXPIRE_AGE = CAMEL_SCALIX_FOLDER_ARG_EXPIRE_AGE | CAMEL_ARG_INT,
	CAMEL_SCALIX_FOLDER_NEED_SYNCH = CAMEL_SCALIX_FOLDER_ARG_NEED_SYNCH | CAMEL_ARG_BOO,
	//CAMEL_SCALIX_FOLDER_FREE_BUSY = CAMEL_SCALIX_FOLDER_ARG_FREE_BUSY | CAMEL_ARG_BOO
};

struct _CamelSCALIXFolder {
	CamelOfflineFolder parent_object;
	
	CamelFolderSearch *search;
	
	CamelOfflineJournal *journal;
	CamelDataCache *cache;
	
	char *cachedir;
	char *utf7_name;
	
	guint32 total;
	guint32 uidnext;
	guint32 flags;
	
	unsigned int read_only:1;
	unsigned int enable_mlist:1;
	unsigned int is_special:1;
	unsigned int is_trash:1;
	unsigned int need_synch:1;
};

struct _CamelSCALIXFolderClass {
	CamelOfflineFolderClass parent_class;
	
};


CamelType camel_scalix_folder_get_type (void);

CamelFolder *camel_scalix_folder_new (CamelStore *store, const char *full_name, guint32 flags, CamelException *ex);

const char *camel_scalix_folder_utf7_name (CamelSCALIXFolder *folder);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_FOLDER_H__ */
