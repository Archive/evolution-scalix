/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifndef __CAMEL_SCALIX_JOURNAL_H__
#define __CAMEL_SCALIX_JOURNAL_H__

#include <stdarg.h>

#include <glib.h>

#include <camel/camel-offline-journal.h>
#include <camel/camel-mime-message.h>

#include <libescalix/scalix-version.h>


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#if !EAPI_CHECK_VERSION (2, 24)
/* In Evolution 2.24, EDListNode deprecated in favour of code-wise identical
   CamelDListNode. GNOME bug #545877. */
#define CamelDListNode EDListNode
#define camel_dlist_addtail e_dlist_addtail
#endif

#define CAMEL_TYPE_SCALIX_JOURNAL            (camel_scalix_journal_get_type ())
#define CAMEL_SCALIX_JOURNAL(obj)            (CAMEL_CHECK_CAST ((obj), CAMEL_TYPE_SCALIX_JOURNAL, CamelSCALIXJournal))
#define CAMEL_SCALIX_JOURNAL_CLASS(klass)    (CAMEL_CHECK_CLASS_CAST ((klass), CAMEL_TYPE_SCALIX_JOURNAL, CamelSCALIXJournalClass))
#define CAMEL_IS_SCALIX_JOURNAL(obj)         (CAMEL_CHECK_TYPE ((obj), CAMEL_TYPE_SCALIX_JOURNAL))
#define CAMEL_IS_SCALIX_JOURNAL_CLASS(klass) (CAMEL_CHECK_CLASS_TYPE ((klass), CAMEL_TYPE_SCALIX_JOURNAL))
#define CAMEL_SCALIX_JOURNAL_GET_CLASS(obj)  (CAMEL_CHECK_GET_CLASS ((obj), CAMEL_TYPE_SCALIX_JOURNAL, CamelSCALIXJournalClass))

typedef struct _CamelSCALIXJournal CamelSCALIXJournal;
typedef struct _CamelSCALIXJournalClass CamelSCALIXJournalClass;
typedef struct _CamelSCALIXJournalEntry CamelSCALIXJournalEntry;

struct _CamelSCALIXFolder;

enum {
	CAMEL_SCALIX_JOURNAL_ENTRY_APPEND,
};

struct _CamelSCALIXJournalEntry {
	CamelDListNode node;
	
	int type;
	
	union {
		char *append_uid;
	} v;
};

struct _CamelSCALIXJournal {
	CamelOfflineJournal parent_object;
	
	GPtrArray *failed;
};

struct _CamelSCALIXJournalClass {
	CamelOfflineJournalClass parent_class;
	
};


CamelType camel_scalix_journal_get_type (void);

CamelOfflineJournal *camel_scalix_journal_new (struct _CamelSCALIXFolder *folder, const char *filename);

void camel_scalix_journal_readd_failed (CamelSCALIXJournal *journal);

/* interfaces for adding a journal entry */
void camel_scalix_journal_append (CamelSCALIXJournal *journal, CamelMimeMessage *message, const CamelMessageInfo *mi,
				 char **appended_uid, CamelException *ex);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CAMEL_SCALIX_JOURNAL_H__ */
