/*
 *  Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *  Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <ctype.h>
#include <errno.h>

#include <camel/camel-sasl.h>
#include <camel/camel-stream-buffer.h>
#include <camel/camel-i18n.h>

#include "camel-scalix-extensions.h"

#include "camel-scalix-summary.h"
#include "camel-scalix-command.h"
#include "camel-scalix-stream.h"
#include "camel-scalix-folder.h"
#include "camel-scalix-utils.h"
#include "camel-scalix-store.h"

#include "camel-scalix-engine.h"

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#define d(x) (camel_debug ("scalix:extensions") ? (x) : 0)


typedef struct {
    unsigned char *key;
    guint          offset;

} SxIdItem;

static SxIdItem sx_id_list[] = {
    {"name", G_STRUCT_OFFSET (CamelSCALIXStore, server_name)},
    {"version", G_STRUCT_OFFSET (CamelSCALIXStore, server_version)},
    {"display-name", G_STRUCT_OFFSET (CamelSCALIXStore, user_name)},
    {"smtp-address", G_STRUCT_OFFSET (CamelSCALIXStore, user_email)},
    {"auth-id", G_STRUCT_OFFSET (CamelSCALIXStore, user_auth_id)},
    {NULL, 0}
};

static int
scalix_engine_nstring (CamelSCALIXEngine * engine,
                       unsigned char ** nstring,
                       camel_scalix_token_t * token,
                       CamelException * ex)
{
    size_t n;
    camel_scalix_token_t temp_token;

    if (token == NULL) {
        token = &temp_token;

        if (camel_scalix_engine_next_token (engine, token, ex) == -1)
                return -1;
    }

    switch (token->token) {
        case CAMEL_SCALIX_TOKEN_NIL:
            *nstring = NULL;
            break;
        case CAMEL_SCALIX_TOKEN_ATOM:
            *nstring = g_strdup (token->v.atom);
            break;
        case CAMEL_SCALIX_TOKEN_QSTRING:
            *nstring = g_strdup (token->v.qstring);
            break;
        case CAMEL_SCALIX_TOKEN_LITERAL:
            if (camel_scalix_engine_literal (engine, nstring, &n, ex) == -1)
                return -1;
            break;
        default:
            camel_scalix_utils_set_unexpected_token_error (ex, engine, token);
            return -1;
    }

    return 0;
}


static int
parse_scalix_id (CamelSCALIXEngine * engine,
                 CamelSCALIXCommand * ic,
                 guint32 index,
                 camel_scalix_token_t * token, CamelException * ex)
{
    CamelSCALIXStore *store;
    SxIdItem *sx_id_list_start = sx_id_list;

    store = CAMEL_SCALIX_STORE (engine->service);

    if (camel_scalix_engine_next_token (engine, token, ex) == -1)
        return -1;

    if (token->token != CAMEL_SCALIX_TOKEN_LPAREN)
        goto unexpected;

    if (camel_scalix_engine_next_token (engine, token, ex) == -1)
        return -1;

    while (token->token != CAMEL_SCALIX_TOKEN_RPAREN) {
        unsigned char *key = NULL;
        unsigned char *value = NULL;
        SxIdItem *iter;

        /* we read a key, value pair */

        scalix_engine_nstring (engine, &key, token, ex);

        if (camel_exception_is_set (ex)) {
            goto caught_exception;
        }

        /* we pass NULL as 3rd parameter to get another token */	
        scalix_engine_nstring (engine, &value, NULL, ex);

        if (camel_exception_is_set (ex)) {
            goto caught_exception;
        }

        for (iter = sx_id_list_start; iter->key; iter++) {
            if (g_str_equal (iter->key, key)) {
                break;
            }
        }

        if (iter->key != NULL) {
            char **sval = (char **) G_STRUCT_MEMBER_P (store, iter->offset);

            g_free (*sval);
            *sval = value;

            value = NULL;

            if (iter == sx_id_list_start) {
                sx_id_list_start++;
            }
        }

        g_free (key);
        g_free (value);

        /* next one please */
        if (camel_scalix_engine_next_token (engine, token, ex) == -1)
            return -1;
    }

    if (camel_scalix_engine_next_token (engine, token, ex) == -1)
        return -1;

    if (token->token != CAMEL_SCALIX_TOKEN_EOLN) {
        goto unexpected;
    }

    return 0;

unexpected:
    camel_scalix_utils_set_unexpected_token_error (ex, engine, token);

caught_exception:
    return -1;
}

/* Threadsafe ? */
const char *
camel_scalix_get_evolution_version (void)
{
    static char *evolution_version = NULL;
    GConfClient *gc;

    if (evolution_version != NULL) {
        return evolution_version;
    }

    gc = gconf_client_get_default ();
    evolution_version =
        gconf_client_get_string (gc, "/apps/evolution/version", NULL);
    g_object_unref (gc);

    return evolution_version;
}

#define CMD_X_SCALIX_ID "X-SCALIX-ID (\"name\" \"Evolution\" \"version\" \"%s\")\r\n"

int
camel_scalix_id (CamelSCALIXEngine * engine, CamelException * ex)
{
    CamelSCALIXCommand *ic;
    int id, retval = 0;
    char *x_scalix_id = NULL;
    const char *evo_version;

    evo_version = camel_scalix_get_evolution_version ();

    x_scalix_id = g_strdup_printf (CMD_X_SCALIX_ID, evo_version);

    ic = camel_scalix_engine_prequeue (engine, NULL, x_scalix_id);
    camel_scalix_command_register_untagged (ic, "X-SCALIX-ID", parse_scalix_id);

    while ((id = camel_scalix_engine_iterate (engine)) < ic->id && id != -1) ;

    if (id == -1 || ic->status != CAMEL_SCALIX_COMMAND_COMPLETE) {
        camel_exception_xfer (ex, &ic->ex);
        retval = -1;
    }

    camel_scalix_command_unref (ic);

    return retval;
}
