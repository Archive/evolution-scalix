
/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <glib.h>
#include <gtk/gtk.h>

#include <gconf/gconf-client.h>

#include <glade/glade.h>

#include <e-util/e-config.h>
#include <e-util/e-plugin.h>

#include <libescalix/scalix-version.h>

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-account-list.h>
#else
#include <e-util/e-account-list.h>
#endif


#include "scalix_logo.xpm"

#include <mail/em-menu.h>
#include <mail/mail-config.h>

#include <libgnome/libgnome.h>

#include <camel/camel-folder.h>
#include <camel/camel-store.h>

#include <glog/glog.h>

#include "scalix-account-utils.h"
#include "scalix-account-selector.h"

#define LICENSE "Copyright 2005-2008 Scalix, Inc. (www.scalix.com)\n\n\
This program is free software; you can redistribute it and/or\n\
modify it under the terms of version 2 of the GNU General Public\n\
License as published by the Free Software Foundation.\n\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n\
GNU General Public License for more details.\n\n\
You should have received a copy of the GNU General Public License\n\
along with this program; if not, write to the Free Software\n\
Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA."

void com_scalix_menu_about (void *ep, gpointer * t);
void com_scalix_rules_wizard_clicked (void *ep, EMMenuTargetSelect * target);

/* ************************************************************************* */

void
com_scalix_menu_about (void *ep, /* ESMenuTargetShell */ gpointer * t)
{
    EAccountList *alist = NULL;
    EIterator *iter;
    GdkPixbuf *pblogo;
    GString *cmt;

    pblogo = gdk_pixbuf_new_from_xpm_data ((const char **) logo_xpm);

    /* Build up the server version info */
    alist = E_ACCOUNT_LIST (mail_config_get_accounts ());
    cmt = g_string_new ("Evolution Connector for Scalix Server\n");

    iter = e_list_get_iterator (E_LIST (alist));

    while (e_iterator_is_valid (iter)) {
        EAccount *account;
        const char *url;

        account = E_ACCOUNT (e_iterator_get (iter));
        url = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

        if (g_str_has_prefix (url, "scalix://")) {
            const char *aname;
            char *sversion;

            aname = e_account_get_string (account, E_ACCOUNT_NAME);

            sversion = scalix_account_prefs_get_sversion (account, NULL);

            if (sversion == NULL) {
                sversion = _("unkown version");
            }

            g_string_append (cmt, _("\nServer Version: "));
            g_string_append_printf (cmt, "%s", sversion);
            g_string_append_printf (cmt, " (%s)", aname);

            g_free (sversion);
        }

        e_iterator_next (iter);
    }

    gtk_show_about_dialog (NULL,
#if GTK_CHECK_VERSION(2, 12, 0)
                           "program-name",
#else
                           "name"
#endif
                           "Evolution-Scalix",
                           "license", LICENSE,
                           "website", "http://www.scalix.com",
                           "version", VERSION,
                           "logo", pblogo,
                           "comments", cmt->str,
                           NULL);

    g_string_free (cmt, TRUE);
}

void
com_scalix_rules_wizard_clicked (void *ep, EMMenuTargetSelect * target)
{
    EAccountList *alist;
    EAccount *account;
    EIterator *eiter;
    char *text_url;
    GConfClient *gcc;
    CamelURL *url;
    int sx_accounts;

    if (target == NULL)
        return;

    if (target->uri && g_str_has_prefix (target->uri, "scalix")) {
        url = camel_url_new (target->uri, NULL);
    } else {
        url = NULL;
    }

    account = NULL;
    gcc = gconf_client_get_default ();
    alist = e_account_list_new (gcc);
    eiter = e_list_get_iterator (E_LIST (alist));
    sx_accounts = 0;

    while (e_iterator_is_valid (eiter)) {
        EAccount *ac;
        const char *surl_txt;
        CamelURL *aurl;

        ac = E_ACCOUNT (e_iterator_get (eiter));
        surl_txt = e_account_get_string (E_ACCOUNT (ac), E_ACCOUNT_SOURCE_URL);

        if (surl_txt == NULL || !g_str_has_prefix (surl_txt, "scalix")) {
            e_iterator_next (eiter);
            continue;
        }

        account = ac;

        if (url != NULL) {
            aurl = camel_url_new (surl_txt, NULL);

            if (aurl && g_str_equal (aurl->user, url->user) &&
                g_str_equal (aurl->host, url->host)) {
                account = ac;
                camel_url_free (aurl);
                break;
            }

            camel_url_free (aurl);
        }

        sx_accounts++;
        e_iterator_next (eiter);
    }

    if ((url == NULL && sx_accounts != 1) || account == NULL) {
        account = scalix_account_selector_run (alist);
    }

    if (account) {
        text_url = scalix_account_prefs_get_rw_url (account, gcc);
        gnome_url_show (text_url, NULL);
        g_free (text_url);
    }

    g_object_unref (eiter);
    g_object_unref (alist);
    g_object_unref (gcc);

    if (url) {
        camel_url_free (url);
    }
}
