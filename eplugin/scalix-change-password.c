/*
 * Copyright (C) 2005-2007 Scalix, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

/* define "HAVE_MAILMSG" needs to be set if some Fedora modifications
 * have been made to the evolution code in mail/mail-mt.h
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <glib.h>
#include <gtk/gtk.h>

#include <gconf/gconf-client.h>

#include <glade/glade.h>

#include <e-util/e-config.h>
#include <e-util/e-plugin.h>
#include <libedataserverui/e-passwords.h>

#include <libescalix/scalix-version.h>

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-account-list.h>
#else
#include <e-util/e-account-list.h>
#endif

#include <mail/em-menu.h>
#include <mail/mail-mt.h>
#include <mail/mail-config.h>

#include <camel/camel-session.h>
#include <camel/camel-folder.h>
#include <camel/camel-store.h>
#include <camel/camel-scalix-store.h>

#include <libescalix/scalix-utils.h>

#include <glog/glog.h>

#include <string.h>

#include "scalix-account-selector.h"
#include "scalix-account-utils.h"

void com_scalix_change_password (void *ep, EMMenuTargetSelect * target);

extern CamelSession *session;

static char *
create_key (CamelURL * url)
{
    char *key;
    char *user;
    char *authmech;

    if (url->protocol == NULL || url->host == NULL || url->user == NULL) {
        return NULL;
    }

    user = camel_url_encode (url->user, ":;@/");

    if (url->authmech != NULL) {
        authmech = camel_url_encode (url->authmech, ":@/");
    } else {
        authmech = NULL;
    }

    if (url->port) {
        key = g_strdup_printf ("%s://%s%s%s@%s:%d/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host, url->port);
    } else {
        key = g_strdup_printf ("%s://%s%s%s@%s/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host);
    }

    g_free (user);
    g_free (authmech);
    return key;
}

typedef struct _SxChangePwDialog SxChangePwDialog;

struct sx_change_pw_msg {
#ifdef HAVE_MAILMSG
    MailMsg base;
#else
    struct _mail_msg msg;
#endif
    char *oldpw;
    char *newpw;
    char *uri;
    CamelException ex;
    SxChangePwDialog *pwd;
};

struct _SxChangePwDialog {
    GtkWidget *dialog;
    gboolean ok;
    GtkWidget *epw;
    GtkWidget *epw_old;
    GtkWidget *epw_confirm;
    GtkWidget *imessage;
    GtkWidget *lmessage;
    GtkWidget *cbutton;
    guint iprg;
    char *uri;
    char *oldpw;
    char *newpw;
    char *key;
    struct sx_change_pw_msg *msg;
};

static void
pwchange_worker (struct sx_change_pw_msg *m)
{
    SxChangePwDialog *pwd;
    CamelStore *store;
    CamelException *ex;
    GString *pwstr;

    pwd = m->pwd;
    ex = &(m->ex);

    camel_exception_init (ex);

    store = camel_session_get_store (session, m->uri, ex);
    if (store == NULL) {
        return;
    }

    pwstr = g_string_new ("\"");
    g_string_append_url_encoded (pwstr, m->oldpw, NULL);
    g_string_append (pwstr, "\"@\"");
    g_string_append_url_encoded (pwstr, m->newpw, NULL);
    g_string_append (pwstr, "\"");
    camel_object_set (store, ex, CAMEL_SCALIX_STORE_NEW_PW, pwstr->str, NULL);

    g_string_free (pwstr, TRUE);

    camel_object_unref (store);
}

static void
pwchange_done (struct sx_change_pw_msg *m)
{
    SxChangePwDialog *pwd;
    char *markup;

    pwd = m->pwd;

    if (m->pwd == NULL) {
        return;
    }

    g_source_remove (pwd->iprg);
    pwd->iprg = 0;

    if (m->ex.id != 0) {

        gtk_image_set_from_stock (GTK_IMAGE (pwd->imessage),
                                  "gtk-dialog-error", GTK_ICON_SIZE_BUTTON);

        markup =
            g_markup_printf_escaped (_("<b>Could not change password:</b> %s"),
                                     m->ex.desc);
        gtk_label_set_markup (GTK_LABEL (pwd->lmessage), markup);
        g_free (markup);

        gtk_widget_show (pwd->imessage);
        gtk_widget_show (pwd->lmessage);

        switch (m->ex.id) {
        case CAMEL_EXCEPTION_INVALID_PARAM:

            gtk_widget_set_sensitive (pwd->epw, TRUE);
            gtk_widget_set_sensitive (pwd->epw_confirm, TRUE);
            gtk_widget_set_sensitive (pwd->epw_old, TRUE);

            gtk_dialog_set_response_sensitive (GTK_DIALOG (pwd->dialog),
                                               GTK_RESPONSE_ACCEPT, TRUE);

            gtk_dialog_set_response_sensitive (GTK_DIALOG (pwd->dialog),
                                               GTK_RESPONSE_CANCEL, TRUE);
            break;

        case CAMEL_EXCEPTION_SYSTEM:
        default:
            /* Fatal error here */
            gtk_dialog_set_response_sensitive (GTK_DIALOG (pwd->dialog),
                                               GTK_RESPONSE_CANCEL, TRUE);

            break;
        }

    } else {
        markup =
            g_markup_printf_escaped (_
                                     ("<b>The Scalix password was successfully changed</b>"));
        gtk_label_set_markup (GTK_LABEL (pwd->lmessage), markup);
        g_free (markup);

        gtk_image_set_from_stock (GTK_IMAGE (pwd->imessage),
                                  "gtk-yes", GTK_ICON_SIZE_BUTTON);

        gtk_widget_show (pwd->imessage);
        gtk_widget_show (pwd->lmessage);

        e_passwords_add_password (pwd->key, pwd->newpw);
        e_passwords_remember_password ("Mail", pwd->key);

        pwd->ok = TRUE;
        gtk_button_set_label (GTK_BUTTON (pwd->cbutton), _("_OK"));
        gtk_dialog_set_response_sensitive (GTK_DIALOG (pwd->dialog),
                                           GTK_RESPONSE_ACCEPT, TRUE);
    }
}

static void
pwchange_free (struct sx_change_pw_msg *m)
{
    g_free (m->oldpw);
    g_free (m->newpw);
    g_free (m->uri);
}

#ifndef HAVE_MAILMSG

static void
pwchange_worker_wrap (struct _mail_msg *mm)
{
    struct sx_change_pw_msg *m;
    m = (struct sx_change_pw_msg *) mm;
    pwchange_worker (m);
}

static void
pwchange_done_wrap (struct _mail_msg *mm)
{
    struct sx_change_pw_msg *m;
    SxChangePwDialog *pwd;
    char *markup;

    m = (struct sx_change_pw_msg *) mm;
    pwchange_done (m);
}

static void
pwchange_free_wrap (struct _mail_msg *mm)
{
    struct sx_change_pw_msg *m;

    m = (struct sx_change_pw_msg *) mm;
    pwchange_free (m);
}

#endif

#ifdef HAVE_MAILMSG

static MailMsgInfo sx_pw_change_info = {
    sizeof (struct sx_change_pw_msg),
    (MailMsgDescFunc) NULL,
    (MailMsgExecFunc) pwchange_worker,
    (MailMsgDoneFunc) pwchange_done,
    (MailMsgFreeFunc) pwchange_free
};

#else

static struct _mail_msg_op sx_pw_change_op = {
    NULL,
    pwchange_worker_wrap,
    pwchange_done_wrap,
    pwchange_free_wrap,
};

#endif

static void
pwd_destroy (SxChangePwDialog * pwd)
{
    if (pwd->msg) {
        pwd->msg->pwd = NULL;
    }

    g_free (pwd->key);
    g_free (pwd->uri);

    g_print ("pwd destroyed\n");
    g_free (pwd);
}

static void
pwd_response (GtkDialog * dialog, gint arg1, gpointer user_data)
{
    SxChangePwDialog *pwd;

    pwd = (SxChangePwDialog *) user_data;

    g_print ("response %d\n", arg1);

    switch (arg1) {

    case GTK_RESPONSE_ACCEPT:

        if (pwd->ok) {
            gtk_widget_destroy (pwd->dialog);
            pwd_destroy (pwd);
            return;
        }

        gtk_widget_set_sensitive (pwd->epw, FALSE);
        gtk_widget_set_sensitive (pwd->epw_old, FALSE);
        gtk_widget_set_sensitive (pwd->epw_confirm, FALSE);

        gtk_dialog_set_response_sensitive (GTK_DIALOG (pwd->dialog),
                                           GTK_RESPONSE_ACCEPT, FALSE);

        gtk_dialog_set_response_sensitive (GTK_DIALOG (pwd->dialog),
                                           GTK_RESPONSE_CANCEL, FALSE);

#ifdef HAVE_MAILMSG
        pwd->msg = mail_msg_new (&sx_pw_change_info);
#else
        pwd->msg = mail_msg_new (&sx_pw_change_op, NULL, sizeof (*(pwd->msg)));
#endif
        pwd->msg->oldpw = g_strdup (pwd->oldpw);
        pwd->msg->newpw = g_strdup (pwd->newpw);
        pwd->msg->uri = g_strdup (pwd->uri);
        pwd->msg->pwd = pwd;

#ifdef HAVE_MAILMSG
        mail_msg_unordered_push (pwd->msg);
#else
        e_thread_put (mail_thread_new, (EMsg *) pwd->msg);
#endif
        break;

    case GTK_RESPONSE_CANCEL:
        gtk_widget_destroy (pwd->dialog);

        pwd_destroy (pwd);
        break;

    case GTK_RESPONSE_NONE:
        /* destroyed */

        pwd_destroy (pwd);
        break;
    default:
        break;
    }
}

static void
epws_changed (GtkEditable * editable, gpointer user_data)
{
    SxChangePwDialog *pwd;
    const char *pw1, *pw2, *opw;
    gboolean check;

    pwd = (SxChangePwDialog *) user_data;

    pw1 = gtk_entry_get_text (GTK_ENTRY (pwd->epw));
    pw2 = gtk_entry_get_text (GTK_ENTRY (pwd->epw_confirm));
    opw = gtk_entry_get_text (GTK_ENTRY (pwd->epw_old));
    check = FALSE;

    if (!pw1 || !pw2 || strlen (pw1) != strlen (pw2)) {
        gtk_widget_hide (pwd->imessage);
        gtk_widget_hide (pwd->lmessage);
    } else if (strlen (pw1) == strlen (pw1) && !g_str_equal (pw1, pw2)) {
        gtk_image_set_from_stock (GTK_IMAGE (pwd->imessage),
                                  "gtk-dialog-warning", GTK_ICON_SIZE_BUTTON);
        gtk_label_set_markup (GTK_LABEL (pwd->lmessage),
                              "Passwords do not match");
        gtk_widget_show (pwd->imessage);
        gtk_widget_show (pwd->lmessage);
    } else {
        pwd->newpw = g_strdup (pw1);
        pwd->oldpw = g_strdup (opw);
        gtk_widget_hide (pwd->imessage);
        gtk_widget_hide (pwd->lmessage);
        check = (opw != NULL && strlen (opw) != 0);;
    }

    gtk_dialog_set_response_sensitive (GTK_DIALOG (pwd->dialog),
                                       GTK_RESPONSE_ACCEPT, check);
}

void
com_scalix_change_password (void *ep, EMMenuTargetSelect * target)
{
    SxChangePwDialog *pwd;
    CamelURL *url;
    GladeXML *xmln;
    GtkWidget *dialog;
    GtkWidget *epw;
    GtkWidget *epw_old;
    GtkWidget *epw_confirm;
    GtkWidget *imessage;
    GtkWidget *lmessage;
    GtkWidget *cbutton;
    char *uri;

    if (target == NULL) {
        return;
    }

    if (target->uri && g_str_has_prefix (target->uri, "scalix")) {
        url = camel_url_new (target->uri, NULL);
        uri = g_strdup (target->uri);
    } else {
        EAccount *account;
        EAccountList *alist;
        const char *surl;

        alist = E_ACCOUNT_LIST (mail_config_get_accounts ());
        account = scalix_account_selector_run (alist);

        if (account == NULL) {
            return;
        }

        surl = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

        if (surl) {
            uri = g_strdup (surl);
        } else {
            uri = NULL;
        }

        g_object_unref (account);
    }

    if (uri == NULL) {
        return;
    }

    url = camel_url_new (uri, NULL);

    if (url == NULL) {
        return;
    }

    pwd = g_new0 (SxChangePwDialog, 1);

    pwd->uri = uri;
    xmln = glade_xml_new (ES_GLADEDIR "/pw-dialog.glade",
                          "change_password_dialog", GETTEXT_PACKAGE);

    dialog = glade_xml_get_widget (xmln, "change_password_dialog");
    imessage = glade_xml_get_widget (xmln, "imessage");
    lmessage = glade_xml_get_widget (xmln, "lmessage");
    epw = glade_xml_get_widget (xmln, "epassword");
    epw_old = glade_xml_get_widget (xmln, "epassword_old");
    epw_confirm = glade_xml_get_widget (xmln, "epassword_confirm");
    cbutton = glade_xml_get_widget (xmln, "create_button");

    pwd->ok = FALSE;
    pwd->dialog = dialog;
    pwd->epw = epw;
    pwd->epw_old = epw_old;
    pwd->epw_confirm = epw_confirm;
    pwd->imessage = imessage;
    pwd->lmessage = lmessage;
    pwd->cbutton = cbutton;

    /* preset old key text entry */
    pwd->key = create_key (url);
    camel_url_free (url);

    g_signal_connect (epw, "changed", G_CALLBACK (epws_changed), pwd);
    g_signal_connect (epw_old, "changed", G_CALLBACK (epws_changed), pwd);
    g_signal_connect (epw_confirm, "changed", G_CALLBACK (epws_changed), pwd);

    g_signal_connect (dialog, "response", G_CALLBACK (pwd_response), pwd);

    gtk_widget_show (dialog);
    gtk_widget_show_all (dialog);

    g_object_set (imessage, "visible", FALSE, NULL);
    g_object_set (lmessage, "visible", FALSE, NULL);
}
