
/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifndef SCALIX_ACCOUNT_UTILS_H
#define SCALIX_ACCOUNT_UTILS_H

#include <libescalix/scalix-version.h>

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-account.h>
#include <libedataserver/e-account-list.h>
#else
#include <e-util/e-account.h>
#include <e-util/e-account-list.h>
#endif

G_BEGIN_DECLS
    EAccount * scalix_account_by_uri (CamelURL * url, GConfClient * gcc);

gboolean scalix_account_remove_sources (EAccount * account);

void scalix_account_prefs_clear (EAccount * account, GConfClient * gcc);

char *scalix_account_prefs_get_rw_url (EAccount * account, GConfClient * gcc);

void scalix_account_prefs_set_rw_url (EAccount * account,
                                      const char *uri, GConfClient * gcc);

char *scalix_account_prefs_get_sversion (EAccount * account,
                                         GConfClient * gcc);

void scalix_account_prefs_set_sversion (EAccount * account,
                                        const char * sversion,
                                        GConfClient * gcc);

#if 0
void scalix_account_prefs_bind_esource_group (EAccount * account,
                                              const char *type,
                                              const char *uid,
                                              GConfClient * gcc);

char *scalix_account_prefs_get_esource_group (EAccount * account,
                                              const char *type,
                                              GConfClient * gcc);
#endif
G_END_DECLS
#endif /* SCALIX_ACCOUNT_UTILS_H */
