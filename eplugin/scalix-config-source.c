/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <gtk/gtkversion.h>

#include <e-util/e-config.h>
#include <mail/em-config.h>
#include <calendar/gui/e-cal-config.h>

#include <libedataserver/e-source.h>
#include <libedataserver/e-url.h>

#include <string.h>

#include "scalix-store-model.h"
#include <addressbook/gui/widgets/eab-config.h>

GtkWidget *com_scalix_config_dummy (EPlugin * epl,
                                    EConfigHookItemFactoryData * data);

void com_scalix_addressbook_config_commit (EPlugin * epl,
                                           EConfigTarget * target);

GtkWidget *com_scalix_addressbook_config_location (EPlugin * epl,
                                                   EConfigHookItemFactoryData *
                                                   data);

void com_scalix_calendar_config_commit (EPlugin * epl, EConfigTarget * target);

GtkWidget *com_scalix_calendar_config_location (EPlugin * epl,
                                                EConfigHookItemFactoryData *
                                                data);

GtkWidget *
com_scalix_config_dummy (EPlugin * epl, EConfigHookItemFactoryData * data)
{
    g_print ("DUMMY\n");
    return NULL;
}

/* Shared functions */

static gboolean
is_toplevel (const char *uri)
{
    gboolean toplevel;
    char *match;

    toplevel = (uri == NULL || uri[0] == '\0');

    if (toplevel == FALSE) {
        match = g_strrstr (uri, "/");
        toplevel = (match == NULL || match == uri);
    }

    return toplevel;
}

static void
sn_changed (ESource * source, gpointer data)
{
    const char *name;
    const char *ruri;

    //char       **old_name;
    char **sv;
    char *nruri;
    guint n;
    gulong *sig_id;

    sig_id = (gulong *) data;
    name = e_source_peek_name (source);
    ruri = e_source_peek_relative_uri (source);

    /* Must block signal emission or this will give
     * a nice infinit chain and stack overflow */
    g_signal_handler_block (source, *sig_id);

    if (is_toplevel (ruri)) {
        e_source_set_relative_uri (source, name);
    } else {
        sv = g_strsplit (ruri, "/", -1);
        n = g_strv_length (sv);

        if (n < 2) {
            g_print ("Error during split! (%s)\n", ruri);
            g_strfreev (sv);
            return;
        }

        g_free (sv[n - 1]);
        sv[n - 1] = g_strdup (name);

        nruri = g_strjoinv ("/", sv);
        e_source_set_relative_uri (source, nruri);
        g_free (nruri);
        g_strfreev (sv);
    }

    g_signal_handler_unblock (source, *sig_id);

    g_print ("RURI: %s\n", e_source_peek_relative_uri (source));
}

static void
selection_changed (GtkTreeSelection * selection, ESource * source)
{
    GtkTreeModel *model;
    GtkTreeIter iter;
    GValue value = { 0, };
    const char *path;

    if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
        char *nruri;
        const char *name;

        gtk_tree_model_get_value (model, &iter, SCM_COL_PATH, &value);

        path = g_value_get_string (&value);
        name = e_source_peek_name (source);

        if (name == NULL || name[0] == '\0') {
            name = "nonameyet";
        }

        if (path == NULL) {
            nruri = g_strdup (name);
        } else {
            nruri = g_build_path ("/", path, name, NULL);
        }

        e_source_set_relative_uri (source, nruri);
        g_free (nruri);

        g_value_unset (&value);
    } else {
        g_warning ("NARF");
    }
}

static void
model_loaded (ScalixStoreModel * model, GtkTreeView * tree_view)
{
    gtk_tree_view_expand_all (tree_view);
}

static GtkDialog *
create_store_view_dialog (ESource * source)
{
    GtkWidget *dialog;
    GtkWidget *swin;
    GtkWidget *tree_view;
    GtkTreeSelection *selection;
    ScalixStoreModel *model;
    GtkTreeViewColumn *column;
    GtkCellRenderer *cell;
    char *text_uri;

    dialog = gtk_dialog_new_with_buttons (_("Select a location"),
                                          NULL, GTK_DIALOG_MODAL,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);

    gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);

    /* Scrolled window */
    swin = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swin),
                                    GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

    gtk_widget_show (swin);

    /* ScalixStoreModel aka TreeModel */
    text_uri = e_source_get_uri (source);
    model = scalix_store_model_new (text_uri);

    /* Treeview */
    tree_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
    g_object_set_data (G_OBJECT (dialog), "treeview", tree_view);

    gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
    gtk_widget_show (tree_view);

    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (swin),
                                           tree_view);

    gtk_widget_add_events (tree_view, GDK_BUTTON_PRESS);

    column = gtk_tree_view_column_new ();

    /* the icon */
    cell = gtk_cell_renderer_pixbuf_new ();

    gtk_tree_view_column_pack_start (column, cell, FALSE);

    gtk_tree_view_column_set_attributes (column, cell,
                                         "pixbuf", SCM_COL_ICON, NULL);

    /* the text */
    cell = gtk_cell_renderer_text_new ();
    gtk_tree_view_column_pack_start (column, cell, FALSE);
    gtk_tree_view_column_set_attributes (column, cell,
                                         "text", SCM_COL_NAME, NULL);

    gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), column);

    /* Selection mode of the treeview */
    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
    gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);
    g_signal_connect (G_OBJECT (selection),
                      "changed", G_CALLBACK (selection_changed), source);

    g_signal_connect (G_OBJECT (model),
                      "loaded", G_CALLBACK (model_loaded), tree_view);

    /* Put everthing together */
    gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), swin);
    gtk_container_set_border_width (GTK_CONTAINER (swin), 6);
    gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 6);

    gtk_window_set_default_size (GTK_WINDOW (dialog), 420, 340);

    return GTK_DIALOG (dialog);
}

static void
location_clicked (GtkButton * button, ESource * source)
{
    GtkDialog *dialog;
    char *ruri_save;
    const char *ruri;
    int resp;

    dialog = create_store_view_dialog (source);

    if (dialog == NULL)
        return;

    ruri_save = (char *) e_source_peek_relative_uri (source);

    if (ruri_save != NULL) {
        ruri_save = g_strdup (ruri_save);
    }

    resp = gtk_dialog_run (dialog);

    if (resp == GTK_RESPONSE_OK) {
        char *nruri = NULL;

        ruri = e_source_peek_relative_uri (source);

        if (is_toplevel (ruri)) {
            ruri = _("Toplevel");
        } else {
            char *match;

            match = g_strrstr (ruri, "/");
            nruri = g_strndup (ruri, match - ruri);
            ruri = nruri;
        }

        gtk_button_set_label (button, ruri);

        g_free (nruri);

    } else {
        e_source_set_relative_uri (source, ruri_save);
    }

    g_free (ruri_save);
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
find_entries_and_set_sensitive (GtkWidget * parent, gboolean sensitive)
{
    GList *list, *iter;

    list = gtk_container_get_children (GTK_CONTAINER (parent));

    for (iter = list; iter; iter = iter->next) {

        if (GTK_IS_ENTRY (iter->data)) {
            gtk_widget_set_sensitive (GTK_WIDGET (iter->data), sensitive);
        } else if (GTK_IS_BOX (iter->data)) {
            find_entries_and_set_sensitive (GTK_WIDGET (iter->data), sensitive);
        }
    }
}

static GtkWidget *
sx_config_source_setup_ui (GtkWidget * parent, ESource * source)
{
    GtkWidget *button;
    GtkWidget *label;
    GtkWidget *text;
    GtkWidget *ret;
    char *uri_text;
    const char *uid;
    const char *ruri;
    const char *name;
    gboolean new_source;
    gulong *sig_id;

    uri_text = e_source_get_uri (source);

    if (!g_str_has_prefix (uri_text, "scalix")) {
        g_free (uri_text);
        return NULL;
    }
    g_free (uri_text);

    g_assert (GTK_IS_BOX (parent) || GTK_IS_TABLE (parent));

    /* Label */
    label = gtk_label_new_with_mnemonic (_("_Location:"));
    gtk_widget_show (label);

    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

    button = gtk_button_new ();

    g_signal_connect (G_OBJECT (button),
                      "clicked", G_CALLBACK (location_clicked), source);

    gtk_widget_show (button);

    uid = e_source_peek_uid (source);
    ruri = e_source_peek_relative_uri (source);
    name = e_source_peek_name (source);

    /* new source */
    new_source = (ruri == NULL || ruri[0] == '\0' || g_str_equal (ruri, uid));

    if (new_source) {

        if (name == NULL || name[0] == '\0')
            name = "nonameyet";

        e_source_set_relative_uri (source, name);
    }

    if (is_toplevel (ruri)) {
        text = gtk_label_new (_("Toplevel"));
    } else {
        text = gtk_label_new (ruri);
    }

    gtk_widget_show (text);

#if (GTK_CHECK_VERSION(2, 6, 0))
    gtk_label_set_ellipsize (GTK_LABEL (text), PANGO_ELLIPSIZE_START);
#endif
    gtk_container_add (GTK_CONTAINER (button), text);

    sig_id = (gulong *) g_malloc0 (sizeof (gulong));

    *sig_id = g_signal_connect (source,
                                "changed", G_CALLBACK (sn_changed), sig_id);

    /* We do not support renames (yet?!) */
    if (new_source == FALSE) {

        gtk_widget_set_sensitive (button, FALSE);

        /* Nasty nasty hack ey ey ey */
        find_entries_and_set_sensitive (parent, FALSE);

    }

    /* attach it */
    if (!GTK_IS_BOX (parent)) {
        int row;

        row = GTK_TABLE (parent)->nrows;

        gtk_table_attach (GTK_TABLE (parent),
                          label, 0, 1, row, row + 1, GTK_FILL, 0, 0, 0);

        gtk_table_attach (GTK_TABLE (parent),
                          button, 1, 2,
                          row, row + 1, GTK_EXPAND | GTK_FILL, 0, 0, 0);

        /* HACK for to have a return value != NULL */
        ret = button;

    } else {
        GtkWidget *hbox;

        hbox = gtk_hbox_new (FALSE, 6);
        gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 6);
        gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 6);
        gtk_box_pack_start (GTK_BOX (parent), hbox, FALSE, FALSE, 6);
        gtk_widget_show_all (hbox);
        ret = hbox;
    }

    return ret;
}

/* ************************************************************************** */

/* Addressbook */

GtkWidget *
com_scalix_addressbook_config_location (EPlugin * epl,
                                        EConfigHookItemFactoryData * data)
{
    EABConfigTargetSource *t = (EABConfigTargetSource *) data->target;
    ESource *source = t->source;
    GtkWidget *widget;

    widget = sx_config_source_setup_ui (data->parent, source);

    return widget;
}

/* ************************************************************************** */

/* Calendar */

GtkWidget *
com_scalix_calendar_config_location (EPlugin * epl,
                                     EConfigHookItemFactoryData * data)
{
    ECalConfigTargetSource *t = (ECalConfigTargetSource *) data->target;
    ESource *source = t->source;
    GtkWidget *widget;

    widget = sx_config_source_setup_ui (data->parent, source);

    return widget;
}
