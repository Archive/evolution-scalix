/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <gtk/gtk.h>

#include <gconf/gconf-client.h>

#include <glade/glade.h>

#include "scalix-account-selector.h"

enum {
    COLUMN_STRING = 0,
    COLUMN_OBJECT,
    N_COLUMNS
};

EAccount *
scalix_account_selector_run (EAccountList * alist)
{
    EIterator *eiter;
    GtkListStore *store;
    GtkTreeIter iter;
    GladeXML *xmln;
    GtkWidget *dialog;
    GtkWidget *selector;
    EAccount *account;
    guint res;
    GtkCellRenderer *renderer;
    guint count;

    store = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_OBJECT);

    eiter = e_list_get_iterator (E_LIST (alist));

    account = NULL;
    count = 0;

    while (e_iterator_is_valid (eiter)) {
        EAccount *ac;
        const char *name;
        const char *url;

        ac = E_ACCOUNT (e_iterator_get (eiter));
        url = e_account_get_string (E_ACCOUNT (ac), E_ACCOUNT_SOURCE_URL);

        if (url == NULL || !g_str_has_prefix (url, "scalix")) {
            e_iterator_next (eiter);
            continue;
        }

        name = e_account_get_string (E_ACCOUNT (ac), E_ACCOUNT_NAME);
        g_print ("Adding %s\n", name);
        gtk_list_store_append (store, &iter);
        gtk_list_store_set (store, &iter,
                            COLUMN_STRING, name, COLUMN_OBJECT, account, -1);
        account = ac;
        count++;
        e_iterator_next (eiter);
    }

    g_object_unref (eiter);
    
    if (count == 0) {
        g_object_unref (store);
        return NULL;
    }

    if (count == 1) {
        /* No need to show dialog if there is only
         * one scalix account */
        g_object_unref (store);
        g_object_ref (account);
        return account;
    }

    xmln = glade_xml_new (ES_GLADEDIR "/scalix-config.glade",
                          "sx_account_selector", NULL);

    dialog = glade_xml_get_widget (xmln, "sx_account_selector");
    selector = glade_xml_get_widget (xmln, "selector");

    gtk_combo_box_set_model (GTK_COMBO_BOX (selector), GTK_TREE_MODEL (store));

    gtk_cell_layout_clear (GTK_CELL_LAYOUT (selector));

    renderer = gtk_cell_renderer_text_new ();

    gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (selector), renderer, FALSE);

    gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (selector),
                                    renderer, "text", 0, NULL);

    gtk_combo_box_set_active (GTK_COMBO_BOX (selector), 0);

    res = gtk_dialog_run (GTK_DIALOG (dialog));

    gtk_combo_box_get_active_iter (GTK_COMBO_BOX (selector), &iter);

    gtk_tree_model_get (GTK_TREE_MODEL (store),
                        &iter, COLUMN_OBJECT, &account, -1);

    if (res != GTK_RESPONSE_OK) {
        account = NULL;
    }

    gtk_widget_destroy (dialog);

    return account;
}
