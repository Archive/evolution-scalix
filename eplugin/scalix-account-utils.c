
/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <glib.h>

#include <libescalix/scalix-version.h>

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-account.h>
#include <libedataserver/e-account-list.h>
#else
#include <e-util/e-account.h>
#include <e-util/e-account-list.h>
#endif

#include <camel/camel-url.h>

#include <libedataserver/e-url.h>
#include <libedataserver/e-source.h>
#include <libedataserver/e-source-list.h>
#include <libedataserver/e-source-group.h>
#include <libebook/e-book.h>

#include <gconf/gconf-client.h>

#include <string.h>

#include "scalix-account-utils.h"

#define CAL_GCONF "/apps/evolution/calendar/sources"

EAccount *
scalix_account_by_uri (CamelURL * url, GConfClient * gcc)
{
    EAccount *account;
    EAccountList *alist;
    EIterator *eiter;

    if (url == NULL) {
        return NULL;
    }

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    account = NULL;

    alist = e_account_list_new (gcc);
    eiter = e_list_get_iterator (E_LIST (alist));

    while (e_iterator_is_valid (eiter)) {
        EAccount *ac;
        const char *surl_txt;
        CamelURL *aurl;

        ac = E_ACCOUNT (e_iterator_get (eiter));
        surl_txt = e_account_get_string (E_ACCOUNT (ac), E_ACCOUNT_SOURCE_URL);
        aurl = camel_url_new (surl_txt, NULL);

        if (aurl && g_str_equal (aurl->user, url->user) &&
            g_str_equal (aurl->host, url->host)) {
            account = ac;
            break;
        }

        e_iterator_next (eiter);
    }

    if (account) {
        g_object_ref ((gpointer) account);
    }

    g_object_unref (eiter);
    g_object_unref (alist);
    g_object_unref (gcc);

    return account;
}

gboolean
scalix_account_remove_sources (EAccount * account)
{
    ESourceList *slist;
    ESourceGroup *group;
    char *guid;
    GConfClient *gcc;

    gcc = gconf_client_get_default ();

    /* calendar */
    group = NULL;
    slist = e_source_list_new_for_gconf_default (CAL_GCONF);

    if (!slist) {
        return FALSE;
    }

    guid = g_strdup_printf ("Calendar@%s", account->uid);
    group = e_source_list_peek_group_by_uid (slist, guid);
    g_free (guid);

    if (group != NULL) {
        e_source_list_remove_group (slist, group);
        e_source_list_sync (slist, NULL /* GError */ );
    }

    g_object_unref (slist);

    /* contacts */
    if (!e_book_get_addressbooks (&slist, NULL)) {
        return FALSE;
    }

    guid = g_strdup_printf ("Contacts@%s", account->uid);
    group = e_source_list_peek_group_by_uid (slist, guid);
    g_free (guid);

    if (group != NULL) {
        GError *error = NULL;

        if (!e_source_list_remove_group (slist, group)) {
            return FALSE;
        }

        if (!e_source_list_sync (slist, &error)) {
            return FALSE;
        }
    }

    g_object_unref (slist);
    g_object_unref (gcc);

    return TRUE;
}

/* ************************************************************************** */
#define SRW_KEY "/apps/evolution-scalix/%s/ScalixRulesWizardURL"
#define SSG_KEY "/apps/evolution-scalix/%s/%s"
#define SAS_KEY "/apps/evolution-scalix/%s"
#define SSV_KEY "/apps/evolution-scalix/%s/ServerVersion"

void
scalix_account_prefs_clear (EAccount * account, GConfClient * gcc)
{
    GError *error;
    char *auid;
    char *gc_key;

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    scalix_account_prefs_set_rw_url (account, NULL, gcc);
    scalix_account_prefs_set_sversion (account, NULL, gcc);

    auid = account->uid;
    /* skip leading zeros */
    while (auid && auid[0] == ' ') {
        auid++;
    }

    gc_key = g_strdup_printf (SAS_KEY, auid);
    error = NULL;

    gconf_client_unset (gcc, gc_key, &error);

    if (error != NULL) {
        g_print ("ERROR: %s,%s\n", error->message, gc_key);
        g_clear_error (&error);
    }

    g_free (gc_key);

    g_object_unref (gcc);
}

char *
scalix_account_prefs_get_rw_url (EAccount * account, GConfClient * gcc)
{
    GError *error;
    char *gc_key;
    char *gc_val;
    char *auid;

    if (account == NULL) {
        return NULL;
    }

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    auid = account->uid;
    /* skip leading zeros */
    while (auid && auid[0] == ' ') {
        auid++;
    }

    gc_key = g_strdup_printf (SRW_KEY, auid);

    error = NULL;
    gc_val = gconf_client_get_string (gcc, gc_key, &error);

    if (error != NULL) {
        g_print ("ERROR: %s,%s,%s\n", error->message, gc_key, gc_val);
        g_clear_error (&error);
        gc_val = NULL;
    }

    g_free (gc_key);

    if (gc_val == NULL) {
        const char *text_url;
        CamelURL *url;

        url = NULL;

        text_url = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

        if (text_url != NULL) {
            url = camel_url_new (text_url, NULL);
        }

        if (url && url->host && url->user) {
            gc_val = g_strdup_printf ("http://%s/Scalix/rw/?username=%s",
                                      url->host, url->user);
        } else if (url && url->host) {
            gc_val = g_strdup_printf ("http://%s/Scalix/rw", url->host);

        } else {
            gc_val = g_strdup ("http://localhost/Scalix/rw");
        }

        if (url) {
            camel_url_free (url);
        }
    }

    g_object_unref (gcc);

    return gc_val;
}

void
scalix_account_prefs_set_rw_url (EAccount * account,
                                 const char * uri,
                                 GConfClient * gcc)
{
    char *gc_key;
    char *auid;

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    auid = account->uid;
    /* skip leading zeros */
    while (auid && auid[0] == ' ') {
        auid++;
    }

    gc_key = g_strdup_printf (SRW_KEY, auid);
    if (uri == NULL) {
        gconf_client_unset (gcc, gc_key, NULL);
    } else {
        gconf_client_set_string (gcc, gc_key, uri, NULL);
    }

    g_free (gc_key);

    g_object_unref (gcc);
}

void
scalix_account_prefs_set_sversion (EAccount * account,
                                   const char * sversion,
                                   GConfClient * gcc)
{
    char *gc_key;
    char *auid;

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    auid = account->uid;
    /* skip leading zeros */
    while (auid && auid[0] == ' ') {
        auid++;
    }

    gc_key = g_strdup_printf (SSV_KEY, auid);

    if (sversion == NULL) {
        gconf_client_unset (gcc, gc_key, NULL);
    } else {

        gconf_client_set_string (gcc, gc_key, sversion, NULL);
    }

    g_free (gc_key);

    g_object_unref (gcc);
}

char *
scalix_account_prefs_get_sversion (EAccount * account,
                                   GConfClient * gcc)
{
    GError *error;
    char *gc_key;
    char *gc_val;
    char *auid;

    if (account == NULL) {
        return NULL;
    }

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    auid = account->uid;
    /* skip leading zeros */
    while (auid && auid[0] == ' ') {
        auid++;
    }

    gc_key = g_strdup_printf (SSV_KEY, auid);

    error = NULL;
    gc_val = gconf_client_get_string (gcc, gc_key, &error);

    if (error != NULL) {
        g_print ("ERROR: %s,%s,%s\n", error->message, gc_key, gc_val);
        g_clear_error (&error);
        gc_val = NULL;
    }

    g_free (gc_key);

    g_object_unref (gcc);

    return gc_val;
}

#if 0

void
scalix_account_prefs_bind_esource_group (EAccount * account,
                                         const char *type,
                                         const char *uid, GConfClient * gcc)
{
    char *gc_key;
    char *auid;

    g_return_if_fail (account != NULL || type != NULL);

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    auid = account->uid;
    /* skip leading zeros */
    while (auid && auid[0] == ' ') {
        auid++;
    }

    gc_key = g_strdup_printf (SSG_KEY, auid, type);

    if (uid == NULL) {
        gconf_client_unset (gcc, gc_key, NULL);
    } else {
        gconf_client_set_string (gcc, gc_key, uid, NULL);
    }

    g_free (gc_key);
    g_object_unref (gcc);
}

char *
scalix_account_prefs_get_esource_group (EAccount * account,
                                        const char *type, GConfClient * gcc)
{
    char *gc_key;
    char *gc_val;
    char *auid;

    if (gcc == NULL) {
        gcc = gconf_client_get_default ();
    } else {
        g_object_ref (gcc);
    }

    auid = account->uid;
    /* skip leading zeros */
    while (auid && auid[0] == ' ') {
        auid++;
    }

    gc_key = g_strdup_printf (SSG_KEY, auid, type);

    gc_val = gconf_client_get_string (gcc, gc_key, NULL);

    g_free (gc_key);
    g_object_unref (gcc);
    return gc_val;

}

#endif
