/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifndef SCALIX_STORE_MODEL_H
#define SCALIX_STORE_MODEL_H

#include <glib.h>
#include <glib-object.h>

#define SCALIX_TYPE_STORE_MODEL             (scalix_store_model_get_type ())
#define SCALIX_STORE_MODEL(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_STORE_MODEL, ScalixStoreModel))
#define SCALIX_STORE_MODEL_CLASS(vtable)    (G_TYPE_CHECK_CLASS_CAST ((vtable), SCALIX_TYPE_STORE_MODEL, ScalixStoreModelClass))
#define SCALIX_IS_STORE_MODEL(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_STORE_MODEL))
#define SCALIX_IS_STORE_MODEL_CLASS(vtable) (G_TYPE_CHECK_CLASS_TYPE ((vtable), SCALIX_TYPE_STORE_MODEL))
#define SCALIX_STORE_MODEL_GET_CLASS(inst)  (G_TYPE_INSTANCE_GET_CLASS ((inst), SCALIX_TYPE_STORE_MODEL, ScalixStoreModelClass))
#define SCALIX_STORE_MODEL_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_STORE_MODEL, ScalixStoreModelPrivate))

typedef struct _ScalixStoreModel ScalixStoreModel;
typedef struct _ScalixStoreModelClass ScalixStoreModelClass;
typedef struct _ScalixStoreModelPrivate ScalixStoreModelPrivate;

struct _ScalixStoreModel {
    GObject parent;
};

struct _ScalixStoreModelClass {
    GObjectClass parent;
    void (*loaded) (ScalixStoreModel * model, gpointer data);
};

enum {

    SCM_COL_ICON,
    SCM_COL_NAME,
    SCM_COL_PATH,
    SCM_N_COLS
};

GType scalix_store_model_get_type (void);
ScalixStoreModel *scalix_store_model_new (const char *text_uri);

#endif /* SCALIX_STORE_MODEL_H */
