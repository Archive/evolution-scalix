/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifndef SCALIX_ACCOUNT_SYNCH_H
#define SCALIX_ACCOUNT_SYNCH_H

#include <glib-object.h>

#include <libescalix/scalix-version.h>

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-account.h>
#include <libedataserver/e-account-list.h>
#else
#include <e-util/e-account.h>
#include <e-util/e-account-list.h>
#endif

G_BEGIN_DECLS
#define SCALIX_TYPE_ACCOUNT_SYNCH             (scalix_account_synch_get_type ())
#define SCALIX_ACCOUNT_SYNCH(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCALIX_TYPE_ACCOUNT_SYNCH, ScalixAccountSynch))
#define SCALIX_ACCOUNT_SYNCH_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), SCALIX_TYPE_ACCOUNT_SYNCH, ScalixAccountSynchClass))
#define SCALIX_IS_ACCOUNT_SYNCH(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCALIX_TYPE_ACCOUNT_SYNCH))
#define SCALIX_IS_ACCOUNT_SYNCH_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), SCALIX_TYPE_ACCOUNT_SYNCH))
#define SCALIX_ACCOUNT_SYNCH_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SCALIX_TYPE_ACCOUNT_SYNCH, ScalixAccountSynchPrivate))
#define SCALIX_ACCOUNT_SYNCH_ERROR            (scalix_account_synch_error_quark ())
typedef struct _ScalixAccountSynch ScalixAccountSynch;
typedef struct _ScalixAccountSynchClass ScalixAccountSynchClass;
typedef struct _ScalixAccountSynchPrivate ScalixAccountSynchPrivate;

struct _ScalixAccountSynch {
    GObject parent;
};

struct _ScalixAccountSynchClass {
    GObjectClass parent_class;
    /* Signals */
    void (*progress) (ScalixAccountSynch * sxas, guint, char *, gpointer);
    void (*progress2) (ScalixAccountSynch * sxas, guint, char *, gpointer);
    void (*message) (ScalixAccountSynch * sxas, guint, char *, gpointer);
    void (*finished) (ScalixAccountSynch * sxas, gboolean, gpointer);
    /* Virtual Table */
};

GType scalix_account_synch_get_type (void);

ScalixAccountSynch *scalix_account_synch_new (EAccount * account);

gboolean scalix_account_synch_run (ScalixAccountSynch * sxas, gboolean wait);

G_END_DECLS
#endif
