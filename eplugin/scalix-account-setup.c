/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <glib.h>
#include <glib/gstdio.h>

#include <gtk/gtk.h>

#include <glade/glade.h>

#include <e-util/e-config.h>
#include <e-util/e-plugin.h>

#include <libescalix/scalix-version.h>

#if EAPI_CHECK_VERSION (2,6)
#include <libedataserver/e-account.h>
#else
#include <e-util/e-account.h>
#endif

#include <libedataserver/e-url.h>
#include <libedataserver/e-account-list.h>
#include <libedataserverui/e-passwords.h>

/* FIXME: Evolution still uses GnomeDruid. This is considered deprecated. */
#undef GNOME_DISABLE_DEPRECATED
#include <libgnomeui/gnome-druid.h>
#include <libgnomeui/gnome-druid-page-standard.h>

#include <camel/camel.h>
#include <camel/camel-store.h>
#include <camel/camel-session.h>
#include <camel/camel-scalix-store.h>

#include <libescalix/scalix.h>
#include <libescalix/scalix-utils.h>
#include <libescalix/scalix-container.h>

#include <string.h>
#include <stdio.h>

#include "scalix_logo.xpm"

#include "scalix-account-synch.h"
#include "scalix-account-utils.h"

#include <mail/mail-config.h>
#include <mail/em-config.h>
#include <mail/mail-mt.h>

#include <shell/es-event.h>

/* prototypes */
int e_plugin_lib_enable (EPluginLib * ep, int enable);
static char *create_key (CamelURL * url);
static void account_removed (EAccountList * account_listener,
                             EAccount * account);
static void cleanup_old_accounts (void);
void com_scalix_state (EPlugin * ep, ESEventTargetState * target);
GtkWidget *com_scalix_dummy (EPlugin * ep,
                             EConfigHookItemFactoryData * hook_data);
GtkWidget *com_scalix_logo (EPlugin * epl, EConfigHookItemFactoryData * data);
GtkWidget *com_scalix_logo_ds (EPlugin * epl,
                               EConfigHookItemFactoryData * data);
GtkWidget *com_scalix_mc_rules (EPlugin * epl,
                                EConfigHookItemFactoryData * data);
void com_scalix_ae_commit (EPlugin * epl, EConfigHookItemFactoryData * data);
GtkWidget *com_scalix_aw_sync (EPlugin * epl,
                               EConfigHookItemFactoryData * data);
gboolean com_scalix_aw_check (EPlugin * epl, EConfigHookPageCheckData * data);
void com_scalix_aw_abort (EPlugin * epl, EConfigHookItemFactoryData * data);

/* ************************************************************************* */

static EAccountList *alist = NULL;

/* eplugin init */
int
e_plugin_lib_enable (EPluginLib * ep, int enable)
{
    g_print ("Scalix ePlugin spinning up ... (%s, %s)\n", SX_LOCALEDIR,
             GETTEXT_PACKAGE);

    if (enable == TRUE) {
        libescalix_init (FALSE);

        if (alist == NULL) {
            alist = E_ACCOUNT_LIST (mail_config_get_accounts ());

            g_signal_connect (E_ACCOUNT_LIST (alist),
                              "account_removed",
                              G_CALLBACK (account_removed), NULL);

            cleanup_old_accounts ();
        }

    } else {
        g_warning ("Scalix ePlugin got disabled");
        if (alist != NULL) {
            g_object_unref (alist);
        }
    }

    return 0;
}

static void
cleanup_old_accounts (void)
{
    GDir *dh;
    const char *fname;
    char *path;

    path = g_build_path (G_DIR_SEPARATOR_S,
                         g_get_home_dir (),
                         ".evolution", "cache", "scalix", NULL);

    dh = g_dir_open (path, 0, NULL);

    while ((fname = g_dir_read_name (dh))) {
        char *fp;

        fp = g_build_path (G_DIR_SEPARATOR_S, path, fname, ".delete_me", NULL);

        if (g_file_test (fp, G_FILE_TEST_EXISTS)) {
            g_free (fp);

            fp = g_build_path (G_DIR_SEPARATOR_S, path, fname, NULL);

            scalix_recursive_delete (fp);
        }

        g_free (fp);
    }

    if (dh != NULL) {
        g_dir_close (dh);
    }

    g_free (path);
}

static void
account_removed (EAccountList * account_list, EAccount * account)
{
    const char *surl;
    const char *name;
    CamelURL *url;
    char *glob;
    char *path;
    char *key;
    char *user;
    GPatternSpec *cglob;
    GDir *dh;

    surl = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

    if (surl == NULL || !g_str_has_prefix (surl, "scalix://"))
        return;

    g_print ("SCALIX Account REMOVED!\n");

    /* Mark cache as to-delete here so the finalize handler of the
     * cache object will clean it up for us. We can't do it here
     * because the cache might still be open (race!) and we dont 
     * wanna crash e-d-s with hat */
    url = camel_url_new (surl, NULL);
    user = camel_url_encode (url->user, "@");
    glob = g_strdup_printf ("scalix___%s@%s*", user, url->host);
    g_free (user);

    path = g_build_path (G_DIR_SEPARATOR_S,
                         g_get_home_dir (),
                         ".evolution", "cache", "scalix", NULL);
    cglob = g_pattern_spec_new (glob);
    dh = g_dir_open (path, 0, NULL);

    while (dh && (name = g_dir_read_name (dh))) {
        char *fp;
        int fd;

        if (g_str_equal (name, ".") || g_str_equal (name, "..")) {
            continue;
        }

        if (!g_pattern_match_string (cglob, name)) {
            continue;
        }

        fp = g_build_filename (path, name, ".delete_me", NULL);
        fd = g_creat (fp, S_IREAD | S_IWUSR | S_IRGRP | S_IWGRP);
        close (fd);
        g_free (fp);
    }

    if (dh != NULL) {
        g_dir_close (dh);
    }

    g_pattern_spec_free (cglob);
    g_free (path);
    g_free (glob);

    /* Now remove the sources and the account uid -> ESourceGroup uid
     * mappings */
    scalix_account_remove_sources (account);
    scalix_account_prefs_clear (account, NULL);

    key = create_key (url);
    e_passwords_forget_password ("Mail", key);
    g_free (key);

    camel_url_free (url);
}

GtkWidget *
com_scalix_logo (EPlugin * epl, EConfigHookItemFactoryData * data)
{
    EMConfigTargetAccount *target_account;
    GtkWidget *hbox, *vbox;
    GdkPixbuf *pblogo;
    GtkWidget *logo;
    const char *surl;

    target_account = (EMConfigTargetAccount *) data->config->target;
    surl = e_account_get_string (target_account->account, E_ACCOUNT_SOURCE_URL);

    if (!g_str_has_prefix (surl, "scalix://")) {
        return NULL;
    }

    if (data->old) {
        g_print ("reusing old data\n");
        return data->old;
    }

    pblogo = gdk_pixbuf_new_from_xpm_data ((const char **) logo_xpm);
    logo = gtk_image_new ();
    gtk_image_set_from_pixbuf (GTK_IMAGE (logo), pblogo);

    hbox = gtk_hbox_new (FALSE, 6);
    gtk_box_pack_end (GTK_BOX (hbox), logo, FALSE, FALSE, 0);

    vbox = gtk_vbox_new (FALSE, 6);
    gtk_box_pack_end (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show_all (vbox);

    gtk_box_pack_end ((GtkBox *) data->parent, vbox, FALSE, FALSE, 0);

    return vbox;
}

GtkWidget *
com_scalix_logo_ds (EPlugin * epl, EConfigHookItemFactoryData * data)
{
    EMConfigTargetAccount *target_account;
    GtkWidget *hbox, *vbox;
    GdkPixbuf *pblogo;
    GtkWidget *logo;
    const char *surl;

    target_account = (EMConfigTargetAccount *) data->config->target;
    surl = e_account_get_string (target_account->account, E_ACCOUNT_SOURCE_URL);

    if (!g_str_has_prefix (surl, "scalix://")) {
        return NULL;
    }

    if (data->old) {
        g_print ("reusing old data\n");
        return data->old;
    }

    pblogo = gdk_pixbuf_new_from_xpm_data ((const char **) logo_xpm);
    logo = gtk_image_new ();
    gtk_image_set_from_pixbuf (GTK_IMAGE (logo), pblogo);

    hbox = gtk_hbox_new (FALSE, 12);
    gtk_box_pack_end (GTK_BOX (hbox), logo, FALSE, FALSE, 0);

    vbox = gtk_vbox_new (FALSE, 12);
    gtk_box_pack_end (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show_all (vbox);

    gtk_box_pack_end ((GtkBox *) data->parent, vbox, FALSE, FALSE, 0);

    return vbox;
}

struct rw_stuff {
    EAccount *account;
    CamelURL *url;
    GConfClient *gcc;
};

static void
rw_stuff_free (gpointer data, GObject * where_the_object_was)
{
    struct rw_stuff *stuff;

    stuff = (struct rw_stuff *) data;

    g_object_unref (stuff->gcc);
    camel_url_free (stuff->url);
    g_free (stuff);
}

static void
eurl_changed (GtkEditable * editable, gpointer user_data)
{
    struct rw_stuff *stuff;
    char *gc_val;

    stuff = (struct rw_stuff *) user_data;

    gc_val = gtk_editable_get_chars (editable, 0, -1);
    scalix_account_prefs_set_rw_url (stuff->account, gc_val, stuff->gcc);
}

GtkWidget *
com_scalix_mc_rules (EPlugin * epl, EConfigHookItemFactoryData * data)
{
    EMConfigTargetAccount *target_account;
    EAccount *account;
    struct rw_stuff *stuff;
    CamelURL *url;
    GladeXML *xmln;
    GtkWidget *frame;
    GtkWidget *eurl;
    const char *surl;
    GConfClient *gcc;
    char *gc_val;

    target_account = (EMConfigTargetAccount *) data->config->target;
    account = target_account->account;
    surl = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

    if (!g_str_has_prefix (surl, "scalix://")) {
        return NULL;
    }

    xmln = glade_xml_new (ES_GLADEDIR "/scalix-config.glade",
                          "toplevel", GETTEXT_PACKAGE);
    frame = glade_xml_get_widget (xmln, "toplevel");
    gtk_widget_show_all (frame);

    gtk_box_pack_end (GTK_BOX (data->parent), frame, FALSE, FALSE, 0);
    eurl = glade_xml_get_widget (xmln, "eUrl");

    url = camel_url_new (surl, NULL);

    gcc = gconf_client_get_default ();
    gc_val = scalix_account_prefs_get_rw_url (account, gcc);
    gtk_entry_set_text (GTK_ENTRY (eurl), gc_val);
    g_free (gc_val);

    stuff = g_new0 (struct rw_stuff, 1);
    stuff->gcc = gcc;
    stuff->account = account;

    g_signal_connect (eurl, "changed", G_CALLBACK (eurl_changed), stuff);

    g_object_weak_ref (G_OBJECT (frame), rw_stuff_free, stuff);

    return frame;
}

/* -------------------------------------------------------------------------- */

typedef struct _SxSynchUIContext {

    ScalixAccountSynch *sxas;
    EAccount *account;
    CamelURL *url;
    gboolean success;
    gboolean synched;
    char *surl;

    GtkWidget *lmessage;
    GtkWidget *imessage;
    GtkWidget *pprg;
    GtkWidget *lprg;
    GtkWidget *pprg2;
    GtkWidget *lprg2;

    /* Only used by druid pages */
    GnomeDruid *druid;
    GtkWidget *bconnect;
    GtkWidget *epassword;

    /* Only used by dialog */
    GtkWidget *dialog;

} SxSynchUIContext;

static void
sd_progress (ScalixAccountSynch * sxas,
             guint prg, char *msg, gpointer user_data)
{
    SxSynchUIContext *sctx;

    sctx = (SxSynchUIContext *) user_data;

    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (sctx->pprg),
                                   (gdouble) prg / 100.0);

    gtk_label_set_markup (GTK_LABEL (sctx->lprg), msg);
}

static void
sd_progress2 (ScalixAccountSynch * sxas,
              guint prg, char *msg, gpointer user_data)
{
    SxSynchUIContext *sctx;

    sctx = (SxSynchUIContext *) user_data;

    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (sctx->pprg2),
                                   (gdouble) prg / 100.0);

    gtk_label_set_markup (GTK_LABEL (sctx->lprg2), msg);
}

static void
sd_message (ScalixAccountSynch * sxas,
            guint type, char *msg, gpointer user_data)
{
    SxSynchUIContext *sctx;

    sctx = (SxSynchUIContext *) user_data;

    switch (type) {

    case 2:
        gtk_image_set_from_stock (GTK_IMAGE (sctx->imessage),
                                  "gtk-dialog-error", GTK_ICON_SIZE_BUTTON);

        gtk_widget_show (sctx->imessage);
        break;

    case 1:
        gtk_image_set_from_stock (GTK_IMAGE (sctx->imessage),
                                  "gtk-dialog-warn", GTK_ICON_SIZE_BUTTON);

        gtk_widget_show (sctx->imessage);
        break;

    case 0:
        gtk_image_set_from_stock (GTK_IMAGE (sctx->imessage),
                                  "gtk-dialog-info", GTK_ICON_SIZE_BUTTON);
        gtk_widget_show (sctx->imessage);
        break;

    default:
        gtk_widget_hide (sctx->imessage);
        break;
    }

    gtk_label_set_markup (GTK_LABEL (sctx->lmessage), msg);
}

static void
sd_finished (ScalixAccountSynch * sxas, gboolean result, gpointer user_data)
{
    SxSynchUIContext *sctx;

    sctx = (SxSynchUIContext *) user_data;

    gtk_dialog_set_response_sensitive (GTK_DIALOG (sctx->dialog),
                                       GTK_RESPONSE_OK, TRUE);
}

static void
sd_response (GtkDialog * dialog, gint arg1, gpointer user_data)
{

}

void
com_scalix_ae_commit (EPlugin * epl, EConfigHookItemFactoryData * data)
{
    EMConfigTargetAccount *target_account;
    SxSynchUIContext *sctx;
    ScalixAccountSynch *sxas;
    EAccount *account;
    GladeXML *xmln;
    GtkWidget *dialog;
    const char *surl;

    target_account = (EMConfigTargetAccount *) data->config->target;
    account = E_ACCOUNT (target_account->account);

    surl = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

    // FIXME: FIXME FIXME
    return;

    if (!g_str_has_prefix (surl, "scalix://")) {
        return;
    }

    xmln = glade_xml_new (ES_GLADEDIR "/syncui.glade",
                          "synch_dialog", GETTEXT_PACKAGE);

    if (xmln == NULL) {
        g_critical ("Could not open Galde file");
        return;
    }

    sctx = g_new0 (SxSynchUIContext, 1);

    dialog = glade_xml_get_widget (xmln, "synch_dialog");

    sctx->dialog = dialog;
    sctx->lmessage = glade_xml_get_widget (xmln, "lMessage");
    sctx->imessage = glade_xml_get_widget (xmln, "iMessage");
    sctx->pprg = glade_xml_get_widget (xmln, "pProgress");
    sctx->lprg = glade_xml_get_widget (xmln, "lProgress");
    sctx->pprg2 = glade_xml_get_widget (xmln, "pProgress2");
    sctx->lprg2 = glade_xml_get_widget (xmln, "lProgress2");

    g_signal_connect (dialog, "response", G_CALLBACK (sd_response), sctx);

    sxas = scalix_account_synch_new (account);

    g_object_set (sxas, "synch-data", TRUE, NULL);

    g_signal_connect (sxas, "progress", G_CALLBACK (sd_progress), sctx);

    g_signal_connect (sxas, "progress2", G_CALLBACK (sd_progress2), sctx);

    g_signal_connect (sxas, "message", G_CALLBACK (sd_message), sctx);

    g_signal_connect (sxas, "finished", G_CALLBACK (sd_finished), sctx);

    scalix_account_synch_run (sxas, FALSE);
    gtk_widget_show_all (dialog);

    /* Do not show the info image */
    g_object_set (sctx->imessage, "visible", FALSE, NULL);

    gtk_dialog_run (GTK_DIALOG (dialog));
    g_object_unref (sxas);
    gtk_widget_destroy (dialog);
}

/* -------------------------------------------------------------------------- */

/* EEWWKK! global var, but don't know how to do it otherweise */
gboolean sac_synched;

static char *
create_key (CamelURL * url)
{
    char *key;
    char *user;
    char *authmech;

    if (url->protocol == NULL || url->host == NULL || url->user == NULL) {
        return NULL;
    }

    user = camel_url_encode (url->user, ":;@/");

    if (url->authmech != NULL) {
        authmech = camel_url_encode (url->authmech, ":@/");
    } else {
        authmech = NULL;
    }

    if (url->port) {
        key = g_strdup_printf ("%s://%s%s%s@%s:%d/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host, url->port);
    } else {
        key = g_strdup_printf ("%s://%s%s%s@%s/",
                               url->protocol,
                               user,
                               authmech ? ";auth=" : "",
                               authmech ? authmech : "", url->host);
    }

    g_free (user);
    g_free (authmech);
    return key;
}

typedef struct _ServerSynchHandle {

    ScalixAccountSynch *sxas;
    EAccount *account;
    CamelURL *url;
    gboolean success;
    gboolean synched;
    char *surl;

    GnomeDruid *druid;
    GtkWidget *frame;
    GtkWidget *bconnect;
    GtkWidget *epassword;
    GtkWidget *fevent;
    GtkWidget *lmessage;
    GtkWidget *imessage;
    GtkWidget *pprg;
    GtkWidget *lprg;
    GtkWidget *pprg2;
    GtkWidget *lprg2;

} ServerSynchHandle;

static void
bconnect_pressed (GtkButton * button, gpointer user_data)
{
    ServerSynchHandle *ssh;
    const char *passwd;
    char *key;

    ssh = (ServerSynchHandle *) user_data;

    passwd = gtk_entry_get_text (GTK_ENTRY (ssh->epassword));
    key = create_key (ssh->url);

    e_passwords_add_password (key, passwd);
    e_passwords_remember_password ("Mail", key);

    gnome_druid_set_buttons_sensitive (ssh->druid, FALSE /* back */ ,
                                       FALSE /* next */ ,
                                       FALSE /* cancel */ ,
                                       TRUE /* help */ );

    gtk_widget_set_sensitive (ssh->bconnect, FALSE);
    gtk_widget_set_sensitive (ssh->epassword, FALSE);
    gtk_widget_show_all (ssh->fevent);

    scalix_account_synch_run (ssh->sxas, FALSE);
}

static void
epasswd_changed (GtkEditable * editable, gpointer user_data)
{
    ServerSynchHandle *ssh;
    gboolean empty;
    const char *passwd;

    ssh = (ServerSynchHandle *) user_data;

    passwd = gtk_entry_get_text (GTK_ENTRY (editable));
    empty = !(passwd && strlen (passwd));
    gtk_widget_set_sensitive (ssh->bconnect, !empty);
}

static void
ss_prepare (GnomeDruidPage * druidpage, GtkWidget * widget, gpointer user_data)
{
    ServerSynchHandle *ssh;
    char *key, *passwd;
    const char *surl;

    ssh = (ServerSynchHandle *) user_data;

    surl = e_account_get_string (ssh->account, E_ACCOUNT_SOURCE_URL);

    if (ssh->synched == TRUE) {
        if (!surl || !ssh->surl || !g_str_equal (ssh->surl, surl)) {
            g_free (ssh->surl);
            ssh->surl = NULL;
            ssh->synched = FALSE;
            sac_synched = FALSE;
        }
    }

    g_print ("PREPARE: Account in synch: %d!\n", ssh->synched);

    if (!ssh->synched) {
        ssh->url = camel_url_new (surl, NULL);

        key = create_key (ssh->url);
        passwd = e_passwords_get_password ("Mail", key);
        g_free (key);

        gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (ssh->pprg), 0.0);

        gtk_widget_set_sensitive (ssh->epassword, TRUE);
        gtk_widget_set_sensitive (ssh->bconnect, passwd != NULL);

        if (passwd != NULL) {
            gtk_entry_set_text (GTK_ENTRY (ssh->epassword), passwd);
            //FIXME g_free (passwd);
        } else {
            gtk_entry_set_text (GTK_ENTRY (ssh->epassword), "");
        }

    } else {
        gtk_widget_set_sensitive (ssh->epassword, FALSE);
        gtk_widget_set_sensitive (ssh->bconnect, FALSE);
    }

    gnome_druid_set_buttons_sensitive (ssh->druid, TRUE /* back */ ,
                                       ssh->synched /* next */ ,
                                       TRUE /* cancel */ ,
                                       TRUE /* help */ );
}

static void
sp_progress (ScalixAccountSynch * sxas,
             guint prg, char *msg, gpointer user_data)
{
    ServerSynchHandle *ssh;

    ssh = (ServerSynchHandle *) user_data;

    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (ssh->pprg),
                                   (gdouble) prg / 100.0);

    gtk_label_set_markup (GTK_LABEL (ssh->lprg), msg);
}

static void
sp_progress2 (ScalixAccountSynch * sxas,
              guint prg, char *msg, gpointer user_data)
{
    ServerSynchHandle *ssh;

    ssh = (ServerSynchHandle *) user_data;

    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (ssh->pprg2),
                                   (gdouble) prg / 100.0);

    gtk_label_set_markup (GTK_LABEL (ssh->lprg2), msg);
}

static void
sp_message (ScalixAccountSynch * sxas,
            guint type, char *msg, gpointer user_data)
{
    ServerSynchHandle *ssh;

    ssh = (ServerSynchHandle *) user_data;

    switch (type) {

    case 2:
        gtk_image_set_from_stock (GTK_IMAGE (ssh->imessage),
                                  "gtk-dialog-error", GTK_ICON_SIZE_BUTTON);

        gtk_widget_show (ssh->imessage);
        break;

    case 1:
        gtk_image_set_from_stock (GTK_IMAGE (ssh->imessage),
                                  "gtk-dialog-warning", GTK_ICON_SIZE_BUTTON);

        gtk_widget_show (ssh->imessage);
        break;

    case 0:
        gtk_image_set_from_stock (GTK_IMAGE (ssh->imessage),
                                  "gtk-dialog-info", GTK_ICON_SIZE_BUTTON);
        gtk_widget_show (ssh->imessage);
        break;

    default:
        gtk_widget_hide (ssh->imessage);
        break;
    }

    gtk_label_set_markup (GTK_LABEL (ssh->lmessage), msg);
}

static void
sp_finished (ScalixAccountSynch * sxas, gboolean result, gpointer user_data)
{
    ServerSynchHandle *ssh;
    const char *url;

    ssh = (ServerSynchHandle *) user_data;

    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (ssh->pprg), 1.0);

    gnome_druid_set_buttons_sensitive (ssh->druid, TRUE /* back */ ,
                                       result /* next */ ,
                                       TRUE /* cancel */ ,
                                       TRUE /* help */ );
    sac_synched = result;

    if (result == TRUE) {
        url = e_account_get_string (ssh->account, E_ACCOUNT_SOURCE_URL);
        ssh->synched = TRUE;
        ssh->surl = g_strdup (url);
    }

}

GtkWidget *
com_scalix_aw_sync (EPlugin * epl, EConfigHookItemFactoryData * data)
{
    EMConfigTargetAccount *target_account;
    ScalixAccountSynch *sxas;
    EAccount *account;
    ServerSynchHandle *ssh;
    GladeXML *xmln;
    GtkWidget *frame;
    GtkWidget *page;
    GtkWidget *container;
    const char *surl;
    char *cptr = NULL;

    target_account = (EMConfigTargetAccount *) data->config->target;
    account = target_account->account;

    surl = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

    if (!g_str_has_prefix (surl, "scalix://")) {
        return NULL;
    }

    /* remove command parameter that may exist from
       choosing a different server type first */
    if(strstr(surl, ";command=") != NULL) {
	char *tmp = g_strdup (surl);
	cptr = strstr(tmp, ";command=");
        *cptr = '\0';
	e_account_set_string (account, E_ACCOUNT_SOURCE_URL, tmp);
	g_free (tmp);
	surl = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);
    }

    ssh = g_new0 (ServerSynchHandle, 1);
    ssh->druid = GNOME_DRUID (data->parent);
    ssh->account = account;
    ssh->synched = FALSE;

    xmln = glade_xml_new (ES_GLADEDIR "/syncui.glade",
                          "toplevel", GETTEXT_PACKAGE);

    if (xmln == NULL) {
        g_critical ("Could not open Galde file");
        return NULL;
    }

    frame = glade_xml_get_widget (xmln, "toplevel");
    ssh->frame = frame;
    ssh->bconnect = glade_xml_get_widget (xmln, "bConnect");
    ssh->epassword = glade_xml_get_widget (xmln, "ePassword");
    ssh->lmessage = glade_xml_get_widget (xmln, "lMessage");
    ssh->imessage = glade_xml_get_widget (xmln, "iMessage");
    ssh->pprg = glade_xml_get_widget (xmln, "pProgress");
    ssh->lprg = glade_xml_get_widget (xmln, "lProgress");
    ssh->pprg2 = glade_xml_get_widget (xmln, "pProgress2");
    ssh->lprg2 = glade_xml_get_widget (xmln, "lProgress2");

    ssh->fevent = glade_xml_get_widget (xmln, "fEvent");

    page = gnome_druid_page_standard_new ();

    container = ((GnomeDruidPageStandard *) page)->vbox;

    gtk_box_pack_start (GTK_BOX (container), frame, FALSE, FALSE, 0);
    gnome_druid_page_standard_set_title (GNOME_DRUID_PAGE_STANDARD (page),
                                         "Synchronizing with Server");

    gnome_druid_append_page (GNOME_DRUID (data->parent),
                             GNOME_DRUID_PAGE (page));

    gtk_widget_show_all (page);

    /* Do not show the info image */
    gtk_widget_hide (ssh->imessage);
    gtk_widget_hide (ssh->fevent);

    g_signal_connect (page, "prepare", G_CALLBACK (ss_prepare), ssh);

    g_signal_connect (ssh->bconnect,
                      "clicked", G_CALLBACK (bconnect_pressed), ssh);

    g_signal_connect (ssh->epassword,
                      "changed", G_CALLBACK (epasswd_changed), ssh);

    sxas = scalix_account_synch_new (account);

    g_object_set (sxas, "synch-dfolder", TRUE, NULL);
    g_object_set (sxas, "synch-data", TRUE, NULL);

    g_signal_connect (sxas, "progress", G_CALLBACK (sp_progress), ssh);

    g_signal_connect (sxas, "progress2", G_CALLBACK (sp_progress2), ssh);

    g_signal_connect (sxas, "message", G_CALLBACK (sp_message), ssh);

    g_signal_connect (sxas, "finished", G_CALLBACK (sp_finished), ssh);

    ssh->sxas = sxas;

    sac_synched = FALSE;
    return page;
}

gboolean
com_scalix_aw_check (EPlugin * epl, EConfigHookPageCheckData * data)
{
    EMConfigTargetAccount *target;

    target = (EMConfigTargetAccount *) data->config->target;

    if (data->pageid && g_str_equal (data->pageid, "50.synch")) {
        return sac_synched;
    }

    return TRUE;
}

void
com_scalix_aw_abort (EPlugin * epl, EConfigHookItemFactoryData * data)
{
    EMConfigTargetAccount *target_account;
    EAccount *account;

    target_account = (EMConfigTargetAccount *) data->config->target;
    account = target_account->account;

    scalix_account_remove_sources (account);
}

/* ---- */
GtkWidget *
com_scalix_dummy (EPlugin * ep, EConfigHookItemFactoryData * data)
{
    g_print ("DUMMY!\n");
    return NULL;
}

/* ************************************************************************* */

static gboolean
idle_sync_sources (gpointer data)
{
    GSList *list;
    GSList *iter;

    list = (GSList *) data;

    for (iter = list; iter; iter = iter->next) {
        ScalixAccountSynch *sxas;
        EAccount *account;

        account = E_ACCOUNT (iter->data);

        sxas = scalix_account_synch_new (account);
        scalix_account_synch_run (sxas, FALSE);
        g_object_unref (sxas);
    }

    g_slist_free (list);

    return FALSE;
}

void
com_scalix_state (EPlugin * ep, ESEventTargetState * target)
{
    GSList *list;
    EAccountList *account_list;
    EAccount *account;
    GConfClient *gconf_client;
    EIterator *iter;

    g_print ("EPlugin: STATE CHANGED\n (%d)", target->state);

    list = NULL;

    gconf_client = gconf_client_get_default ();
    account_list = e_account_list_new (gconf_client);
    g_object_unref (gconf_client);

    iter = e_list_get_iterator (E_LIST (account_list));

    while (e_iterator_is_valid (iter)) {
        const char *url;

        account = E_ACCOUNT (e_iterator_get (iter));

        url = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);

        if (g_str_has_prefix (url, "scalix://")) {
            g_print ("Scheduling %s for update\n", account->name);
            list = g_slist_prepend (list, g_object_ref (account));
        }

        e_iterator_next (iter);
    }

    g_object_unref (account_list);

    if (list == NULL) {
        g_print ("No Scalix accounts to update ...\n");
    } else {
        g_idle_add (idle_sync_sources, list);
    }
}
