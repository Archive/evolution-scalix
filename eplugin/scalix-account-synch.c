
/*
 * Copyright (C) 2005-2007 Scalix, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian.Kellner@scalix.com
 */

/* define "HAVE_MAILMSG" needs to be set if some Fedora modifications
 * have been made to the evolution code in mail/mail-mt.h
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <glib.h>

#include <camel/camel-session.h>
#include <camel/camel-store.h>
#include <camel/camel-offline-store.h>
#include <camel/camel-folder.h>

#include <libedataserver/e-url.h>
#include <libedataserver/e-source.h>
#include <libedataserver/e-source-group.h>
#include <libedataserver/e-source-list.h>

#include <libebook/e-book.h>

#include <libescalix/scalix-utils.h>
#include <libescalix/scalix-object.h>
#include <libescalix/scalix-marshal.h>
#include <libescalix/scalix-container.h>
#include <libescalix/scalix-camel-session.h>

#include <camel/camel-scalix-utils.h>
#include <camel/camel-scalix-store.h>

#include <e-util/e-plugin.h>
#include <calendar/gui/e-cal-config.h>

#include "scalix-account-synch.h"
#include "scalix-account-utils.h"

#include <mail/mail-mt.h>

#define ACCOUNT_ID "AccountId"

struct _ScalixAccountSynchPrivate {
    EAccount *account;
    gboolean synch_dfolder;
    gboolean synch_data;
    /* used by synch_data */
    guint items;
    guint items_done;
};

static void set_property (GObject * object,
                          guint prop_id,
                          const GValue * value, GParamSpec * pspec);
static void get_property (GObject * object,
                          guint prop_id, GValue * value, GParamSpec * pspec);
static char * url_set_path_and_get_string (CamelURL *url, const char *path);
/* ************************************************************************** */

/* GObject Stuff */

enum {
    SAS_SIGNAL_PROGRESS,
    SAS_SIGNAL_PROGRESS2,
    SAS_SIGNAL_MESSAGE,
    SAS_SIGNAL_FINISHED,
    SAS_LAST_SIGNAL
};

static guint sxas_signals[SAS_LAST_SIGNAL] = { 0 };

enum {
    PROP_0,
    PROP_ACCOUNT,
    PROP_SYNCH_DFOLDER,
    PROP_SYNCH_DATA
};

static GObjectClass *parent_class = NULL;

G_DEFINE_TYPE (ScalixAccountSynch, scalix_account_synch, G_TYPE_OBJECT);

static void
finalize (GObject * object)
{
    ScalixAccountSynch *sxas;
    ScalixAccountSynchPrivate *priv;

    sxas = SCALIX_ACCOUNT_SYNCH (object);
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    if (priv->account) {
        g_object_unref (priv->account);
    }

    if (G_OBJECT_CLASS (parent_class)->finalize)
        (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
scalix_account_synch_class_init (ScalixAccountSynchClass * klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);

    g_type_class_add_private (klass, sizeof (ScalixAccountSynchPrivate));

    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;
    gobject_class->finalize = finalize;

    /* Properties */
    g_object_class_install_property (gobject_class,
                                     PROP_ACCOUNT,
                                     g_param_spec_object ("account",
                                                          "EAccount",
                                                          NULL,
                                                          E_TYPE_ACCOUNT,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE |
                                                          G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (gobject_class,
                                     PROP_SYNCH_DFOLDER,
                                     g_param_spec_boolean ("synch-dfolder",
                                                           "Synchronize default folders",
                                                           NULL,
                                                           FALSE,
                                                           G_PARAM_READABLE |
                                                           G_PARAM_WRITABLE |
                                                           G_PARAM_CONSTRUCT));

    g_object_class_install_property (gobject_class,
                                     PROP_SYNCH_DATA,
                                     g_param_spec_boolean ("synch-data",
                                                           "Synchronize data",
                                                           NULL,
                                                           FALSE,
                                                           G_PARAM_READABLE |
                                                           G_PARAM_WRITABLE |
                                                           G_PARAM_CONSTRUCT));

    /* Signals */
    sxas_signals[SAS_SIGNAL_PROGRESS] =
        g_signal_new ("progress",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_CLEANUP,
                      G_STRUCT_OFFSET (ScalixAccountSynchClass, progress),
                      NULL,
                      NULL,
                      scalix_g_cclosure_marshal_VOID__UINT_STRING,
                      G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_STRING);

    sxas_signals[SAS_SIGNAL_PROGRESS2] =
        g_signal_new ("progress2",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_CLEANUP,
                      G_STRUCT_OFFSET (ScalixAccountSynchClass, progress2),
                      NULL,
                      NULL,
                      scalix_g_cclosure_marshal_VOID__UINT_STRING,
                      G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_STRING);

    sxas_signals[SAS_SIGNAL_MESSAGE] =
        g_signal_new ("message",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_CLEANUP,
                      G_STRUCT_OFFSET (ScalixAccountSynchClass, message),
                      NULL,
                      NULL,
                      scalix_g_cclosure_marshal_VOID__UINT_STRING,
                      G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_STRING);

    sxas_signals[SAS_SIGNAL_FINISHED] =
        g_signal_new ("finished",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_CLEANUP,
                      G_STRUCT_OFFSET (ScalixAccountSynchClass, finished),
                      NULL,
                      NULL,
                      scalix_g_cclosure_marshal_VOID__BOOLEAN,
                      G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

}

static void
scalix_account_synch_init (ScalixAccountSynch * sxas)
{
    ScalixAccountSynchPrivate *priv;

    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);
}

static void
set_property (GObject * object,
              guint prop_id, const GValue * value, GParamSpec * pspec)
{
    ScalixAccountSynch *sxas;
    ScalixAccountSynchPrivate *priv;

    sxas = SCALIX_ACCOUNT_SYNCH (object);
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    switch (prop_id) {

    case PROP_ACCOUNT:
        priv->account = E_ACCOUNT (g_value_dup_object (value));
        break;

    case PROP_SYNCH_DFOLDER:
        priv->synch_dfolder = g_value_get_boolean (value);
        break;

    case PROP_SYNCH_DATA:
        priv->synch_data = g_value_get_boolean (value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
get_property (GObject * object,
              guint prop_id, GValue * value, GParamSpec * pspec)
{
    ScalixAccountSynch *sxas;
    ScalixAccountSynchPrivate *priv;

    sxas = SCALIX_ACCOUNT_SYNCH (object);
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    switch (prop_id) {

    case PROP_ACCOUNT:
        g_value_set_object (value, G_OBJECT (priv->account));
        break;

    case PROP_SYNCH_DFOLDER:
        g_value_set_boolean (value, priv->synch_dfolder);
        break;

    case PROP_SYNCH_DATA:
        g_value_set_boolean (value, priv->synch_data);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

/* ************************************************************************* */

/* Private methods */

static GSList *
folder_tree_flatten (CamelFolderInfo * ftree, int type, GSList ** def)
{
    CamelFolderInfo *iter;
    GSList *list;

    list = NULL;

    for (iter = ftree; iter; iter = iter->next) {

        if (((iter->flags & CAMEL_SCALIX_SFOLDER_MASK) == type)) {
            list = g_slist_prepend (list, iter);

            if (iter->flags & CAMEL_FOLDER_IS_DEFAULT) {
                *def = list;
            }
        }

        if (iter->child) {
            GSList *clist = NULL;

            clist = folder_tree_flatten (iter->child, type, def);
            list = g_slist_concat (list, clist);
        }
    }

    return list;
}

static CamelFolderInfo *
folder_tree_find_special (CamelFolderInfo * ftree, int type)
{
    CamelFolderInfo *iter;

    for (iter = ftree; iter; iter = iter->next) {

        if (((iter->flags & CAMEL_SCALIX_SFOLDER_MASK) == type)) {
            break;
        }

        if (iter->child) {
            CamelFolderInfo *fi;

            fi = folder_tree_find_special (iter->child, type);

            if (fi != NULL) {
                iter = fi;
                break;
            }
        }

    }

    return iter;
}

/* ************************************************************************* */
static gchar *
create_base_uri (EUri * uri)
{
    char *uri_string = NULL;
    char *user;

    user = camel_url_encode (uri->user, "@");

    if (uri->port != 0) {
        uri_string = g_strdup_printf ("%s://%s%s%s%s%s:%d/",
				      uri->protocol,
                                      user,
				      uri->authmech ? ";auth=" : "",
				      uri->authmech ? uri->authmech : "",
				      uri->user ? "@" : "",
				      uri->host,
				      uri->port);
    } else {
        uri_string = g_strdup_printf ("%s://%s%s%s%s%s/",
				      uri->protocol,
                                      user,
				      uri->authmech ? ";auth=" : "",
				      uri->authmech ? uri->authmech : "",
				      uri->user ? "@" : "",
				      uri->host);
    }

    g_free (user);

    return uri_string;
}

static ESourceGroup *
create_group (EAccount * account, const char *type_string)
{
    ESourceGroup *gfake;
    char *uid;
    char *base_uri;
    xmlDocPtr doc;
    xmlNodePtr root;
    EUri *euri;
    const char *surl;

    uid = g_strdup_printf ("%s@%s", type_string, account->uid);

    doc = xmlNewDoc ("1.0");
    root = xmlNewDocNode (doc, NULL, "group", NULL);
    xmlDocSetRootElement (doc, root);

    surl = e_account_get_string (account, E_ACCOUNT_SOURCE_URL);
    euri = e_uri_new (surl);
    base_uri = create_base_uri (euri);
    e_uri_free (euri);

    xmlSetProp (root, "uid", uid);
    xmlSetProp (root, "name", account->name);
    xmlSetProp (root, "base_uri", base_uri);

    gfake = e_source_group_new_from_xmldoc (doc);
    g_free (base_uri);
    g_free (uid);
    xmlFreeDoc (doc);

    return gfake;
}

static void
sync_source_group (EAccount * account,
                   ESourceGroup * group,
                   int folder_type, CamelFolderInfo * ftree)
{
    GSList *fi_list;
    GSList *def;
    GSList *iter;

    fi_list = folder_tree_flatten (ftree, folder_type, &def);

    for (iter = fi_list; iter != NULL; iter = g_slist_next (iter)) {
        ESource *source;
        CamelURL *url;
        CamelFolderInfo *info;
        const char *fuid;
	const char *pssl;
        char *uid;
        xmlNodePtr node;

        info = (CamelFolderInfo *) iter->data;

        url = camel_url_new (info->uri, NULL);
	g_print ("FOLDER_URI: %s\n", info->uri);
        if (url == NULL) {
            camel_url_free (url);
            continue;
        }

        /* the uid part */
        fuid = camel_url_get_param (url, "uid");

        if (fuid == NULL) {
            g_warning ("No Folder UID\n");
            camel_url_free (url);
            continue;
        }

        uid = g_strdup_printf ("%s@%s", fuid, account->uid);


        /* there is no such thing as e_source_set_uid 
         * so we have to cheat here a bit */

        node = xmlNewNode (NULL, "source");
        xmlSetProp (node, "uid", uid);
        xmlSetProp (node, "name", info->name);
        xmlSetProp (node, "relative_uri", info->full_name);

        source = e_source_new_from_xml_node (node);

        xmlFreeNode (node);
        g_free (uid);

        if (source == NULL) {
            g_warning ("Invalid source, skipping");
	    camel_url_free (url);
            /* FIXME free resources */
            continue;
        }

	pssl = camel_url_get_param (url, "use_ssl");
	if (pssl != NULL) {
		e_source_set_property (source, "use_ssl", pssl);
	}

        if (info->flags & CAMEL_FOLDER_IS_DEFAULT) {
            e_source_set_property (source, "default", "TRUE");

            if (folder_type == CAMEL_SCALIX_FOLDER_CONTACT) {
                e_source_set_property (source, "completion", "TRUE");
            }
        }

	camel_url_free (url);
        e_source_group_add_source (group, source, -1);
    }
}

static void
create_ldap_source (EAccount * account, ESourceGroup * group)
{
    ESource *source;
    EUri *euri;
    char *uri;
    char *uid;
    xmlNodePtr node;

    euri = e_uri_new (account->source->url);

    uri = g_strdup_printf ("ldap://%s:389/o=Scalix?sub", euri->host);
    uid = g_strdup_printf ("system-ldap@%s", account->uid);

    node = xmlNewNode (NULL, "source");
    xmlSetProp (node, "uid", uid);
    xmlSetProp (node, "name", _("System"));
    xmlSetProp (node, "uri", uri);

    source = e_source_new_from_xml_node (node);
    xmlFreeNode (node);
    e_source_set_property (source, "completion", "TRUE");

    if (!e_source_group_add_source (group, source, 1)) {
        g_print ("Could not add ldap source\n");
    }

    g_free (uri);
    g_free (uid);
    e_uri_free (euri);
}

/* ************************************************************************* */

struct sxas_msg_ctx {
    ScalixAccountSynch *sxas;
    guint type;
    guint idata;
    const char *msg;
};

static void *
_send_signal_s (gpointer data)
{
    ScalixAccountSynchPrivate *priv;
    struct sxas_msg_ctx *ctx;

    ctx = (struct sxas_msg_ctx *) data;
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (ctx->sxas);

    g_print ("SxAS: emiting singal [s]\n");

    g_signal_emit (ctx->sxas, sxas_signals[ctx->type], 0, ctx->idata, ctx->msg);

    g_print ("SxAS: done [s]\n");

    /* back to worker thread */
    return NULL;
}

static void
signal_send (ScalixAccountSynch * sxas,
             guint type, guint idata, const char *msg)
{
    ScalixAccountSynchPrivate *priv;
    struct sxas_msg_ctx ctx;

    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    ctx.sxas = sxas;
    ctx.type = type;
    ctx.idata = idata;
    ctx.msg = msg;

    g_print ("SxAS: waiting for synch idle\n");
    mail_call_main (MAIL_CALL_p_p, _send_signal_s, &ctx);
    g_print ("SxAS: back\n");
}

#define signal_progress(__sxas, __prg, __msg) \
		signal_send (__sxas, SAS_SIGNAL_PROGRESS, __prg, __msg)

#define signal_progress2(__sxas, __prg, __msg) \
		signal_send (__sxas, SAS_SIGNAL_PROGRESS2, __prg, __msg)

#define signal_info(__sxas, __msg) \
		signal_send (__sxas, SAS_SIGNAL_MESSAGE, 0, __msg)

#define signal_warning(__sxas, __msg) \
		signal_send (__sxas, SAS_SIGNAL_MESSAGE, 1, __msg)

#define signal_error(__sxas, __msg) \
		signal_send (__sxas, SAS_SIGNAL_MESSAGE, 2, __msg)

/* Evolutions "global" CamelSession */
extern CamelSession *session;

#define MSG_SVER "<span weight=\"light\">Found Server Version: %s</span>"

struct account_synch_msg {
#ifdef HAVE_MAILMSG
    MailMsg base;
#else
    struct _mail_msg msg;
#endif
    ScalixAccountSynch *sxas;
    char *sversion;
    gboolean success;
    ESourceGroup *esg_cals;
    ESourceGroup *esg_cnts;
};

static void
container_object_removed_cb (ScalixContainer * container,
                             ScalixObject * object, gpointer data)
{
    ScalixAccountSynch *sxas;
    ScalixAccountSynchPrivate *priv;
    guint prg;

    sxas = SCALIX_ACCOUNT_SYNCH (data);
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    if (priv->items == 0) {
        return;
    }

    (priv->items_done)++;

    prg = (guint) ((((double) priv->items_done) /
                    ((double) priv->items)) * 100.0);
    signal_progress2 (sxas, prg, "");
}

static void
container_object_added_cb (ScalixContainer * container,
                           ScalixObject * object, gpointer data)
{
    ScalixAccountSynch *sxas;
    ScalixAccountSynchPrivate *priv;
    guint prg;

    sxas = SCALIX_ACCOUNT_SYNCH (data);
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    if (priv->items == 0) {
        return;
    }

    (priv->items_done)++;

    prg = (guint) ((((double) priv->items_done) /
                    ((double) priv->items)) * 100.0);
    signal_progress2 (sxas, prg, "");
}

static void
container_object_changed_cb (ScalixContainer * container,
                             ScalixObject * old_object,
                             ScalixObject * object, gpointer data)
{
    ScalixAccountSynch *sxas;
    ScalixAccountSynchPrivate *priv;
    guint prg;

    sxas = SCALIX_ACCOUNT_SYNCH (data);
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    if (priv->items == 0) {
        return;
    }

    (priv->items_done)++;

    prg = (guint) ((((double) priv->items_done) /
                    ((double) priv->items)) * 100.0);
    signal_progress2 (sxas, prg, "");
}

static gboolean
synch_data (ScalixAccountSynch * sxas,
            CamelFolderInfo * ftree, int type, guint prg_offset)
{
    ScalixAccountSynchPrivate *priv;
    GSList *def;
    GSList *list;
    GSList *iter;
    guint n, i, prg;
    CamelURL *url;
    const char *uri;

    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    if (priv->synch_data == FALSE) {
        return TRUE;
    }

    uri = e_account_get_string (priv->account, E_ACCOUNT_SOURCE_URL);
	url = camel_url_new (uri, NULL);

    if (url == NULL) {
        return FALSE;
    }

    list = folder_tree_flatten (ftree, type, &def);
    n = g_slist_length (list);

    for (iter = list, i = 1; iter; iter = g_slist_next (iter), i++) {
        ScalixContainer *container;
        CamelFolderInfo *fi;
        char *curi;
        char *msg;

        fi = (CamelFolderInfo *) iter->data;

		curi = url_set_path_and_get_string (url, fi->full_name);

        prg = prg_offset + ((guint) ((((gdouble) i) / ((gdouble) n)) * 30.0));

        msg = g_strdup_printf (_("Sychronizing %s..."), fi->name);
        signal_progress (sxas, prg, msg);
        g_free (msg);

        container = scalix_container_open (curi);
        g_free (curi);

        if (container == NULL) {
            g_debug ("SxAS: Could not open container\n");
            continue;
        }

        g_object_set (container, "online", TRUE, NULL);
        g_object_get (container, "items", &(priv->items), NULL);

        priv->items_done = 0;

        g_signal_connect (container,
                          "object_added",
                          G_CALLBACK (container_object_added_cb), sxas);

        g_signal_connect (container,
                          "object_changed",
                          G_CALLBACK (container_object_changed_cb), sxas);

        g_signal_connect (container,
                          "object_removed",
                          G_CALLBACK (container_object_removed_cb), sxas);

        signal_progress2 (sxas, 0, "");
        scalix_container_sync (container);
        signal_progress2 (sxas, 100, "");
        g_object_unref (container);

    }

	camel_url_free (url);
	return TRUE;
}

static char *
url_set_path_and_get_string (CamelURL *url, const char *path)
{
	char *url_str;
	char *tmp = NULL;

	if (!path || !path[0]) {
		return NULL;
	}

	if (path[0] != '/') {
		tmp = g_strconcat ("/", path, NULL);
		path = tmp;
	}

	camel_url_set_path (url, path);
	url_str = camel_url_to_string (url, 0);

	g_free (tmp);

	return url_str;
}

/* The worker thread function with all the complex
 * logic in it! Big fat beast! Please watch your
 * children ... */
static void
synch_worker (struct account_synch_msg *m)
{
    ScalixAccountSynch *sxas;
    ScalixAccountSynchPrivate *priv;
    CamelException ex;
    CamelStore *store;
    CamelFolderInfo *ftree;
    CamelFolderInfo *sf;
    CamelSession *cs;
    const char *uri;
    gboolean res;
    char *sversion;
    char *markup;

    sxas = SCALIX_ACCOUNT_SYNCH (m->sxas);
    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    uri = e_account_get_string (priv->account, E_ACCOUNT_SOURCE_URL);

    g_print ("SxAS: starting synch for %s\n", uri);

    signal_progress (sxas, 1, _("Trying to connect to server"));

    camel_exception_init (&ex);
    cs = scalix_camel_session_get_default ();
    camel_session_set_online (cs, TRUE);

    store = camel_session_get_store (cs, uri, &ex);

    if (store == NULL) {
        signal_error (sxas, _("Authentication failed. Check for correct hostname, username and password."));
        return;
    }

    /* Must be online! */
    camel_offline_store_set_network_state (CAMEL_OFFLINE_STORE (store),
                                           CAMEL_OFFLINE_STORE_NETWORK_AVAIL,
                                           &ex);

    camel_service_connect (CAMEL_SERVICE (store), &ex);

    if (camel_exception_is_set (&ex)) {
        signal_error (sxas, _("Error while trying to connect to server."));
        return;
    }

    /* Always check for minimal required server version  */
    signal_progress (sxas, 10, _("Checking version of Scalix server"));
    g_print ("SxAS: Checking for minimal server version\n");

    sversion = NULL;
    camel_object_get (store, NULL,
                      CAMEL_SCALIX_STORE_SERVER_VERSION, &sversion, NULL);

    res = scalix_check_min_server_version (sversion);

    if (res == FALSE) {
        signal_error (sxas, _("Wrong version of Scalix server detected"));
        return;
    }

    m->sversion = g_strdup (sversion);
    g_print ("SxAS: sversion:%s\n", m->sversion);

    markup = g_markup_printf_escaped (MSG_SVER, sversion);
    signal_info (sxas, markup);
    g_free (markup);

    signal_progress (sxas, 20, _("Getting list of folders"));
    ftree = camel_store_get_folder_info (store,
                                         NULL,
                                         CAMEL_STORE_FOLDER_INFO_RECURSIVE |
                                         CAMEL_SCALIX_STORE_SHOW_SFOLDER, &ex);

    if (ftree == NULL) {
        camel_object_unref (store);
        signal_error (sxas, _("Could not obtain folder listening"));
        return;

    }

    /* Calendar  */
    signal_progress (sxas, 30, _("Loading calendar data..."));
    synch_data (sxas, ftree, CAMEL_SCALIX_FOLDER_CALENDAR, 30);

    m->esg_cals = create_group (priv->account, "Calendar");
    sync_source_group (priv->account,
                       m->esg_cals, CAMEL_SCALIX_FOLDER_CALENDAR, ftree);

    /* Contacts */
    signal_progress (sxas, 60, _("Loading contacts..."));
    synch_data (sxas, ftree, CAMEL_SCALIX_FOLDER_CONTACT, 60);

    m->esg_cnts = create_group (priv->account, "Contacts");
    sync_source_group (priv->account,
                       m->esg_cnts, CAMEL_SCALIX_FOLDER_CONTACT, ftree);

    create_ldap_source (priv->account, m->esg_cnts);

    /* Sent Items and Drafts Folder */
    if (priv->synch_dfolder) {
        char *url_str;
	CamelURL *url;

	url = camel_url_new (uri, NULL);

        signal_progress (sxas, 95, _("Synchronizing additional data"));

        sf = folder_tree_find_special (ftree, CAMEL_SCALIX_FOLDER_SENT);

        if (url && sf) {
	    url_str = url_set_path_and_get_string (url, sf->full_name);

            e_account_set_string (priv->account,
                                  E_ACCOUNT_SENT_FOLDER_URI, url_str);
            g_free (url_str);
        }

        sf = folder_tree_find_special (ftree, CAMEL_SCALIX_FOLDER_DRAFTS);

        if (url && sf) {
	    url_str = url_set_path_and_get_string (url, sf->full_name);
            e_account_set_string (priv->account,
                                  E_ACCOUNT_DRAFTS_FOLDER_URI, url_str);

            g_free (url_str);
        }

        if (url) {
            gboolean save_pw;

            camel_url_set_path (url, NULL);

            if (url->authmech == NULL) {
		    camel_url_set_authmech (url, "PLAIN");
            }

	    url_str = camel_url_to_string (url, 0);
            e_account_set_string (priv->account,
                                  E_ACCOUNT_TRANSPORT_URL, url_str);

            save_pw = e_account_get_bool (priv->account,
                                          E_ACCOUNT_SOURCE_SAVE_PASSWD);

            e_account_set_bool (priv->account,
                                E_ACCOUNT_TRANSPORT_SAVE_PASSWD, save_pw);
        }

        if (url) {
		camel_url_free (url);
        }
    }

    m->success = TRUE;

    g_print ("SxAS: DONE!\n");

    camel_object_unref (store);
    signal_progress (sxas, 100, _("Done loading"));
    signal_info (sxas, _("Initial loading complete. Please click Forward."));
    g_print ("SxAS: Freeing DONE\n");
    return;
}

static void
synch_done (struct account_synch_msg *m)
{

    g_print ("SxAS: Emitting finished signal (%d) [%p]\n",
             m->success, g_thread_self ());

    /* Workaround for broken e_source_list and/or gconf */
    if (m->success) {
        ESourceList *slist;
        ESourceGroup *group;
        const char *group_uid;
        gboolean changed;
        ScalixAccountSynchPrivate *priv;

        changed = FALSE;
        slist = NULL;
        e_cal_get_sources (&slist, E_CAL_SOURCE_TYPE_EVENT, NULL);

        group_uid = e_source_group_peek_uid (m->esg_cals);
        group = e_source_list_peek_group_by_uid (slist, group_uid);

        g_print ("Cal: %s %p\n", group_uid, group);

        if (group == NULL) {
            e_source_list_add_group (slist, m->esg_cals, -1);
        } else {
            char *xml;

            xml = e_source_group_to_xml (m->esg_cals);
            g_print ("Xml: %s\n", xml);
            e_source_group_update_from_xml (group, xml, &changed);
            g_free (xml);
        }

        e_source_list_sync (slist, NULL);
        g_object_unref (slist);

        slist = NULL;
        changed = FALSE;
        e_book_get_addressbooks (&slist, NULL);

        group_uid = e_source_group_peek_uid (m->esg_cnts);
        group = e_source_list_peek_group_by_uid (slist, group_uid);
        g_print ("Cnt: %s %p\n", group_uid, group);

        if (group == NULL) {
            e_source_list_add_group (slist, m->esg_cnts, -1);
        } else {
            char *xml;

            xml = e_source_group_to_xml (m->esg_cnts);
            g_print ("Xml: %s\n", xml);
            e_source_group_update_from_xml (group, xml, &changed);
            g_free (xml);
        }

        e_source_list_sync (slist, NULL);
        g_object_unref (slist);

        priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (m->sxas);

        g_print ("SV: %s\n", m->sversion);

        scalix_account_prefs_set_sversion (priv->account, m->sversion, NULL);

    }

    g_signal_emit (m->sxas, sxas_signals[SAS_SIGNAL_FINISHED], 0, m->success);

    g_print ("SxAS: Emitting done\n");

}

static void
synch_free (struct account_synch_msg *m)
{

    g_free (m->sversion);
    g_object_unref (m->sxas);

    g_print ("SxAS: Freeing msg done\n");

}

#ifndef HAVE_MAILMSG

static void
synch_worker_wrap (struct _mail_msg *mm)
{
    struct account_synch_msg *m;
    m = (struct account_synch_msg *) mm;
    synch_worker (m);
}

static void
synch_done_wrap (struct _mail_msg *mm)
{
    struct account_synch_msg *m;
    m = (struct account_synch_msg *) mm;
    synch_done (m);
}

static void
synch_free_wrap (struct _mail_msg *mm)
{
    struct account_synch_msg *m;
    m = (struct account_synch_msg *) mm;
    synch_free (m);
}

#endif

#ifdef HAVE_MAILMSG

static MailMsgInfo account_synch_info = {
    sizeof (struct account_synch_msg),
    (MailMsgDescFunc) NULL,
    (MailMsgExecFunc) synch_worker,
    (MailMsgDoneFunc) synch_done,
    (MailMsgFreeFunc) synch_free
};

#else

static struct _mail_msg_op account_synch_op = {
    NULL,
    synch_worker_wrap,
    synch_done_wrap,
    synch_free_wrap,
};

#endif

/* ************************************************************************* */

/* Public Interface */

ScalixAccountSynch *
scalix_account_synch_new (EAccount * account)
{
    ScalixAccountSynch *sxas;

    sxas = g_object_new (SCALIX_TYPE_ACCOUNT_SYNCH, "account", account, NULL);

    return sxas;
}

gboolean
scalix_account_synch_run (ScalixAccountSynch * sxas, gboolean wait)
{
    ScalixAccountSynchPrivate *priv;
    struct account_synch_msg *m;

#ifdef HAVE_MAILMSG
    m = mail_msg_new (&account_synch_info);
#else
    m = mail_msg_new (&account_synch_op, NULL, sizeof (*m));
#endif

    if (m == NULL) {
        return FALSE;
    }

    priv = SCALIX_ACCOUNT_SYNCH_GET_PRIVATE (sxas);

    m->sxas = g_object_ref (sxas);
    m->success = FALSE;
    m->sversion = NULL;

#ifdef HAVE_MAILMSG
    mail_msg_unordered_push (m);
#else
    e_thread_put (mail_thread_new, (EMsg *) m);
#endif

    return TRUE;
}

#if 0
gboolean
scalix_account_synch_stop (ScalixAccountSynch * sxas, gboolean wait)
{
    return FALSE;
}

#endif
