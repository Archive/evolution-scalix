/*
 * Copyright 2005 Scalix, Inc. (www.scalix.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 * Authors: Christian Kellner <Christian.Kellner@scalix.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <gtk/gtktreemodel.h>

#include <camel/camel.h>
#include <camel/camel-store.h>
#include <camel/camel-session.h>

#include <e-util/e-icon-factory.h>

#include <libescalix/scalix-marshal.h>
#include <camel/camel-scalix-utils.h>
#include <camel/camel-scalix-store.h>
#include "scalix-store-model.h"

#include <string.h>

struct _ScalixStoreModelPrivate {
    CamelStore *store;
    CamelFolderInfo root;
    guint32 stamp;
    char *text_uri;
    gboolean loaded;
};

enum {
    PROP_0 = 0,
    PROP_URI
};

enum {
    LOADED_SIGNAL,
    LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

GType col_types[] = {
    G_TYPE_STRING,
    G_TYPE_STRING
};

/* Prototypes */
static void tree_model_iface_init (GtkTreeModelIface * iface);

static void scalix_store_model_set_property (GObject * object,
                                             guint prop_id,
                                             const GValue * value,
                                             GParamSpec * pspec);

static void scalix_store_model_get_property (GObject * object,
                                             guint prop_id,
                                             GValue * value,
                                             GParamSpec * pspec);

static void init_folder_icons (void);

G_DEFINE_TYPE_EXTENDED (ScalixStoreModel,
                        scalix_store_model,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (GTK_TYPE_TREE_MODEL,
                                               tree_model_iface_init));

static void
scalix_store_model_class_init (ScalixStoreModelClass * klass)
{
    GObjectClass *gobject_class;

    gobject_class = (GObjectClass *) klass;

    gobject_class->set_property = scalix_store_model_set_property;
    gobject_class->get_property = scalix_store_model_get_property;

    g_type_class_add_private (klass, sizeof (ScalixStoreModelPrivate));

    g_object_class_install_property (gobject_class,
                                     PROP_URI,
                                     g_param_spec_string ("uri",
                                                          "Uri of the store",
                                                          "",
                                                          NULL,
                                                          G_PARAM_READABLE |
                                                          G_PARAM_WRITABLE |
                                                          G_PARAM_CONSTRUCT_ONLY));

    /* signals */
    signals[LOADED_SIGNAL] =
        g_signal_new ("loaded",
                      G_OBJECT_CLASS_TYPE (gobject_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (ScalixStoreModelClass, loaded),
                      NULL, NULL,
                      scalix_g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    init_folder_icons ();
}

static void
scalix_store_model_init (ScalixStoreModel * self)
{
    ScalixStoreModelPrivate *priv;

    priv = SCALIX_STORE_MODEL_GET_PRIVATE (self);
    priv->loaded = FALSE;

    do {
        priv->stamp = g_random_int ();
    } while (priv->stamp == 0);

    memset (&(priv->root), 0, sizeof (CamelFolderInfo));
    priv->root.name = _("Loading ...");
}

static void
scalix_store_model_set_property (GObject * object,
                                 guint prop_id,
                                 const GValue * value, GParamSpec * pspec)
{
    ScalixStoreModelPrivate *priv;

    priv = SCALIX_STORE_MODEL_GET_PRIVATE (object);

    switch (prop_id) {

    case PROP_URI:
        priv->text_uri = g_value_dup_string (value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        return;
    }

}

static void
scalix_store_model_get_property (GObject * object,
                                 guint prop_id,
                                 GValue * value, GParamSpec * pspec)
{
    ScalixStoreModelPrivate *priv;

    priv = SCALIX_STORE_MODEL_GET_PRIVATE (object);

    switch (prop_id) {

    case PROP_URI:
        g_value_set_string (value, priv->text_uri);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        return;
    }

}

/* ************************************************************************** */

/* copied from evo */

enum {
    FOLDER_ICON_NORMAL,
    FOLDER_ICON_INBOX,
    FOLDER_ICON_OUTBOX,
    FOLDER_ICON_TRASH,
    FOLDER_ICON_JUNK,
    FOLDER_ICON_SHARED_TO_ME,
    FOLDER_ICON_SHARED_BY_ME,
    FOLDER_ICON_CALENDAR,
    FOLDER_ICON_CONTACTS,
    FOLDER_ICON_LAST
};

static GdkPixbuf *folder_icons[FOLDER_ICON_LAST];
static GdkPixbuf *refresh_icon;

static void
init_folder_icons ()
{
    folder_icons[FOLDER_ICON_NORMAL] =
        e_icon_factory_get_icon ("stock_folder", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_INBOX] =
        e_icon_factory_get_icon ("stock_inbox", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_OUTBOX] =
        e_icon_factory_get_icon ("stock_outbox", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_TRASH] =
        e_icon_factory_get_icon ("stock_delete", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_JUNK] =
        e_icon_factory_get_icon ("stock_spam", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_SHARED_TO_ME] =
        e_icon_factory_get_icon ("stock_shared-to-me", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_SHARED_BY_ME] =
        e_icon_factory_get_icon ("stock_shared-by-me", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_CALENDAR] =
        e_icon_factory_get_icon ("stock_calendar", E_ICON_SIZE_MENU);
    folder_icons[FOLDER_ICON_CONTACTS] =
        e_icon_factory_get_icon ("stock_addressbook", E_ICON_SIZE_MENU);

    refresh_icon = e_icon_factory_get_icon ("stock_refresh", E_ICON_SIZE_MENU);
}

static GdkPixbuf *
icon_by_folder_type (guint32 flags)
{
    guint32 type;
    gint icon_id;

    icon_id = FOLDER_ICON_NORMAL;
    type = (flags & CAMEL_FOLDER_TYPE_MASK);

    switch (type) {

    case CAMEL_FOLDER_TYPE_INBOX:
        icon_id = FOLDER_ICON_INBOX;
        break;

    case CAMEL_FOLDER_TYPE_OUTBOX:
        icon_id = FOLDER_ICON_OUTBOX;
        break;

    case CAMEL_FOLDER_TYPE_TRASH:
        icon_id = FOLDER_ICON_TRASH;
        break;

    case CAMEL_FOLDER_TYPE_JUNK:
        icon_id = FOLDER_ICON_JUNK;
        break;

    }

    if (type == CAMEL_FOLDER_TYPE_NORMAL) {
        type = flags & CAMEL_SCALIX_SFOLDER_MASK;
    }

    switch (type) {

    case CAMEL_SCALIX_FOLDER_CONTACT:
        icon_id = FOLDER_ICON_CONTACTS;
        break;

    case CAMEL_SCALIX_FOLDER_CALENDAR:
        icon_id = FOLDER_ICON_CALENDAR;
        break;
    }

    return folder_icons[icon_id];
}

/* ************************************************************************** */

static GtkTreeModelFlags
tree_model_get_flags (GtkTreeModel * tree_model)
{
    g_return_val_if_fail (SCALIX_IS_STORE_MODEL (tree_model), 0);

    return GTK_TREE_MODEL_ITERS_PERSIST;
}

static gint
tree_model_get_n_columns (GtkTreeModel * tree_model)
{
    return SCM_N_COLS;
}

static GType
tree_model_get_column_type (GtkTreeModel * tree_model, gint index)
{
    g_return_val_if_fail (SCALIX_IS_STORE_MODEL (tree_model), G_TYPE_INVALID);
    g_return_val_if_fail (index < SCM_N_COLS && index >= 0, G_TYPE_INVALID);

    return col_types[index];
}

static void
tree_model_get_value (GtkTreeModel * tree_model,
                      GtkTreeIter * iter, gint column, GValue * value)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *info;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    g_return_if_fail (SCALIX_IS_STORE_MODEL (model));
    g_return_if_fail (iter != NULL);
    g_return_if_fail (iter->stamp == priv->stamp);
    g_return_if_fail (column < SCM_N_COLS);

    info = (CamelFolderInfo *) iter->user_data;

    if (info == NULL && priv->loaded == TRUE) {

        return;
    }

    switch (column) {

    case SCM_COL_ICON:
        g_value_init (value, GDK_TYPE_PIXBUF);

        if (priv->loaded != TRUE) {
            g_value_set_object (value, refresh_icon);
        } else {
            g_value_set_object (value, icon_by_folder_type (info->flags));
        }

        break;

    case SCM_COL_NAME:
        g_value_init (value, G_TYPE_STRING);

        if (priv->loaded != TRUE) {
            g_value_set_static_string (value, _("Loading ..."));
        } else {
            g_value_set_string (value, info->name);
        }

        break;

    case SCM_COL_PATH:
        g_value_init (value, G_TYPE_STRING);
        g_value_set_string (value, info->full_name);
        break;

    default:
        g_assert_not_reached ();
    }

}

static gboolean
tree_model_get_iter (GtkTreeModel * tree_model,
                     GtkTreeIter * iter, GtkTreePath * path)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *info;
    gint *indices;
    gint depth, i, d;
    gboolean res;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    g_return_val_if_fail (SCALIX_IS_STORE_MODEL (model), FALSE);

    indices = gtk_tree_path_get_indices (path);
    depth = gtk_tree_path_get_depth (path);

    info = &(priv->root);
    res = TRUE;

    for (i = d = 0; d < depth && info; i++) {

        if (i == indices[d]) {
            if (++d == depth) {
                break;
            }

            i = -1;

            info = info->child;
        } else {
            info = info->next;

        }

    }

    if (info != NULL) {

        iter->stamp = priv->stamp;
        iter->user_data = info;
        return TRUE;
    }

    return FALSE;
}

static gboolean
tree_model_iter_has_child (GtkTreeModel * tree_model, GtkTreeIter * iter)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *info;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    if (priv->loaded != TRUE) {
        return FALSE;
    }

    g_return_val_if_fail (SCALIX_IS_STORE_MODEL (model), FALSE);
    g_return_val_if_fail (iter != NULL, FALSE);
    g_return_val_if_fail (iter->stamp == priv->stamp, FALSE);

    info = iter->user_data;

    return info && info->child != NULL;
}

static gboolean
tree_model_iter_next (GtkTreeModel * tree_model, GtkTreeIter * iter)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *info;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    if (priv->loaded != TRUE) {
        return FALSE;
    }

    g_return_val_if_fail (SCALIX_IS_STORE_MODEL (model), FALSE);
    g_return_val_if_fail (iter != NULL, FALSE);
    g_return_val_if_fail (iter->stamp == priv->stamp, FALSE);

    info = iter->user_data;

    if (info && (info = info->next) != NULL) {
        iter->user_data = info;
        iter->stamp = priv->stamp;
    } else {
        return FALSE;
    }

    return TRUE;
}

static gboolean
tree_model_iter_children (GtkTreeModel * tree_model,
                          GtkTreeIter * iter, GtkTreeIter * parent)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *child;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    /* FIXME chekcs */

    if (parent != NULL) {
        child = ((CamelFolderInfo *) parent->user_data)->child;
    } else {
        child = &(priv->root);
    }

    if (child) {
        iter->stamp = priv->stamp;
        iter->user_data = child;
        return TRUE;
    }

    return FALSE;
}

static gint
tree_model_iter_n_children (GtkTreeModel * tree_model, GtkTreeIter * iter)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *info;
    gint n;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    /* FIXME checks */

    if (iter != NULL) {
        info = (CamelFolderInfo *) iter->user_data;
    } else {
        info = &(priv->root);
    }

    info = info->child;

    n = 0;

    while (info) {
        n++;
        info = info->next;
    }

    return n;
}

static gboolean
tree_model_iter_nth_child (GtkTreeModel * tree_model,
                           GtkTreeIter * iter, GtkTreeIter * parent, gint n)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *info;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    /* FIXME: checks */

    if (parent != NULL) {
        info = (CamelFolderInfo *) parent->user_data;

        if (info == NULL || info->child == NULL) {
            return FALSE;
        }

        info = info->child;
    } else {
        info = &(priv->root);
    }

    while (n > 0 && info) {
        n--;
        info = info->next;
    }

    return n == 0;
}

static gboolean
tree_model_iter_parent (GtkTreeModel * tree_model,
                        GtkTreeIter * iter, GtkTreeIter * child)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *info;

    model = SCALIX_STORE_MODEL (tree_model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    info = (CamelFolderInfo *) child->user_data;

    if (info != &(priv->root)) {
        iter->user_data = info->parent;
        iter->stamp = priv->stamp;
        return TRUE;
    }

    return FALSE;
}

static void
tree_model_iface_init (GtkTreeModelIface * iface)
{
    iface->get_flags = tree_model_get_flags;
    iface->get_n_columns = tree_model_get_n_columns;
    iface->get_column_type = tree_model_get_column_type;
    iface->get_value = tree_model_get_value;

    iface->get_iter = tree_model_get_iter;
    iface->iter_has_child = tree_model_iter_has_child;
    iface->iter_next = tree_model_iter_next;
    iface->iter_children = tree_model_iter_children;
    iface->iter_n_children = tree_model_iter_n_children;
    iface->iter_nth_child = tree_model_iter_nth_child;
    iface->iter_parent = tree_model_iter_parent;

#if 0
    iface->get_path = gtk_tree_store_get_path;

#endif

}

/* ************************************************************************** */

/* use the global evoltion CamelSession */
extern CamelSession *session;

typedef struct {
    ScalixStoreModel *model;
    CamelException ex;
    CamelFolderInfo *root;
    CamelStore *store;
} _loading_context;

static void
bla (ScalixStoreModel * model, CamelFolderInfo * info, GtkTreePath * path)
{
    ScalixStoreModelPrivate *priv;
    CamelFolderInfo *iter;
    GtkTreeIter tree_iter;

    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    gtk_tree_path_append_index (path, 0);

    for (iter = info; iter; iter = iter->next) {

        tree_iter.stamp = priv->stamp;
        tree_iter.user_data = iter;

        gtk_tree_model_row_inserted (GTK_TREE_MODEL (model), path, &tree_iter);

        if (iter->child) {
            bla (model, iter->child, gtk_tree_path_copy (path));
        }

        gtk_tree_path_next (path);

    }

    gtk_tree_path_free (path);
}

static gboolean
loading_done (gpointer data)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    GtkTreeIter iter;
    GtkTreePath *path;

    _loading_context *lc;

    lc = (_loading_context *) data;

    model = SCALIX_STORE_MODEL (lc->model);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    /* FIXME: Error handling? */

    priv->store = lc->store;

    /* the root node is special */
    path = gtk_tree_path_new_first ();
    /* Not sure if somebody wants to translate this */
    priv->root.name = _("Scalix Server");
    priv->loaded = TRUE;

    iter.stamp = priv->stamp;
    iter.user_data = &(priv->root);

    /* attach the hierachy to root here */
    gtk_tree_model_row_changed (GTK_TREE_MODEL (model), path, &iter);

    priv->root.child = lc->root;
    bla (model, lc->root, path);

    g_free (data);

    g_signal_emit (model, signals[LOADED_SIGNAL], 0);
    return FALSE;
}

static gpointer
load_folder_info_bg (gpointer data)
{
    ScalixStoreModel *model;
    ScalixStoreModelPrivate *priv;
    _loading_context *lc;

    lc = g_new0 (_loading_context, 1);
    camel_exception_init (&(lc->ex));

    model = SCALIX_STORE_MODEL (data);
    priv = SCALIX_STORE_MODEL_GET_PRIVATE (model);

    lc->model = model;
    lc->store = camel_session_get_store (session, priv->text_uri, &(lc->ex));
    if (lc->store == NULL) {
        g_warning ("Could not obtain store!");
        goto out;
    }

    /* fetch folder list from server */
    lc->root = camel_store_get_folder_info (lc->store,
                                            NULL,
                                            CAMEL_STORE_FOLDER_INFO_RECURSIVE |
                                            CAMEL_SCALIX_STORE_SHOW_SFOLDER,
                                            &(lc->ex));

  out:
    g_idle_add (loading_done, lc);
    return NULL;
}

ScalixStoreModel *
scalix_store_model_new (const char *text_uri)
{
    ScalixStoreModel *model;

    model = g_object_new (SCALIX_TYPE_STORE_MODEL, "uri", text_uri, NULL);

    g_thread_create (load_folder_info_bg, model, FALSE, NULL);

    return model;
}
